package com.itrustconcierge.atlantahome;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;

public class ContactFranchiseActivity extends AppCompatActivity {

    public static ContactFranchiseActivity contactFranchiseActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_franchise);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Talk to Concierge");
        toolbar.setTitleTextColor(Color.WHITE);

        contactFranchiseActivity = this;

        Button callButton = (Button) findViewById(R.id.callButton);
        Button scheduleButton = (Button) findViewById(R.id.schedule);
        Button laterButton = (Button) findViewById(R.id.later);
        TextView welcomeTextView = (TextView) findViewById(R.id.textView23);
        TextView toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        toolBarButton.setVisibility(View.GONE);
        TextView managedByTextView = (TextView) findViewById(R.id.textView24);
        welcomeTextView.setText("Welcome to " + authenticateVoLocal.getTenantSetting().getDisplayName() + "!");

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + authenticateVoLocal.getFranchise().getMobileNumber()));
                if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ContactFranchiseActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                            1);
                    return;
                }
                startActivity(callIntent);
            }
        });

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ScheduleVisitActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        laterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactFranchiseActivity.this, MainActivity.class);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                ActivityCompat.finishAffinity(ContactFranchiseActivity.this);
            }
        });
    }
}

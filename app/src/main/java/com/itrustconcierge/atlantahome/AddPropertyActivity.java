package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.DeviceType;
import com.itrustconcierge.atlantahome.API_Manager.enums.FloorSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.LotSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.PropertyType;
import com.itrustconcierge.atlantahome.API_Manager.enums.Role;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.AddressVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.DeviceInfoVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyFloorSizeVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter;
import com.itrustconcierge.atlantahome.Adapters.SingleItemRowHolder;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.Item;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableAdapter;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableViewHolder;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter.imageUrlMap;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.propertyVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.AUTHTOKEN;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_PASSWORD;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_PREF_NAME;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_USERNAME;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TEXT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TITTLE;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class AddPropertyActivity extends AppCompatActivity implements OnMapReadyCallback {

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    private static final String TAG = "Google Places API";
    MapView mMapView;
    private GoogleMap googleMap;
    RecyclerView propertyTypeRecyclerView, floorSizeRecyclerView, lotSizeRecyclerView, imageRecyclerView;
    SelectableAdapter propertyTypeSelectableAdapter, floorSizeSelectableAdapter, lotSizeSelectableAdapter;
    SelectableViewHolder.OnItemSelectedListener propertyTypeListener, floorSizeListener, lotSizeListener;
    SingleItemRowHolder.OnItemSelectedListener imageOnItemSelectedListener;
    EditText selectAddressEditText;
    private File filetoUpload;
    private String mimeType;
    private String fileName;
    ImageListViewAdapter itemListDataAdapter;
    LinearLayoutManager mLinearLayoutManager;
    TextView tittle, subTittle, toolBarButton;
    PropertyType propertyType;
    long floorSizeInSqFt = 0;
    LotSize lotSize;
    FloorSize floorSize;
    private String pinCode = "";
    private double longitude, latitude;
    List<AuthenticateVo> franchiseVoList = new ArrayList<>();
    private String propertyTypeValue;
    private double lotSizeInAcres;
    public static boolean autoAdd = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        pathList.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (autoAdd) {
            getAddressFromLatLon();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_property);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Add Property");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(AddPropertyActivity.this);

        ItrustHOApplication application = (ItrustHOApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Add property Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        selectAddressEditText = (EditText) findViewById(R.id.selectAddress);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        tittle = (TextView) findViewById(R.id.tittle);
        subTittle = (TextView) findViewById(R.id.subTittle);
        tittle.setText("ADD PROPERTY");
        subTittle.setText("Enter your property address and select the property details");

        imageRecyclerView = (RecyclerView) findViewById(R.id.image_view_list);
        propertyTypeRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_property_type);
        floorSizeRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_floor_size);
        lotSizeRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_lot_size);

        imageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(String item) {
                if (item.equals("o")) {
                    showAddPicturePopup();
                }
            }
        };

        propertyTypeListener = new SelectableViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                if (item.getName().contains("Single family residence")) {
                    propertyType = PropertyType.SINGLE_FAMILY_RESIDENCE;
                } else if (item.getName().contains("Condo")) {
                    propertyType = PropertyType.CONDO;
                } else if (item.getName().contains("Apartment")) {
                    propertyType = PropertyType.APARTMENT;
                } else if (item.getName().contains("Townhome")) {
                    propertyType = PropertyType.TOWNHOME;
                }
            }
        };

        floorSizeListener = new SelectableViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                try {
                    if (item.getName().contains("Less than 3000")) {
//                    floorSize = FloorSize.SQFT_LESS_THEN_2000;
                        floorSize = FloorSize.SQFT_0_TO_3000;
                    } else if (item.getName().contains("3000 to 5000")) {
//                    floorSize = FloorSize.SQFT_2000_TO_4000;
                        floorSize = FloorSize.SQFT_3000_TO_5000;
                    } else if (item.getName().contains("Above 5000")) {
//                    floorSize = FloorSize.SQFT_ABOVE_8000;
                        floorSize = FloorSize.SQFT_ABOVE_5000;
                    }
                    floorSizeInSqFt = Long.parseLong(item.getSurname());
                    if (Long.parseLong(item.getSurname()) > 5000) {
                        showEnterFloorsizePopup();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            private void showEnterFloorsizePopup() {
                @SuppressLint("RestrictedApi") final Dialog requestDialog = new Dialog(new ContextThemeWrapper(AddPropertyActivity.this, R.style.dialogAnimation));
                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                requestDialog.setContentView(R.layout.enter_floor_size);
                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                requestDialog.setCancelable(true);
                requestDialog.getWindow().setGravity(Gravity.CENTER);

                final EditText codeEditText;
                Button cancelButton, validateButton;

                codeEditText = (EditText) requestDialog.findViewById(R.id.code);
                cancelButton = (Button) requestDialog.findViewById(R.id.button5);
                validateButton = (Button) requestDialog.findViewById(R.id.button4);

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                        requestDialog.dismiss();
                    }
                });

                validateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            floorSizeInSqFt = Long.parseLong(codeEditText.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                        requestDialog.dismiss();
                    }
                });
                requestDialog.show();
            }
        };

        lotSizeListener = new SelectableViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                if (item.getName().contains("Less than 1 acre")) {
                    lotSize = LotSize.ACRE_LESS_THAN_1;
                } else if (item.getName().contains("1 to 3 acre")) {
                    lotSize = LotSize.ACRE_1_TO_3;
                } else if (item.getName().contains("Above 3 acre")) {
                    lotSize = LotSize.ACRE_ABOVE_3;
                }
            }
        };

        pathList.add("o");
        itemListDataAdapter = new ImageListViewAdapter(AddPropertyActivity.this, pathList, imageOnItemSelectedListener, UploadType.PROPERTY, true, authenticateVoLocal.getId());
        imageRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(AddPropertyActivity.this, LinearLayoutManager.HORIZONTAL, false);
        imageRecyclerView.setLayoutManager(mLinearLayoutManager);
        imageRecyclerView.setAdapter(itemListDataAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        propertyTypeRecyclerView.setLayoutManager(layoutManager);
        propertyTypeRecyclerView.setHasFixedSize(true);
        propertyTypeRecyclerView.setItemViewCacheSize(20);
        propertyTypeRecyclerView.setDrawingCacheEnabled(true);
        propertyTypeRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(propertyTypeRecyclerView.getContext(),
                new LinearLayoutManager(AddPropertyActivity.this).getOrientation());
        propertyTypeRecyclerView.addItemDecoration(dividerItemDecoration);
        List<Item> selectableItems = getPropertyTypes();
        propertyTypeSelectableAdapter = new SelectableAdapter(propertyTypeListener, selectableItems, false);
        propertyTypeRecyclerView.setAdapter(propertyTypeSelectableAdapter);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        floorSizeRecyclerView.setLayoutManager(layoutManager1);
        floorSizeRecyclerView.setHasFixedSize(true);
        floorSizeRecyclerView.setItemViewCacheSize(20);
        floorSizeRecyclerView.setDrawingCacheEnabled(true);
        floorSizeRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(floorSizeRecyclerView.getContext(),
                new LinearLayoutManager(AddPropertyActivity.this).getOrientation());
        floorSizeRecyclerView.addItemDecoration(dividerItemDecoration1);
        List<Item> selectableItems1 = getFloorSizes();
        floorSizeSelectableAdapter = new SelectableAdapter(floorSizeListener, selectableItems1, false);
        floorSizeRecyclerView.setAdapter(floorSizeSelectableAdapter);

        getFloorSizesApi();

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        lotSizeRecyclerView.setLayoutManager(layoutManager2);
        lotSizeRecyclerView.setHasFixedSize(true);
        lotSizeRecyclerView.setItemViewCacheSize(20);
        lotSizeRecyclerView.setDrawingCacheEnabled(true);
        lotSizeRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration2 = new DividerItemDecoration(lotSizeRecyclerView.getContext(),
                new LinearLayoutManager(AddPropertyActivity.this).getOrientation());
        lotSizeRecyclerView.addItemDecoration(dividerItemDecoration2);
        List<Item> selectableItems2 = getLotSizes();
        lotSizeSelectableAdapter = new SelectableAdapter(lotSizeListener, selectableItems2, false);
        lotSizeRecyclerView.setAdapter(lotSizeSelectableAdapter);

        selectAddressEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("US")
                            .build();
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .setFilter(typeFilter)
                                    .build(AddPropertyActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectAddressEditText.getText().toString().isEmpty()) {
                    if (propertyType != null) {
                        if (floorSize != null || floorSizeInSqFt != 0) {
                            if (lotSize != null) {
                                if (!pathList.isEmpty()) {
                                    getAddressFromLatLon();
                                } else {
                                    getAddressFromLatLon();
                                }
                            } else {
                                Snackbar.make(selectAddressEditText, "Please select lot size!", Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(selectAddressEditText, "Please select floor size!", Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(selectAddressEditText, "Please select property type!", Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(selectAddressEditText, "Please select address!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getFloorSizesApi() {
        ApiInterface apiService = ApiClient.getClient(AddPropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyFloorSizeVo>> call = apiService.getPropertyFloorSizes();
        call.enqueue(new Callback<ResponseWrapper<PropertyFloorSizeVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyFloorSizeVo>> call, Response<ResponseWrapper<PropertyFloorSizeVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                List<PropertyFloorSizeVo> propertyFloorSizeVos = response.body().getList();
                                List<Item> selectableItems = new ArrayList<>();
                                for (PropertyFloorSizeVo propertyFloorSizeVo1 : propertyFloorSizeVos) {
                                    selectableItems.add(new Item(propertyFloorSizeVo1.getDisplayName(), String.valueOf(propertyFloorSizeVo1.getMaximum() - 1)));
                                }
                                floorSizeSelectableAdapter = new SelectableAdapter(floorSizeListener, selectableItems, false);
                                floorSizeRecyclerView.setAdapter(floorSizeSelectableAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                        }
                    } else {
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyFloorSizeVo>> call, Throwable t) {
                Log.e("Error", t.toString());
            }
        });
    }

    private void addProperty() {
        PropertyVo propertyVo = new PropertyVo();
        propertyVo.setOwner(authenticateVoLocal);
        if(propertyType != null)
            propertyVo.setPropertyType(propertyType.name());
        if(floorSize != null)
            propertyVo.setFloorSize(floorSize.name());
        if(lotSize != null)
            propertyVo.setLotSize(lotSize.name());
        propertyVo.setFloorSizeInSqFt(floorSizeInSqFt);
        AddressVo addressVo = new AddressVo();
        addressVo.setDisplayAddress(selectAddressEditText.getText().toString());
        propertyVo.setAddress(addressVo);
        propertyVo.setPropertyTypeValue(propertyTypeValue);
        propertyVo.setLotSizeInAcres(lotSizeInAcres);
        String imageUrl = null;
        if (!imageUrlMap.isEmpty()) {
            StringBuilder result = new StringBuilder();
            for (String string : imageUrlMap.values()) {
                result.append(string);
                result.append(",");
            }
            imageUrl = result.length() > 0 ? result.substring(0, result.length() - 1) : "";
        }
        propertyVo.setImage(imageUrl);

        final Dialog loadingDialog = Utility.loadingDialog(AddPropertyActivity.this);
        ApiInterface apiService = ApiClient.getClient(AddPropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyVo>> call = null;
        if (authenticateVoLocal != null) {
            if (authenticateVoLocal.getUserMappedStatus() != null) {
                if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                    call = apiService.createUnmappedProperty(propertyVo);
                } else {
                    call = apiService.createProperty(propertyVo);
                }
            } else {
                call = apiService.createProperty(propertyVo);
            }
        }
        call.enqueue(new Callback<ResponseWrapper<PropertyVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyVo>> call, Response<ResponseWrapper<PropertyVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                propertyVoLocal = response.body().getData();
                                if(floorSize != null)
                                    propertyVoLocal.setFloorSize(floorSize.name());
                                imageUrlMap.clear();
//                                if (franchiseVoList.isEmpty()) {
//                                    Intent intent = new Intent(AddPropertyActivity.this, FranchiseNotFoundActivity.class);
//                                    startActivity(intent);
//                                    finish();
//                                } else {
                                    Intent intent = new Intent(AddPropertyActivity.this, Activity_Slider.class);
                                    startActivity(intent);
                                    finish();
//                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Snackbar.make(selectAddressEditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(selectAddressEditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getAddressFromLatLon() {
        Geocoder coder = new Geocoder(getApplicationContext());
        try {
            ArrayList<android.location.Address> adresses = (ArrayList<android.location.Address>) coder.getFromLocation(latitude, longitude, 5);
            if (adresses != null && adresses.size() != 0) {
                for (android.location.Address add : adresses) {
                    try {
                        if (add.getPostalCode() != null) {
                            if (add.getPostalCode().trim().length() != 0) {
                                pinCode = add.getPostalCode();
                            }
                        }
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                getFranchiseList(pinCode);
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFranchiseList(String pinCode) {
        final Dialog loadingDialog = Utility.loadingDialog(AddPropertyActivity.this);
        ApiInterface apiService = ApiClient.getClient(AddPropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<AuthenticateVo>> call = apiService.getTenants(pinCode);
        call.enqueue(new Callback<ResponseWrapper<AuthenticateVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<AuthenticateVo>> call, Response<ResponseWrapper<AuthenticateVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    franchiseVoList = response.body().getList();
                                    if (!franchiseVoList.isEmpty()) {
                                        final Dialog loadingDialog = Utility.loadingDialog(AddPropertyActivity.this);
                                        ApiInterface apiService = ApiClient.getClient(AddPropertyActivity.this).create(ApiInterface.class);
                                        Call<ResponseWrapper<Map<String, Boolean>>> call1 = apiService.mapToFranchise(String.valueOf(franchiseVoList.get(0).getTenantId()));
                                        call1.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                            @Override
                                            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                                loadingDialog.dismiss();
                                                if (response.isSuccessful()) {
                                                    callSignIn();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                                // Log error here since request failed
                                                loadingDialog.dismiss();
                                                Log.e("Error", t.toString());
                                                Snackbar.make(selectAddressEditText, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                            }
                                        });

                                    } else {
                                        addProperty();
                                    }
                                } else {
                                    addProperty();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                addProperty();
                            }
                        } else {
                            Snackbar.make(selectAddressEditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(selectAddressEditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(selectAddressEditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<AuthenticateVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(selectAddressEditText, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void callSignIn() {
        SharedPreferences pref = getSharedPreferences(LOGIN_PREF_NAME, MODE_PRIVATE);
        Map<String, Object> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("userName", pref.getString(LOGIN_USERNAME, ""));
        if (pref.getString(LOGIN_PASSWORD, "").isEmpty()) {
            stringStringHashMap.put("token", pref.getString(AUTHTOKEN, ""));
        } else {
            stringStringHashMap.put("token", pref.getString(LOGIN_PASSWORD, ""));
        }
        List<Role> roles = new ArrayList<>();
        roles.add(Role.HO);
        stringStringHashMap.put("roles", roles);
        DeviceInfoVo deviceInfo = new DeviceInfoVo();
        deviceInfo.setDeviceType(DeviceType.ANDROID);
        WindowManager wm = (WindowManager) AddPropertyActivity.this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        deviceInfo.setHeight(height);
        deviceInfo.setWidth(width);
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences(Utility.SHARED_PREF, 0);
        String regId = pref1.getString("regId", null);
        Log.e("FCM", "Firebase reg id: " + regId);
        if (regId != null) {
            deviceInfo.setDeviceToken(regId);
        }
        stringStringHashMap.put("deviceInfo", deviceInfo);
        if (checkInternetConenction(AddPropertyActivity.this)) {
            ApiInterface apiService = ApiClient.getClient(AddPropertyActivity.this).create(ApiInterface.class);
            Call<AuthenticateVo> call = apiService.signIn(stringStringHashMap);
            call.enqueue(new Callback<AuthenticateVo>() {
                @Override
                public void onResponse(Call<AuthenticateVo> call, Response<AuthenticateVo> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                StaticInfo.authenticateVoLocal = response.body();
                                addProperty();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<AuthenticateVo> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                }
            });
        }
    }

    private void showAddPicturePopup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(AddPropertyActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.photos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(AddPropertyActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CAMERA);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else if (checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(AddPropertyActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(AddPropertyActivity.this, new String[]{android.Manifest.permission.CAMERA},
                                SELECT_FILE);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else {
                selectFile();
            }
        } else {
            selectFile();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            final Dialog dialog1 = new Dialog(AddPropertyActivity.this);
                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog1.setContentView(R.layout.allowlocation);
                            dialog1.setCancelable(false);
                            final Window window = dialog1.getWindow();
                            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            ImageView imageView;
                            TextView tittleTextView, textView, skipTextView;
                            Button okTextView;

                            imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                            tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                            textView = (TextView) dialog1.findViewById(R.id.textView);
                            skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                            okTextView = (Button) dialog1.findViewById(R.id.ok);

                            tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                            textView.setText(PERMISSION_IMAGE_TEXT);
                            imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                            okTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                    ActivityCompat.requestPermissions(AddPropertyActivity.this, new String[]{android.Manifest.permission.CAMERA},
                                            SELECT_FILE);
                                }
                            });

                            skipTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                }
                            });

                            dialog1.show();

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant

                            return;
                        } else {
                            selectFile();
                        }
                    } else {
                        selectFile();
                    }
                } else {
                    // permission denied
                }
                return;
            }

            case 2: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    selectFile();
                } else {
                    // permission denied
                }
                return;
            }
            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    String userChoosenTask;
    int REQUEST_CAMERA = 1;
    int SELECT_FILE = 2;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 10;
    public static List<String> pathList = new ArrayList<>();

    private void selectFile() {
        final CharSequence[] items = {"Open Camera and Take Picture", "Choose from Library",
                "No Thanks"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPropertyActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_dialogue_custom_tittle, null);
        TextView textView = (TextView) view.findViewById(R.id.tittle);
        textView.setText("Add Photos");
        builder.setCustomTitle(view);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera and Take Picture")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals("No Thanks")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent mIntent = new Intent(AddPropertyActivity.this, PickImageActivity.class);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 5);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
        startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                selectAddressEditText.setText(place.getAddress());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(place.getLatLng());
                googleMap.addMarker(markerOptions);
                googleMap.setLatLngBoundsForCameraTarget(place.getViewport());
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude)).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

                String zipcode = "";
                Geocoder coder = new Geocoder(getApplicationContext());
                try {
                    ArrayList<android.location.Address> adresses = (ArrayList<android.location.Address>) coder.getFromLocation(latitude, longitude, 5);
                    if (adresses != null && adresses.size() != 0) {
                        for (android.location.Address add : adresses) {
                            try {
                                if (add.getPostalCode() != null) {
                                    if (add.getPostalCode().trim().length() != 0) {
                                        zipcode = add.getPostalCode();
                                    }
                                }
                                break;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                new HouseCanary().execute(String.valueOf(place.getAddress()), zipcode, "");
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            List<String> strings = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (strings != null) {
                this.pathList.addAll(strings);
            }
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < pathList.size(); i++) {
                    sb.append("Photo" + (i + 1) + ":" + pathList.get(i));
                    sb.append("\n");
                }
                Log.d("pathList", sb.toString());
            }
            itemListDataAdapter.notifyDataSetChanged();
        } else if (requestCode == REQUEST_CAMERA) {
            onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                Calendar.getInstance().getTimeInMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mimeType = "image/jpeg";
        fileName = destination.getName();
        filetoUpload = destination;

        filetoUpload = destination;
        pathList.add(destination.getAbsolutePath());
        itemListDataAdapter.notifyDataSetChanged();
//        uploadPicture(filetoUpload, fileName, mimeType);
    }

    public List<Item> getPropertyTypes() {
        List<Item> selectableItems = new ArrayList<>();
        selectableItems.add(new Item("Single family residence", "Single family residence"));
        selectableItems.add(new Item("Condo", "Condo"));
        selectableItems.add(new Item("Apartment", "Apartment"));
        selectableItems.add(new Item("Townhome", "Townhome"));
        return selectableItems;
    }

    public List<Item> getFloorSizes() {
        List<Item> selectableItems = new ArrayList<>();
        selectableItems.add(new Item("Less than 3000 sqft", "2999"));
        selectableItems.add(new Item("3000 to 5000 sqft", "4000"));
//        selectableItems.add(new Item("4000 to 8000 sqft", "4000 to 8000 sqft"));
        selectableItems.add(new Item("Above 5000 sqft", "5001"));
        return selectableItems;
    }

    public List<Item> getLotSizes() {
        List<Item> selectableItems = new ArrayList<>();
        selectableItems.add(new Item("Less than 1 acre", "Less than 1 acre"));
        selectableItems.add(new Item("1 to 3 acre", "1 to 3 acre"));
        selectableItems.add(new Item("Above 3 acre", "Above 3 acre"));
        return selectableItems;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }


    class HouseCanary extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        String paramString, address;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(AddPropertyActivity.this, "Getting your house details", "Please wait...");
        }

        @Override
        protected String doInBackground(String... strings) {
            paramString = strings[0];
            address = paramString;
            String zipcode = strings[1];
            paramString = paramString.replace(zipcode, "");
            paramString = paramString.replace(" ", "+");
            paramString = paramString.replace(",", "");
            String result = null;
            try {
                URL url = new URL("https://api.housecanary.com/v2/property/details?address=" + paramString + "&zipcode=" + zipcode);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                String userpassword = "9QSUQB8SSWLUQA098J7U" + ":" + "arNMl9oNp5AQc49XDJ7HSqWkGHt64mM0";
                String encoded = Base64.encodeToString((userpassword).getBytes("UTF-8"), Base64.NO_WRAP);
                urlConnection.setRequestProperty("Authorization", "Basic " +
                        encoded);

//                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                result = inputStreamToString(in);

                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                Log.d("House", s);
                s = s.replace("\\/", "_");
                JSONArray jsonArr = new JSONArray(s);
                JSONObject jsonObject = jsonArr.getJSONObject(0);
                Log.d("HouseJson", jsonObject.toString());
                JSONObject propertyDetails = jsonObject.getJSONObject("property_details");
                JSONObject result = propertyDetails.getJSONObject("result");
                JSONObject property = result.getJSONObject("property");
                propertyTypeValue = property.getString("property_type");
                lotSizeInAcres = property.getDouble("site_area_acres");
                floorSizeInSqFt = property.getLong("building_area_sq_ft");

                Intent intent = new Intent(AddPropertyActivity.this, HcResultActivity.class);
                intent.putExtra("propertyTypeValue", propertyTypeValue);
                intent.putExtra("lotSizeInAcres", lotSizeInAcres);
                intent.putExtra("floorSizeInSqFt", floorSizeInSqFt);
                intent.putExtra("address", address);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}

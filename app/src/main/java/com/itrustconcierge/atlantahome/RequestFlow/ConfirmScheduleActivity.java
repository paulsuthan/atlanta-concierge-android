package com.itrustconcierge.atlantahome.RequestFlow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ScheduleVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class ConfirmScheduleActivity extends AppCompatActivity {

    TextView tittleTextView, infoTextView;
    EditText date1EditText, date2EditText, date3EditText;
    Button chatButton;
    public static Activity confirmScheduleActivity;
    List<EventVo> eventVos;
    public static EventVo eventVo;
    ScheduleVo scheduleVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        confirmScheduleActivity = this;

        tittleTextView = (TextView) findViewById(R.id.tittle);
        infoTextView = (TextView) findViewById(R.id.infoText);
        date1EditText = (EditText) findViewById(R.id.date1);
        date2EditText = (EditText) findViewById(R.id.date2);
        date3EditText = (EditText) findViewById(R.id.date3);
        chatButton = (Button) findViewById(R.id.chat);

        try {
            if (homeOwnerRequestVoStatic != null) {
                if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)) {
                    if (homeOwnerRequestVoStatic.getAccessKey() != null) {
                        if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_ME_FOR_ACCESS_KEY")) {
                            tittleTextView.setText("iTrust Pro would like to schedule a visit to provide an estimate. Please choose a date and time from the proposed schedule below.");
                            date1EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date1EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getVisitSchedule().get(0)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "First proposed schedule " + date1EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getVisitSchedule().get(0));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                            date2EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date2EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getVisitSchedule().get(1)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "Second proposed schedule " + date2EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getVisitSchedule().get(1));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                            date3EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date3EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getVisitSchedule().get(2)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "Third proposed schedule " + date3EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getVisitSchedule().get(2));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                        } else if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_MASTER_CONCIERGE")) {
                            tittleTextView.setText("iTrust Pro would like to schedule a visit to provide an estimate. Atlanta Concierge will choose a date and time from the proposed schedule below.");
                        } else {
                            tittleTextView.setText("iTrust Pro would like to schedule a visit to provide an estimate.");
                        }
                    }

                    date1EditText.setVisibility(View.GONE);
                    date2EditText.setVisibility(View.GONE);
                    date3EditText.setVisibility(View.GONE);

                    try {
                        if (homeOwnerRequestVoStatic.getVisitSchedule().get(0) != null) {
                            date1EditText.setVisibility(View.VISIBLE);
                            date1EditText.setText(getDate(homeOwnerRequestVoStatic.getVisitSchedule().get(0)));
                        } else {
                            date1EditText.setVisibility(View.GONE);
                        }
                        if (homeOwnerRequestVoStatic.getVisitSchedule().get(1) != null) {
                            date2EditText.setVisibility(View.VISIBLE);
                            date2EditText.setText(getDate(homeOwnerRequestVoStatic.getVisitSchedule().get(1)));
                        } else {
                            date2EditText.setVisibility(View.GONE);
                        }
                        if (homeOwnerRequestVoStatic.getVisitSchedule().get(2) != null) {
                            date3EditText.setVisibility(View.VISIBLE);
                            date3EditText.setText(getDate(homeOwnerRequestVoStatic.getVisitSchedule().get(2)));
                        } else {
                            date3EditText.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)) {
                    if (homeOwnerRequestVoStatic.getAccessKey() != null) {
                        if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_ME_FOR_ACCESS_KEY")) {
                            tittleTextView.setText("iTrust Pro would like to schedule the job. Please choose a date and time from the proposed schedule below.");
                            date1EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date1EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getAvailableScheduleDates().get(0)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "First proposed schedule " + date1EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getAvailableScheduleDates().get(0));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                            date2EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date2EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getAvailableScheduleDates().get(1)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "Second proposed schedule " + date2EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getAvailableScheduleDates().get(1));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                            date3EditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!date3EditText.getText().toString().isEmpty()) {
                                        if (eventVos != null) {
                                            for (EventVo eventVo1 : eventVos) {
                                                if (eventVo1.getStartTime() == homeOwnerRequestVoStatic.getAvailableScheduleDates().get(2)) {
                                                    eventVo = eventVo1;
                                                    if (scheduleVo != null) {
                                                        ScheduleVo scheduleVo1 = new ScheduleVo();
                                                        scheduleVo1.setId(scheduleVo.getId());
                                                        eventVo.setSchedule(scheduleVo1);
                                                    }
                                                }
                                            }
                                        }
                                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                                        try {
                                            intent.putExtra("fromConfirmDate", true);
                                            intent.putExtra("tittle", "Schedule");
                                            intent.putExtra("dateTittle", "Third proposed schedule " + date3EditText.getText().toString());
                                            intent.putExtra("date", homeOwnerRequestVoStatic.getAvailableScheduleDates().get(2));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        v.getContext().startActivity(intent);
                                    }
                                }
                            });
                        } else if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_MASTER_CONCIERGE")) {
                            tittleTextView.setText("iTrust Pro would like to schedule the job. Atlanta Concierge will choose a date and time from the proposed schedule below.");
                        } else {
                            tittleTextView.setText("iTrust Pro would like to schedule the job.");
                        }
                    }

                    date1EditText.setVisibility(View.GONE);
                    date2EditText.setVisibility(View.GONE);
                    date3EditText.setVisibility(View.GONE);

                    try {
                        if (homeOwnerRequestVoStatic.getAvailableScheduleDates().get(0) != null && !homeOwnerRequestVoStatic.getAvailableScheduleDates().get(0).toString().isEmpty()) {
                            date1EditText.setVisibility(View.VISIBLE);
                            date1EditText.setText(getDate(homeOwnerRequestVoStatic.getAvailableScheduleDates().get(0)));
                        } else {
                            date1EditText.setVisibility(View.GONE);
                        }
                        if (homeOwnerRequestVoStatic.getAvailableScheduleDates().get(1) != null && !homeOwnerRequestVoStatic.getAvailableScheduleDates().get(1).toString().isEmpty()) {
                            date2EditText.setVisibility(View.VISIBLE);
                            date2EditText.setText(getDate(homeOwnerRequestVoStatic.getAvailableScheduleDates().get(1)));
                        } else {
                            date2EditText.setVisibility(View.GONE);
                        }
                        if (homeOwnerRequestVoStatic.getAvailableScheduleDates().get(2) != null && !homeOwnerRequestVoStatic.getAvailableScheduleDates().get(2).toString().isEmpty()) {
                            date3EditText.setVisibility(View.VISIBLE);
                            date3EditText.setText(getDate(homeOwnerRequestVoStatic.getAvailableScheduleDates().get(2)));
                        } else {
                            date3EditText.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConfirmScheduleActivity.this, RequestMessagesActivity.class);
                try {
                    i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
            }
        });

        if (getIntent().getExtras().getLong("scheduleId", 0) != 0) {
            final Dialog loadingDialog = Utility.loadingDialog(ConfirmScheduleActivity.this);
            ApiInterface apiService = ApiClient.getClient(ConfirmScheduleActivity.this).create(ApiInterface.class);
            Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(getIntent().getExtras().getLong("scheduleId", 0));
            call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                scheduleVo = response.body().getData();
                                eventVos = scheduleVo.getEvents();
                            } else {
                                Snackbar.make(date3EditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(date3EditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(date3EditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                    loadingDialog.dismiss();
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                    Snackbar.make(date3EditText, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }
}

package com.itrustconcierge.atlantahome.RequestFlow;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ScheduleVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.MainActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class WorkHistoryActivity extends AppCompatActivity {

    private Bundle bundle;
    public static HomeOwnerRequestVo homeOwnerRequestVoStatic;
    TextView statusTextView, dateTextView, tittleTextView, descriptionTextView, descriptionNew;
    private ViewPager viewPager;
    List<RequestHistoryVo> requestHistoryVos = new ArrayList<>();
    List<RequestHistoryVo> quoteRequestHistoryVos = new ArrayList<>();
    List<RequestHistoryVo> invoiceRequestHistoryVos = new ArrayList<>();
    List<String> images = new ArrayList<>();
    RelativeLayout relativeLayout1;
    FloatingActionButton fab;
    Button deleteButton, payNow;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        homeOwnerRequestVoStatic = null;
        try {
            if (bundle.getBoolean("isFromPush")) {
                Utility.gotoMyJobs = true;
                Utility.gotoLeads = true;
                Intent intent = new Intent(WorkHistoryActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                ActivityCompat.finishAffinity(WorkHistoryActivity.this);
            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_history);

        bundle = getIntent().getExtras();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Work History");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
        TabLayout tabs = (TabLayout) findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);

        statusTextView = (TextView) findViewById(R.id.status);
        dateTextView = (TextView) findViewById(R.id.date);
        tittleTextView = (TextView) findViewById(R.id.service);
        descriptionTextView = (TextView) findViewById(R.id.description);
        descriptionNew = (TextView) findViewById(R.id.descriptionNew);
        relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        payNow = (Button) findViewById(R.id.payNow);
        fab = findViewById(R.id.fab);

//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!images.isEmpty()) {
//                    try {
//                        Bundle bundle1 = new Bundle();
//                        bundle1.putSerializable("images", (Serializable) images);
//                        bundle1.putInt("position", 0);
//                        bundle1.putString("uploadType", "REQUEST");
//                        bundle1.putString("id", String.valueOf(authenticateVoLocal.getId()));
//                        FragmentTransaction ft = WorkHistoryActivity.this.getSupportFragmentManager().beginTransaction();
//                        SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
//                        newFragment.setArguments(bundle1);
//                        newFragment.show(ft, "slideshow");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(WorkHistoryActivity.this, RequestMessagesActivity.class);
                try {
                    i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WorkHistoryActivity.this, QuoteSummaryActivity.class);
                try {
                    intent.putExtra("requestId", homeOwnerRequestVoStatic.getApprovedQuote().getId().toString());
                    intent.putExtra("sp_name", homeOwnerRequestVoStatic.getApprovedQuote().getSender().getFirstName() + " " + homeOwnerRequestVoStatic.getApprovedQuote().getSender().getLastName());
                    intent.putExtra("spId", homeOwnerRequestVoStatic.getApprovedQuote().getSender().getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(WorkHistoryActivity.this, R.style.dialogAnimation));
                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                requestDialog.setCancelable(false);
                requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                TextView tittleTextView = requestDialog.findViewById(R.id.tilltle);
                TextView descriptionTextView = requestDialog.findViewById(R.id.description);
                Button positiveButton = requestDialog.findViewById(R.id.positivebutton);
                Button negativeButton = requestDialog.findViewById(R.id.negativebutton);

                tittleTextView.setText("Are You Sure?");
                descriptionTextView.setText("This job has been closed by concierge, Would you like to delete this job?");

                negativeButton.setText("Cancel");
                positiveButton.setText("Delete");

                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        requestDialog.dismiss();
                        deleteJob(homeOwnerRequestVoStatic.getId());
                    }
                });

                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestDialog.dismiss();
                    }
                });

                requestDialog.show();
            }
        });
    }

    private void deleteJob(Long id) {
        Map<String, String> input = new HashMap<>();
        input.put("note", "Deleted by homeowner");
        final Dialog loadingDialog = Utility.loadingDialog(WorkHistoryActivity.this);
        ApiInterface apiService = ApiClient.getClient(WorkHistoryActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.deleteJob(id, input);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bundle != null) {
            if (bundle.getLong("requestId") != 0) {
                if (checkInternetConenction(WorkHistoryActivity.this)) {
                    getRequest(bundle.getLong("requestId"));
                } else {
                    Snackbar.make(relativeLayout1, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    private void getRequest(Long requestId) {
        final Dialog loadingDialog = Utility.loadingDialog(WorkHistoryActivity.this);
        ApiInterface apiService = ApiClient.getClient(WorkHistoryActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequest(requestId);
        call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                homeOwnerRequestVoStatic = new HomeOwnerRequestVo();
                                homeOwnerRequestVoStatic = response.body().getData();
                                if (homeOwnerRequestVoStatic.getFromPackage() == null)
                                    homeOwnerRequestVoStatic.setFromPackage(false);
                                if (homeOwnerRequestVoStatic.getPreferredTimeToCompletion() != null) {
                                    descriptionNew.setText(homeOwnerRequestVoStatic.getDescription());
                                    descriptionTextView.setText("Preferred time to complete: " + homeOwnerRequestVoStatic.getPreferredTimeToCompletion());
                                } else {
                                    descriptionNew.setText(homeOwnerRequestVoStatic.getDescription());
                                }

                                dateTextView.setText(getDate(homeOwnerRequestVoStatic.getLastUpdatedOn()));
                                tittleTextView.setText(homeOwnerRequestVoStatic.getService().getName());
                                if (homeOwnerRequestVoStatic.getImage() != null) {
                                    if (homeOwnerRequestVoStatic.getImage().contains(",")) {
                                        images = Arrays.asList(homeOwnerRequestVoStatic.getImage().split(","));
                                    } else {
                                        images.add(homeOwnerRequestVoStatic.getImage());
                                    }
                                }
                                statusTextView.setText(homeOwnerRequestVoStatic.getStatus().getName());
                                requestHistoryVos = new ArrayList<RequestHistoryVo>();
                                quoteRequestHistoryVos = new ArrayList<RequestHistoryVo>();
                                invoiceRequestHistoryVos = new ArrayList<RequestHistoryVo>();
                                for (RequestHistoryVo requestHistoryVo : homeOwnerRequestVoStatic.getHistoryItems()) {
                                    requestHistoryVo.setRequest(homeOwnerRequestVoStatic);
                                    requestHistoryVo.setSender(homeOwnerRequestVoStatic.getHomeOwner());
                                    if (!requestHistoryVo.getType().equals(RequestHistoryType.LEAD)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.ACCEPTED)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.EMP_ACCEPTED)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.EMP_CANCELLED)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.EMP_DECLINED)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.INVOICE)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.ASSIGNED)
                                            && !requestHistoryVo.getType().equals(RequestHistoryType.JOB_CLOSED)) {
                                        requestHistoryVos.add(requestHistoryVo);
                                    }
                                    if (requestHistoryVo.getType().equals(RequestHistoryType.QUOTE)) {
                                        quoteRequestHistoryVos.add(requestHistoryVo);
                                    }
                                    if (requestHistoryVo.getType().equals(RequestHistoryType.APPROVED_INVOICE)) {
                                        invoiceRequestHistoryVos.add(requestHistoryVo);
                                    }
                                }

                                if (homeOwnerRequestVoStatic.getScheduleId() != null)
                                    getScheduleById(homeOwnerRequestVoStatic.getScheduleId());
                                else
                                    setupViewPager(viewPager);

                                fab.setVisibility(View.VISIBLE);
                                if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.JOB_CLOSED))
                                    deleteButton.setVisibility(View.VISIBLE);
                                else
                                    deleteButton.setVisibility(View.GONE);

                                if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.APPROVED_INVOICE))
                                    payNow.setVisibility(View.VISIBLE);
                                else
                                    payNow.setVisibility(View.GONE);

                                ItrustHOApplication application = (ItrustHOApplication) getApplication();
                                Tracker mTracker = application.getDefaultTracker();
                                mTracker.setScreenName("Job Screen - "+homeOwnerRequestVoStatic.getService().getName());
                                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(viewPager, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getScheduleById(Long scheduleId) {
        final Dialog loadingDialog = Utility.loadingDialog(WorkHistoryActivity.this);
        ApiInterface apiService = ApiClient.getClient(WorkHistoryActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(scheduleId);
        call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                ScheduleVo scheduleVo = response.body().getData();
                                if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)) {
                                    List<Long> dates = new ArrayList<>();
                                    for (EventVo eventVo : scheduleVo.getEvents()) {
                                        dates.add(eventVo.getStartTime());
                                    }
                                    homeOwnerRequestVoStatic.setVisitSchedule(dates);
                                } else if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)) {
                                    List<Long> dates = new ArrayList<>();
                                    for (EventVo eventVo : scheduleVo.getEvents()) {
                                        dates.add(eventVo.getStartTime());
                                    }
                                    homeOwnerRequestVoStatic.setAvailableScheduleDates(dates);
                                    StringBuffer dates1 = new StringBuffer();
                                    dates1.append("\n");
                                } else if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.VISIT_SCHEDULED)) {
                                    List<Long> dates = new ArrayList<>();
                                    for (EventVo eventVo : scheduleVo.getEvents()) {
                                        dates.add(eventVo.getStartTime());
                                        if (eventVo.getStatus().equals(EventStatus.PENDING)) {
                                            homeOwnerRequestVoStatic.setVisitForQuoteScheduledOn(eventVo.getStartTime());
                                        }
                                    }
                                } else if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_CONFIRMED)) {
                                    List<Long> dates = new ArrayList<>();
                                    for (EventVo eventVo : scheduleVo.getEvents()) {
                                        dates.add(eventVo.getStartTime());
                                        if (eventVo.getStatus().equals(EventStatus.PENDING)) {
                                            homeOwnerRequestVoStatic.setScheduledOn(eventVo.getStartTime());
                                        }
                                    }
                                }
                                setupViewPager(viewPager);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(viewPager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(StatusFragment.newInstance(1, "STATUS", requestHistoryVos), "Status");
        adapter.addFragment(RequestPhotosFragment.newInstance(homeOwnerRequestVoStatic), "Photos");
        if (homeOwnerRequestVoStatic.getFromPackage()) {

        } else {
            adapter.addFragment(QuotesFragment.newInstance(1, "QUOTE", quoteRequestHistoryVos), "Quote");
            adapter.addFragment(InvoiceFragment.newInstance(1, "INVOICE", invoiceRequestHistoryVos), "Invoice");
        }
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

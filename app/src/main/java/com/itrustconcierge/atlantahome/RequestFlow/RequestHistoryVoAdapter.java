package com.itrustconcierge.atlantahome.RequestFlow;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.ScheduleStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ScheduleVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class RequestHistoryVoAdapter extends RecyclerView.Adapter<RequestHistoryVoAdapter.ViewHolder> {

    private final List<RequestHistoryVo> mValues;
    Activity activity;
    DecimalFormat formatter = new DecimalFormat("#.00");

    public RequestHistoryVoAdapter(List<RequestHistoryVo> items, Activity activity) {
        mValues = items;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_status, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        try {
            final RequestHistoryVo requestHistoryVo = mValues.get(position);
            final ViewHolder bodyViewHolder = (ViewHolder) holder;
            if (requestHistoryVo.getType().equals(RequestHistoryType.REQUEST)) {
                bodyViewHolder.txtTitle.setText("Request Created");
                bodyViewHolder.messageTextView.setText("You have created a request.\n" + requestHistoryVo.getRequest().getService().getName() + "\n" + requestHistoryVo.getRequest().getDescription());
                bodyViewHolder.quoteImage.setImageResource(R.mipmap.photos);
                if (requestHistoryVo.getRequest().getImage() != null) {
                    if (requestHistoryVo.getRequest().getImage().contains(",")) {
                        List<String> images = Arrays.asList(requestHistoryVo.getRequest().getImage().split(","));
                        ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                        Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.REQUEST, images.get(0), "image/jpg", requestHistoryVo.getRequest().getHomeOwner().getId());
                        call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                UploadVo uploadVo1 = response.body().getData();
                                                String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                Log.d("DownloadUrl", decodeUrl);
                                                Glide.with(activity).load(decodeUrl).into(bodyViewHolder.quoteImage);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                } else {

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("Error", t.toString());
                            }
                        });
                    } else {
                        ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                        Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.REQUEST, requestHistoryVo.getRequest().getImage(), "image/jpg", requestHistoryVo.getRequest().getHomeOwner().getId());
                        call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                UploadVo uploadVo1 = response.body().getData();
                                                String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                Log.d("DownloadUrl", decodeUrl);
                                                Glide.with(activity).load(decodeUrl).into(bodyViewHolder.quoteImage);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                } else {

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("Error", t.toString());
                            }
                        });
                    }
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.VISIT_REQUESTED)) {
                bodyViewHolder.txtTitle.setText("Visit Requested - Awaiting confirmation");
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                final StringBuffer[] dates = {new StringBuffer()};
                dates[0].append("");
                try {
                    for (long l : homeOwnerRequestVoStatic.getVisitSchedule()) {
                        dates[0].append(getDate(l) + "  ");
                    }
                    if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_ME_FOR_ACCESS_KEY")) {
                        bodyViewHolder.messageTextView.setText("iTrust pro has requested you the following dates to confirm the visit: " + dates[0]);
                    } else if (homeOwnerRequestVoStatic.getAccessKey().equals("CONTACT_MASTER_CONCIERGE")) {
                        bodyViewHolder.messageTextView.setText("iTrust pro has requested Atlanta Concierge the following dates to confirm the visit: " + dates[0]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    bodyViewHolder.messageTextView.setText("iTrust pro has requested you the following dates to confirm the visit: ");
                }

                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                if (requestHistoryVo.getScheduleId() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(requestHistoryVo.getScheduleId());
                    call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
//                            try {
                                        ScheduleVo scheduleVoTemp = response.body().getData();
                                        List<EventVo> eventVos = scheduleVoTemp.getEvents();
                                        if(dates[0].length() != 0)
                                            dates[0] = new StringBuffer();
                                        int i = 0;
                                        for (EventVo temp : eventVos) {
                                            if (i == 0) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            if (i == 1) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            if (i == 2) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            i++;
                                        }

                                        bodyViewHolder.messageTextView.setText("iTrust Pro has requested you to confirm the job schedule for following dates: " + dates[0]);

                                    } else {
                                    }
                                } else {
                                }
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
//                                Snackbar.make(parent, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }

            } else if (requestHistoryVo.getType().equals(RequestHistoryType.VISIT_SCHEDULED)) {
                bodyViewHolder.txtTitle.setText("Visit Scheduled");
                final StringBuffer[] dates = {new StringBuffer()};
                dates[0].append("");
                if (requestHistoryVo.getRequest().getVisitForQuoteScheduledOn() != null) {
                    bodyViewHolder.messageTextView.setText("iTrust Pro's visit has been scheduled on " + getDate(requestHistoryVo.getRequest().getVisitForQuoteScheduledOn()));
                } else {
                    bodyViewHolder.messageTextView.setText("iTrust Pro's visit has been scheduled.");
                }
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                if (requestHistoryVo.getScheduleId() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(requestHistoryVo.getScheduleId());
                    call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
//                            try {
                                        if(dates[0].length() != 0)
                                            dates[0] = new StringBuffer();
                                        ScheduleVo scheduleVoTemp = response.body().getData();
                                        List<EventVo> eventVos = scheduleVoTemp.getEvents();
                                        int i = 0;
                                        for (EventVo temp : eventVos) {
                                            if (temp.getStatus().equals(EventStatus.PENDING)) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                        }
                                        bodyViewHolder.messageTextView.setText("iTrust Pro's visit has been scheduled on " + dates[0]);

                                    } else {
                                    }
                                } else {
                                }
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
//                                Snackbar.make(parent, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.QUOTE)) {
                bodyViewHolder.txtTitle.setText("Quote Received");
                bodyViewHolder.messageTextView.setText("Atlanta Concierge has sent a quote for " + " $" + String.valueOf(formatter.format(requestHistoryVo.getQuote().getHomeOwnerPayableAmount())));
                bodyViewHolder.quoteImage.setImageResource(R.drawable.ic_assignment_black_24dp);
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.APPROVED_QUOTE)) {
                bodyViewHolder.txtTitle.setText("Quote Approved");
                bodyViewHolder.messageTextView.setText("You approved the quote of Atlanta Concierge");
                bodyViewHolder.quoteImage.setImageResource(R.drawable.ic_assignment_black_24dp);
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.AUTO_APPROVED_QUOTE)) {
                bodyViewHolder.txtTitle.setText("Quote Approved");
                bodyViewHolder.messageTextView.setText("Quote of Atlanta Concierge has been auto approved");
                bodyViewHolder.quoteImage.setImageResource(R.drawable.ic_assignment_black_24dp);

            } else if (requestHistoryVo.getType().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)) {
                bodyViewHolder.txtTitle.setText("Job Schedule Requested");
                bodyViewHolder.messageTextView.setText("iTrust Pro has requested you to confirm the job schedule.");
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                final StringBuffer[] dates = {new StringBuffer()};
                dates[0].append("");
                try {
                    for (long l : homeOwnerRequestVoStatic.getAvailableScheduleDates()) {
                        dates[0].append(getDate(l) + "  ");
                    }
                    bodyViewHolder.messageTextView.setText("iTrust Pro has requested you to confirm the job schedule for following dates: " + dates[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                    bodyViewHolder.messageTextView.setText("iTrust Pro has requested you to confirm the job schedule.");
                }

                if (requestHistoryVo.getScheduleId() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(requestHistoryVo.getScheduleId());
                    call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
//                            try {
                                        if(dates[0].length() != 0)
                                            dates[0] = new StringBuffer();
                                        ScheduleVo scheduleVoTemp = response.body().getData();
                                        List<EventVo> eventVos = scheduleVoTemp.getEvents();
                                        int i = 0;
                                        for (EventVo temp : eventVos) {
                                            if (i == 0) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            if (i == 1) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            if (i == 2) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                            i++;
                                        }

                                        bodyViewHolder.messageTextView.setText("iTrust Pro has requested you to confirm the job schedule for following dates: " + dates[0]);

                                    } else {
                                    }
                                } else {
                                }
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
//                                Snackbar.make(parent, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.JOB_SCHEDULE_CONFIRMED)) {
                bodyViewHolder.txtTitle.setText("Job Scheduled");
                final StringBuffer[] dates = {new StringBuffer()};
                dates[0].append("");
                if (requestHistoryVo.getRequest().getVisitForQuoteScheduledOn() != null) {
                    bodyViewHolder.messageTextView.setText("Job has been scheduled on " + getDate(requestHistoryVo.getRequest().getVisitForQuoteScheduledOn()));
                } else {
                    bodyViewHolder.messageTextView.setText("Job has been scheduled.");
                }
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
                if (requestHistoryVo.getScheduleId() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<ScheduleVo>> call = apiService.getScheduleById(requestHistoryVo.getScheduleId());
                    call.enqueue(new Callback<ResponseWrapper<ScheduleVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<ScheduleVo>> call, Response<ResponseWrapper<ScheduleVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
//                            try {
                                        if(dates[0].length() != 0)
                                            dates[0] = new StringBuffer();
                                        ScheduleVo scheduleVoTemp = response.body().getData();
                                        List<EventVo> eventVos = scheduleVoTemp.getEvents();
                                        for (EventVo temp : eventVos) {
                                            if (temp.getStatus().equals(EventStatus.PENDING)) {
                                                dates[0].append(getDate(temp.getStartTime()) + "  ");
                                            }
                                        }
                                        bodyViewHolder.messageTextView.setText("Job has been scheduled on " + dates[0]);

                                    } else {
                                    }
                                } else {
                                }
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<ScheduleVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
//                                Snackbar.make(parent, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.STARTED)) {
                bodyViewHolder.txtTitle.setText("Work Started");
                bodyViewHolder.messageTextView.setText("iTrust Pro has started the work.");
                bodyViewHolder.quoteImage.setImageResource(R.mipmap.photos);
                if (requestHistoryVo.getImage() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.REQUEST, requestHistoryVo.getImage(), "image/jpg", requestHistoryVo.getRequest().getId());
                    call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
                                        try {
                                            UploadVo uploadVo1 = response.body().getData();
                                            String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                            Log.d("DownloadUrl", decodeUrl);
                                            Glide.with(activity).load(decodeUrl).into(bodyViewHolder.quoteImage);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } else {

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
                        }
                    });
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.COMPLETED)) {
                bodyViewHolder.txtTitle.setText("Work Completed");
                bodyViewHolder.messageTextView.setText("iTrust Pro has completed the work.");
                bodyViewHolder.quoteImage.setImageResource(R.mipmap.photos);
                if (requestHistoryVo.getImage() != null) {
                    ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                    Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.REQUEST, requestHistoryVo.getImage(), "image/jpg", requestHistoryVo.getRequest().getId());
                    call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
                                        try {
                                            UploadVo uploadVo1 = response.body().getData();
                                            String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                            Log.d("DownloadUrl", decodeUrl);
                                            Glide.with(activity).load(decodeUrl).into(bodyViewHolder.quoteImage);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } else {

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
                        }
                    });
                }
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.INVOICE)) {
                bodyViewHolder.txtTitle.setText("Received an Invoice");
                bodyViewHolder.messageTextView.setText("Atlanta Concierge has sent a invoice for " + " $" + String.valueOf(formatter.format(requestHistoryVo.getQuote().getHomeOwnerPayableAmount())));
                bodyViewHolder.quoteImage.setImageResource(R.drawable.ic_assignment_black_24dp);
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.APPROVED_INVOICE)) {
                bodyViewHolder.txtTitle.setText("Received an Invoice");
                bodyViewHolder.messageTextView.setText("Atlanta Concierge has sent a invoice for " + " $" + String.valueOf(formatter.format(requestHistoryVo.getQuote().getHomeOwnerPayableAmount())));
                bodyViewHolder.quoteImage.setImageResource(R.drawable.ic_assignment_black_24dp);
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.CLOSED)) {
                bodyViewHolder.txtTitle.setText("Payment Done");
                bodyViewHolder.messageTextView.setText("Request has been closed.");
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
            } else if (requestHistoryVo.getType().equals(RequestHistoryType.JOB_CLOSED)) {
                bodyViewHolder.txtTitle.setText("Request Closed");
                bodyViewHolder.messageTextView.setText("This request has been closed by Concierge.");
                bodyViewHolder.quoteImage.setVisibility(View.GONE);
            }

            try {
                if (position == 0) {
                    if (requestHistoryVo.getType().equals(RequestHistoryType.CLOSED)) {
                        bodyViewHolder.view.setImageResource(R.drawable.ic_check_circle_black_24dp);
                        bodyViewHolder.view.setColorFilter(bodyViewHolder.view.getContext().getResources().getColor(R.color.greenButton));
                    } else {
                        bodyViewHolder.view.setImageResource(R.drawable.ic_adjust_black_24dp);
                        bodyViewHolder.view.setColorFilter(bodyViewHolder.view.getContext().getResources().getColor(R.color.grayDark));
                    }
                } else {
                    bodyViewHolder.view.setImageResource(R.drawable.ic_check_circle_black_24dp);
                    bodyViewHolder.view.setColorFilter(bodyViewHolder.view.getContext().getResources().getColor(R.color.greenButton));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            bodyViewHolder.dateTextView.setText(getDate(requestHistoryVo.getCreatedOn()));

            bodyViewHolder.txtTitle.setText(requestHistoryVo.getType().getName());

            bodyViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (requestHistoryVo.getType().equals(RequestHistoryType.VISIT_REQUESTED)) {
                        if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)) {
                            Intent intent = new Intent(activity, ConfirmScheduleActivity.class);
                            try {
                                intent.putExtra("requestId", requestHistoryVo.getRequest().getId().toString());
                                intent.putExtra("sp_name", requestHistoryVo.getSender().getFirstName() + " " + requestHistoryVo.getSender().getLastName());
                                intent.putExtra("spId", requestHistoryVo.getSender().getId());
                                intent.putExtra("scheduleId", requestHistoryVo.getScheduleId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            activity.startActivity(intent);
                        }
                    } else if (requestHistoryVo.getType().equals(RequestHistoryType.QUOTE)) {
                        Intent intent = new Intent(activity, QuoteSummaryActivity.class);
                        try {
                            intent.putExtra("requestId", requestHistoryVo.getQuote().getId().toString());
                            intent.putExtra("sp_name", requestHistoryVo.getQuote().getSender().getFirstName() + " " + requestHistoryVo.getQuote().getSender().getLastName());
                            intent.putExtra("spId", requestHistoryVo.getQuote().getSender().getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.startActivity(intent);
                    } else if (requestHistoryVo.getType().equals(RequestHistoryType.APPROVED_QUOTE)) {
                        Intent intent = new Intent(activity, QuoteSummaryActivity.class);
                        try {
                            intent.putExtra("requestId", requestHistoryVo.getQuote().getId().toString());
                            intent.putExtra("sp_name", requestHistoryVo.getQuote().getSender().getFirstName() + " " + requestHistoryVo.getQuote().getSender().getLastName());
                            intent.putExtra("spId", requestHistoryVo.getQuote().getSender().getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.startActivity(intent);
                    } else if (requestHistoryVo.getType().equals(RequestHistoryType.AUTO_APPROVED_QUOTE)) {
                        Intent intent = new Intent(activity, QuoteSummaryActivity.class);
                        try {
                            intent.putExtra("requestId", requestHistoryVo.getQuote().getId().toString());
                            intent.putExtra("sp_name", requestHistoryVo.getQuote().getSender().getFirstName() + " " + requestHistoryVo.getQuote().getSender().getLastName());
                            intent.putExtra("spId", requestHistoryVo.getQuote().getSender().getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.startActivity(intent);
                    } else if (requestHistoryVo.getType().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)) {
                        if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)) {
                            Intent intent = new Intent(activity, ConfirmScheduleActivity.class);
                            try {
                                intent.putExtra("requestId", requestHistoryVo.getRequest().getId().toString());
                                intent.putExtra("sp_name", requestHistoryVo.getQuote().getSender().getFirstName() + " " + requestHistoryVo.getQuote().getSender().getLastName());
                                intent.putExtra("spId", requestHistoryVo.getQuote().getSender().getId());
                                intent.putExtra("scheduleId", requestHistoryVo.getScheduleId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            activity.startActivity(intent);
                        }
                    } else if (requestHistoryVo.getType().equals(RequestHistoryType.APPROVED_INVOICE)) {
                        Intent intent = new Intent(activity, QuoteSummaryActivity.class);
                        try {
                            intent.putExtra("requestId", requestHistoryVo.getQuote().getId().toString());
                            intent.putExtra("sp_name", requestHistoryVo.getQuote().getSender().getFirstName() + " " + requestHistoryVo.getQuote().getSender().getLastName());
                            intent.putExtra("spId", requestHistoryVo.getQuote().getSender().getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.startActivity(intent);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView dateTextView, txtTitle, messageTextView;
        ImageView view, quoteImage;
        View itemView;
        public RequestHistoryVo mItem;

        public ViewHolder(View view) {
            super(view);
            this.itemView = view;
            this.txtTitle = (TextView) view.findViewById(R.id.tittle);
            this.dateTextView = (TextView) view.findViewById(R.id.date);
            this.messageTextView = (TextView) view.findViewById(R.id.description);
            this.view = (ImageView) view.findViewById(R.id.view);
            this.quoteImage = (ImageView) view.findViewById(R.id.quoteImage);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtTitle.getText() + "'";
        }
    }
}

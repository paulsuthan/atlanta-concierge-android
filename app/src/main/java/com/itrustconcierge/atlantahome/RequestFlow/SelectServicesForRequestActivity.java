package com.itrustconcierge.atlantahome.RequestFlow;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.FranchiseCategoryVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ServiceVo;
import com.itrustconcierge.atlantahome.Adapters.SelectableImageAdapter;
import com.itrustconcierge.atlantahome.Adapters.SelectableImageViewHolder;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.Item;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableItem;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class SelectServicesForRequestActivity extends AppCompatActivity {

    RecyclerView mostCommonRecyclerView, otherServicesRecyclerView;
    SelectableImageAdapter mostUsedSelectableImageAdapter, otherSelectableImageAdapter;
    SelectableImageViewHolder.OnItemSelectedListener mostUsedItemSelectedListener, otherItemSelectedListener;
    TextView toolBarButton, textViewTittle;
    List<Item> mostCommon = new ArrayList<Item>();
    List<Item> other = new ArrayList<Item>();
    public static Item selectedServiceItem = null;
    LinearLayout layout;
    TextView mostCommonTextView, otherTextView;
    List<Item> filteredCommon = new ArrayList();
    List<Item> filteredOther = new ArrayList();
    SearchView searchView;
    boolean searchOn = true;
    ImageView toolbarTittleImageView;
    FloatingActionButton floatingActionButton;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filter(query);
                searchOn = true;
                Utility.hideKeyboard(SelectServicesForRequestActivity.this);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                searchOn = true;
                return true;
            }

            private void filter(String newText) {
                if (searchOn) {
                    try {
                        searchOn = true;
                        filteredCommon.clear();
                        filteredOther.clear();
                        for (Item d : mostCommon) {
                            if (d.getName().toLowerCase().contains(newText.toLowerCase())) {
                                filteredCommon.add(d);
                            }
                        }
                        if (mostUsedSelectableImageAdapter != null)
                            mostUsedSelectableImageAdapter.updateList(filteredCommon);
                        if (filteredCommon.isEmpty()) {
                            mostCommonTextView.setVisibility(View.GONE);
                        } else {
                            mostCommonTextView.setVisibility(View.VISIBLE);
                        }
                        for (Item d : other) {
                            if (d.getName().toLowerCase().contains(newText.toLowerCase())) {
                                filteredOther.add(d);
                            }
                        }
                        if (mostUsedSelectableImageAdapter != null)
                            otherSelectableImageAdapter.updateList(filteredOther);
                        if (filteredOther.isEmpty()) {
                            otherTextView.setVisibility(View.GONE);
                        } else {
                            otherTextView.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_services_for_request);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTittleImageView = (ImageView) findViewById(R.id.toolbarTittleImageView);
        toolbarTittleImageView.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Select a Service");
        textViewTittle = (TextView) findViewById(R.id.textViewTittle);
        textViewTittle.setText("Select a Service");
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + authenticateVoLocal.getFranchise().getMobileNumber()));
                if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SelectServicesForRequestActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                            1);
                    return;
                }
                startActivity(callIntent);
            }
        });

        Button notlisted = (Button) findViewById(R.id.notlisted);

        RelativeLayout relativeLayoutToolbar = (RelativeLayout) findViewById(R.id.relativeLayoutToolbar);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relativeLayoutToolbar.getLayoutParams();
        params.removeRule(RelativeLayout.CENTER_IN_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_START);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.setMarginStart(0);
        params.setMarginEnd(0);
        params.setMargins(0, 0, 0, 0);
        params.addRule(RelativeLayout.START_OF, R.id.toolBarButton);
        relativeLayoutToolbar.setLayoutParams(params);

        RelativeLayout.LayoutParams paramsText = (RelativeLayout.LayoutParams) textViewTittle.getLayoutParams();
        paramsText.setMarginStart(0);
        params.setMarginEnd(0);
        paramsText.setMargins(0, 0, 0, 0);

        textViewTittle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);

        RelativeLayout.LayoutParams paramsToolbar = (RelativeLayout.LayoutParams) toolBarButton.getLayoutParams();
        paramsToolbar.setMarginStart(0);
        paramsToolbar.setMarginEnd(0);
        paramsToolbar.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
        toolBarButton.setLayoutParams(paramsToolbar);
        toolBarButton.setPadding(10, 10, 10, 10);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layout = (LinearLayout) findViewById(R.id.layout);

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utility.hideKeyboard(SelectServicesForRequestActivity.this);
                return true;
            }
        });

        mostCommonTextView = (TextView) findViewById(R.id.mostCommonTextView);
        otherTextView = (TextView) findViewById(R.id.otherTextView);

        mostCommonRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_most_common);
        otherServicesRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_other_services);
        toolBarButton.setText("NEXT");
        toolBarButton.setVisibility(View.GONE);

        notlisted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedServiceItem = new Item("Other Service", "Other Service");
                selectedServiceItem.setId(0);
                Intent intent = new Intent(SelectServicesForRequestActivity.this, CreateRequestActivity.class);
                startActivity(intent);
            }
        });

        mostUsedItemSelectedListener = new SelectableImageViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                searchOn = false;
                selectedServiceItem = item;
                selectedServiceItem.setId(item.getId());
                toolBarButton.setVisibility(View.VISIBLE);
                if (!filteredCommon.isEmpty()) {
                    otherSelectableImageAdapter.updateList(filteredOther);
                } else {
                    otherSelectableImageAdapter = new SelectableImageAdapter(otherItemSelectedListener, other, false);
                    otherServicesRecyclerView.setAdapter(otherSelectableImageAdapter);
                }
                invalidateOptionsMenu();
            }
        };

        otherItemSelectedListener = new SelectableImageViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                searchOn = false;
                selectedServiceItem = item;
                selectedServiceItem.setId(item.getId());
                toolBarButton.setVisibility(View.VISIBLE);
                if (!filteredOther.isEmpty()) {
                    mostUsedSelectableImageAdapter.updateList(filteredCommon);
                } else {
                    mostUsedSelectableImageAdapter = new SelectableImageAdapter(mostUsedItemSelectedListener, mostCommon, false);
                    mostCommonRecyclerView.setAdapter(mostUsedSelectableImageAdapter);
                }
                invalidateOptionsMenu();
            }
        };

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(SelectServicesForRequestActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mostCommonRecyclerView.setLayoutManager(mLinearLayoutManager);
        mostCommonRecyclerView.setHasFixedSize(true);
        mostCommonRecyclerView.setItemViewCacheSize(20);
        mostCommonRecyclerView.setDrawingCacheEnabled(true);
        mostCommonRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        GridLayoutManager mGridLayoutManager;
        mGridLayoutManager = new GridLayoutManager(SelectServicesForRequestActivity.this, 3);
        otherServicesRecyclerView.setLayoutManager(mGridLayoutManager);
        otherServicesRecyclerView.setHasFixedSize(true);
        otherServicesRecyclerView.setItemViewCacheSize(20);
        otherServicesRecyclerView.setDrawingCacheEnabled(true);
        otherServicesRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        if (checkInternetConenction(SelectServicesForRequestActivity.this)) {
            getCategories();
        } else {
            Snackbar.make(otherServicesRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectServicesForRequestActivity.this, CreateRequestActivity.class);
                startActivity(intent);
            }
        });

    }

    private void getCategories() {
        final Dialog loadingDialog = Utility.loadingDialog(SelectServicesForRequestActivity.this);
        ApiInterface apiService = ApiClient.getClient(SelectServicesForRequestActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<FranchiseCategoryVo>> call = apiService.getCategories();
        call.enqueue(new Callback<ResponseWrapper<FranchiseCategoryVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<FranchiseCategoryVo>> call, Response<ResponseWrapper<FranchiseCategoryVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                List<FranchiseCategoryVo> franchiseCategoryVos = new ArrayList<FranchiseCategoryVo>();
                                for (FranchiseCategoryVo fr : response.body().getList()) {
                                    franchiseCategoryVos.add(fr);
                                }
                                try {
                                    for (FranchiseCategoryVo fr : franchiseCategoryVos) {
                                        if (getIntent().getExtras().getBoolean("home", false)) {
                                            if (fr.getName().contains("Home")) {
                                                for (ServiceVo serviceVo : fr.getServices()) {
                                                    if (serviceVo.getType() != null) {
                                                        if (serviceVo.getType().equals("common")) {
                                                            Item item = new Item(serviceVo.getName(), serviceVo.getIconUrl());
                                                            item.setId(serviceVo.getId());
                                                            mostCommon.add(item);
                                                        }
                                                    } else {
                                                        Item item = new Item(serviceVo.getName(), serviceVo.getIconUrl());
                                                        item.setId(serviceVo.getId());
                                                        other.add(item);
                                                    }
                                                }
                                            }
                                        } else {
                                            if (!fr.getName().contains("Home")) {
                                                for (ServiceVo serviceVo : fr.getServices()) {
                                                    if (serviceVo.getType() != null) {
                                                        if (serviceVo.getType().equals("common")) {
                                                            Item item = new Item(serviceVo.getName(), serviceVo.getIconUrl());
                                                            item.setId(serviceVo.getId());
                                                            mostCommon.add(item);
                                                        }
                                                    } else {
                                                        Item item = new Item(serviceVo.getName(), serviceVo.getIconUrl());
                                                        item.setId(serviceVo.getId());
                                                        other.add(item);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    mostUsedSelectableImageAdapter = new SelectableImageAdapter(mostUsedItemSelectedListener, mostCommon, false);
                                    mostCommonRecyclerView.setAdapter(mostUsedSelectableImageAdapter);
                                    otherSelectableImageAdapter = new SelectableImageAdapter(otherItemSelectedListener, other, false);
                                    otherServicesRecyclerView.setAdapter(otherSelectableImageAdapter);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                        }
                    } else {
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<FranchiseCategoryVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(otherServicesRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}

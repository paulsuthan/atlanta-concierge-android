package com.itrustconcierge.atlantahome.RequestFlow;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentType;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IhcFileVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestFileVo;
import com.itrustconcierge.atlantahome.AddCardActivity;
import com.itrustconcierge.atlantahome.MainActivity;
import com.itrustconcierge.atlantahome.PaymentActivity;
import com.itrustconcierge.atlantahome.PaymentNewActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.SubcriptionDetailsActivity;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.Item;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableAdapter;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableViewHolder;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter.imageUrlMap;
import static com.itrustconcierge.atlantahome.RequestFlow.CreateRequestActivity.homeOwnerRequestVoInput;
import static com.itrustconcierge.atlantahome.RequestFlow.CreateRequestActivity.requestImagespathList;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoMyJobs;

public class CreateRequestFinalActivity extends AppCompatActivity {

    TextView toolBarButton, myImageViewText;
    RecyclerView propertyAccessRecyclerView;
    RecyclerView frequencyRecyclerView;
    SelectableViewHolder.OnItemSelectedListener frequencyListener;
    SelectableViewHolder.OnItemSelectedListener propertyAccessListener;
    SelectableAdapter frequencySelectableAdapter;
    SelectableAdapter propertyAccessSelectableAdapter;
    String accessKey;
    String selectedDate;
    TimePickerDialog timePickDialogTime = null;
    String selectedTime;
    String slectedDateTime;
    String preferredTimeToComplete;
    ImageView myImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_request_final);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Request");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);

        toolBarButton.setText("DONE");
        myImageViewText.setText("JOB SCHEDULE");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);

        propertyAccessRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_property_access);
        frequencyRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_frequency);

        propertyAccessListener = new SelectableViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                if (item.getName().contains("Contact me")) {
                    accessKey = "CONTACT_ME_FOR_ACCESS_KEY";
                } else if (item.getName().contains("Contact Atlanta Concierge")) {
                    accessKey = "CONTACT_MASTER_CONCIERGE";
                } else {
                    accessKey = "NOT_APPLICABLE";
                }
            }
        };

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                selectedDate = sdf.format(myCalendar.getTime());

//                int hour = 18;
                int hour = 7;
                int minute = 0;
                timePickDialogTime = new TimePickerDialog(CreateRequestFinalActivity.this,
                        new TimePickHandlerTime(), hour, minute, false);
                timePickDialogTime.show();
            }

        };

        frequencyListener = new SelectableViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableItem item) {
                if (item.getName().contains("date")) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(CreateRequestFinalActivity.this, date1, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                } else {
                    if (item.getName().contains("Immediately")) {
                        preferredTimeToComplete = "Immediate";
                    } else {
                        preferredTimeToComplete = "Flexible";
                    }
                }
                if (item.getName().contains("Immediately")) {
                    homeOwnerRequestVoInput.setUrgent(true);
                } else {
                    homeOwnerRequestVoInput.setUrgent(false);
                }
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        propertyAccessRecyclerView.setLayoutManager(layoutManager);
        propertyAccessRecyclerView.setHasFixedSize(true);
        propertyAccessRecyclerView.setItemViewCacheSize(20);
        propertyAccessRecyclerView.setDrawingCacheEnabled(true);
        propertyAccessRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(propertyAccessRecyclerView.getContext(),
                new LinearLayoutManager(CreateRequestFinalActivity.this).getOrientation());
        propertyAccessRecyclerView.addItemDecoration(dividerItemDecoration);
        List<Item> selectableItems = getPropertyAccess();
        propertyAccessSelectableAdapter = new SelectableAdapter(propertyAccessListener, selectableItems, false);
        propertyAccessRecyclerView.setAdapter(propertyAccessSelectableAdapter);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        frequencyRecyclerView.setLayoutManager(layoutManager1);
        frequencyRecyclerView.setHasFixedSize(true);
        frequencyRecyclerView.setItemViewCacheSize(20);
        frequencyRecyclerView.setDrawingCacheEnabled(true);
        frequencyRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(frequencyRecyclerView.getContext(),
                new LinearLayoutManager(CreateRequestFinalActivity.this).getOrientation());
        frequencyRecyclerView.addItemDecoration(dividerItemDecoration1);
        frequencyItems = getFrequency();
        frequencySelectableAdapter = new SelectableAdapter(frequencyListener, frequencyItems, false);
        frequencyRecyclerView.setAdapter(frequencySelectableAdapter);

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (accessKey != null) {
                    if (preferredTimeToComplete != null) {
                        homeOwnerRequestVoInput.setAccessKey(accessKey);
                        homeOwnerRequestVoInput.setPreferredTimeToCompletion(preferredTimeToComplete);
                        if (checkInternetConenction(CreateRequestFinalActivity.this)) {
                            createRequest(homeOwnerRequestVoInput);
                        } else {
                            Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
//                        if (authenticateVoLocal.getSubscriptionRequired()) {
//                            if (authenticateVoLocal.getSubscriptionStatus().equals(SubscriptionStatus.SUBSCRIBED)) {
//                                if (checkInternetConenction(CreateRequestFinalActivity.this)) {
//                                    createRequest(homeOwnerRequestVoInput);
//                                } else {
//                                    Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
//                                }
//                            } else {
//                                if (checkInternetConenction(CreateRequestFinalActivity.this)) {
//                                    createRequest(homeOwnerRequestVoInput);
////                                    getPayments();
//                                    getPaymentforSubscription();
//                                } else {
//                                    Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
//                                }
//                            }
//                        } else {
//                            if (checkInternetConenction(CreateRequestFinalActivity.this)) {
//                                createRequest(homeOwnerRequestVoInput);
//                            } else {
//                                Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
//                            }
//                        }
                    } else {
                        Snackbar.make(frequencyRecyclerView, "Please select preferred time to complete!", Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(frequencyRecyclerView, "Please select property access!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getPaymentforSubscription() {
        final Dialog loadingDialog = Utility.loadingDialog(CreateRequestFinalActivity.this);
        ApiInterface apiService = ApiClient.getClient(CreateRequestFinalActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PaymentVo>> call = apiService.getPropertySubscriptionPayment(StaticInfo.authenticateVoLocal.getId(), homeOwnerRequestVoInput.getProperty().getId());
        call.enqueue(new Callback<ResponseWrapper<PaymentVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PaymentVo>> call, Response<ResponseWrapper<PaymentVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            PaymentVo paymentVo = response.body().getData();
                            if (paymentVo.getPaymentType().equals(PaymentType.SUBSCRIBE)) {
                                QuoteSummaryActivity.paymentVo = new PaymentVo();
                                QuoteSummaryActivity.paymentVo = paymentVo;
                                Toast.makeText(CreateRequestFinalActivity.this, "Please complete your subscription first!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(CreateRequestFinalActivity.this, PaymentNewActivity.class);
                                startActivity(intent);
                            } else {
                                createRequest(homeOwnerRequestVoInput);
                            }
                        } else {
                            createRequest(homeOwnerRequestVoInput);
                        }
                    } else {
                        createRequest(homeOwnerRequestVoInput);
                    }
                } else {
                    createRequest(homeOwnerRequestVoInput);
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PaymentVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getPayments() {
        final Dialog loadingDialog = Utility.loadingDialog(CreateRequestFinalActivity.this);
        ApiInterface apiService = ApiClient.getClient(CreateRequestFinalActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PaymentVo>> call = apiService.getPayments(PaymentStatus.PENDING);
        call.enqueue(new Callback<ResponseWrapper<PaymentVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PaymentVo>> call, Response<ResponseWrapper<PaymentVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    for (PaymentVo vo : response.body().getList()) {
                                        if (vo.getPaymentType().equals(PaymentType.SUBSCRIBE)) {
                                            QuoteSummaryActivity.paymentVo = new PaymentVo();
                                            QuoteSummaryActivity.paymentVo = vo;
                                        }
                                    }
                                    Toast.makeText(CreateRequestFinalActivity.this, "Please complete your subscription first!", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(CreateRequestFinalActivity.this, PaymentNewActivity.class);
                                    startActivity(intent);
                                } else {

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(frequencyRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(frequencyRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(frequencyRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PaymentVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(frequencyRecyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void createRequest(final HomeOwnerRequestVo homeOwnerRequestVoInput) {
        final Dialog loadingDialog = Utility.loadingDialog(CreateRequestFinalActivity.this);
        ApiInterface apiService = ApiClient.getClient(CreateRequestFinalActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.createHomeOwnerRequest(homeOwnerRequestVoInput);
        call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                try {
                                    HomeOwnerRequestVo homeOwnerRequestVo = null;
                                    homeOwnerRequestVo = response.body().getData();
                                    for (Map.Entry<String, String> imageUrlMap : imageUrlMap.entrySet()) {
                                        final File file = new File(imageUrlMap.getKey());
                                        String fileName = file.getName();
                                        final String mimeType = "image/jpeg";
                                        String physicalFileName = imageUrlMap.getValue();
                                        RequestFileVo requestFileVo = new RequestFileVo();
                                        requestFileVo.setRequest(homeOwnerRequestVo);
                                        IhcFileVo ihcFileVo = new IhcFileVo();
                                        ihcFileVo.setDeleted(false);
                                        ihcFileVo.setMimeType(mimeType);
                                        ihcFileVo.setName(fileName);
                                        ihcFileVo.setPhysicalFileName(physicalFileName);
                                        ihcFileVo.setTypeId(authenticateVoLocal.getId());
                                        ihcFileVo.setUploadedBy(authenticateVoLocal);
                                        ihcFileVo.setType("REQUEST");
                                        requestFileVo.setType("REQUEST");
                                        requestFileVo.setFile(ihcFileVo);
                                        ApiInterface apiService = ApiClient.getClient(CreateRequestFinalActivity.this).create(ApiInterface.class);
                                        Call<ResponseWrapper<RequestFileVo>> call1 = apiService.addRequestPhotos(requestFileVo);
                                        call1.enqueue(new Callback<ResponseWrapper<RequestFileVo>>() {
                                            @Override
                                            public void onResponse(Call<ResponseWrapper<RequestFileVo>> call, Response<ResponseWrapper<RequestFileVo>> response) {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        if (response.body().getData() != null) {
                                                            try {
                                                                RequestFileVo requestFileVo1 = response.body().getData();
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseWrapper<RequestFileVo>> call, Throwable t) {
                                                // Log error here since request failed
                                                Log.e("Error", t.toString());
                                            }
                                        });
                                    }
                                } catch (Exception e){
                                    e.printStackTrace();
                                }

                                gotoMyJobs = true;
                                CreateRequestActivity.homeOwnerRequestVoInput = null;
                                imageUrlMap.clear();
                                requestImagespathList.clear();
                                final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(CreateRequestFinalActivity.this, R.style.dialogAnimation));
                                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                requestDialog.setContentView(R.layout.after_request_popup);
                                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                requestDialog.setCancelable(false);
                                requestDialog.getWindow().setGravity(Gravity.CENTER);
                                Button positiveButton = (Button) requestDialog.findViewById(R.id.addRequest);
                                Button negativeButton = (Button) requestDialog.findViewById(R.id.contact);
                                Button nofurther = (Button) requestDialog.findViewById(R.id.nofurther);

                                ImageButton close = (ImageButton) requestDialog.findViewById(R.id.close);

                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        requestDialog.dismiss();
                                        Intent intent = new Intent(CreateRequestFinalActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        startActivity(intent);
                                        finishAffinity();
                                    }
                                });

                                nofurther.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        requestDialog.dismiss();
                                        Intent intent = new Intent(CreateRequestFinalActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        startActivity(intent);
                                        finishAffinity();
                                    }
                                });

                                negativeButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                                        callIntent.setData(Uri.parse("tel:" + authenticateVoLocal.getFranchise().getMobileNumber()));
                                        if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                            ActivityCompat.requestPermissions(CreateRequestFinalActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                                                    1);
                                            return;
                                        }
                                        startActivity(callIntent);
                                    }
                                });

                                positiveButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub
                                        requestDialog.dismiss();
                                        Intent intent = new Intent(CreateRequestFinalActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        startActivity(intent);
                                        finishAffinity();
                                    }
                                });

                                requestDialog.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Snackbar.make(frequencyRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(frequencyRecyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public List<Item> getPropertyAccess() {
        List<Item> selectableItems = new ArrayList<>();
        selectableItems.add(new Item("Contact me for property access", "Contact me for property access"));
        selectableItems.add(new Item("Contact Atlanta Concierge for property access", "Contact Atlanta Concierge for property access"));
        selectableItems.add(new Item("Not applicable", "Not applicable"));
        return selectableItems;
    }

    List<Item> frequencyItems = new ArrayList<>();

    public List<Item> getFrequency() {
        frequencyItems.add(new Item("Immediately", "Immediately"));
        frequencyItems.add(new Item("I'm flexible", "I'm flexible"));
        frequencyItems.add(new Item("By specific date", "By specific date"));
        return frequencyItems;
    }

    private class TimePickHandlerTime implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            selectedTime = String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US);
//            startTime = String.format("%02d:%02d", hourOfDay, minute, Locale.US);
            slectedDateTime = selectedDate + " " + selectedTime;
            Item item = new Item("By specific date " + slectedDateTime, "By specific date " + slectedDateTime);
            SelectableItem selectableItem = new SelectableItem(item, true);
//            selectableItem.setSelected(false);
            selectableItem.setSelected(true);
            frequencyItems.remove(2);
            frequencyItems.add(selectableItem);
            timePickDialogTime.hide();
            frequencySelectableAdapter = new SelectableAdapter(frequencyListener, frequencyItems, false);
            frequencyRecyclerView.setAdapter(frequencySelectableAdapter);
            preferredTimeToComplete = slectedDateTime;
//            frequencySelectableAdapter.notifyDataSetChanged();
        }
    }

}

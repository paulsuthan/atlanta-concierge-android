package com.itrustconcierge.atlantahome.RequestFlow;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.MainActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.CreateRequestActivity.homeOwnerRequestVoInput;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class RequestServiceActivity extends AppCompatActivity {

    RelativeLayout homeMaintenance, personalServices;
    TextView toolBarButton, myImageViewText;
    PropertyVo propertyVoTemp;
    List<PropertyVo> propertyVos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_service);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Request Service");
//        toolbar.setTitleTextColor(Color.WHITE);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        homeMaintenance = (RelativeLayout) findViewById(R.id.homeMaintenance);
        personalServices = (RelativeLayout) findViewById(R.id.personalServices);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        myImageViewText.setText("SERVICE REQUEST");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        toolBarButton.setVisibility(View.GONE);

        propertyVos = new ArrayList<>();

        homeMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RequestServiceActivity.this, SelectServicesForRequestActivity.class);
                intent.putExtra("home", true);
                homeOwnerRequestVoInput = new HomeOwnerRequestVo();
                propertyVoTemp = propertyVos.get(0);
                homeOwnerRequestVoInput.setProperty(propertyVoTemp);
                startActivity(intent);
            }
        });

        personalServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RequestServiceActivity.this, SelectServicesForRequestActivity.class);
                intent.putExtra("home", false);
                homeOwnerRequestVoInput = new HomeOwnerRequestVo();
                propertyVoTemp = propertyVos.get(0);
                homeOwnerRequestVoInput.setProperty(propertyVoTemp);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!getUserVisibleHint()) {
//            return;
//        }
        if (checkInternetConenction(this)) {
            getProperties();
        } else
            Toast.makeText(this, "NO_INTERNET", Toast.LENGTH_SHORT).show();
    }

    private void getProperties() {
        final Dialog loadingDialog = Utility.loadingDialog(this);
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyVo>> call = apiService.getPropertiesByUser();
        call.enqueue(new Callback<ResponseWrapper<PropertyVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyVo>> call, Response<ResponseWrapper<PropertyVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
//                    getUnmappedProperties();
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    propertyVos.clear();
//                                    MainActivity.navigation.restoreBottomNavigation();
                                    propertyVos = response.body().getList();

                                } else {
//                                    MainActivity.navigation.hideBottomNavigation();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
//                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
//                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
//                                MainActivity.navigation.hideBottomNavigation();
                            } else {
//                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
//                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
//                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
//                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}

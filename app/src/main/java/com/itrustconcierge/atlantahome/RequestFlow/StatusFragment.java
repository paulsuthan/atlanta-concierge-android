package com.itrustconcierge.atlantahome.RequestFlow;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.R;

import java.util.ArrayList;
import java.util.List;

public class StatusFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    static List<RequestHistoryVo> requestHistoryVos = new ArrayList<>();
    RecyclerView recyclerView;

    public StatusFragment() {
    }

    public static StatusFragment newInstance(int columnCount, String s, List<RequestHistoryVo> requestHistoryVos1) {
        StatusFragment fragment = new StatusFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        requestHistoryVos = requestHistoryVos1;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.requesthistoryvo_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        RequestHistoryVoAdapter requestHistoryVoAdapter = new RequestHistoryVoAdapter(requestHistoryVos, getActivity());
        recyclerView.setAdapter(requestHistoryVoAdapter);
        requestHistoryVoAdapter.notifyDataSetChanged();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

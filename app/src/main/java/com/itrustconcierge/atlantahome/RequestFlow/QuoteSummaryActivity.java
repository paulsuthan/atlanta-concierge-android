package com.itrustconcierge.atlantahome.RequestFlow;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Line;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.FeedbackFromType;
import com.itrustconcierge.atlantahome.API_Manager.enums.FeedbackType;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentType;
import com.itrustconcierge.atlantahome.API_Manager.enums.QuoteType;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.vo.FeedbackParameterVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.FeedbackVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.QuoteVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.Adapters.FeedbackParameterVoAdapter;
import com.itrustconcierge.atlantahome.PaymentActivity;
import com.itrustconcierge.atlantahome.PaymentNewActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Adapters.FeedbackParameterVoAdapter.parameterInfos;
import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.formatter;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class QuoteSummaryActivity extends AppCompatActivity {

    TextView date_TextView, nameTextView, total_amount_TextView, textViewTittle;
    Bundle bundle;
    ImageView profileImageView;
    ImageButton chatImageButton;
    LinearLayout material_layout;
    Button approveButton;
    QuoteVo quoteVoStatic = new QuoteVo();
    public static PaymentVo paymentVo = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (getIntent().getExtras().getBoolean("isFromPush")) {
                Utility.gotoMyJobs = true;
                Intent resultIntent = new Intent(getApplicationContext(), WorkHistoryActivity.class);
                resultIntent.putExtra("requestId", quoteVoStatic.getRequest().getId());
                resultIntent.putExtra("isFromPush", true);
                startActivity(resultIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_summary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bundle = getIntent().getExtras();

        date_TextView = (TextView) findViewById(R.id.date);
        textViewTittle = (TextView) findViewById(R.id.name);
        nameTextView = (TextView) findViewById(R.id.name);
        profileImageView = (ImageView) findViewById(R.id.view);
        material_layout = (LinearLayout) findViewById(R.id.material_layout);
        total_amount_TextView = (TextView) findViewById(R.id.textView75);
        chatImageButton = (ImageButton) findViewById(R.id.chatImageButton);
        chatImageButton.setVisibility(View.GONE);
        approveButton = (Button) findViewById(R.id.approve);

        approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.QUOTE)) {
                    final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(QuoteSummaryActivity.this, R.style.dialogAnimation));
                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    requestDialog.setCancelable(true);
                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                    TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                    TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                    tittleTextView.setText("Estimate Acceptance");
                    descriptionTextView.setText("Our iTrust Pro will begin working on your request as soon as you approve our estimate.");
                    negativeButton.setText("Cancel");
                    positiveButton.setText("Accept");

                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            requestDialog.dismiss();
                            if (checkInternetConenction(QuoteSummaryActivity.this)) {
                                approveQuote();
                            } else {
                                Snackbar.make(material_layout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    });

                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestDialog.dismiss();
                        }
                    });

                    requestDialog.show();
                } else if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.APPROVED_INVOICE)) {

                    final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(QuoteSummaryActivity.this, R.style.dialogAnimation));
                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    requestDialog.setContentView(R.layout.rating_dialogue);
                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    requestDialog.setCancelable(true);
                    requestDialog.getWindow().setGravity(Gravity.CENTER);
                    final EditText reviewEditText = (EditText) requestDialog.findViewById(R.id.descriptionEdit);
                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);
                    final RecyclerView recyclerView = (RecyclerView) requestDialog.findViewById(R.id.recycler_view);

                    ApiInterface apiService = ApiClient.getClient(QuoteSummaryActivity.this).create(ApiInterface.class);
                    Call<ResponseWrapper<FeedbackParameterVo>> call = apiService.getFeedbackParameters(FeedbackType.JOB);
                    call.enqueue(new Callback<ResponseWrapper<FeedbackParameterVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<FeedbackParameterVo>> call, Response<ResponseWrapper<FeedbackParameterVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getList() != null) {
                                        try {
                                            if (!response.body().getList().isEmpty()) {
                                                List<FeedbackParameterVo> parameterInfos = new ArrayList<>();
                                                parameterInfos = response.body().getList();
                                                FeedbackParameterVoAdapter feedbackParameterVoAdapter = new FeedbackParameterVoAdapter(parameterInfos, QuoteSummaryActivity.this);
                                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(QuoteSummaryActivity.this);
                                                recyclerView.setLayoutManager(mLayoutManager);
                                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                                recyclerView.setAdapter(feedbackParameterVoAdapter);
                                            } else {

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<FeedbackParameterVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
                            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });

                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            FeedbackVo feedbackVo = new FeedbackVo();
                            feedbackVo.setParameters(parameterInfos);
                            feedbackVo.setFeedbackType(FeedbackType.JOB);
                            feedbackVo.setDescription(reviewEditText.getText().toString());
                            feedbackVo.setFeedbackFrom(String.valueOf(authenticateVoLocal.getId()));
                            feedbackVo.setFeedbackTo(String.valueOf(quoteVoStatic.getRequest().getId()));
                            feedbackVo.setFeedbackFromType(FeedbackFromType.HO);
                            final Dialog loadingDialog = Utility.loadingDialog(QuoteSummaryActivity.this);
                            ApiInterface apiService = ApiClient.getClient(QuoteSummaryActivity.this).create(ApiInterface.class);
                            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.addFeedback(feedbackVo);
                            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                @Override
                                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                    loadingDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        requestDialog.dismiss();
                                        paymentVo = new PaymentVo();
                                        paymentVo.setId(quoteVoStatic.getPaymentId());
                                        paymentVo.setAmount(quoteVoStatic.getHomeOwnerPayableAmount());
                                        paymentVo.setPaymentType(PaymentType.INVOICE);
                                        paymentVo.setPaymentStatus(PaymentStatus.PENDING);
                                        paymentVo.setTitle("Invoice Payment");
                                        paymentVo.setSubject(homeOwnerRequestVoStatic.getId());
                                        paymentVo.setLastUpdatedOn(quoteVoStatic.getLastUpdatedOn());
                                        paymentVo.setCreatedOn(quoteVoStatic.getCreatedOn());
                                        Intent intent = new Intent(QuoteSummaryActivity.this, PaymentNewActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(QuoteSummaryActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                    // Log error here since request failed
                                    loadingDialog.dismiss();
                                    Log.e("Error", t.toString());
                                    Toast.makeText(QuoteSummaryActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestDialog.dismiss();
                            paymentVo = new PaymentVo();
                            paymentVo.setId(quoteVoStatic.getPaymentId());
                            paymentVo.setAmount(quoteVoStatic.getHomeOwnerPayableAmount());
                            paymentVo.setPaymentType(PaymentType.INVOICE);
                            paymentVo.setPaymentStatus(PaymentStatus.PENDING);
                            paymentVo.setTitle("Invoice Payment");
                            paymentVo.setSubject(homeOwnerRequestVoStatic.getId());
                            paymentVo.setLastUpdatedOn(quoteVoStatic.getLastUpdatedOn());
                            paymentVo.setCreatedOn(quoteVoStatic.getCreatedOn());
                            Intent intent = new Intent(QuoteSummaryActivity.this, PaymentNewActivity.class);
                            startActivity(intent);
                        }
                    });

                    requestDialog.show();
                }

            }
        });

        chatImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(homeOwnerRequestVoStatic != null) {
                    Intent i = new Intent(QuoteSummaryActivity.this, RequestMessagesActivity.class);
                    try {
                        i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivity(i);
                } else {
                    final Dialog loadingDialog = Utility.loadingDialog(QuoteSummaryActivity.this);
                    ApiInterface apiService = ApiClient.getClient(QuoteSummaryActivity.this).create(ApiInterface.class);
                    Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequest(quoteVoStatic.getRequest().getId());
                    call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                            loadingDialog.dismiss();
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
                                        try {
                                            homeOwnerRequestVoStatic = new HomeOwnerRequestVo();
                                            homeOwnerRequestVoStatic = response.body().getData();
                                            Intent i = new Intent(QuoteSummaryActivity.this, RequestMessagesActivity.class);
                                            try {
                                                i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            startActivity(i);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                            // Log error here since request failed
                            loadingDialog.dismiss();
                            Log.e("Error", t.toString());
                            Snackbar.make(material_layout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (bundle != null) {
            if (!bundle.getString("requestId", "").isEmpty()) {
                if (checkInternetConenction(QuoteSummaryActivity.this)) {
                    getQuote(bundle.getString("requestId", ""));
                } else {
                    Snackbar.make(material_layout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    private void getQuote(String requestId) {
        final Dialog loadingDialog = Utility.loadingDialog(QuoteSummaryActivity.this);
        ApiInterface apiService = ApiClient.getClient(QuoteSummaryActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<QuoteVo>> call = apiService.getQuote(requestId);
        call.enqueue(new Callback<ResponseWrapper<QuoteVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<QuoteVo>> call, Response<ResponseWrapper<QuoteVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                quoteVoStatic = new QuoteVo();
                                quoteVoStatic = response.body().getData();
                                if (quoteVoStatic != null) {
                                    chatImageButton.setVisibility(View.VISIBLE);
                                    LinearLayout totalLayout = (LinearLayout) findViewById(R.id.Total);
                                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) totalLayout.getLayoutParams();

                                    try {
//                                        nameTextView.setText(quoteVoStatic.getSender().getFirstName() + " " + quoteVoStatic.getSender().getLastName());
                                        if (quoteVoStatic.getType().equals(QuoteType.QUOTE)) {
                                            textViewTittle.setText("Quote - " + quoteVoStatic.getRequest().getHomeOwner().getFirstName() + "  " + quoteVoStatic.getRequest().getHomeOwner().getLastName());
                                        } else {
                                            textViewTittle.setText("Invoice - " + quoteVoStatic.getRequest().getHomeOwner().getFirstName() + "  " + quoteVoStatic.getRequest().getHomeOwner().getLastName());
                                            if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.APPROVED_INVOICE)) {
                                            } else if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.INVOICE)) {
                                            } else {
                                                textViewTittle.setText("Closed Job - " + quoteVoStatic.getRequest().getHomeOwner().getFirstName() + "  " + quoteVoStatic.getRequest().getHomeOwner().getLastName());
                                            }
                                        }
                                        if (quoteVoStatic.getStatus().equals(RequestHistoryType.QUOTE)) {
                                            date_TextView.setText(getDate(quoteVoStatic.getLastUpdatedOn()));
                                        } else {
                                            date_TextView.setText(getDate(quoteVoStatic.getCreatedOn()));
                                        }

                                        if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.QUOTE)) {
                                            approveButton.setVisibility(View.VISIBLE);
                                            approveButton.setText("Approve");
                                            params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                            totalLayout.setLayoutParams(params);
                                        } else if (quoteVoStatic.getRequest().getStatus().equals(RequestHistoryType.APPROVED_INVOICE)) {
                                            approveButton.setVisibility(View.VISIBLE);
                                            approveButton.setText("Pay Now");
                                            params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                            totalLayout.setLayoutParams(params);
                                        } else {
                                            approveButton.setVisibility(View.GONE);
                                            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                                            totalLayout.setLayoutParams(params);
                                        }

//                                        if (quoteVoStatic.getRequest().getHomeOwner().getProfileImage() != null) {
//                                            Glide.with(profileImageView.getContext()).load(quoteVoStatic.getRequest().getHomeOwner().getProfileImage()).asBitmap().centerCrop().into(new BitmapImageViewTarget(profileImageView) {
//                                                @Override
//                                                protected void setResource(Bitmap resource) {
//                                                    RoundedBitmapDrawable circularBitmapDrawable =
//                                                            RoundedBitmapDrawableFactory.create(profileImageView.getContext().getResources(), resource);
//                                                    circularBitmapDrawable.setCircular(true);
//                                                    profileImageView.setImageDrawable(circularBitmapDrawable);
//                                                }
//                                            });
//                                        }

                                        if (quoteVoStatic.getItems() != null) {
                                            material_layout.removeAllViews();
                                            for (int i = 0; i < quoteVoStatic.getItems().size(); i++) {
                                                LayoutInflater layoutInflater =
                                                        (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                                final View addView = layoutInflater.inflate(R.layout.material_view_row, null);
                                                TextView item_name = (TextView) addView.findViewById(R.id.description);
                                                TextView quantity = (TextView) addView.findViewById(R.id.qty);
                                                TextView rate_per_item = (TextView) addView.findViewById(R.id.rate);
                                                TextView amount = (TextView) addView.findViewById(R.id.total);

                                                String name = "", quantity_str = "", rate = "0.00", tax = "0.00", amount_str = "0.00";

                                                if (quoteVoStatic.getItems().get(i).getName() != null) {
                                                    name = quoteVoStatic.getItems().get(i).getName();
                                                }
                                                if (String.valueOf(quoteVoStatic.getItems().get(i).getQuantity()) != null) {
                                                    quantity_str = String.valueOf(quoteVoStatic.getItems().get(i).getQuantity());
                                                }
                                                if (String.valueOf(quoteVoStatic.getItems().get(i).getAmount()) != null) {
                                                    rate = formatter.format(quoteVoStatic.getItems().get(i).getAmount());
                                                }
                                                if (String.valueOf(quoteVoStatic.getItems().get(i).getTotal()) != null) {
                                                    amount_str = formatter.format(quoteVoStatic.getItems().get(i).getTotal());
                                                }
                                                try {
                                                    if (quoteVoStatic.getItems().get(i).getTaxAmount() != null) {
                                                        tax = formatter.format(quoteVoStatic.getItems().get(i).getTaxAmount());
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                item_name.setText(name);
                                                quantity.setText(quantity_str);
                                                if (tax.equals("0.00")) {
                                                    rate_per_item.setText("$" + rate);
                                                } else {
                                                    rate_per_item.setText("$" + rate + "  + $" + tax);
                                                }
                                                amount.setText("$" + amount_str);
                                                material_layout.addView(addView);
                                            }
                                        }
                                        String total_amount = "0.00";
                                        if (String.valueOf(quoteVoStatic.getFinalTotal()) != null) {
                                            total_amount = String.valueOf(formatter.format(quoteVoStatic.getHomeOwnerPayableAmount()));
                                        }
                                        total_amount_TextView.setText("$" + total_amount);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<QuoteVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(material_layout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void approveQuote() {
        QuoteVo quoteVo = new QuoteVo();
        quoteVo.setId(quoteVoStatic.getId());
        final Dialog loadingDialog = Utility.loadingDialog(QuoteSummaryActivity.this);
        ApiInterface apiService = ApiClient.getClient(QuoteSummaryActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.approveQuote(quoteVo);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(material_layout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}

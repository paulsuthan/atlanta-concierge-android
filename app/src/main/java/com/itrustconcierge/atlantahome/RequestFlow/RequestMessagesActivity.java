package com.itrustconcierge.atlantahome.RequestFlow;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IhcMessageTargetVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IhcMessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestMessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserVo;
import com.itrustconcierge.atlantahome.Adapters.RequestMessageAdapter;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class RequestMessagesActivity extends AppCompatActivity {

    LinearLayout loading_layout;
    ProgressBar progressBar;
    List<RequestMessageVo> messages = new ArrayList<>();
    RequestMessageAdapter mAdapter;
    LinearLayout chooseTargetLayout;
    EditText messageEdit;
    RecyclerView recyclerView;
    ImageButton chatSendButton, closeButton;
    CheckBox bothCheckBox, hoCheckBox, conciergeCheckBox;
    TextView hoName, conciergeName;
    ImageView singleConciergeImage, bothConciergeImage, singleHoImage, bothHoImage;
    View serviceProLine, bothLine;
    RelativeLayout serviceProLayout, bothLayout;
    UserVo spUser;
    TextView toolBarButton, myImageViewText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_messages);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Request");
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);

        Drawable img = toolBarButton.getContext().getResources().getDrawable(R.drawable.ic_info_outline_black_24dp);
        img.setBounds(0, 0, 60, 60);
        img.setTint(getResources().getColor(R.color.white));
        toolBarButton.setCompoundDrawables(null, img, null, null);
        toolBarButton.setCompoundDrawablePadding(10);
        toolBarButton.setText("info");

        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        if (homeOwnerRequestVoStatic.getDescription() != null)
            myImageViewText.setText(homeOwnerRequestVoStatic.getService().getName() + " - " + homeOwnerRequestVoStatic.getDescription());
        else
            myImageViewText.setText(homeOwnerRequestVoStatic.getService().getName());
        toolbar.setTitleTextColor(Color.WHITE);

        chooseTargetLayout = findViewById(R.id.chooseTargetLayout);
        messageEdit = findViewById(R.id.messageEdit);

        loading_layout = (LinearLayout) findViewById(R.id.loading_layout);
        loading_layout.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        recyclerView = (RecyclerView) findViewById(R.id.messagesContainer);
        chatSendButton = (ImageButton) findViewById(R.id.chatSendButton);
        closeButton = (ImageButton) findViewById(R.id.closeButton);
        messageEdit = (EditText) findViewById(R.id.messageEdit);
        bothCheckBox = findViewById(R.id.checkboth);
        conciergeCheckBox = findViewById(R.id.checkconcierge);
        hoCheckBox = findViewById(R.id.checkHo);
        conciergeName = findViewById(R.id.conciergeName);
        hoName = findViewById(R.id.hoName);
        singleConciergeImage = findViewById(R.id.myImageView1);
        bothConciergeImage = findViewById(R.id.myImageView);
        singleHoImage = findViewById(R.id.myImageView2);
        bothHoImage = findViewById(R.id.myImageView3);
        serviceProLine = findViewById(R.id.serviceProLine);
        serviceProLayout = findViewById(R.id.serviceProLayout);
        bothLine = findViewById(R.id.bothLine);
        bothLayout = findViewById(R.id.bothLayout);

        if (homeOwnerRequestVoStatic.getJobRequests() != null && !homeOwnerRequestVoStatic.getJobRequests().isEmpty())
            spUser = homeOwnerRequestVoStatic.getJobRequests().get(0).getUser();

        conciergeName.setText(authenticateVoLocal.getTenantSetting().getDisplayName());
        if (spUser != null) {
            hoName.setText(spUser.getFirstName() + " " + spUser.getLastName());
        } else {
            serviceProLine.setVisibility(View.GONE);
            serviceProLayout.setVisibility(View.GONE);
            bothLine.setVisibility(View.GONE);
            bothLayout.setVisibility(View.GONE);
        }


        bothCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkBoth();
            }
        });

        conciergeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkConcierge();
            }
        });

        hoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkHo();
            }
        });

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), WorkHistoryActivity.class);
                try {
                    i.putExtra("requestId", homeOwnerRequestVoStatic.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(i);
            }
        });

        singleConciergeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConcierge();
            }
        });

        bothConciergeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBoth();
            }
        });

        bothHoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBoth();
            }
        });

        singleHoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkHo();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(RequestMessagesActivity.this);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.scrollToPosition(messages.size() - 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        messageEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (!chooseTargetLayout.isShown())
                        chooseTargetLayout.setVisibility(View.VISIBLE);
                } else {
                    if (chooseTargetLayout.isShown())
                        chooseTargetLayout.setVisibility(View.GONE);
                }
            }
        });

        messageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!chooseTargetLayout.isShown())
                    chooseTargetLayout.setVisibility(View.VISIBLE);
            }
        });

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("requestId") != null) {
                if (checkInternetConenction(RequestMessagesActivity.this)) {
                    getRequestMessages(getIntent().getExtras().getString("requestId"));
                } else
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        }

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseTargetLayout.setVisibility(View.GONE);
                Utility.hideKeyboard(RequestMessagesActivity.this);
            }
        });

        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageEdit.getText().toString().trim().length() != 0) {
                    if (bothCheckBox.isChecked() || conciergeCheckBox.isChecked() || hoCheckBox.isChecked()) {
                        RequestMessageVo requestMessageVo = new RequestMessageVo();
                        HomeOwnerRequestVo request = new HomeOwnerRequestVo();
                        request.setId(homeOwnerRequestVoStatic.getId());
                        IhcMessageVo message = new IhcMessageVo();
                        List<IhcMessageTargetVo> targets = new ArrayList<>();
                        if (bothCheckBox.isChecked()) {
                            IhcMessageTargetVo ihcMessageTargetVo = new IhcMessageTargetVo();
                            UserVo target = new UserVo();
                            target.setId(authenticateVoLocal.getFranchise().getId());
                            ihcMessageTargetVo.setTarget(target);
                            targets.add(ihcMessageTargetVo);

                            if (spUser != null) {
                                IhcMessageTargetVo ihcMessageTargetVo1 = new IhcMessageTargetVo();
                                UserVo target1 = new UserVo();
                                target1.setId(spUser.getId());
                                ihcMessageTargetVo1.setTarget(target1);
                                targets.add(ihcMessageTargetVo1);
                            }
                        } else if (conciergeCheckBox.isChecked()) {
                            IhcMessageTargetVo ihcMessageTargetVo = new IhcMessageTargetVo();
                            UserVo target = new UserVo();
                            target.setId(authenticateVoLocal.getFranchise().getId());
                            ihcMessageTargetVo.setTarget(target);
                            targets.add(ihcMessageTargetVo);
                        } else if (hoCheckBox.isChecked()) {
                            if (spUser != null) {
                                IhcMessageTargetVo ihcMessageTargetVo1 = new IhcMessageTargetVo();
                                UserVo target1 = new UserVo();
                                target1.setId(spUser.getId());
                                ihcMessageTargetVo1.setTarget(target1);
                                targets.add(ihcMessageTargetVo1);
                            } else {
                                IhcMessageTargetVo ihcMessageTargetVo = new IhcMessageTargetVo();
                                UserVo target = new UserVo();
                                target.setId(authenticateVoLocal.getFranchise().getId());
                                ihcMessageTargetVo.setTarget(target);
                                targets.add(ihcMessageTargetVo);
                            }
                        }
                        message.setTargets(targets);
                        message.setMessage(messageEdit.getText().toString().trim());
                        requestMessageVo.setMessage(message);
                        requestMessageVo.setRequest(request);

                        sendMessage(requestMessageVo);
                    } else {
                        Toast.makeText(RequestMessagesActivity.this, "Please select whom you want to message.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendMessage(RequestMessageVo requestMessageVo) {
        final Dialog loadingDialog = Utility.loadingDialog(RequestMessagesActivity.this);
        ApiInterface apiService = ApiClient.getClient(RequestMessagesActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<RequestMessageVo>> call1 = apiService.saveRequestMessage(requestMessageVo);
        call1.enqueue(new Callback<ResponseWrapper<RequestMessageVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<RequestMessageVo>> call, Response<ResponseWrapper<RequestMessageVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                messageEdit.setText("");
                                RequestMessageVo requestMessageVo = response.body().getData();
                                chooseTargetLayout.setVisibility(View.GONE);
                                Utility.hideKeyboard(RequestMessagesActivity.this);
                                messages.add(requestMessageVo);
                                if (messages != null) {
                                    mAdapter = new RequestMessageAdapter(messages, RequestMessagesActivity.this);
                                    recyclerView.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(RequestMessagesActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RequestMessagesActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RequestMessagesActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<RequestMessageVo>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
                Toast.makeText(RequestMessagesActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void checkBoth() {
        bothCheckBox.setChecked(true);
        conciergeCheckBox.setChecked(false);
        hoCheckBox.setChecked(false);
    }

    private void checkConcierge() {
        bothCheckBox.setChecked(false);
        conciergeCheckBox.setChecked(true);
        hoCheckBox.setChecked(false);
    }

    private void checkHo() {
        bothCheckBox.setChecked(false);
        conciergeCheckBox.setChecked(false);
        hoCheckBox.setChecked(true);
    }

    private void getRequestMessages(String id) {
        ApiInterface apiService = ApiClient.getClient(RequestMessagesActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<RequestMessageVo>> call1 = apiService.getRequestMessages(id, authenticateVoLocal.getId());
        call1.enqueue(new Callback<ResponseWrapper<RequestMessageVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<RequestMessageVo>> call, Response<ResponseWrapper<RequestMessageVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                messages = response.body().getList();
                                Collections.reverse(messages);
                                mAdapter = new RequestMessageAdapter(messages, RequestMessagesActivity.this);
                                recyclerView.setAdapter(mAdapter);

                                RequestMessageVo message = messages.get(messages.size() - 1);

                                if (message.getMessage().getTargets().size() == 2) {
                                    checkBoth();
                                } else {
                                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getUserTenantId())) {
                                        checkConcierge();
                                    } else {
                                        checkHo();
                                    }
                                }

                                if (spUser == null)
                                    checkConcierge();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<RequestMessageVo>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
            }
        });
    }
}

package com.itrustconcierge.atlantahome.RequestFlow;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ServiceVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter;
import com.itrustconcierge.atlantahome.Adapters.SingleItemRowHolder;
import com.itrustconcierge.atlantahome.AddPropertyActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter.imageUrlMap;
import static com.itrustconcierge.atlantahome.RequestFlow.SelectServicesForRequestActivity.selectedServiceItem;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TEXT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TITTLE;

public class CreateRequestActivity extends AppCompatActivity {

    private RecyclerView imageRecyclerView;
    private SingleItemRowHolder.OnItemSelectedListener imageOnItemSelectedListener;
    private ImageListViewAdapter itemListDataAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    TextView toolBarButton;
    ImageView imageView;
    TextView serviceName;
    EditText description;
    public static HomeOwnerRequestVo homeOwnerRequestVoInput;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        requestImagespathList.clear();
        imageUrlMap.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_request);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Request");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolBarButton = findViewById(R.id.toolBarButton);
        imageRecyclerView = findViewById(R.id.image_view_list);
        imageView = findViewById(R.id.myImageView);
        serviceName = findViewById(R.id.myImageViewText);
        description = findViewById(R.id.description);
//        textViewTittle = (TextView) findViewById(R.id.textViewTittle);
//        textViewTittle.setText("Request");
        serviceName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);

        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + authenticateVoLocal.getFranchise().getMobileNumber()));
                if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CreateRequestActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                            1);
                    return;
                }
                startActivity(callIntent);
            }
        });

        try {
            serviceName.setText(selectedServiceItem.getName());
            Glide.with(imageView.getContext()).load("https://s3.amazonaws.com/itrustconcierge/assets/services/" + selectedServiceItem.getSurname()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageView.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(String item) {
                if (item.equals("o")) {
                    showAddPicturePopup();
                }
            }
        };

        requestImagespathList.add("o");
        itemListDataAdapter = new ImageListViewAdapter(CreateRequestActivity.this, requestImagespathList, imageOnItemSelectedListener, UploadType.REQUEST, true, authenticateVoLocal.getId());
        imageRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(CreateRequestActivity.this, LinearLayoutManager.HORIZONTAL, false);
        imageRecyclerView.setLayoutManager(mLinearLayoutManager);
        imageRecyclerView.setAdapter(itemListDataAdapter);

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestImagespathList.size() == 1) {
                    final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(CreateRequestActivity.this, R.style.dialogAnimation));
                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    requestDialog.setCancelable(false);
                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                    TextView tittleTextView = requestDialog.findViewById(R.id.tilltle);
                    TextView descriptionTextView = requestDialog.findViewById(R.id.description);
                    Button positiveButton = requestDialog.findViewById(R.id.positivebutton);
                    Button negativeButton = requestDialog.findViewById(R.id.negativebutton);

                    tittleTextView.setText("Add Picture");
                    descriptionTextView.setText("Pictures can help to understand your request better.");

                    negativeButton.setText("SKIP");
                    positiveButton.setText("ADD PICTURE");

                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            requestDialog.dismiss();
                            showAddPicturePopup();
                        }
                    });

                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestDialog.dismiss();
                            gotofinalActivity();
                        }
                    });

                    requestDialog.show();
                } else {
                    if (requestImagespathList.size() == imageUrlMap.size()+1) {
                        gotofinalActivity();
                    } else {
                        Toast.makeText(CreateRequestActivity.this, "Uploading photos... Please wait...", Toast.LENGTH_LONG).show();
                        waitForUpload();
                    }
                }
            }
        });
    }

    Dialog loadingDialog = null;
    private void waitForUpload() {
        if(loadingDialog == null)
            loadingDialog = Utility.loadingDialogWithMessage(CreateRequestActivity.this, "Uploading pictures... Please wait...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (requestImagespathList.size() != imageUrlMap.size()+1) {
                    waitForUpload();
                } else {
                    if(loadingDialog != null)
                        loadingDialog.dismiss();
                    gotofinalActivity();
                }
            }
        }, 3000);
    }

    private void gotofinalActivity() {
        if (selectedServiceItem != null) {
            if(selectedServiceItem.getId() != 0) {
                ServiceVo service = new ServiceVo();
                service.setId(selectedServiceItem.getId());
                service.setName(selectedServiceItem.getName());
                homeOwnerRequestVoInput.setService(service);
            }
        }
        homeOwnerRequestVoInput.setDescription(description.getText().toString().trim());
        String imageUrl = null;
        if (!imageUrlMap.isEmpty()) {
            StringBuilder result = new StringBuilder();
            for (String string : imageUrlMap.values()) {
                result.append(string);
                result.append(",");
            }
            imageUrl = result.length() > 0 ? result.substring(0, result.length() - 1) : "";
        }
//        homeOwnerRequestVoInput.setImage(imageUrl);
        Intent intent = new Intent(CreateRequestActivity.this, CreateRequestFinalActivity.class);
        startActivity(intent);
    }

    private void showAddPicturePopup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(CreateRequestActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = dialog1.findViewById(R.id.imageView3);
                tittleTextView = dialog1.findViewById(R.id.tittle);
                textView = dialog1.findViewById(R.id.textView);
                skipTextView = dialog1.findViewById(R.id.skip);
                okTextView = dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.photos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(CreateRequestActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CAMERA);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else if (checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(CreateRequestActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = dialog1.findViewById(R.id.imageView3);
                tittleTextView = dialog1.findViewById(R.id.tittle);
                textView = dialog1.findViewById(R.id.textView);
                skipTextView = dialog1.findViewById(R.id.skip);
                okTextView = dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(CreateRequestActivity.this, new String[]{android.Manifest.permission.CAMERA},
                                SELECT_FILE);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else {
                selectFile();
            }
        } else {
            selectFile();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            final Dialog dialog1 = new Dialog(CreateRequestActivity.this);
                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog1.setContentView(R.layout.allowlocation);
                            dialog1.setCancelable(false);
                            final Window window = dialog1.getWindow();
                            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            ImageView imageView;
                            TextView tittleTextView, textView, skipTextView;
                            Button okTextView;

                            imageView = dialog1.findViewById(R.id.imageView3);
                            tittleTextView = dialog1.findViewById(R.id.tittle);
                            textView = dialog1.findViewById(R.id.textView);
                            skipTextView = dialog1.findViewById(R.id.skip);
                            okTextView = dialog1.findViewById(R.id.ok);

                            tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                            textView.setText(PERMISSION_IMAGE_TEXT);
                            imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                            okTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                    ActivityCompat.requestPermissions(CreateRequestActivity.this, new String[]{android.Manifest.permission.CAMERA},
                                            SELECT_FILE);
                                }
                            });

                            skipTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                }
                            });

                            dialog1.show();

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant

                            return;
                        } else {
                            selectFile();
                        }
                    } else {
                        selectFile();
                    }
                } else {
                    // permission denied
                }
                return;
            }

            case 2: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    selectFile();
                } else {
                    // permission denied
                }
                return;
            }
            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    String userChoosenTask;
    int REQUEST_CAMERA = 1;
    int SELECT_FILE = 2;
    public static List<String> requestImagespathList = new ArrayList<>();

    private void selectFile() {
        final CharSequence[] items = {"Open Camera and Take Picture", "Choose from Library",
                "No Thanks"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateRequestActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_dialogue_custom_tittle, null);
        TextView textView = view.findViewById(R.id.tittle);
        textView.setText("Add Photos");
        builder.setCustomTitle(view);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera and Take Picture")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals("No Thanks")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent mIntent = new Intent(CreateRequestActivity.this, PickImageActivity.class);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 5);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
        startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            List<String> strings = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (strings != null) {
                requestImagespathList.addAll(strings);
            }
            if (requestImagespathList != null && !requestImagespathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < requestImagespathList.size(); i++) {
                    sb.append("Photo" + (i + 1) + ":" + requestImagespathList.get(i));
                    sb.append("\n");
                }
                Log.d("pathList", sb.toString());
            }
            itemListDataAdapter.notifyDataSetChanged();
            getUploadurl();
        } else if (requestCode == REQUEST_CAMERA) {
            onCaptureImageResult(data);
        }
    }

    private void getUploadurl() {
        if(requestImagespathList != null && !requestImagespathList.isEmpty()) {
            for(final String imagePath : requestImagespathList) {
                final File file = new File(imagePath);
                String fileName = file.getName();
                final String mimeType = "image/jpeg";
                if (!imageUrlMap.containsKey(imagePath)) {
                    ApiInterface apiService = ApiClient.getClient(CreateRequestActivity.this).create(ApiInterface.class);
                    Call<ResponseWrapper<UploadVo>> call = apiService.getUploadUrl(UploadType.REQUEST, fileName, mimeType);
                    call.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                        @Override
                        public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().getData() != null) {
                                        try {
                                            UploadVo uploadVo = response.body().getData();
                                            Log.e("uploadUrl", uploadVo.getUrl());
                                            new uploadImageTask().execute(uploadVo.getUrl(), uploadVo, file, mimeType, imagePath);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("Error", t.toString());
                        }
                    });
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    Calendar.getInstance().getTimeInMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            requestImagespathList.add(destination.getAbsolutePath());
            itemListDataAdapter.notifyDataSetChanged();
            getUploadurl();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class uploadImageTask extends AsyncTask {

        UploadVo uploadVo = new UploadVo();
        String mimeType;
        String imagePath;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Integer doInBackground(Object[] params) {
            int responseCode = 0;
            this.uploadVo = (UploadVo) params[1];
            this.mimeType = (String) params[3];
            this.imagePath = (String) params[4];
            try {
                String decodeUrl = java.net.URLDecoder.decode((String) params[0], "UTF-8");
                URL url = new URL(decodeUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("content-type", (String) params[3]);
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                FileInputStream fileInputStream = new FileInputStream((File) params[2]);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    out.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                out.flush();
                out.close();
                responseCode = connection.getResponseCode();
                System.out.println("Service returned response code " + responseCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onPostExecute(responseCode);
            return responseCode;
        }

        protected void onPostExecute(Integer result) {
            if (result.equals(200)) {
                imageUrlMap.put(imagePath, uploadVo.getPhysicalFilename());
            }
        }
    }
}

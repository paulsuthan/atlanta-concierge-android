package com.itrustconcierge.atlantahome.RequestFlow;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestFileVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter;
import com.itrustconcierge.atlantahome.Adapters.SingleItemRowHolder;
import com.itrustconcierge.atlantahome.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class RequestPhotosFragment extends Fragment {

    private List<String> requestImageList = new ArrayList<>();
    private List<String> beforeImageList = new ArrayList<>();
    private List<String> afterImageList = new ArrayList<>();
    static HomeOwnerRequestVo homeOwnerRequestVo = new HomeOwnerRequestVo();
    RecyclerView requestRecyclerView, beforePhotosRecyclerView, afterPhotosRecyclerView;

    public RequestPhotosFragment() {
        // Required empty public constructor
    }

    public static RequestPhotosFragment newInstance(HomeOwnerRequestVo homeOwnerRequestVo1) {
        RequestPhotosFragment fragment = new RequestPhotosFragment();
        homeOwnerRequestVo = homeOwnerRequestVo1;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_photos, container, false);

        requestRecyclerView = (RecyclerView) view.findViewById(R.id.request_image_view_list);
        beforePhotosRecyclerView = (RecyclerView) view.findViewById(R.id.before_work_image_view_list);
        afterPhotosRecyclerView = (RecyclerView) view.findViewById(R.id.after_work_image_view_list);

        try {
            requestImageList = new ArrayList<>();
            beforeImageList = new ArrayList<>();
            afterImageList = new ArrayList<>();
            if (homeOwnerRequestVo.getImage() != null) {
                if (homeOwnerRequestVo.getImage().contains(",")) {
                    requestImageList = Arrays.asList(homeOwnerRequestVo.getImage().split(","));
                } else {
                    requestImageList.add(homeOwnerRequestVo.getImage());
                }
            }

            for (RequestHistoryVo requestHistoryVo : homeOwnerRequestVo.getHistoryItems()) {
                if (requestHistoryVo.getType().equals(RequestHistoryType.STARTED)) {
                    if (requestHistoryVo.getImage() != null) {
                        if (requestHistoryVo.getImage().contains(",")) {
                            beforeImageList = Arrays.asList(requestHistoryVo.getImage().split(","));
                        } else {
                            beforeImageList.add(requestHistoryVo.getImage());
                        }
                    }
                }
            }
            for (RequestHistoryVo requestHistoryVo : homeOwnerRequestVo.getHistoryItems()) {
                if (requestHistoryVo.getType().equals(RequestHistoryType.COMPLETED)) {
                    if (requestHistoryVo.getImage() != null) {
                        if (requestHistoryVo.getImage().contains(",")) {
                            afterImageList = Arrays.asList(requestHistoryVo.getImage().split(","));
                        } else {
                            afterImageList.add(requestHistoryVo.getImage());
                        }
                    }
                }
            }

            SingleItemRowHolder.OnItemSelectedListener requestImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                @Override
                public void onItemSelected(String path) {

                }
            };

            ImageListViewAdapter requestItemListDataAdapter = new ImageListViewAdapter(getActivity(), requestImageList, requestImageOnItemSelectedListener, UploadType.REQUEST, false, authenticateVoLocal.getId());
            requestRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            requestRecyclerView.setLayoutManager(mLinearLayoutManager);
            requestRecyclerView.setAdapter(requestItemListDataAdapter);

            SingleItemRowHolder.OnItemSelectedListener beforeImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                @Override
                public void onItemSelected(String path) {

                }
            };

            ImageListViewAdapter beforeItemListDataAdapter = new ImageListViewAdapter(getActivity(), beforeImageList, beforeImageOnItemSelectedListener, UploadType.REQUEST, false, homeOwnerRequestVo.getId());
            beforePhotosRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            beforePhotosRecyclerView.setLayoutManager(mLinearLayoutManager1);
            beforePhotosRecyclerView.setAdapter(beforeItemListDataAdapter);

            SingleItemRowHolder.OnItemSelectedListener afterImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                @Override
                public void onItemSelected(String path) {

                }
            };

            ImageListViewAdapter afterItemListDataAdapter = new ImageListViewAdapter(getActivity(), afterImageList, afterImageOnItemSelectedListener, UploadType.REQUEST, false, homeOwnerRequestVo.getId());
            afterPhotosRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            afterPhotosRecyclerView.setLayoutManager(mLinearLayoutManager2);
            afterPhotosRecyclerView.setAdapter(afterItemListDataAdapter);

            if (checkInternetConenction(getContext())) {
                getRequestImages(homeOwnerRequestVo.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void getRequestImages(Long id) {
        ApiInterface apiService = ApiClient.getClient(getContext()).create(ApiInterface.class);
        Call<ResponseWrapper<RequestFileVo>> call1 = apiService.getRequestPhotos(id);
        call1.enqueue(new Callback<ResponseWrapper<RequestFileVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<RequestFileVo>> call, Response<ResponseWrapper<RequestFileVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                List<RequestFileVo> requestFileVos = response.body().getList();

                                for(RequestFileVo requestFileVo1 : requestFileVos) {
                                    String decodeUrl = java.net.URLDecoder.decode(requestFileVo1.getFile().getUrl(), "UTF-8");
                                    if (requestFileVo1.getType().equals("REQUEST")) {
                                        requestImageList.add(decodeUrl);
                                    } else if (requestFileVo1.getType().equals("STARTED")) {
                                        beforeImageList.add(decodeUrl);
                                    } else if (requestFileVo1.getType().equals("COMPLETED")) {
                                        afterImageList.add(decodeUrl);
                                    }
                                }

                                SingleItemRowHolder.OnItemSelectedListener requestImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(String path) {

                                    }
                                };

                                ImageListViewAdapter requestItemListDataAdapter = new ImageListViewAdapter(getActivity(), requestImageList, requestImageOnItemSelectedListener, UploadType.REQUEST, false, homeOwnerRequestVo.getHomeOwner().getId());
                                requestRecyclerView.setHasFixedSize(true);
                                LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                requestRecyclerView.setLayoutManager(mLinearLayoutManager);
                                requestRecyclerView.setAdapter(requestItemListDataAdapter);

                                SingleItemRowHolder.OnItemSelectedListener beforeImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(String path) {

                                    }
                                };

                                ImageListViewAdapter beforeItemListDataAdapter = new ImageListViewAdapter(getActivity(), beforeImageList, beforeImageOnItemSelectedListener, UploadType.REQUEST, false, homeOwnerRequestVo.getId());
                                beforePhotosRecyclerView.setHasFixedSize(true);
                                LinearLayoutManager mLinearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                beforePhotosRecyclerView.setLayoutManager(mLinearLayoutManager1);
                                beforePhotosRecyclerView.setAdapter(beforeItemListDataAdapter);

                                SingleItemRowHolder.OnItemSelectedListener afterImageOnItemSelectedListener = new SingleItemRowHolder.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(String path) {

                                    }
                                };

                                ImageListViewAdapter afterItemListDataAdapter = new ImageListViewAdapter(getActivity(), afterImageList, afterImageOnItemSelectedListener, UploadType.REQUEST, false, homeOwnerRequestVo.getId());
                                afterPhotosRecyclerView.setHasFixedSize(true);
                                LinearLayoutManager mLinearLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                afterPhotosRecyclerView.setLayoutManager(mLinearLayoutManager2);
                                afterPhotosRecyclerView.setAdapter(afterItemListDataAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<RequestFileVo>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
            }
        });
    }

}

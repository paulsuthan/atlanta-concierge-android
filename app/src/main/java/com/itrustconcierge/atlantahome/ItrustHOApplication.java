package com.itrustconcierge.atlantahome;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import io.branch.referral.Branch;


public class ItrustHOApplication extends Application {

    public static boolean isVisible = true;
    public static BroadcastReceiver noInternetHandlingReceiver;
    public static InternetConnectivityReceiverListener internetConnectivityReceiverListener;
    private static ItrustHOApplication mInstance;
    Fragment fragment;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    public static ItrustHOApplication getInstance() {
        return mInstance;
    }

    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) ItrustHOApplication.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        // Branch logging for debugging
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(this);

        sAnalytics = GoogleAnalytics.getInstance(this);
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

    public void registerReceiver() {
        IntentFilter noInternetIntentFilter = new IntentFilter();
        noInternetIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        noInternetHandlingReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager cm = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null
                        && activeNetwork.isConnectedOrConnecting();
//                Toast.makeText(context, activeNetwork.getReason(), Toast.LENGTH_LONG).show();
                if (internetConnectivityReceiverListener != null) {
                    internetConnectivityReceiverListener.onNetworkConnectionChanged(isConnected);
                }
            }
        };
        registerReceiver(noInternetHandlingReceiver, noInternetIntentFilter);
    }

    public void unRegisterReceiver() {
        unregisterReceiver(noInternetHandlingReceiver);
    }


    public void setConnectivityListener(InternetConnectivityReceiverListener listener) {
        ItrustHOApplication.internetConnectivityReceiverListener = listener;
    }

    public interface InternetConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}

package com.itrustconcierge.atlantahome;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.AuthenticateType;
import com.itrustconcierge.atlantahome.API_Manager.enums.DeviceType;
import com.itrustconcierge.atlantahome.API_Manager.enums.Role;
import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.DeviceInfoVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserVo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.WELCOME_TEXT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.isNotFirstTime;
import static com.itrustconcierge.atlantahome.Utilities.Utility.isValidEmail;
import static com.itrustconcierge.atlantahome.Utilities.Utility.setIsLoggedin;
import static com.itrustconcierge.atlantahome.Utilities.Utility.setNotFirstTime;

public class SignUpActivity extends AppCompatActivity {

    Bundle bundle;
    Button signupButton, cancelButton;
    TextView termsAndContitionsTextView;
    CoordinatorLayout coordinatorLayout;
    EditText fNameEditText, lNameEditText, emailEditText, mobileEditText, passswordEditText, confirmPasswordEditText, codeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

//        mButtonGravity = actionBar.getInteger(R.styleable.Toolbar_buttonGravity, Gravity.TOP);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bundle = getIntent().getExtras();

        signupButton = (Button) findViewById(R.id.signup);
        cancelButton = (Button) findViewById(R.id.cancel);
        termsAndContitionsTextView = (TextView) findViewById(R.id.terms);
        fNameEditText = (EditText) findViewById(R.id.f_name);
        lNameEditText = (EditText) findViewById(R.id.l_name);
        emailEditText = (EditText) findViewById(R.id.email);
        mobileEditText = (EditText) findViewById(R.id.mobile);
        mobileEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher("US"));
        passswordEditText = (EditText) findViewById(R.id.password);
        confirmPasswordEditText = (EditText) findViewById(R.id.confirm_password);
        codeEditText = (EditText) findViewById(R.id.code);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);

        SpannableString termsConditions = new SpannableString("By tapping 'JOIN' you agree to the Terms of Service and Stripe's Terms & Conditions.");

        ClickableSpan clickableSpanTerms = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.itrustconcierge.com/app-pages/home-owners-tos.html"));
                startActivity(intent);
            }
        };
        termsConditions.setSpan(clickableSpanTerms, 35, 51, 0);

        ClickableSpan clickableSpanStripe = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://stripe.com/us/legal"));
                startActivity(intent);
            }
        };
        termsConditions.setSpan(clickableSpanStripe, 56, 83, 0);

        termsAndContitionsTextView.setMovementMethod(LinkMovementMethod.getInstance());
        termsAndContitionsTextView.setText(termsConditions, TextView.BufferType.SPANNABLE);

        ItrustHOApplication application = (ItrustHOApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Signup Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if (bundle != null) {
            fNameEditText.setText(bundle.getString("firstName"));
            lNameEditText.setText(bundle.getString("lastName"));
            emailEditText.setText(bundle.getString("email"));
            mobileEditText.setText(bundle.getString("mobileNumber"));
            codeEditText.setText(bundle.getString("inviteCode"));
            passswordEditText.setFocusable(true);
            passswordEditText.requestFocus();

            if (!fNameEditText.getText().toString().isEmpty())
                fNameEditText.setEnabled(false);
            if (!lNameEditText.getText().toString().isEmpty())
                lNameEditText.setEnabled(false);
            if (!emailEditText.getText().toString().isEmpty())
                emailEditText.setEnabled(false);
            if (!mobileEditText.getText().toString().isEmpty())
                mobileEditText.setEnabled(false);
            if (!codeEditText.getText().toString().isEmpty())
                codeEditText.setEnabled(false);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(passswordEditText, InputMethodManager.SHOW_IMPLICIT);
//            showInfoPopup();
        }

        termsAndContitionsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.itrustconcierge.com/app-pages/home-owners-tos.html"));
                startActivity(intent);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEditText.setText(mobileEditText.getText().toString().replaceAll("[^0-9]", ""));
                if (fNameEditText.getText().toString().trim().length() >= 2) {
                    if (lNameEditText.getText().toString().trim().length() >= 2) {
                        if (isValidEmail(emailEditText.getText().toString().trim())) {
                            if (mobileEditText.getText().toString().trim().length() == 10) {
                                if (!passswordEditText.getText().toString().isEmpty() && !confirmPasswordEditText.getText().toString().isEmpty()) {
                                    if (passswordEditText.getText().toString().trim().equals(confirmPasswordEditText.getText().toString().trim())) {
                                        if (isPasswordValid(passswordEditText.getText().toString().trim())) {
                                            if (checkInternetConenction(SignUpActivity.this)) {
                                                checkUserExist(emailEditText.getText().toString(), mobileEditText.getText().toString());
                                            } else {
                                                Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Snackbar.make(coordinatorLayout, "Password is Too Short!", Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Snackbar.make(coordinatorLayout, "Passwords are not matching!", Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(coordinatorLayout, "Enter Password to Proceed!", Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                mobileEditText.setError("Enter valid Mobile number!");
                            }
                        } else {
                            emailEditText.setError("Enter valid Email!");
                        }
                    } else {
                        lNameEditText.setError("Enter valid last name!");
                    }
                } else

                {
                    fNameEditText.setError("Enter valid first name!");
                }
            }
        });
    }

    private void checkUserExist(String email, String mobile) {
        final Dialog loadingDialog = Utility.loadingDialog(SignUpActivity.this);
        ApiInterface apiService = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.checkUserExist(email, mobile);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                Map<String, Boolean> stringBooleanMap = response.body().getData();
                                Boolean isEmailExist = stringBooleanMap.get("email");
                                Boolean isMobileExist = stringBooleanMap.get("mobileNumber");
                                if (!isEmailExist && !isMobileExist) {
                                    if (codeEditText.getText().toString().isEmpty()) {
                                        if (checkInternetConenction(SignUpActivity.this)) {
                                            doActivate();
                                        } else {
                                            Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        if (checkInternetConenction(SignUpActivity.this)) {
                                            checkCode(codeEditText.getText().toString().trim());
                                        } else {
                                            Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                        }
                                    }
                                } else {
                                    final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SignUpActivity.this, R.style.dialogAnimation));
                                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    requestDialog.setCancelable(false);
                                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                                    TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                                    TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                                    tittleTextView.setText("Account already exists");
                                    descriptionTextView.setText("An account with this email or mobile already exists.");

                                    negativeButton.setVisibility(View.GONE);
                                    positiveButton.setText("Okay");

                                    positiveButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method stub
                                            requestDialog.dismiss();
                                        }
                                    });

                                    requestDialog.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });

    }

    private void checkCode(String trim) {
        final Dialog loadingDialog = Utility.loadingDialog(SignUpActivity.this);
        ApiInterface apiService = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Object>>> call = apiService.tacBasedOnInviteCode(trim);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Object>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Object>>> call, Response<ResponseWrapper<Map<String, Object>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                Map<String, Object> stringObjectMap = response.body().getData();
                                Map<String, Object> stringObjectMap1 = (Map<String, Object>) stringObjectMap.get("tac");
                                String s = (String) stringObjectMap1.get("body");
                                final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SignUpActivity.this, R.style.dialogAnimation));
                                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                requestDialog.setCancelable(false);
                                requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                                TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                                TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                                Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                                Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                                tittleTextView.setVisibility(View.GONE);
                                descriptionTextView.setText(Html.fromHtml(s));

                                negativeButton.setText("Disagree");
                                positiveButton.setText("Agree");

                                positiveButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub
                                        requestDialog.dismiss();
                                        if (checkInternetConenction(SignUpActivity.this)) {
                                            doActivate();
                                        } else {
                                            Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                        }
                                    }
                                });

                                negativeButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub
                                        requestDialog.dismiss();
                                    }
                                });

                                requestDialog.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(coordinatorLayout, "Invalid Invite Code!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Snackbar.make(coordinatorLayout, "Invalid Invite Code!", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Object>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 5;
    }

    private void doActivate() {
        try {
            final AuthenticateVo authenticateVoInput = new AuthenticateVo();
            List<Role> roles = new ArrayList<>();
            roles.add(Role.HO);
            authenticateVoInput.setRoles(roles);
            authenticateVoInput.setFirstName(fNameEditText.getText().toString());
            authenticateVoInput.setLastName(lNameEditText.getText().toString());
            authenticateVoInput.setEmail(emailEditText.getText().toString());
            authenticateVoInput.setMobileNumber(mobileEditText.getText().toString());
            authenticateVoInput.setToken(confirmPasswordEditText.getText().toString().trim());
            authenticateVoInput.setInviteCode(codeEditText.getText().toString().trim());
            authenticateVoInput.setType(AuthenticateType.TOKEN);
            authenticateVoInput.setTenantCode("atlanta-concierge");
            DeviceInfoVo deviceInfo = new DeviceInfoVo();
            deviceInfo.setDeviceType(DeviceType.ANDROID);
            WindowManager wm = (WindowManager) SignUpActivity.this.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            deviceInfo.setHeight(height);
            deviceInfo.setWidth(width);
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Utility.SHARED_PREF, 0);
            String regId = pref.getString("regId", null);
            Log.e("FCM", "Firebase reg id: " + regId);
            if (regId != null) {
                deviceInfo.setDeviceToken(regId);
            }
            authenticateVoInput.setDeviceInfo(deviceInfo);

            if (checkInternetConenction(SignUpActivity.this)) {
                final Dialog loadingDialog = Utility.loadingDialog(SignUpActivity.this);
                ApiInterface apiService = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
                Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.signUp(authenticateVoInput);
                call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            doLogin();
                        } else {
                            Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                        // Log error here since request failed
                        loadingDialog.dismiss();
                        Log.e("Error", t.toString());
                        Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                });
            } else {
                Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void doLogin() {
        Map<String, Object> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("userName", emailEditText.getText().toString());
        stringStringHashMap.put("token", confirmPasswordEditText.getText().toString());
        List<Role> roles = new ArrayList<>();
        roles.add(Role.HO);
        stringStringHashMap.put("roles", roles);
        DeviceInfoVo deviceInfo = new DeviceInfoVo();
        deviceInfo.setDeviceType(DeviceType.ANDROID);
        WindowManager wm = (WindowManager) SignUpActivity.this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        deviceInfo.setHeight(height);
        deviceInfo.setWidth(width);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Utility.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("FCM", "Firebase reg id: " + regId);
        if (regId != null) {
            deviceInfo.setDeviceToken(regId);
        }
        stringStringHashMap.put("deviceInfo", deviceInfo);
        if (checkInternetConenction(SignUpActivity.this)) {
            final Dialog loadingDialog = Utility.loadingDialog(SignUpActivity.this);
            ApiInterface apiService = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
            Call<AuthenticateVo> call = apiService.signIn(stringStringHashMap);
            call.enqueue(new Callback<AuthenticateVo>() {
                @Override
                public void onResponse(Call<AuthenticateVo> call, Response<AuthenticateVo> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                authenticateVoLocal = response.body();
                                setIsLoggedin(SignUpActivity.this, true, authenticateVoLocal.getEmail(), confirmPasswordEditText.getText().toString().trim(), "SP", authenticateVoLocal.getToken());
                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(SignUpActivity.this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AuthenticateVo> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            });
        } else {
            Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showWelcomePopUp() {
        if (!isNotFirstTime(SignUpActivity.this)) {
            setNotFirstTime(SignUpActivity.this, true);
            final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SignUpActivity.this, R.style.dialogAnimation));
            requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            requestDialog.setContentView(R.layout.custom_alrert_dialogue);
            requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            requestDialog.setCancelable(false);
            requestDialog.getWindow().setGravity(Gravity.BOTTOM);
            TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
            TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
            Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
            Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

            tittleTextView.setText("WELCOME");
            descriptionTextView.setText(WELCOME_TEXT);

            negativeButton.setVisibility(View.GONE);
            positiveButton.setText("GET STARTED");

            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    requestDialog.dismiss();
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(SignUpActivity.this);
                }
            });

            requestDialog.show();
        } else {
            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            ActivityCompat.finishAffinity(SignUpActivity.this);
        }
    }

    private void showInfoPopup() {
        final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SignUpActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(true);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        tittleTextView.setVisibility(View.GONE);
        descriptionTextView.setText("Your details are pre filled from GC's Code You have entered.");

        negativeButton.setVisibility(View.GONE);
        positiveButton.setText("OKAY");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
            }
        });

        requestDialog.show();
    }

    private void getInfoByCode() {
        if (bundle.getString("code") != null) {
            if (checkInternetConenction(SignUpActivity.this)) {
                final Dialog loadingDialog = Utility.loadingDialog(SignUpActivity.this);
                ApiInterface apiService = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
                Call<ResponseWrapper<UserVo>> call = apiService.getUserByCode(bundle.getString("code"));
                call.enqueue(new Callback<ResponseWrapper<UserVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UserVo>> call, Response<ResponseWrapper<UserVo>> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
//                                        userVoLocal = response.body().getData();
//                                        fNameEditText.setText(userVoLocal.getFirstName());
//                                        lNameEditText.setText(userVoLocal.getLastName());
//                                        emailEditText.setText(userVoLocal.getEmail());
//                                        mobileEditText.setText(userVoLocal.getMobileNumber());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UserVo>> call, Throwable t) {
                        // Log error here since request failed
                        loadingDialog.dismiss();
                        Log.e("Error", t.toString());
                        Snackbar.make(coordinatorLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                });
            } else {
                Snackbar.make(coordinatorLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}

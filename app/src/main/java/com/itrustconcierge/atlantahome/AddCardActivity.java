package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.CardDetail;
import com.itrustconcierge.atlantahome.Utilities.Utility;
import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.state_code;

public class AddCardActivity extends AppCompatActivity {

    EditText nameOnCardEditText, firstNameEditText, lastNameEditText,
            streetEditText, cityEditText, zipcodeEditText, emailEditText, mobileEditText;
    AutoCompleteTextView stateEditText;
    TextView myImageViewText, toolBarButton;
    String[] expiryDate;
    CardInputWidget card_input_widget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        nameOnCardEditText = (EditText) findViewById(R.id.nameOnCard);
        firstNameEditText = (EditText) findViewById(R.id.firstname);
        lastNameEditText = (EditText) findViewById(R.id.lastname);
        streetEditText = (EditText) findViewById(R.id.street);
        cityEditText = (EditText) findViewById(R.id.city);
        stateEditText = (AutoCompleteTextView) findViewById(R.id.state);
        zipcodeEditText = (EditText) findViewById(R.id.zipcode);
        emailEditText = (EditText) findViewById(R.id.email);
        mobileEditText = (EditText) findViewById(R.id.mobile);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        card_input_widget = (CardInputWidget) findViewById(R.id.card_input_widget);
        myImageViewText.setText("ADD CREDIT CARD");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        toolBarButton.setText("DONE");

        final List<String> states = new ArrayList<>();
        try {
            JSONObject jsonObj = new JSONObject(state_code);
            JSONArray jsonArray = jsonObj.getJSONArray("stateList");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonobject = jsonArray.getJSONObject(i);
                String name = jsonobject.getString("name");
                states.add(name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, states);
        stateEditText.setAdapter(adapter);
        stateEditText.setThreshold(1);

        try {
            emailEditText.setText(authenticateVoLocal.getEmail());
            mobileEditText.setText(authenticateVoLocal.getMobileNumber());
        } catch (Exception e) {
            e.printStackTrace();
        }

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Card cardToSave = card_input_widget.getCard();
                    if (cardToSave != null) {
                        if (!nameOnCardEditText.getText().toString().trim().isEmpty()) {
                            if (nameOnCardEditText.getText().toString().trim().length() >= 2) {
                                if (!emailEditText.getText().toString().trim().isEmpty()) {
                                    if (emailEditText.getText().toString().trim().length() >= 2) {
                                        if (!mobileEditText.getText().toString().trim().isEmpty()) {
                                            if (mobileEditText.getText().toString().trim().length() >= 2) {
                                                getCardDetails();
                                            } else {
                                                Snackbar.make(toolBarButton, "Please enter valid Mobile number", Snackbar.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Snackbar.make(toolBarButton, "Please enter valid Mobile number", Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Snackbar.make(toolBarButton, "Please enter valid Email", Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(toolBarButton, "Please enter valid Email", Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                Snackbar.make(toolBarButton, "Please enter valid name on card", Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(toolBarButton, "Please enter valid name on card", Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(toolBarButton, "Invalid Card Data", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                if (checkInternetConenction(AddCardActivity.this)) {
//
//                    CardDetail cardDetail = new CardDetail();
//                    cardDetail.setCardNumber(cardNumberEditText.getText().toString().replace(" ", "").trim());
//                    try {
//                        String[] strings = expiryDateEditText.getText().toString().trim().split("/");
//                        cardDetail.setExpiryMonth(strings[0]);
//                        cardDetail.setExpiryYear(strings[1]);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    cardDetail.setCvc(cvvEditText.getText().toString().trim());
//                    cardDetail.setNameOnCard(nameOnCardEditText.getText().toString().trim());
//                    cardDetail.setAddressLine1(streetEditText.getText().toString().trim());
//                    cardDetail.setAddressCity(cityEditText.getText().toString().trim());
//                    cardDetail.setAddressState(stateEditText.getText().toString().trim());
//                    cardDetail.setAddressZip(zipcodeEditText.getText().toString().trim());
//                    cardDetail.setEmail(emailEditText.getText().toString().trim());
//                    cardDetail.setMobileNumber(mobileEditText.getText().toString().trim());
//                    addCard(cardDetail);
//                } else {
//                    Snackbar.make(toolBarButton, NO_INTERNET, Snackbar.LENGTH_LONG).show();
//                }
            }
        });

    }

    private void getCardDetails() {
        CardDetail cardDetail = new CardDetail();
        cardDetail.setCardNumber(card_input_widget.getCard().getNumber());
        cardDetail.setExpiryMonth(String.valueOf(card_input_widget.getCard().getExpMonth()));
        cardDetail.setExpiryYear(String.valueOf(card_input_widget.getCard().getExpYear()));
        cardDetail.setCvc(card_input_widget.getCard().getCVC());
        cardDetail.setNameOnCard(nameOnCardEditText.getText().toString().trim());
        cardDetail.setAddressLine1(streetEditText.getText().toString().trim());
        cardDetail.setAddressCity(cityEditText.getText().toString().trim());
        cardDetail.setAddressState(stateEditText.getText().toString().trim());
        cardDetail.setAddressZip(zipcodeEditText.getText().toString().trim());
        cardDetail.setEmail(emailEditText.getText().toString().trim());
        cardDetail.setMobileNumber(mobileEditText.getText().toString().trim());
        addCard(cardDetail);
    }

    private void addCard(CardDetail cardDetail) {
        final Dialog loadingDialog = Utility.loadingDialog(AddCardActivity.this);
        ApiInterface apiService = ApiClient.getClient(AddCardActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.addCard(cardDetail);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                loadingDialog.dismiss();
                try {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(AddCardActivity.this, R.style.dialogAnimation));
                            requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                            requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                            requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            requestDialog.setCancelable(false);
                            requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                            TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                            TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                            Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                            Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                            tittleTextView.setText("Credit Card added Successfully.");
                            descriptionTextView.setText("Your Credit Card is added successfully.");
                            negativeButton.setVisibility(View.GONE);
                            positiveButton.setText("Okay");

                            positiveButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    requestDialog.dismiss();
                                    finish();
                                }
                            });

                            requestDialog.show();
                        } else if (response.body().getError() != null) {
                            Toast.makeText(AddCardActivity.this, response.body().getError().get("detailsMessage").toString(), Toast.LENGTH_LONG).show();
                        }
                    } else if (response.errorBody() != null) {
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());
                            JSONObject errorJson = json.getJSONObject("error");
                            String errorMessage = errorJson.getString("detailsMessage");
                            Toast.makeText(AddCardActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AddCardActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AddCardActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Toast.makeText(AddCardActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
            }
        });
    }
}

package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;

import static com.itrustconcierge.atlantahome.API_Manager.ApiClient.HOST;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class HomeDashWebActivity extends AppCompatActivity {

    WebView webView;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_dash_web);

        webView = (WebView) findViewById(R.id.webview);

        if (getIntent().getExtras().getString("homeDashUrl", "").trim().length() == 0) {
            if (HOST.contains("uat")) {
                url = "https://uat.itrustconcierge.com/ihc/" + "app-pages/home-dash.html#/dashboard/" + authenticateVoLocal.getToken() + "/" + "inventory/" + getIntent().getExtras().getString("propertyId") + "?time=";
            } else if (HOST.contains("www")) {
                url = "https://www.itrustconcierge.com/" + "app-pages/home-dash.html#/dashboard/" + authenticateVoLocal.getToken() + "/" + "inventory/" + getIntent().getExtras().getString("propertyId") + "?time=";
            }
        } else {
            url = getIntent().getExtras().getString("homeDashUrl");
        }

        if (checkInternetConenction(HomeDashWebActivity.this)) {
            webView.loadUrl(url);
            Log.d("profile_url", url);
        } else {
            Snackbar.make(webView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new loadingWebViewClient());
    }

    private class loadingWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

}

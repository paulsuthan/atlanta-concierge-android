package com.itrustconcierge.atlantahome.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.itrustconcierge.atlantahome.database.model.Json;

import java.util.ArrayList;
import java.util.List;

import static com.itrustconcierge.atlantahome.database.model.Json.TABLE_NAME;

/**
 * Created by ravi on 15/03/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "json_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(Json.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertJson(String json) {
        // get database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` will be inserted automatically.
        // no need to add them
        values.put(Json.COLUMN_JSON, json);

        // insert row
        long id = db.insert(TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Json getJson(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{Json.COLUMN_ID, Json.COLUMN_JSON},
                Json.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare json object
        Json json = new Json(
                cursor.getInt(cursor.getColumnIndex(Json.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Json.COLUMN_JSON)));

        // close the db connection
        cursor.close();

        return json;
    }

    public List<Json> getAllJson() {
        List<Json> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Json json = new Json();
                json.setId(cursor.getInt(cursor.getColumnIndex(Json.COLUMN_ID)));
                json.setJson(cursor.getString(cursor.getColumnIndex(Json.COLUMN_JSON)));

                notes.add(json);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public int getJsonsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public int updateJson(Json json) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Json.COLUMN_JSON, json.getJson());

        // updating row
        return db.update(TABLE_NAME, values, Json.COLUMN_ID + " = ?",
                new String[]{String.valueOf(json.getId())});

    }

    public void deleteJson(Json json) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, Json.COLUMN_ID + " = ?",
                new String[]{String.valueOf(json.getId())});
        db.close();
    }

    public void deleteAllJson() {
        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("delete from "+ TABLE_NAME);
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

}

package com.itrustconcierge.atlantahome.database.model;

/**
 * Created by ravi on 20/02/18.
 */

public class Json {
    public static final String TABLE_NAME = "json";

    public static final String COLUMN_ID = "id";
    //    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_JSON = "json";
    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_JSON + " TEXT"
                    + ")";
    private int id;
    //    private String name;
    private String json;

    public Json() {
    }

    public Json(int id, String json) {
        this.id = id;
        this.json = json;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}

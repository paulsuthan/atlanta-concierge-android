package com.itrustconcierge.atlantahome;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.Utilities.Utility;

public class HcResultActivity extends AppCompatActivity {

    TextView addressTextView, propertyTypeTextView, floorSizeTextView, lotSizeTextView;
    Button cancelButton, enterButton;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hc_result);

        addressTextView = (TextView) findViewById(R.id.address);
        propertyTypeTextView = (TextView) findViewById(R.id.propertyTypeText);
        floorSizeTextView = (TextView) findViewById(R.id.sqft);
        lotSizeTextView = (TextView) findViewById(R.id.lotsize);
        cancelButton = (Button) findViewById(R.id.cancel);
        enterButton = (Button) findViewById(R.id.enter);

        try {
            addressTextView.setText(getIntent().getExtras().getString("address", ""));
            propertyTypeTextView.setText(getIntent().getExtras().getString("propertyTypeValue", ""));
            floorSizeTextView.setText(Utility.formatter.format(getIntent().getExtras().getLong("floorSizeInSqFt")));
            lotSizeTextView.setText(Utility.formatter.format(getIntent().getExtras().getDouble("lotSizeInAcres")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPropertyActivity.autoAdd = false;
                finish();
            }
        });

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPropertyActivity.autoAdd = true;
                finish();
            }
        });
    }
}

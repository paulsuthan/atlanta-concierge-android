package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyPackageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ServiceDetailVo;
import com.itrustconcierge.atlantahome.Adapters.PackageExpandableAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectServicesActivity extends AppCompatActivity {

    PackageExpandableAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<PropertyPackageVo>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_services);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Services");
        toolbar.setTitleTextColor(Color.WHITE);
        @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        expListView.setHasTransientState(true);
        expListView.setDrawingCacheEnabled(true);
        expListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        prepareListData();
    }

    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<PropertyPackageVo>>();
        final List<ServiceDetailVo> serviceDetailVos = new ArrayList<>();

        listDataHeader.add("Enhanced Property Security");
        listDataHeader.add("Weekly Home Cleaning");
        listDataHeader.add("Shoreline Services");
        listDataHeader.add("Enhanced Property Security");
        listDataHeader.add("Weekly Home Cleaning");
        listDataHeader.add("Shoreline Services");
    }
}

package com.itrustconcierge.atlantahome.Firebase;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.itrustconcierge.atlantahome.SplashActivity;
import com.itrustconcierge.atlantahome.Utilities.NotificationUtils;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Paul on 9/11/2017.
 */

public class ITCFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = ITCFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), String.valueOf(remoteMessage.getSentTime()));
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                Map<String, String> params = remoteMessage.getData();
                JSONObject json = new JSONObject(params);
//                JSONObject json = new JSONObject(String.valueOf(remoteMessage.getData()));
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String tittle, String message, String timeStamp) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Utility.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            showNotificationMessage(getApplicationContext(), tittle, message, timeStamp, pushNotification);
            // play notification sound
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
//            JSONObject data = json.getJSONObject("data");

            String type = json.getString("type");
            String message = json.getString("message");
            String typeId = json.getString("typeId");
            String notifId = json.getString("notifId");
            String sourceId = json.getString("sourceId");
            String targetId = json.getString("targetId");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
            String timestamp = simpleDateFormat.format(new Date());
            Log.e(TAG, "type: " + type);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "typeId: " + typeId);
            Log.e(TAG, "timestamp: " + timestamp);

            Intent pushNotification = new Intent(Utility.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            pushNotification.putExtra("type", type);
            pushNotification.putExtra("typeId", typeId);
            pushNotification.putExtra("notifId", notifId);
            pushNotification.putExtra("sourceId", sourceId);
            pushNotification.putExtra("targetId", targetId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("type", type);
            resultIntent.putExtra("typeId", typeId);
            resultIntent.putExtra("notifId", notifId);
            resultIntent.putExtra("sourceId", sourceId);
            resultIntent.putExtra("targetId", targetId);

            if (type.equals("MESSAGE")) {
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                } else {
                    showNotificationMessage(getApplicationContext(), "iTrust Home", message, timestamp, resultIntent);
                }
            } else {
                showNotificationMessage(getApplicationContext(), "iTrust Home", message, timestamp, resultIntent);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
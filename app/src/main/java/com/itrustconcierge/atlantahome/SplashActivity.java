package com.itrustconcierge.atlantahome;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.DeviceType;
import com.itrustconcierge.atlantahome.API_Manager.enums.Role;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserType;
import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.DeviceInfoVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyOwnerVo;
import com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity;
import com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;
import com.itrustconcierge.atlantahome.database.DatabaseHelper;
import com.itrustconcierge.atlantahome.database.model.Json;
import com.smartlook.sdk.smartlook.Smartlook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ReviewPackagesActivity.propertyVoPackage;
import static com.itrustconcierge.atlantahome.Utilities.Utility.AUTHTOKEN;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_PASSWORD;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_PREF_NAME;
import static com.itrustconcierge.atlantahome.Utilities.Utility.LOGIN_USERNAME;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SERVER_MAINTENANCE;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SPLASH_TIME_OUT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getIsLoggedin;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences pref;
    private DatabaseHelper db;
    private boolean showMandatoryUpdatePopup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        Smartlook.init("c4d637c4d494d7f135c1a777ddbb1380088140d1");
        db = new DatabaseHelper(this);

        pref = getSharedPreferences(LOGIN_PREF_NAME, MODE_PRIVATE);

        if (getIsLoggedin(SplashActivity.this)) {
            if (checkInternetConenction(SplashActivity.this)) {
                doLogin();
            } else {
                try {
                    Json json = db.getJson(1);
                    Gson gson = new Gson();
                    StaticInfo.authenticateVoLocal = gson.fromJson(json.getJson(), AuthenticateVo.class);
                    StaticInfo.isAlreadyLoaded = false;
                    final Intent[] resultIntent = {new Intent(SplashActivity.this, MainActivity.class)};
                    try {
                        if (!getIntent().getExtras().getString("type", "").isEmpty()) {
                            String type = getIntent().getExtras().getString("type", "");
                            String typeId = getIntent().getExtras().getString("typeId", "");
                            if (type.equals("REQUEST")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                resultIntent[0].putExtra("isFromPush", true);
                            } else if (type.equals("LEAD")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                resultIntent[0].putExtra("isFromPush", true);
                            } else if (type.equals("MESSAGE")) {
                                Utility.gotoMessages = true;
                                resultIntent[0] = new Intent(SplashActivity.this, MainActivity.class);
                                resultIntent[0].putExtra("targetId", typeId);
                                resultIntent[0].putExtra("isFromPush", true);
                            } else if (type.equals("QUOTE")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", typeId);
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("INVOICE_APPROVED")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", typeId);
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("INVOICE")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", typeId);
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("VISIT_REQUESTED")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("JOB_SCHEDULE_REQUESTED")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("JOB_STARTED")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("JOB_COMPLETED")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("REQUEST_CLOSE")) {
                                Utility.gotoMyJobs = true;
                                resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                try {
                                    resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                    resultIntent[0].putExtra("isFromPush", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (type.equals("PACKAGE")) {
                                final Dialog loadingDialog = Utility.loadingDialog(SplashActivity.this);
                                ApiInterface apiService = ApiClient.getClient(SplashActivity.this).create(ApiInterface.class);
                                Call<ResponseWrapper<PropertyOwnerVo>> call1 = apiService.getPropertyById(Long.parseLong(typeId));
                                call1.enqueue(new Callback<ResponseWrapper<PropertyOwnerVo>>() {
                                    @Override
                                    public void onResponse(Call<ResponseWrapper<PropertyOwnerVo>> call, Response<ResponseWrapper<PropertyOwnerVo>> response) {
                                        loadingDialog.dismiss();
                                        if (response.isSuccessful()) {
                                            if (response.body() != null) {
                                                if (response.body().getData() != null) {
                                                    try {
                                                        PropertyOwnerVo propertyOwnerVo = response.body().getData();
                                                        propertyVoPackage = propertyOwnerVo.getProperty();
                                                        resultIntent[0] = new Intent(SplashActivity.this, ReviewPackagesActivity.class);
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<PropertyOwnerVo>> call, Throwable t) {
                                        // Log error here since request failed
                                        loadingDialog.dismiss();
                                        Log.e("Error", t.toString());
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivity(resultIntent[0]);
                    finish();
//                connectionLostDialogue();
                } catch (Exception e) {
                    e.printStackTrace();
                    doLogin();
                }
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void connectionLostDialogue() {
        final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SplashActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(false);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        tittleTextView.setText("Connection Lost!");
        descriptionTextView.setText(NO_INTERNET);

        negativeButton.setVisibility(View.GONE);
        positiveButton.setText("Try Again");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                if (checkInternetConenction(SplashActivity.this)) {
                    doLogin();
                } else {
                    connectionLostDialogue();
                }
            }
        });

        requestDialog.show();
    }

    private void ServerDownDialogue() {
        final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SplashActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(false);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        tittleTextView.setText("Server Under Maintenance!");
        descriptionTextView.setText(SERVER_MAINTENANCE);

        negativeButton.setVisibility(View.GONE);
        positiveButton.setText("Try Again");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                if (checkInternetConenction(SplashActivity.this)) {
                    doLogin();
                } else {
                    connectionLostDialogue();
                }
            }
        });

        requestDialog.show();
    }

    private void doLogin() {
//        ApiInterface apiService1 = ApiClient.getClient(SplashActivity.this).create(ApiInterface.class);
//        Call<ResponseWrapper<Map<String, String>>> call1 = apiService1.versionCheck(DeviceType.ANDROID, UserType.HO);
//        call1.enqueue(new Callback<ResponseWrapper<Map<String, String>>>() {
//            @Override
//            public void onResponse(Call<ResponseWrapper<Map<String, String>>> call1, Response<ResponseWrapper<Map<String, String>>> response) {
//                if (response.isSuccessful()) {
//                    if (response.body() != null) {
//                        if (response.body().getData() != null) {
//                            try {
//                                Map<String, String> map = response.body().getData();
//
//                                String[] parts = map.get("version").split(Pattern.quote("."));
//                                String part1 = parts[0];
//                                String part2 = parts[1];
//                                int minVersion_0 = Integer.parseInt(part1);
//                                int minVersion_1 = Integer.parseInt(part2);
//
//                                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                                String version = pInfo.versionName;
//                                String[] parts11 = version.split(Pattern.quote("."));
//                                String part111 = parts11[0];
//                                String part211 = parts11[1];
//                                int myVersion_0 = Integer.parseInt(part111);
//                                int myVersion_1 = Integer.parseInt(part211);
//                                if (minVersion_0 > myVersion_0) {
//                                    showMandatoryUpdatePopup = true;
//                                } else if (minVersion_1 > myVersion_1) {
//                                    showMandatoryUpdatePopup = true;
//                                }
//
//                                if (!showMandatoryUpdatePopup) {
                                    Map<String, Object> stringStringHashMap = new HashMap<>();
                                    stringStringHashMap.put("userName", pref.getString(LOGIN_USERNAME, ""));
                                    if (pref.getString(LOGIN_PASSWORD, "").isEmpty()) {
                                        stringStringHashMap.put("token", pref.getString(AUTHTOKEN, ""));
                                    } else {
                                        stringStringHashMap.put("token", pref.getString(LOGIN_PASSWORD, ""));
                                    }
                                    List<Role> roles = new ArrayList<>();
                                    roles.add(Role.HO);
                                    stringStringHashMap.put("roles", roles);
                                    DeviceInfoVo deviceInfo = new DeviceInfoVo();
                                    deviceInfo.setDeviceType(DeviceType.ANDROID);
                                    WindowManager wm = (WindowManager) SplashActivity.this.getSystemService(Context.WINDOW_SERVICE);
                                    Display display = wm.getDefaultDisplay();
                                    DisplayMetrics metrics = new DisplayMetrics();
                                    display.getMetrics(metrics);
                                    int width = metrics.widthPixels;
                                    int height = metrics.heightPixels;
                                    deviceInfo.setHeight(height);
                                    deviceInfo.setWidth(width);
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Utility.SHARED_PREF, 0);
                                    String regId = pref.getString("regId", null);
                                    Log.e("FCM", "Firebase reg id: " + regId);
                                    if (regId != null) {
                                        deviceInfo.setDeviceToken(regId);
                                    }
                                    stringStringHashMap.put("deviceInfo", deviceInfo);
                                    if (checkInternetConenction(SplashActivity.this)) {
                                        ApiInterface apiService = ApiClient.getClient(SplashActivity.this).create(ApiInterface.class);
                                        Call<AuthenticateVo> call = apiService.signIn(stringStringHashMap);
                                        call.enqueue(new Callback<AuthenticateVo>() {
                                            @Override
                                            public void onResponse(Call<AuthenticateVo> call, Response<AuthenticateVo> response) {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        try {
                                                            StaticInfo.authenticateVoLocal = response.body();

                                                            final Intent[] resultIntent = {new Intent(SplashActivity.this, MainActivity.class)};
                                                            try {
                                                                if (!getIntent().getExtras().getString("type", "").isEmpty()) {
                                                                    String type = getIntent().getExtras().getString("type", "");
                                                                    String typeId = getIntent().getExtras().getString("typeId", "");
                                                                    if (type.equals("REQUEST")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                        resultIntent[0].putExtra("isFromPush", true);
                                                                    } else if (type.equals("LEAD")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                        resultIntent[0].putExtra("isFromPush", true);
                                                                    } else if (type.equals("MESSAGE")) {
                                                                        Utility.gotoMessages = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, MainActivity.class);
                                                                        resultIntent[0].putExtra("targetId", typeId);
                                                                        resultIntent[0].putExtra("isFromPush", true);
                                                                    } else if (type.equals("QUOTE")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", typeId);
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("INVOICE_APPROVED")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", typeId);
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("INVOICE")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, QuoteSummaryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", typeId);
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("VISIT_REQUESTED")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("JOB_SCHEDULE_REQUESTED")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("JOB_STARTED")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("JOB_COMPLETED")) {
                                                                        Utility.gotoMyJobs = true;
                                                                        resultIntent[0] = new Intent(SplashActivity.this, WorkHistoryActivity.class);
                                                                        try {
                                                                            resultIntent[0].putExtra("requestId", Long.parseLong(typeId));
                                                                            resultIntent[0].putExtra("isFromPush", true);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else if (type.equals("PACKAGE")) {
                                                                        final Dialog loadingDialog = Utility.loadingDialog(SplashActivity.this);
                                                                        ApiInterface apiService = ApiClient.getClient(SplashActivity.this).create(ApiInterface.class);
                                                                        Call<ResponseWrapper<PropertyOwnerVo>> call1 = apiService.getPropertyById(Long.parseLong(typeId));
                                                                        call1.enqueue(new Callback<ResponseWrapper<PropertyOwnerVo>>() {
                                                                            @Override
                                                                            public void onResponse(Call<ResponseWrapper<PropertyOwnerVo>> call, Response<ResponseWrapper<PropertyOwnerVo>> response) {
                                                                                loadingDialog.dismiss();
                                                                                if (response.isSuccessful()) {
                                                                                    if (response.body() != null) {
                                                                                        if (response.body().getData() != null) {
                                                                                            try {
                                                                                                PropertyOwnerVo propertyOwnerVo = response.body().getData();
                                                                                                propertyVoPackage = propertyOwnerVo.getProperty();
                                                                                                resultIntent[0] = new Intent(SplashActivity.this, ReviewPackagesActivity.class);
                                                                                            } catch (Exception e) {
                                                                                                e.printStackTrace();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<ResponseWrapper<PropertyOwnerVo>> call, Throwable t) {
                                                                                // Log error here since request failed
                                                                                loadingDialog.dismiss();
                                                                                Log.e("Error", t.toString());
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                            startActivity(resultIntent[0]);
                                                            finish();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<AuthenticateVo> call, Throwable t) {
                                                // Log error here since request failed
                                                Log.e("Error", t.toString());
                                                ServerDownDialogue();
                                            }
                                        });
                                    } else {
                                        connectionLostDialogue();
                                    }
//                                } else {
//                                    showUpdatePopupDialogue();
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                } else {
//                    ServerDownDialogue();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseWrapper<Map<String, String>>> call1, Throwable t) {
//                // Log error here since request failed
//                Log.e("Error", t.toString());
//                ServerDownDialogue();
//            }
//        });
    }

    private void showUpdatePopupDialogue() {
        if (showMandatoryUpdatePopup) {
            new AlertDialog.Builder(SplashActivity.this)
                    .setTitle("Update Available!")
                    .setMessage("Please update the app to enjoy the newer features and enhancements.")
                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent in = new Intent(Intent.ACTION_VIEW);
                            in.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.itrustconcierge.atlantahome"));
                            startActivity(in);
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .show();
        } else {
            doLogin();
        }
    }
}

package com.itrustconcierge.atlantahome.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.R;

public class DonePaymentFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;


    public DonePaymentFragment() {
    }

    public static DonePaymentFragment newInstance(String s) {
        DonePaymentFragment fragment = new DonePaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, s);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_done_payment, container, false);
        if (getArguments() != null) {
            TextView priceTextView = (TextView) view.findViewById(R.id.price);
            priceTextView.setText(getArguments().getString(ARG_PARAM1));
        }

        return view;
    }
}

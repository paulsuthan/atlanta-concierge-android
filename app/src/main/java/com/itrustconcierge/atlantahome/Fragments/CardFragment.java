package com.itrustconcierge.atlantahome.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.CardDetail;
import com.itrustconcierge.atlantahome.AddCardActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.CardItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardAdapter;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardViewHolder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class CardFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static CardDetail cardDetail;
    RecyclerView recyclerView;
    List<CardDetail> cardDetails = new ArrayList<>();
    //    private CardAdapter cardAdapter;
    Button addCardButton;
    SelectableCardViewHolder.OnItemSelectedListener cardListener;
    SelectableCardAdapter selectableCardAdapter;
    List<CardItem> itemsCardDetails = new ArrayList<CardItem>();
    private String mParam1;
    private String mParam2;

    public CardFragment() {
        // Required empty public constructor
    }

    public static CardFragment newInstance(String param1, String param2) {
        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        addCardButton = (Button) view.findViewById(R.id.addCard);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        cardAdapter = new CardFragment.CardAdapter(cardDetails);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(getContext()).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddCardActivity.class);
                startActivity(intent);
            }
        });

        cardListener = new SelectableCardViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableCardItem item) {
                for (CardDetail cardDetailTemp : cardDetails) {
                    if (item.getId().equals(cardDetailTemp.getId()) && item.getCardNumber().equals(cardDetailTemp.getCardNumber())) {
                        cardDetail = cardDetailTemp;
                    }
                }
            }
        };

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
            getCards();
        } else {
            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }
    }

    private void getCards() {
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<CardDetail>> call = apiService.getCardDetails();
        call.enqueue(new Callback<ResponseWrapper<CardDetail>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<CardDetail>> call, Response<ResponseWrapper<CardDetail>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    cardDetails.clear();
                                    cardDetail = null;
//                                    cardDetails = new ArrayList<>();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    cardDetails = response.body().getList();
                                    itemsCardDetails = new ArrayList<>();
//                                    int i = 0;
//                                    for (CardDetail cardDetail : cardDetails) {
//                                        Item item = new Item(cardDetail.getCardType(), cardDetail.getCardNumber()+""+i);
////                                        item.setId(Long.parseLong(cardDetail.getId()));
//                                        item.setId(i);
//                                        i++;
//                                        itemsCardDetails.add(item);
//                                    }

//                                    int i = 0;
                                    int i = 0;
                                    for (CardDetail cardDetailTemp : cardDetails) {
                                        if (i == 0) {
                                            CardItem cardItem = new CardItem(cardDetailTemp.getId(), cardDetailTemp.getCardType(), cardDetailTemp.getCardNumber());
                                            SelectableCardItem selectableCardItem = new SelectableCardItem(cardItem, true);
                                            itemsCardDetails.add(selectableCardItem);
                                            cardDetail = cardDetailTemp;
                                        } else {
                                            itemsCardDetails.add(new CardItem(cardDetailTemp.getId(), cardDetailTemp.getCardType(), cardDetailTemp.getCardNumber()));
                                        }
                                        i++;
                                    }

                                    selectableCardAdapter = new SelectableCardAdapter(cardListener, itemsCardDetails, false);
                                    recyclerView.setAdapter(selectableCardAdapter);
//                                    selectableCardAdapter.notifyDataSetChanged();
//                                    cardDetail = new CardDetail();
//                                    cardDetail = cardDetails.get(0);
                                } else {

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<CardDetail>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

//    public List<Item> getCardItems(){
//        itemsCardDetails.add(new Item("Immediately", "Immediately"));
//        itemsCardDetails.add(new Item("I'm flexible", "I'm flexible"));
//        itemsCardDetails.add(new Item("By specific date", "By specific date"));
//        return itemsCardDetails;
//    }

//    public class CardAdapter extends RecyclerView.Adapter<CardFragment.CardAdapter.ViewHolder> {
//
//        List<CardDetail> cardDetails = new ArrayList<>();
//
//        public CardAdapter(List<CardDetail> cardDetails) {
//            this.cardDetails = cardDetails;
//        }
//
//        @Override
//        public CardFragment.CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View view = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.card_list_item, parent, false);
//            return new CardFragment.CardAdapter.ViewHolder(view);
//        }
//
//        @Override
//        public void onBindViewHolder(final CardFragment.CardAdapter.ViewHolder holder, int position) {
//            CardDetail cardDetail = cardDetails.get(position);
//
//            holder.card.setText(cardDetail.getCardType() + " Ending in " + cardDetail.getCardNumber());
//
//            if (position == 0) {
//                holder.card.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_circle_black_24dp, 0);
//            } else {
//                holder.card.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//            }
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return cardDetails.size();
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//            public View mView;
//            public Button card;
//
//            public ViewHolder(View view) {
//                super(view);
//                mView = view;
//                card = (Button) view.findViewById(R.id.card);
//            }
//
//            @Override
//            public String toString() {
//                return super.toString() + " '" + card.getText() + "'";
//            }
//        }
//    }
}

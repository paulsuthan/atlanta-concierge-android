package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.AddPropertyActivity;
import com.itrustconcierge.atlantahome.HomeDashWebActivity;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.MainActivity;
import com.itrustconcierge.atlantahome.PropertyActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestFlow.RequestServiceActivity;
import com.itrustconcierge.atlantahome.RequestFlow.SelectServicesForRequestActivity;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.ReviewPackagesActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.RequestFlow.CreateRequestActivity.homeOwnerRequestVoInput;
import static com.itrustconcierge.atlantahome.ReviewPackagesActivity.propertyVoPackage;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.isAlreadyLoaded;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class PropertyListFragment extends BottomSheetDialogFragment implements ItrustHOApplication.InternetConnectivityReceiverListener {

    List<PropertyVo> propertyVos;
    PropertyVo propertyVoTemp;
    LinearLayout emptyView, linearLayout1;
    SwipeRefreshLayout swipeRefreshLayout;
    ScrollView emptyViewScroll;
    RecyclerView recyclerView;
    Button addPropertyButton;
    PropertyAdapter propertyAdapter;
    RelativeLayout homeMaintenance, personalServices, relativeLayout;
    TextView textViewName, noInternetTextview, noInternetTextView;
    Snackbar noInternetSnackbar;
    PopupWindow noInternetPopupWindow;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;

    public static PropertyListFragment newInstance() {
        final PropertyListFragment fragment = new PropertyListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        propertyVos = new ArrayList<>();

        return inflater.inflate(R.layout.fragment_property_list, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        textViewName = (TextView) v.findViewById(R.id.textViewName);
        emptyView = (LinearLayout) v.findViewById(R.id.empty_view);
        homeMaintenance = (RelativeLayout) v.findViewById(R.id.homeMaintenance);
        personalServices = (RelativeLayout) v.findViewById(R.id.personalServices);
        emptyView.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        emptyViewScroll = (ScrollView) v.findViewById(R.id.emptyViewScroll);
        emptyViewScroll.setVisibility(View.GONE);
        addPropertyButton = (Button) v.findViewById(R.id.addProperty);
        linearLayout1 = (LinearLayout) v.findViewById(R.id.linearLayout1);
        noInternetTextView = (TextView) v.findViewById(R.id.noInternetTextView);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
        noInternetTextview = (TextView) v.findViewById(R.id.noInternetTextview);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (checkInternetConenction(getActivity())) {
                    getProperties();
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    initializeNoInternetDialogue();
                }
            }
        });

        addPropertyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    Intent intent = new Intent(getActivity(), AddPropertyActivity.class);
                    startActivity(intent);
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });

        homeMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    Intent intent = new Intent(getContext(), SelectServicesForRequestActivity.class);
                    intent.putExtra("home", true);
                    homeOwnerRequestVoInput = new HomeOwnerRequestVo();
                    propertyVoTemp = propertyVos.get(0);
                    homeOwnerRequestVoInput.setProperty(propertyVoTemp);
                    startActivity(intent);
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });

        personalServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    Intent intent = new Intent(getContext(), SelectServicesForRequestActivity.class);
                    intent.putExtra("home", false);
                    homeOwnerRequestVoInput = new HomeOwnerRequestVo();
                    propertyVoTemp = propertyVos.get(0);
                    homeOwnerRequestVoInput.setProperty(propertyVoTemp);
                    startActivity(intent);
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
            noInternetTextView.setVisibility(View.GONE);
            isAlreadyLoaded = true;
            getProperties();
        } else {
            recyclerView.setVisibility(View.GONE);
            noInternetTextView.setVisibility(View.VISIBLE);
        }
        ItrustHOApplication.getInstance().setConnectivityListener(PropertyListFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        noInternetTextView.setVisibility(View.GONE);
    }

    private void getProperties() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyVo>> call = apiService.getPropertiesByUser();
        call.enqueue(new Callback<ResponseWrapper<PropertyVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyVo>> call, Response<ResponseWrapper<PropertyVo>> response) {
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    getUnmappedProperties();
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    propertyVos.clear();
                                    MainActivity.navigation.restoreBottomNavigation();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                    emptyViewScroll.setVisibility(View.GONE);

                                    propertyVos = response.body().getList();
                                    propertyAdapter = new PropertyAdapter(propertyVos);
                                    recyclerView.setAdapter(propertyAdapter);
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    linearLayout1.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                    emptyViewScroll.setVisibility(View.VISIBLE);
                                    MainActivity.navigation.hideBottomNavigation();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    getUnmappedProperties();
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                                recyclerView.setVisibility(View.GONE);
                                linearLayout1.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                emptyViewScroll.setVisibility(View.VISIBLE);
                                MainActivity.navigation.hideBottomNavigation();
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyVo>> call, Throwable t) {
                // Log error here since request failed
                getUnmappedProperties();
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getUnmappedProperties() {
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyVo>> call1 = apiService.getUnmappedPropertiesByUser();
        call1.enqueue(new Callback<ResponseWrapper<PropertyVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyVo>> call, Response<ResponseWrapper<PropertyVo>> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                propertyVos.addAll(response.body().getList());
                                if (!propertyVos.isEmpty()) {
                                    MainActivity.navigation.restoreBottomNavigation();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                    emptyViewScroll.setVisibility(View.GONE);
                                    propertyAdapter = new PropertyAdapter(propertyVos);
                                    recyclerView.setAdapter(propertyAdapter);
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    linearLayout1.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                    emptyViewScroll.setVisibility(View.VISIBLE);
                                    MainActivity.navigation.hideBottomNavigation();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                                recyclerView.setVisibility(View.GONE);
                                linearLayout1.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                emptyViewScroll.setVisibility(View.VISIBLE);
                                MainActivity.navigation.hideBottomNavigation();
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyVo>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            isAlreadyLoaded = true;
            noInternetTextView.setVisibility(View.GONE);
            getProperties();
//            }

        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
        }
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(getContext(), R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnail;
        Button requestServiceButton, reviewPackage, homeDash;
        TextView managedBy, addressTextView, temp;
        ImageView callImageButton, chatImageButton;
        CardView mView;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_property_list__item, parent, false));
            mView = (CardView) itemView.findViewById(R.id.card_view);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            requestServiceButton = (Button) itemView.findViewById(R.id.requestService);
            reviewPackage = (Button) itemView.findViewById(R.id.reviewPackage);
            homeDash = (Button) itemView.findViewById(R.id.homeDash);
            managedBy = (TextView) itemView.findViewById(R.id.managedBy);
            addressTextView = (TextView) itemView.findViewById(R.id.title);
            callImageButton = (ImageView) itemView.findViewById(R.id.call);
            chatImageButton = (ImageView) itemView.findViewById(R.id.chat);
            temp = (TextView) itemView.findViewById(R.id.textViewLastItem);
        }

    }

    private class PropertyAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final int mItemCount;
        private final List<PropertyVo> propertyVos;

        PropertyAdapter(List<PropertyVo> propertyVos) {
            mItemCount = propertyVos.size();
            this.propertyVos = propertyVos;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                final PropertyVo propertyVo = propertyVos.get(position);

                if (propertyVo.getHomeDash() == null)
                    propertyVo.setHomeDash(false);

                if (propertyVo.getHomeDash()) {
                    holder.homeDash.setVisibility(View.VISIBLE);
                } else {
                    holder.homeDash.setVisibility(View.GONE);
                }

                if (propertyVo.getReviewPackageAvailable() == null)
                    propertyVo.setReviewPackageAvailable(false);

                if (propertyVo.getReviewPackageAvailable()) {
                    holder.reviewPackage.setVisibility(View.VISIBLE);
                } else {
                    holder.reviewPackage.setVisibility(View.GONE);
                }

                holder.temp.setVisibility(View.GONE);
                if (propertyVo.getAddress() != null) {
                    if (propertyVo.getAddress().getDisplayAddress() != null) {
                        holder.addressTextView.setText(propertyVo.getAddress().getDisplayAddress());
                    } else {
                        holder.addressTextView.setText(propertyVo.getAddress().getLine1());
                    }
                }
                if (propertyVo.getImage() != null) {
                    if (!propertyVo.getImage().contains(",")) {
                        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
                        Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROPERTY, propertyVo.getImage(), "image/jpg", authenticateVoLocal.getId());
                        call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                UploadVo uploadVo1 = response.body().getData();
                                                String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                Log.d("DownloadUrl", decodeUrl);
                                                Glide.with(getActivity()).load(decodeUrl).placeholder(R.drawable.property_default).into(holder.thumbnail);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                holder.thumbnail.setImageResource(R.drawable.property_default);
                                            }
                                        }
                                    }
                                } else {

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("Error", t.toString());
                                holder.thumbnail.setImageResource(R.drawable.property_default);
                            }
                        });
                    } else {
                        List<String> images = Arrays.asList(propertyVo.getImage().split(","));
                        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
                        Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROPERTY, images.get(0), "image/jpg", authenticateVoLocal.getId());
                        call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                UploadVo uploadVo1 = response.body().getData();
                                                String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                Log.d("DownloadUrl", decodeUrl);
                                                Glide.with(getActivity()).load(decodeUrl).placeholder(R.drawable.property_default).into(holder.thumbnail);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                holder.thumbnail.setImageResource(R.drawable.property_default);
                                            }
                                        }
                                    }
                                } else {

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("Error", t.toString());
                                holder.thumbnail.setImageResource(R.drawable.property_default);
                            }
                        });
                    }
                } else {
                    holder.thumbnail.setImageResource(R.drawable.property_default);
                }

                holder.managedBy.setText("Managed by " + authenticateVoLocal.getTenantSetting().getDisplayName());

                holder.requestServiceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            homeOwnerRequestVoInput = new HomeOwnerRequestVo();
                            homeOwnerRequestVoInput.setProperty(propertyVo);
                            startActivity(new Intent(v.getContext(), RequestServiceActivity.class));
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.reviewPackage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            propertyVoPackage = propertyVo;
                            startActivity(new Intent(v.getContext(), ReviewPackagesActivity.class));
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            v.getContext().startActivity(new Intent(v.getContext(), PropertyActivity.class).putExtra("propertyId", propertyVo.getId()));
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            v.getContext().startActivity(new Intent(v.getContext(), PropertyActivity.class).putExtra("propertyId", propertyVo.getId()));
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.chatImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            Intent i = new Intent(v.getContext(), RequestHistory.class);
                            try {
                                i.putExtra("targetId", authenticateVoLocal.getFranchise().getId());
                                i.putExtra("HO_Name", authenticateVoLocal.getTenantSetting().getDisplayName());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            startActivity(i);
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.callImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + authenticateVoLocal.getFranchise().getMobileNumber()));
                            if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE},
                                        1);
                                return;
                            }
                            getActivity().startActivity(callIntent);
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                holder.homeDash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            Intent i = new Intent(v.getContext(), HomeDashWebActivity.class);
                            try {
                                i.putExtra("propertyId", propertyVo.getId().toString());
                                i.putExtra("homeDashUrl", "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            startActivity(i);
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mItemCount;
        }

    }
}

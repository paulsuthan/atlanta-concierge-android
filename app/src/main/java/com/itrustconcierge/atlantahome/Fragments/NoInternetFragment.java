package com.itrustconcierge.atlantahome.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.R;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.MainActivity.navigation;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.isAlreadyLoaded;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class NoInternetFragment extends Fragment implements ItrustHOApplication.InternetConnectivityReceiverListener {

    Button buttonTapToRetry;
    LinearLayout linearLayout;

    public NoInternetFragment() {
    }

    public static MessagesFragment newInstance() {
        MessagesFragment fragment = new MessagesFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_no_internet, container,
                false);

        buttonTapToRetry = (Button) v.findViewById(R.id.buttonTapToRetry);
        linearLayout = (LinearLayout) v.findViewById(R.id.linearLayout);

        buttonTapToRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    navigation.setCurrentItem(navigation.getCurrentItem());
                } else {
                    Snackbar.make(linearLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkInternetConenction(getActivity())) {
            isAlreadyLoaded = true;
            navigation.setCurrentItem(navigation.getCurrentItem());
        } else {
        }
        ItrustHOApplication.getInstance().setConnectivityListener(NoInternetFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            isAlreadyLoaded = true;
            navigation.setCurrentItem(navigation.getCurrentItem());
        } else {
        }
    }
}

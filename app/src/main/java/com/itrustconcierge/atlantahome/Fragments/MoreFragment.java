package com.itrustconcierge.atlantahome.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserVo;
import com.itrustconcierge.atlantahome.Adapters.MoreItemsAdapter;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TEXT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TITTLE;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class MoreFragment extends Fragment implements ItrustHOApplication.InternetConnectivityReceiverListener {

    public static final int MEDIA_TYPE_IMAGE = 10;
    RecyclerView recyclerView;
    Snackbar noInternetSnackbar;
    TextView nameTextView, noInternetTextView;
    LinearLayout linearLayout;
    PopupWindow noInternetPopupWindow;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;
    String userChoosenTask;
    int REQUEST_CAMERA = 1;
    int SELECT_FILE = 2;
    UploadVo uploadVo;
    private File filetoUpload;
    private String mimeType = "image/jpeg";
    private String fileName;
    private ImageView avatar;
    private Uri fileUri;

    public MoreFragment() {

    }

    public static MoreFragment newInstance() {
        MoreFragment fragment = new MoreFragment();
        return fragment;
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "ITC");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ITC", "Oops! Failed create "
                        + "ITC" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        TextView addressTextView = (TextView) view.findViewById(R.id.addressTextView);
        avatar = (ImageView) view.findViewById(R.id.view);
        TextView emailTextView = (TextView) view.findViewById(R.id.emailTextView);
        TextView phoneTextView = (TextView) view.findViewById(R.id.phoneTextView);
        noInternetTextView = (TextView) view.findViewById(R.id.noInternetTextView);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);

        try {
            if (authenticateVoLocal != null) {
                nameTextView.setText(authenticateVoLocal.getFirstName() + " " + authenticateVoLocal.getLastName());
                if (authenticateVoLocal.getAddress() != null) {
                    addressTextView.setText(authenticateVoLocal.getAddress().getDisplayAddress());
                }
                if (authenticateVoLocal.getEmail() != null) {
                    emailTextView.setText(authenticateVoLocal.getEmail());
                }
                if (authenticateVoLocal.getMobileNumber() != null) {
                    phoneTextView.setText(authenticateVoLocal.getMobileNumber());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (authenticateVoLocal.getProfileImage() != null) {
            ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
            Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROFILE_PIC, authenticateVoLocal.getProfileImage(), mimeType, authenticateVoLocal.getId());
            call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    UploadVo uploadVo1 = response.body().getData();
                                    String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                    Log.d("DownloadUrl", decodeUrl);
                                    Glide.with(getActivity()).load(decodeUrl).asBitmap().centerCrop().placeholder(R.mipmap.ic_avatar).into(new BitmapImageViewTarget(avatar) {
                                        @Override
                                        protected void setResource(Bitmap resource) {
                                            RoundedBitmapDrawable circularBitmapDrawable =
                                                    RoundedBitmapDrawableFactory.create(avatar.getContext().getResources(), resource);
                                            circularBitmapDrawable.setCircular(true);
                                            avatar.setImageDrawable(circularBitmapDrawable);
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                }
            });
        }

        List<String> mValues = new ArrayList<>();
        mValues.add("Payment Details");
        mValues.add("Calendar");
        mValues.add("Tell Friends & Family");
        mValues.add("space");
        mValues.add("LOGOUT");

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    showAddPicturePopup();
                } else {
                    initializeNoInternetDialogue();
//                    showNoInternetDialog();
                }
            }
        });


        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new MoreItemsAdapter(mValues, getActivity()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(getContext()).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

    }

    private void showAddPicturePopup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(getActivity());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.photos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            dialog1.dismiss();
                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                    1);
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            dialog1.dismiss();
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else if (getActivity().checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(getActivity());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            dialog1.dismiss();
                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA},
                                    2);
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkInternetConenction(getActivity())) {
                            dialog1.dismiss();
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else {
                selectFile();
            }
        } else {
            selectFile();
        }
    }

    private void selectFile() {
        final CharSequence[] items = {"Open Camera and Take Picture", "Choose from Library",
                "No Thanks"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_dialogue_custom_tittle, null);
        TextView textView = (TextView) view.findViewById(R.id.tittle);
        textView.setText("Select a Profile Picture");
        builder.setCustomTitle(view);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (checkInternetConenction(getActivity())) {
                    if (items[item].equals("Open Camera and Take Picture")) {
                        userChoosenTask = "Take Photo";
                        cameraIntent();

                    } else if (items[item].equals("Choose from Library")) {
                        userChoosenTask = "Choose from Library";
                        galleryIntent();

                    } else if (items[item].equals("No Thanks")) {
                        dialog.dismiss();
                    }
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri returnUri = data.getData();
        mimeType = getActivity().getContentResolver().getType(returnUri);
        Cursor returnCursor =
                getActivity().getContentResolver().query(returnUri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        fileName = returnCursor.getString(nameIndex);

        filetoUpload = destination;
        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(getActivity(), R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_image_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(false);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);
        final ImageView image = (ImageView) requestDialog.findViewById(R.id.image);

        image.post(new Runnable() {
            @Override
            public void run() {
                Bitmap myBitmap = BitmapFactory.decodeFile(filetoUpload.getPath());
                int nh = (int) (myBitmap.getHeight() * (512.0 / myBitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(myBitmap, 512, nh, true);
                image.setImageBitmap(scaled);
            }
        });
        negativeButton.setText("Cancel");
        positiveButton.setText("Save");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    // TODO Auto-generated method stub
                    requestDialog.dismiss();
                    uploadPicture(filetoUpload, fileName, mimeType);
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(getActivity())) {
                    requestDialog.dismiss();
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });

        requestDialog.show();
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                "before_work" + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mimeType = "image/jpeg";
        fileName = destination.getName();
        filetoUpload = destination;

        filetoUpload = destination;
        uploadPicture(filetoUpload, fileName, mimeType);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private void uploadPicture(File filetoUpload, String fileName, String mimeType) {
        if (filetoUpload != null) {
            final Dialog loadingDialog = Utility.loadingDialog(getActivity());
            ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
            Call<ResponseWrapper<UploadVo>> call = apiService.getUploadUrl(UploadType.PROFILE_PIC, fileName, mimeType, authenticateVoLocal.getId());
            call.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    uploadVo = response.body().getData();
                                    new uploadImageTask().execute(uploadVo.getUrl());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            noInternetTextView.setVisibility(View.GONE);
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
        }
    }

    private void updateProfilePic(final String physicalFileName) {
        try {
            UserVo userVo = new UserVo();
            userVo.setProfileImage(physicalFileName);
            userVo.setId(authenticateVoLocal.getId());
            final Dialog loadingDialog = Utility.loadingDialog(getActivity());
            ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
            Call<ResponseWrapper<UserVo>> call = apiService.updateProfilePic(userVo);
            call.enqueue(new Callback<ResponseWrapper<UserVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<UserVo>> call, Response<ResponseWrapper<UserVo>> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    authenticateVoLocal.setProfileImage(physicalFileName);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<UserVo>> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkInternetConenction(getActivity())) {
            noInternetTextView.setVisibility(View.GONE);
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
        }
        ItrustHOApplication.getInstance().setConnectivityListener(MoreFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        noInternetTextView.setVisibility(View.GONE);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(getContext(), R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

    private class uploadImageTask extends AsyncTask {
        Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            loadingDialog = Utility.loadingDialog(getActivity());
        }

        @Override
        protected Integer doInBackground(Object[] params) {
            int responseCode = 0;
            try {
                String decodeUrl = java.net.URLDecoder.decode((String) params[0], "UTF-8");
                URL url = new URL(decodeUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("content-type", mimeType);
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                FileInputStream fileInputStream = new FileInputStream(filetoUpload);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    out.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                out.flush();
                out.close();
                responseCode = connection.getResponseCode();
                System.out.println("Service returned response code " + responseCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onPostExecute(responseCode);
            return responseCode;
        }

        protected void onPostExecute(Integer result) {
            if (result.equals(200)) {
                ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
                Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROFILE_PIC, uploadVo.getPhysicalFilename(), mimeType, authenticateVoLocal.getId());
                call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                        if (loadingDialog != null)
                            loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
                                        UploadVo uploadVo1 = response.body().getData();
                                        String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                        Log.d("DownloadUrl", decodeUrl);
                                        Glide.with(getActivity()).load(decodeUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(avatar) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                RoundedBitmapDrawable circularBitmapDrawable =
                                                        RoundedBitmapDrawableFactory.create(avatar.getContext().getResources(), resource);
                                                circularBitmapDrawable.setCircular(true);
                                                avatar.setImageDrawable(circularBitmapDrawable);
                                            }
                                        });
                                        updateProfilePic(uploadVo1.getPhysicalFilename());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                });
            } else {
                if (loadingDialog != null)
                    loadingDialog.dismiss();
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}

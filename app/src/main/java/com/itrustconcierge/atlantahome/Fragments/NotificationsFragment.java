package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.NotificationType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.NotificationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyOwnerVo;
import com.itrustconcierge.atlantahome.HomeDashWebActivity;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity;
import com.itrustconcierge.atlantahome.RequestFlow.RequestMessagesActivity;
import com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.ReviewPackagesActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.MainActivity.readNotifications;
import static com.itrustconcierge.atlantahome.ReviewPackagesActivity.propertyVoPackage;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class NotificationsFragment extends BottomSheetDialogFragment implements ItrustHOApplication.InternetConnectivityReceiverListener {

    public static NotificationVo notificationVoStatic;
    List<NotificationVo> notificationVos = new ArrayList<>();
    //    List<ItemType> consolidatedList = new ArrayList<>();
    RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;
    Snackbar noInternetSnackbar;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;
    RelativeLayout relativeLayout;
    TextView noInternetTextView;
    private TextView emptyView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static NotificationsFragment newInstance() {
        final NotificationsFragment fragment = new NotificationsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification_list, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        emptyView = (TextView) v.findViewById(R.id.empty_view);
        emptyView.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
        noInternetTextView = (TextView) v.findViewById(R.id.noInternetTextView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (checkInternetConenction(getActivity())) {
                    getNotifications();
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    initializeNoInternetDialogue();
                }
            }
        });

        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
            noInternetTextView.setVisibility(View.GONE);
            getNotifications();
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setText("Latest notifications from homeowners and Atlanta Concierge will appear here.");
            emptyView.setVisibility(View.VISIBLE);
        }
        ItrustHOApplication.getInstance().setConnectivityListener(NotificationsFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        noInternetTextView.setVisibility(View.GONE);
        super.onPause();
    }

    private void getNotifications() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<NotificationVo>> call = apiService.getNotifications();
        call.enqueue(new Callback<ResponseWrapper<NotificationVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<NotificationVo>> call, Response<ResponseWrapper<NotificationVo>> response) {
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
//                                consolidatedList.clear();
                                notificationVos.clear();
                                notificationVos = new ArrayList<>();

                                for (NotificationVo notificationVo : response.body().getList()) {
                                    if (!notificationVo.getType().equals(NotificationType.MESSAGE)) {
                                        notificationVos.add(notificationVo);
                                    }
                                }

//                                HashMap<String, List<NotificationVo>> groupedHashMap = groupDataIntoHashMap(notificationVos);
//
//                                for (String tittle : groupedHashMap.keySet()) {
//                                    TittleItem tittleItem = new TittleItem();
//                                    tittleItem.setTittle(tittle);
//                                    consolidatedList.add(tittleItem);
//                                    for (NotificationVo notificationVo : groupedHashMap.get(tittle)) {
//                                        BodyItem<NotificationVo> notificationVoBodyItem = new BodyItem<NotificationVo>();
//                                        notificationVoBodyItem.setBody(notificationVo);
//                                        consolidatedList.add(notificationVoBodyItem);
//                                    }
//                                }
                                if (!notificationVos.isEmpty()) {
                                    emptyView.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    notificationAdapter = new NotificationAdapter(notificationVos);
                                    recyclerView.setAdapter(notificationAdapter);
                                    notificationAdapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyView.setText("Latest notifications from homeowners and Atlanta Concierge will appear here.");
                                    emptyView.setVisibility(View.VISIBLE);
                                }

                                if (unReadNotificationVos != null) {
                                    if (!unReadNotificationVos.isEmpty()) {
                                        if (checkInternetConenction(getActivity())) {
                                            readNotifications(getActivity(), unReadNotificationVos);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setText("All caught up!");
                                emptyView.setVisibility(View.VISIBLE);
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<NotificationVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private HashMap<String, List<NotificationVo>> groupDataIntoHashMap(List<NotificationVo> notificationVos) {
        HashMap<String, List<NotificationVo>> groupedHashMap = new HashMap<>();
        for (NotificationVo notificationVo : notificationVos) {
            String hashMapKey = notificationVo.getType().name();
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey).add(notificationVo);
            } else {
                List<NotificationVo> list = new ArrayList<>();
                list.add(notificationVo);
                groupedHashMap.put(hashMapKey, list);
            }
        }
        return groupedHashMap;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            noInternetTextView.setVisibility(View.GONE);
            getNotifications();
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
        }
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(getContext(), R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

    private class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int mItemCount;
        //        private List<ItemType> itemTypes;
        RecyclerView.ViewHolder viewHolder = null;
        List<NotificationVo> notificationVos;

        NotificationAdapter(List<NotificationVo> notificationVos) {
            mItemCount = notificationVos.size();
            this.notificationVos = notificationVos;
//            itemTypes = itemTypes;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v2 = inflater.inflate(R.layout.fragment_notification_list_item, parent, false);
            viewHolder = new BodyViewHolder(v2);
//            switch (viewType) {
//                case ItemType.TYPE_TITTLE:
//                    View v1 = inflater.inflate(R.layout.list_view_tittle, parent, false);
//                    viewHolder = new TittleViewHolder(v1);
//                    break;
//                case ItemType.TYPE_BODY:
//                    View v2 = inflater.inflate(R.layout.fragment_notification_list_item, parent, false);
//                    viewHolder = new BodyViewHolder(v2);
//                    break;
//            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            try {
                final NotificationVo notificationVoBodyItem = notificationVos.get(position);
                final BodyViewHolder bodyViewHolder = (BodyViewHolder) viewHolder;
                bodyViewHolder.txtTitle.setText(notificationVoBodyItem.getSource().getFirstName() + " " + notificationVoBodyItem.getSource().getLastName());
                bodyViewHolder.messageTextView.setText(notificationVoBodyItem.getMessage());
                bodyViewHolder.dateTextView.setText(getDate(notificationVoBodyItem.getCreatedOn()));

                if (notificationVoBodyItem.getConsumed() == null)
                    notificationVoBodyItem.setConsumed(false);
                if (!notificationVoBodyItem.getConsumed()) {
                    bodyViewHolder.cardView.setCardBackgroundColor(getResources().getColor(R.color.greyUnRead));
                    bodyViewHolder.txtTitle.setTypeface(bodyViewHolder.txtTitle.getTypeface(), Typeface.BOLD);
                    bodyViewHolder.messageTextView.setTypeface(bodyViewHolder.messageTextView.getTypeface(), Typeface.BOLD);
                    bodyViewHolder.dateTextView.setTypeface(bodyViewHolder.dateTextView.getTypeface(), Typeface.BOLD);
                } else {
                    bodyViewHolder.cardView.setCardBackgroundColor(getResources().getColor(R.color.white));
                    bodyViewHolder.txtTitle.setTypeface(bodyViewHolder.txtTitle.getTypeface(), Typeface.NORMAL);
                    bodyViewHolder.messageTextView.setTypeface(bodyViewHolder.messageTextView.getTypeface(), Typeface.NORMAL);
                    bodyViewHolder.dateTextView.setTypeface(bodyViewHolder.dateTextView.getTypeface(), Typeface.NORMAL);
                }

//                if (notificationVoBodyItem.getSource().getProfileImage() != null) {
//                    ApiInterface apiService = ApiClient.getClient(bodyViewHolder.avatar.getContext()).create(ApiInterface.class);
//                    Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROFILE_PIC, notificationVoBodyItem.getSource().getProfileImage(), "image/jpg", notificationVoBodyItem.getSource().getId());
//                    call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
//                        @Override
//                        public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
//                            if (response.isSuccessful()) {
//                                if (response.body() != null) {
//                                    if (response.body().getData() != null) {
//                                        try {
//                                            UploadVo uploadVo1 = response.body().getData();
//                                            String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
//                                            Log.d("DownloadUrl", decodeUrl);
//                                            Glide.with(bodyViewHolder.avatar.getContext()).load(decodeUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(bodyViewHolder.avatar) {
//                                                @Override
//                                                protected void setResource(Bitmap resource) {
//                                                    RoundedBitmapDrawable circularBitmapDrawable =
//                                                            RoundedBitmapDrawableFactory.create(bodyViewHolder.avatar.getContext().getResources(), resource);
//                                                    circularBitmapDrawable.setCircular(true);
//                                                    bodyViewHolder.avatar.setImageDrawable(circularBitmapDrawable);
//                                                }
//                                            });
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                }
//                            } else {
//
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
//                            // Log error here since request failed
//                            Log.e("Error", t.toString());
//                        }
//                    });
//                }

                bodyViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (checkInternetConenction(getActivity())) {
                            try {
                                markAsConsumed(notificationVoBodyItem.getId());
                                if (notificationVoBodyItem.getType().equals(NotificationType.REQUEST)) {
                                    Intent intent = new Intent(getActivity(), WorkHistoryActivity.class);
                                    intent.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    startActivity(intent);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.LEAD)) {
                                    Intent intent = new Intent(getActivity(), WorkHistoryActivity.class);
                                    intent.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    startActivity(intent);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.MESSAGE)) {
                                    Intent intent = new Intent(getActivity(), RequestHistory.class);
                                    intent.putExtra("targetId", notificationVoBodyItem.getSource().getId());
                                    intent.putExtra("HO_Name", notificationVoBodyItem.getSource().getFirstName() + " " + notificationVoBodyItem.getSource().getLastName());
                                    startActivity(intent);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.QUOTE)) {
                                    Intent i = new Intent(v.getContext(), QuoteSummaryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId().toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.REQUEST_CHAT)) {
                                    final Dialog loadingDialog = Utility.loadingDialog(getContext());
                                    ApiInterface apiService = ApiClient.getClient(getContext()).create(ApiInterface.class);
                                    Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequest(notificationVoBodyItem.getTypeId());
                                    call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
                                        @Override
                                        public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                                            loadingDialog.dismiss();
                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {
                                                    if (response.body().getData() != null) {
                                                        try {
                                                            WorkHistoryActivity.homeOwnerRequestVoStatic = new HomeOwnerRequestVo();
                                                            WorkHistoryActivity.homeOwnerRequestVoStatic = response.body().getData();
                                                            Intent i = new Intent(v.getContext(), RequestMessagesActivity.class);
                                                            try {
                                                                i.putExtra("requestId", notificationVoBodyItem.getTypeId().toString());
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                            v.getContext().startActivity(i);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        Snackbar.make(bodyViewHolder.itemView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                                    }
                                                } else {
                                                    Snackbar.make(bodyViewHolder.itemView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                                }
                                            } else {
                                                Snackbar.make(bodyViewHolder.itemView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                                            // Log error here since request failed
                                            loadingDialog.dismiss();
                                            Log.e("Error", t.toString());
                                            Snackbar.make(bodyViewHolder.itemView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                        }
                                    });
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.INVOICE_APPROVED)) {
                                    Intent i = new Intent(v.getContext(), QuoteSummaryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId().toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.INVOICE)) {
                                    Intent i = new Intent(v.getContext(), QuoteSummaryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId().toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.VISIT_REQUESTED)) {
                                    Intent i = new Intent(v.getContext(), WorkHistoryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.JOB_SCHEDULE_REQUESTED)) {
                                    Intent i = new Intent(v.getContext(), WorkHistoryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.JOB_STARTED)) {
                                    Intent i = new Intent(v.getContext(), WorkHistoryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.JOB_COMPLETED)) {
                                    Intent i = new Intent(v.getContext(), WorkHistoryActivity.class);
                                    try {
                                        i.putExtra("requestId", notificationVoBodyItem.getTypeId());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.INVENTORY_REVIEWED)) {
                                    Intent i = new Intent(v.getContext(), HomeDashWebActivity.class);
                                    try {
                                        String url = "";
                                        i.putExtra("propertyId", notificationVoBodyItem.getTypeId());
                                        if (ApiClient.HOST.contains("uat")) {
                                            url = "https://uat.itrustconcierge.com/ihc/" + "app-pages/home-dash.html#/inventory/" + authenticateVoLocal.getToken() + "/" + notificationVoBodyItem.getTypeId() + "?time=";
                                        } else if (ApiClient.HOST.contains("www")) {
                                            url = "https://www.itrustconcierge.com/" + "app-pages/home-dash.html#/inventory/" + authenticateVoLocal.getToken() + "/" + notificationVoBodyItem.getTypeId() + "?time=";
                                        }
                                        i.putExtra("homeDashUrl", url);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    v.getContext().startActivity(i);
                                } else if (notificationVoBodyItem.getType().equals(NotificationType.PACKAGE)) {
                                    final Dialog loadingDialog = Utility.loadingDialog(v.getContext());
                                    ApiInterface apiService = ApiClient.getClient(v.getContext()).create(ApiInterface.class);
                                    Call<ResponseWrapper<PropertyOwnerVo>> call1 = apiService.getPropertyById(notificationVoBodyItem.getTypeId());
                                    call1.enqueue(new Callback<ResponseWrapper<PropertyOwnerVo>>() {
                                        @Override
                                        public void onResponse(Call<ResponseWrapper<PropertyOwnerVo>> call, Response<ResponseWrapper<PropertyOwnerVo>> response) {
                                            loadingDialog.dismiss();
                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {
                                                    if (response.body().getData() != null) {
                                                        try {
                                                            PropertyOwnerVo propertyOwnerVo = response.body().getData();
                                                            propertyVoPackage = propertyOwnerVo.getProperty();
                                                            Intent i = new Intent(v.getContext(), ReviewPackagesActivity.class);
                                                            v.getContext().startActivity(i);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseWrapper<PropertyOwnerVo>> call, Throwable t) {
                                            // Log error here since request failed
                                            loadingDialog.dismiss();
                                            Log.e("Error", t.toString());
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            initializeNoInternetDialogue();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
//            switch (viewHolder.getItemViewType()) {
//                case ItemType.TYPE_TITTLE:
//                    TittleItem tittleItem = (TittleItem) consolidatedList.get(position);
//                    TittleViewHolder tittleViewHolder = (TittleViewHolder) viewHolder;
//                    tittleViewHolder.txtTitle.setText(tittleItem.getTittle());
//                    break;
//
//                case ItemType.TYPE_BODY:
//                    try {
//                        final BodyItem<NotificationVo> notificationVoBodyItem = (BodyItem<NotificationVo>) consolidatedList.get(position);
//                        final BodyViewHolder bodyViewHolder = (BodyViewHolder) viewHolder;
//                        bodyViewHolder.txtTitle.setText(notificationVoBodyItem.getBody().getSource().getFirstName() + " " + notificationVoBodyItem.getBody().getSource().getLastName());
//                        bodyViewHolder.messageTextView.setText(notificationVoBodyItem.getBody().getMessage());
//                        bodyViewHolder.dateTextView.setText(getDate(notificationVoBodyItem.getBody().getCreatedOn()));
//                        if (notificationVoBodyItem.getBody().getSource().getAvatar() != null) {
//                            Glide.with(bodyViewHolder.avatar.getContext()).load(notificationVoBodyItem.getBody().getSource().getAvatar()).asBitmap().centerCrop().into(new BitmapImageViewTarget(bodyViewHolder.avatar) {
//                                @Override
//                                protected void setResource(Bitmap resource) {
//                                    RoundedBitmapDrawable circularBitmapDrawable =
//                                            RoundedBitmapDrawableFactory.create(bodyViewHolder.avatar.getContext().getResources(), resource);
//                                    circularBitmapDrawable.setCircular(true);
//                                    bodyViewHolder.avatar.setImageDrawable(circularBitmapDrawable);
//                                }
//                            });
//                        }
//
//                        bodyViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                try {
//                                    if (notificationVoBodyItem.getBody().getType().equals(NotificationType.REQUEST)) {
//                                        Intent intent = new Intent(getActivity(), IhoLead.class);
//                                        intent.putExtra("requestId", notificationVoBodyItem.getBody().getTypeId().toString());
//                                        startActivity(intent);
//                                    } else if (notificationVoBodyItem.getBody().getType().equals(NotificationType.MESSAGE)) {
//                                        Intent intent = new Intent(getActivity(), RequestHistory.class);
//                                        notificationVoStatic = notificationVoBodyItem.getBody();
//                                        intent.putExtra("conversationId", notificationVoBodyItem.getBody().getTypeId().toString());
//                                        startActivity(intent);
//                                    } else if (notificationVoBodyItem.getBody().getType().equals(NotificationType.QUOTE)) {
//                                        Intent i = new Intent(v.getContext(), Activity_Quote_Summary_View.class);
//                                        try {
//                                            i.putExtra("requestId", notificationVoBodyItem.getBody().getTypeId().toString());
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        v.getContext().startActivity(i);
//                                    } else if (notificationVoBodyItem.getBody().getType().equals(NotificationType.APPROVED_INVOICE)) {
//                                        Intent i = new Intent(v.getContext(), Activity_Quote_Summary_View.class);
//                                        try {
//                                            i.putExtra("requestId", notificationVoBodyItem.getBody().getTypeId().toString());
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        v.getContext().startActivity(i);
//                                    }
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    break;
//            }
        }

        private void markAsConsumed(Long id) {
            NotificationVo notificationVo = new NotificationVo();
            notificationVo.setId(id);
            ApiInterface apiService = ApiClient.getClient(viewHolder.itemView.getContext()).create(ApiInterface.class);
            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.consumeNotification(notificationVo);
            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                    if (response.isSuccessful()) {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItemCount;
        }

        class TittleViewHolder extends RecyclerView.ViewHolder {
            protected TextView txtTitle;

            public TittleViewHolder(View v) {
                super(v);
                this.txtTitle = (TextView) v.findViewById(R.id.text);

            }
        }

//        @Override
//        public int getItemViewType(int position) {
//            return consolidatedList.get(position).getType();
//        }

        class BodyViewHolder extends RecyclerView.ViewHolder {
            protected TextView dateTextView, txtTitle, messageTextView;
            //            ImageView avatar;
            View itemView;
            CardView cardView;

            public BodyViewHolder(View v) {
                super(v);
                this.itemView = v;
                this.txtTitle = (TextView) v.findViewById(R.id.tittle);
                this.dateTextView = (TextView) v.findViewById(R.id.date);
                this.messageTextView = (TextView) v.findViewById(R.id.description);
                this.cardView = (CardView) v.findViewById(R.id.card_view);
//                this.avatar = (ImageView) v.findViewById(R.id.view);
            }
        }
    }
}

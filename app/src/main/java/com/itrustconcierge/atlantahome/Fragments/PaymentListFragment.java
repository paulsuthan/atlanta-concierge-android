package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.PaymentActivity;
import com.itrustconcierge.atlantahome.PaymentNewActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity.paymentVo;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

/**
 * Created by paul on 8/7/2017.
 */

public class PaymentListFragment extends Fragment {

    List<PaymentVo> paymentVos;
    private static final String ARG_PARAM1 = "param1";
    private String paymentListMode;
    DecimalFormat formatter = new DecimalFormat("#.00");
    RecyclerView recyclerView;
    private PaymentAdapter paymentAdapter;
    TextView empty_view;
    SwipeRefreshLayout swipeRefreshLayout;

    public static PaymentListFragment newInstance(String mode) {
        final PaymentListFragment fragment = new PaymentListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (paymentVos != null)
            paymentVos.clear();
        return inflater.inflate(R.layout.fragment_notification_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        paymentVos = new ArrayList<>();
        if (getArguments() != null) {
            paymentListMode = getArguments().getString(ARG_PARAM1);
        }
        empty_view = (TextView) view.findViewById(R.id.empty_view);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        paymentAdapter = new PaymentAdapter(paymentVos);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                new LinearLayoutManager(getContext()).getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (paymentVo != null) {
                    paymentVos.clear();
                    recyclerView.setVisibility(View.VISIBLE);
                    paymentVos.add(paymentVo);
                    paymentAdapter = new PaymentAdapter(paymentVos);
                    recyclerView.setAdapter(paymentAdapter);
                    paymentAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    if (checkInternetConenction(getActivity())) {
                        if (getArguments() != null) {
                            paymentListMode = getArguments().getString(ARG_PARAM1);
                            getPayments(paymentListMode);
                        }
                    } else {
                        Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (paymentVo != null) {
            paymentVos.clear();
            recyclerView.setVisibility(View.VISIBLE);
            paymentVos.add(paymentVo);
            paymentAdapter = new PaymentAdapter(paymentVos);
            recyclerView.setAdapter(paymentAdapter);
            paymentAdapter.notifyDataSetChanged();
        } else {
            if (checkInternetConenction(getActivity())) {
                if (getArguments() != null) {
                    paymentListMode = getArguments().getString(ARG_PARAM1);
                    getPayments(paymentListMode);
                }
            } else {
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void getPayments(final String paymentListMode) {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<PaymentVo>> call = apiService.getPayments(PaymentStatus.fromString(paymentListMode));
        call.enqueue(new Callback<ResponseWrapper<PaymentVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PaymentVo>> call, Response<ResponseWrapper<PaymentVo>> response) {
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    paymentVos.clear();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    empty_view.setVisibility(View.GONE);
                                    paymentVos = response.body().getList();
                                    paymentAdapter = new PaymentAdapter(paymentVos);
                                    recyclerView.setAdapter(paymentAdapter);
                                    paymentAdapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    empty_view.setVisibility(View.VISIBLE);
                                    if (paymentListMode.contains("PENDING")) {
                                        empty_view.setText("You don't have any pending payments!");
                                    } else {
                                        empty_view.setText("You don't have any payments!");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PaymentVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (paymentVos != null)
            paymentVos.clear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

        List<PaymentVo> paymentVos = new ArrayList<>();

        public PaymentAdapter(List<PaymentVo> paymentVos) {
            this.paymentVos = paymentVos;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_payment_list_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final PaymentVo paymentVo1 = paymentVos.get(position);
            try {
                holder.date.setText(getDate(paymentVo1.getLastUpdatedOn()));
                holder.tittle.setText(paymentVo1.getTitle());
                holder.description.setText(paymentVo1.getPaymentStatus().name());
                holder.price.setText(String.valueOf("$" + formatter.format(paymentVo1.getAmount())));

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (paymentVo1.getPaymentStatus().equals(PaymentStatus.PENDING)) {
                            paymentVo = new PaymentVo();
                            paymentVo = paymentVo1;
                            Intent intent = new Intent(v.getContext(), PaymentNewActivity.class);
                            startActivity(intent);
                        }
                    }
                });

                if (paymentVo1.getPaymentStatus().equals(PaymentStatus.PENDING)) {
//                    holder.avatar.setImageResource(R.drawable.pending_icon);
                } else {
//                    holder.avatar.setImageResource(R.drawable.paid_icon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return paymentVos.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public View mView;
            public TextView date, tittle, description, price;
            public ImageView avatar;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                date = (TextView) view.findViewById(R.id.date);
                tittle = (TextView) view.findViewById(R.id.tittle);
                description = (TextView) view.findViewById(R.id.description);
                price = (TextView) view.findViewById(R.id.price);
                avatar = (ImageView) view.findViewById(R.id.view);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + tittle.getText() + "'";
            }
        }
    }
}

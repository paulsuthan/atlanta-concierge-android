package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.Adapters.MyRequestsAdapter;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.MainActivity.readNotifications;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadJobsNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.clearDeepLinkingRecord;

public class MyRequestsFragment extends Fragment implements ItrustHOApplication.InternetConnectivityReceiverListener {

    List<HomeOwnerRequestVo> homeOwnerRequestVos = new ArrayList<>();
    RecyclerView recyclerView;
    TextView emptyView, noInternetTextView;
    SwipeRefreshLayout swipeRefreshLayout;
    ScrollView emptyViewScroll;
    Snackbar noInternetSnackbar;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;
    private MyRequestsAdapter mAdapter;

    public MyRequestsFragment() {
    }

    public static MyRequestsFragment newInstance() {
        MyRequestsFragment fragment = new MyRequestsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_requests_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        emptyView = (TextView) view.findViewById(R.id.empty_view);
        emptyView.setVisibility(View.GONE);
        emptyViewScroll = (ScrollView) view.findViewById(R.id.emptyViewScroll);
        emptyViewScroll.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        noInternetTextView = (TextView) view.findViewById(R.id.noInternetTextView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (checkInternetConenction(getActivity())) {
                    getRequestHistory();
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    initializeNoInternetDialogue();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noInternetSnackbar = Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_INDEFINITE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
            noInternetTextView.setVisibility(View.GONE);
            getRequestHistory();
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyViewScroll.setVisibility(View.VISIBLE);
        }
        ItrustHOApplication.getInstance().setConnectivityListener(MyRequestsFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        noInternetTextView.setVisibility(View.GONE);

    }

    private void getRequestHistory() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequestHistoriesWithoutStatus();
        call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    homeOwnerRequestVos.clear();
                                    for (HomeOwnerRequestVo homeOwnerRequestVo : response.body().getList()) {
                                        if (homeOwnerRequestVo.getFromPackage() == null) {
                                            homeOwnerRequestVo.setFromPackage(false);
                                        }
                                        if (!homeOwnerRequestVo.getFromPackage() && !homeOwnerRequestVo.getDeleted()) {
                                                homeOwnerRequestVos.add(homeOwnerRequestVo);
                                        }
                                    }
                                    mAdapter = new MyRequestsAdapter(getActivity(), homeOwnerRequestVos);
                                    recyclerView.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();
                                }
                                if (homeOwnerRequestVos.isEmpty()) {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                    emptyViewScroll.setVisibility(View.VISIBLE);
                                } else {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                    emptyViewScroll.setVisibility(View.GONE);
                                }
                                clearDeepLinkingRecord();
                                if (unReadJobsNotificationVos != null) {
                                    if (!unReadJobsNotificationVos.isEmpty()) {
                                        if (checkInternetConenction(getActivity())) {
                                            readNotifications(getActivity(), unReadJobsNotificationVos);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                emptyViewScroll.setVisibility(View.VISIBLE);
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            noInternetTextView.setVisibility(View.GONE);
            getRequestHistory();
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
        }
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(getContext(), R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }
}

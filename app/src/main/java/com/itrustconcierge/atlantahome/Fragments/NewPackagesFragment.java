package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.PackageServiceVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyPackageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertySubscriptionVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ServiceDetailVo;
import com.itrustconcierge.atlantahome.Adapters.PackageExpandableAdapter;
import com.itrustconcierge.atlantahome.PaymentListActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestFlow.CreateRequestActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ReviewPackagesActivity.propertyVoPackage;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

/**
 * Created by Paul on 1/19/2018.
 */

public class NewPackagesFragment extends Fragment {

    PackageExpandableAdapter listAdapter;
    ExpandableListView expListView;
    List<PropertyPackageVo> listDataHeader;
    HashMap<PropertyPackageVo, List<String>> listDataChild;
    Button cancelButton, doneButton;
    TextView emptyView;
    public static List<PropertySubscriptionVo> propertySubscriptionVos = new ArrayList<>();

    public NewPackagesFragment() {
    }

    public static PackageItemsFragment newInstance() {
        PackageItemsFragment fragment = new PackageItemsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_packages, container, false);

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        expListView.setHasTransientState(true);
        expListView.setDrawingCacheEnabled(true);
        expListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);
        doneButton = (Button) view.findViewById(R.id.done_button);
        emptyView = (TextView) view.findViewById(R.id.empty_view);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyVoPackage = null;
                propertySubscriptionVos = new ArrayList<>();
                getActivity().finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<PropertySubscriptionVo> propertySubscriptionVos1 = new ArrayList<>();
                for(PropertySubscriptionVo propertySubscriptionVo : propertySubscriptionVos){
                    if(propertySubscriptionVo.getPayment() == null){
                        propertySubscriptionVos1.add(propertySubscriptionVo);
                    }
                }

                if (!propertySubscriptionVos1.isEmpty()) {
                    propertyVoPackage.setSubscriptions(propertySubscriptionVos1);
                    if (checkInternetConenction(getActivity())) {
                        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(getActivity(), R.style.dialogAnimation));
                        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        requestDialog.setCancelable(false);
                        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                        TextView tittleTextView = requestDialog.findViewById(R.id.tilltle);
                        TextView descriptionTextView = requestDialog.findViewById(R.id.description);
                        Button positiveButton = requestDialog.findViewById(R.id.positivebutton);
                        Button negativeButton = requestDialog.findViewById(R.id.negativebutton);

                        tittleTextView.setText("Maintenance Package Acceptance");
                        descriptionTextView.setText("Our concierge will begin working on your maintenance package jobs as soon as you pay for the maintenance package.");

                        negativeButton.setText("CANCEL");
                        positiveButton.setText("ACCEPT");

                        positiveButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                requestDialog.dismiss();
                                addPackage();
                            }
                        });

                        negativeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestDialog.dismiss();
                            }
                        });

                        requestDialog.show();
                    } else
                        Snackbar.make(expListView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                } else {
                    getActivity().finish();
                }
            }
        });

        return view;
    }

    private void addPackage() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Object>>> call = apiService.addPackages(propertyVoPackage);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Object>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Object>>> call, Response<ResponseWrapper<Map<String, Object>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    propertyVoPackage = null;
                    propertySubscriptionVos = new ArrayList<>();
                    Intent intent = new Intent(getActivity(), PaymentListActivity.class);
                    intent.putExtra("fromMore", false);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                } else {
                    Snackbar.make(expListView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Object>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(expListView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
            getPropertyPackage();
        } else
            Snackbar.make(expListView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        propertySubscriptionVos = new ArrayList<>();
    }

    private void getPropertyPackage() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyPackageVo>> call = apiService.getPropertyPackage(propertyVoPackage.getId());
        call.enqueue(new Callback<ResponseWrapper<PropertyPackageVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyPackageVo>> call, Response<ResponseWrapper<PropertyPackageVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                listDataHeader = new ArrayList<PropertyPackageVo>();
                                listDataChild = new HashMap<PropertyPackageVo, List<String>>();
                                if (!response.body().getList().isEmpty()) {
                                    final List<ServiceDetailVo> serviceDetailVos = new ArrayList<>();
                                    boolean matched;
                                    try {
                                        List<PropertySubscriptionVo> propertySubscriptionVos = new ArrayList<>();
                                        for (PropertyPackageVo propertyPackageVo : response.body().getList()) {
                                            propertyPackageVo.setActive(false);
                                            matched = false;
                                            for (PropertySubscriptionVo propertySubscriptionVo : propertySubscriptionVos) {
                                                if (propertySubscriptionVo.getPayment() == null) {
                                                    matched = true;
                                                } else if(propertySubscriptionVo.getPayment().getPaymentStatus().equals(PaymentStatus.PENDING)){
                                                    matched = true;
                                                } else {
                                                    matched = false;
                                                }
                                            }
                                            if (!matched) {
                                                listDataHeader.add(propertyPackageVo);
                                                List<String> strings = new ArrayList<>();
                                                strings.add(propertyPackageVo.getDescription());
                                                for (PackageServiceVo packageServiceVo : propertyPackageVo.getServices()) {
                                                    strings.add(packageServiceVo.getService().getService().getName());
                                                }
                                                listDataChild.put(propertyPackageVo, strings);
                                            }
                                        }
                                        listAdapter = new PackageExpandableAdapter(getActivity(), listDataHeader, listDataChild, "fromNewPackageFragment");
                                        expListView.setAdapter(listAdapter);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                }
                                if (listDataHeader.isEmpty()) {
                                    emptyView.setVisibility(View.VISIBLE);
                                    cancelButton.setVisibility(View.GONE);
                                    doneButton.setVisibility(View.GONE);
                                } else {
                                    emptyView.setVisibility(View.GONE);
                                    cancelButton.setVisibility(View.VISIBLE);
                                    doneButton.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(expListView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(expListView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(expListView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyPackageVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(expListView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

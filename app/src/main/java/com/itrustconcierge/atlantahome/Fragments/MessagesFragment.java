package com.itrustconcierge.atlantahome.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.ConversationVo;
import com.itrustconcierge.atlantahome.Adapters.ConversationAdapter;
import com.itrustconcierge.atlantahome.ItrustHOApplication;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ItrustHOApplication.noInternetHandlingReceiver;
import static com.itrustconcierge.atlantahome.MainActivity.readNotifications;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadMessagesNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

/**
 * Created by DELL on 10/3/2017.
 */

public class MessagesFragment extends Fragment implements ItrustHOApplication.InternetConnectivityReceiverListener {
    private static final String ARG_COLUMN_COUNT = "column-count";
    TextView emptyView, noInternetTextView;
    SwipeRefreshLayout swipeRefreshLayout;
    ScrollView emptyViewScroll;
    Snackbar noInternetSnackbar;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;
    private int mColumnCount = 1;
    private List<ConversationVo> conversationVos = new ArrayList<>();
    private RecyclerView recyclerView;
    private ConversationAdapter mAdapter;

    public MessagesFragment() {
    }

    public static MessagesFragment newInstance(int columnCount) {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_messages_summary, container,
                false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        emptyView = (TextView) v.findViewById(R.id.empty_view);
        emptyView.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        emptyViewScroll = (ScrollView) v.findViewById(R.id.emptyViewScroll);
        emptyViewScroll.setVisibility(View.GONE);
        noInternetTextView = (TextView) v.findViewById(R.id.noInternetTextView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        noInternetSnackbar = Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_INDEFINITE);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                new LinearLayoutManager(getContext()).getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (checkInternetConenction(getActivity())) {
                    getConversations();
                } else {
//                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), NO_INTERNET, Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                    initializeNoInternetDialogue();
                }
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        noInternetSnackbar = Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_INDEFINITE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (checkInternetConenction(getActivity())) {
//            if (noInternetPopupBar != null) {
//                noInternetPopupBar.dismiss();
//            }
            noInternetTextView.setVisibility(View.GONE);
            getConversations();
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setText("Messages will appear here");
            emptyView.setVisibility(View.VISIBLE);
            emptyViewScroll.setVisibility(View.VISIBLE);
            noInternetTextView.setVisibility(View.VISIBLE);
//            initiatePopupBar();
        }
        ItrustHOApplication.getInstance().setConnectivityListener(MessagesFragment.this);
        ItrustHOApplication.getInstance().registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        ItrustHOApplication.getInstance().setConnectivityListener(null);
        if (noInternetHandlingReceiver != null) {
            ItrustHOApplication.getInstance().unRegisterReceiver();
            noInternetHandlingReceiver = null;
        }
        noInternetTextView.setVisibility(View.GONE);
//        if (noInternetPopupBar != null) {
//            noInternetPopupBar.dismiss();
//        }
    }

    private void getConversations() {
        final Dialog loadingDialog = Utility.loadingDialog(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ResponseWrapper<ConversationVo>> call = apiService.getConversations();
        call.enqueue(new Callback<ResponseWrapper<ConversationVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ConversationVo>> call, Response<ResponseWrapper<ConversationVo>> response) {
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    conversationVos.clear();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                    emptyViewScroll.setVisibility(View.GONE);
                                    conversationVos = response.body().getList();
                                    mAdapter = new ConversationAdapter(getContext(), conversationVos);
                                    recyclerView.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();

                                    if (unReadMessagesNotificationVos != null) {
                                        if (!unReadMessagesNotificationVos.isEmpty()) {
                                            if (checkInternetConenction(getActivity())) {
                                                readNotifications(getActivity(), unReadMessagesNotificationVos);
                                            }
                                        }
                                    }
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyView.setText("You have no messages.");
                                    emptyView.setVisibility(View.VISIBLE);
                                    emptyViewScroll.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ConversationVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
//            if (noInternetPopupBar != null) {
//                noInternetPopupBar.dismiss();
            noInternetTextView.setVisibility(View.GONE);
            getConversations();
//            }
        } else {
            noInternetTextView.setVisibility(View.VISIBLE);
//            initiatePopupBar();
        }
    }

    private void initiatePopupBar() {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
//            LayoutInflater inflater = (LayoutInflater) getActivity()
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            //Inflate the view from a predefined XML layout
//            View layout = inflater.inflate(R.layout.no_internet_popup_window,
//                    (ViewGroup) findViewById(R.id.noInternetPopupLayout));

            View popupView = LayoutInflater.from(getActivity()).inflate(R.layout.no_internet_pop_up_bar, null);
            noInternetPopupBar = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            // create a 300px width and 470px height PopupWindow
//            noInternetPopupWindow = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            noInternetPopupBar.setOutsideTouchable(false);
            noInternetPopupBar.setFocusable(false);
            noInternetPopupBar.showAtLocation(swipeRefreshLayout, Gravity.BOTTOM, 0, 200);

            // finally show up your popwindow
//            noInternetPopupBar.showAsDropDown(popupView, 0, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(getContext(), R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

}

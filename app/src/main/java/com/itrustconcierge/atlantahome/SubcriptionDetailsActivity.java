package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.SubscriptionBenefitVo;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity.paymentVo;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.propertyVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.formatter;

public class SubcriptionDetailsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView subscriptionAmount, benifitsTextView;
    Button subscribeNowButton, talkToConcierge;
    List<String> mValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcription_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.list);
        subscriptionAmount = (TextView) findViewById(R.id.subscriptionAmount);
        benifitsTextView = (TextView) findViewById(R.id.text1);
        subscribeNowButton = (Button) findViewById(R.id.subscribeNow);
        talkToConcierge = (Button) findViewById(R.id.upgrade);

        if (checkInternetConenction(SubcriptionDetailsActivity.this)) {
            if (propertyVoLocal != null)
                getPaymentforSubscription();
        } else
            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();

        mValues = new ArrayList<>();
        mValues.add("Single point of contact for all your home and lifestyle needs");
        mValues.add("24*7 access to your Atlanta Concierge");
        mValues.add("Access to trusted service pros");
        mValues.add("Proactive annual home inspection");
        mValues.add("Detailed annual home inventory done by your concierge");
        mValues.add("Access to Home Dashboard");

        recyclerView.setLayoutManager(new LinearLayoutManager(SubcriptionDetailsActivity.this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                new LinearLayoutManager(SubcriptionDetailsActivity.this).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        subscribeNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentVo != null) {
                    Intent intent = new Intent(SubcriptionDetailsActivity.this, PaymentNewActivity.class);
                    startActivity(intent);
                } else
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });

        talkToConcierge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubcriptionDetailsActivity.this, ContactFranchiseActivity.class);
                startActivity(intent);
            }
        });

        getSubscriptionbenefits();

//        try{
//            if(propertyVoLocal != null){
//                if(propertyVoLocal.getFloorSize() != null){
//                    benifitsTextView.setText("iTrust membership for your "+ propertyVoLocal.getFloorSize().getName()+" sq.ft home includes");
//                } else if(propertyVoLocal.getFloorSizeInSqFt() != 0){
//                    benifitsTextView.setText("iTrust membership for your "+propertyVoLocal.getFloorSizeInSqFt()+" sq.ft home includes");
//                } else {
//                    benifitsTextView.setText("iTrust membership for your home includes");
//                }
//            }
//        } catch (Exception e){
//            e.printStackTrace();
//        }
    }

    private void getSubscriptionbenefits() {
        final Dialog loadingDialog = Utility.loadingDialog(SubcriptionDetailsActivity.this);
        ApiInterface apiService = ApiClient.getClient(SubcriptionDetailsActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<SubscriptionBenefitVo>> call = apiService.getSubscriptionbenefits();
        call.enqueue(new Callback<ResponseWrapper<SubscriptionBenefitVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<SubscriptionBenefitVo>> call, Response<ResponseWrapper<SubscriptionBenefitVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                List<SubscriptionBenefitVo> subscriptionBenefitVos = response.body().getList();
                                mValues.clear();
                                for (SubscriptionBenefitVo subscriptionBenefitVo : subscriptionBenefitVos) {
                                    mValues.add(subscriptionBenefitVo.getDescription());
                                }
                                recyclerView.setAdapter(new SubcriptionItemsAdapter(mValues, SubcriptionDetailsActivity.this));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                        }
                    } else {
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<SubscriptionBenefitVo>> call, Throwable t) {
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkInternetConenction(SubcriptionDetailsActivity.this)) {
            if (propertyVoLocal != null)
                getPaymentforSubscription();
        } else
            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
    }

    private void getPaymentforSubscription() {
        final Dialog loadingDialog = Utility.loadingDialog(SubcriptionDetailsActivity.this);
        ApiInterface apiService = ApiClient.getClient(SubcriptionDetailsActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PaymentVo>> call = apiService.getPropertySubscriptionPayment(StaticInfo.authenticateVoLocal.getId(), propertyVoLocal.getId());
        call.enqueue(new Callback<ResponseWrapper<PaymentVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PaymentVo>> call, Response<ResponseWrapper<PaymentVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            paymentVo = response.body().getData();
                            recyclerView.setAdapter(new SubcriptionItemsAdapter(mValues, SubcriptionDetailsActivity.this));
                            subscriptionAmount.setText("$" + formatter.format(paymentVo.getAmount()));
//                            if (paymentVo.getAmount() < 395.00) {
//                                upgradeLayout.setVisibility(View.VISIBLE);
//                            } else {
//                                upgradeLayout.setVisibility(View.GONE);
//                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PaymentVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    class SubcriptionItemsAdapter extends RecyclerView.Adapter<SubcriptionItemsAdapter.ViewHolder> {

        private final List<String> mValues;
        private Activity activity;

        public SubcriptionItemsAdapter(List<String> items, Activity activity) {
            mValues = items;
            this.activity = activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.subscription_detail_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mContentView.setText(mValues.get(position));

//            if (paymentVo != null) {
//                if (paymentVo.getAmount() < 395.00) {
//                    if (holder.mContentView.getText().toString().contains("Proactive")) {
//                        holder.imageView.setImageResource(R.drawable.ic_not_interested_black_24dp);
//                        holder.imageView.setColorFilter(ContextCompat.getColor(activity, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
//                    }
//                    if (holder.mContentView.getText().toString().contains("Detailed annual")) {
//                        holder.imageView.setImageResource(R.drawable.ic_not_interested_black_24dp);
//                        holder.imageView.setColorFilter(ContextCompat.getColor(activity, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
//                    }
//                    if (holder.mContentView.getText().toString().contains("Access to Home Dash")) {
//                        holder.imageView.setImageResource(R.drawable.ic_not_interested_black_24dp);
//                        holder.imageView.setColorFilter(ContextCompat.getColor(activity, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
//                    }
//                } else {
//
//                }
//            }
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mContentView;
            public String mItem;
            public ImageView imageView;
            public LinearLayout linearLayoutMore;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mContentView = (TextView) view.findViewById(R.id.content);
                imageView = (ImageView) view.findViewById(R.id.view);
                linearLayoutMore = (LinearLayout) view.findViewById(R.id.linearLayoutMore);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }
}

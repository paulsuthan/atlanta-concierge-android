package com.itrustconcierge.atlantahome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.itrustconcierge.atlantahome.Utilities.ViewPagerAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class Activity_Slider extends FragmentActivity {

    private ViewPager _mViewPager;
    private ViewPagerAdapter _adapter;
    private Button _btn1, _btn2, _btn3, btnNext;

    Runnable mUpdateResults;
    Handler mHandler;
    Timer timer;

    int count = 0;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);
        setUpView();
        setTab();

        mHandler = new Handler();

        // Create runnable for posting
        mUpdateResults = new Runnable() {
            public void run() {

                showNextSlide(count);

            }
        };

        int delay = 3000;

        int period = 6000;

        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                count = count + 1;
                mHandler.post(mUpdateResults);
                if (count == 3) {
                    count = 0;
                }
            }

        }, delay, period);


    }

    private void setUpView() {
        _mViewPager = (ViewPager) findViewById(R.id.viewPager);
        _adapter = new ViewPagerAdapter(this, getSupportFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);
        initButton();
    }

    private void setTab() {
        _mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                btnAction(position);
            }

        });

    }

    void showNextSlide(int count) {

        Log.d("count is :", " " + count);
        if (count == 1) {
            _mViewPager.setCurrentItem(0);
        } else if (count == 2) {
            _mViewPager.setCurrentItem(1);
        } else if (count == 0) {
            _mViewPager.setCurrentItem(2);
        }

    }


    private void btnAction(int action) {
        switch (action) {
            case 0:
                setButton(_btn1, "", 10, 10);
                setButton(_btn2, "", 10, 10);
                setButton(_btn3, "", 10, 10);

                _btn1.setBackground(getResources().getDrawable(R.drawable.rounded_cell_selected));
                _btn2.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                _btn3.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                btnNext.setText("NEXT");
                break;

            case 1:
                setButton(_btn2, "", 10, 10);
                setButton(_btn1, "", 10, 10);
                _btn1.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                _btn2.setBackground(getResources().getDrawable(R.drawable.rounded_cell_selected));
                _btn3.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                btnNext.setText("NEXT");
                break;

            case 2:
                setButton(_btn2, "", 10, 10);
                setButton(_btn1, "", 10, 10);
                _btn1.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                _btn2.setBackground(getResources().getDrawable(R.drawable.rounded_cell));
                _btn3.setBackground(getResources().getDrawable(R.drawable.rounded_cell_selected));
                btnNext.setText("NEXT");
                break;
        }
    }

    private void initButton() {
        _btn1 = (Button) findViewById(R.id.btn1);
        _btn2 = (Button) findViewById(R.id.btn2);
        _btn3 = (Button) findViewById(R.id.btn3);
        btnNext = (Button) findViewById(R.id.btnNext);
        setButton(_btn1, "", 10, 10);
        _btn1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        _btn1.setBackground(getResources().getDrawable(R.drawable.rounded_cell_selected));
        setButton(_btn2, "", 10, 10);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.removeCallbacks(mUpdateResults);
                timer.cancel();
                Intent intent = new Intent(Activity_Slider.this, NeedSomethingTextActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setButton(Button btn, String text, int h, int w) {
        btn.setWidth(w);
        btn.setHeight(h);
        btn.setText(text);
    }

}

package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyOwnerVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.Fragments.SlideshowDialogFragment;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Adapters.ImageListViewAdapter.imageUrlMap;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class PropertyActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView imageView;
    List<String> pathList;
    ImageButton addImage;
    TextView managedBy, address, propertyType, floorSize, lotSize;
    private List<String> images = new ArrayList<>();
    PropertyVo propertyVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedAppBar);
        imageView = (ImageView) findViewById(R.id.imageView9);
        addImage = (ImageButton) findViewById(R.id.addImage);
        managedBy = (TextView) findViewById(R.id.managedBy);
        address = (TextView) findViewById(R.id.address);
        propertyType = (TextView) findViewById(R.id.propertyType);
        floorSize = (TextView) findViewById(R.id.floorSize);
        lotSize = (TextView) findViewById(R.id.lotSize);
//        Glide.with(PropertyActivity.this).load("http://www.architectureartdesigns.com/wp-content/uploads/2015/07/713.jpg").into(imageView);

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(PropertyActivity.this, PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 5);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!images.isEmpty()) {
                    try {
                        Bundle bundle1 = new Bundle();
                        bundle1.putSerializable("images", (Serializable) images);
                        bundle1.putInt("position", 0);
                        bundle1.putString("uploadType", "PROPERTY");
                        bundle1.putString("id", String.valueOf(authenticateVoLocal.getId()));
                        FragmentTransaction ft = PropertyActivity.this.getSupportFragmentManager().beginTransaction();
                        SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                        newFragment.setArguments(bundle1);
                        newFragment.show(ft, "slideshow");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (checkInternetConenction(PropertyActivity.this)) {
                getPropertyById(getIntent().getExtras().getLong("propertyId"));
            } else
                Snackbar.make(collapsingToolbarLayout, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPropertyById(long propertyId) {
        final Dialog loadingDialog = Utility.loadingDialog(PropertyActivity.this);
        ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyOwnerVo>> call = apiService.getPropertyById(propertyId);
        call.enqueue(new Callback<ResponseWrapper<PropertyOwnerVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyOwnerVo>> call, Response<ResponseWrapper<PropertyOwnerVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                PropertyOwnerVo propertyOwnerVo = response.body().getData();
                                propertyVo = propertyOwnerVo.getProperty();
                                if (propertyVo.getAddress() != null) {
                                    if (propertyVo.getAddress().getDisplayAddress() != null) {
                                        collapsingToolbarLayout.setTitle(propertyVo.getAddress().getDisplayAddress());
                                        address.setText(propertyVo.getAddress().getDisplayAddress());
                                    } else {
                                        collapsingToolbarLayout.setTitle(propertyVo.getAddress().getLine1());
                                        address.setText(propertyVo.getAddress().getLine1());
                                    }
                                    managedBy.setText(authenticateVoLocal.getTenantSetting().getDisplayName());
                                    floorSize.setText(propertyVo.getFloorSize());
                                    lotSize.setText(propertyVo.getLotSize());
                                    propertyType.setText(propertyVo.getPropertyType());
                                    if (propertyVo.getImage() != null) {
                                        if (!propertyVo.getImage().contains(",")) {
                                            images.add(propertyVo.getImage());
                                            ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
                                            Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROPERTY, propertyVo.getImage(), "image/jpg", authenticateVoLocal.getId());
                                            call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                                                @Override
                                                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            if (response.body().getData() != null) {
                                                                try {
                                                                    UploadVo uploadVo1 = response.body().getData();
                                                                    String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                                    Log.d("DownloadUrl", decodeUrl);
                                                                    Glide.with(PropertyActivity.this).load(decodeUrl).into(imageView);
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }
                                                    } else {

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                                    // Log error here since request failed
                                                    Log.e("Error", t.toString());
                                                }
                                            });
                                        } else {
                                            images = Arrays.asList(propertyVo.getImage().split(","));
                                            ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
                                            Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROPERTY, images.get(0), "image/jpg", authenticateVoLocal.getId());
                                            call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                                                @Override
                                                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            if (response.body().getData() != null) {
                                                                try {
                                                                    UploadVo uploadVo1 = response.body().getData();
                                                                    String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                                                    Log.d("DownloadUrl", decodeUrl);
                                                                    Glide.with(PropertyActivity.this).load(decodeUrl).into(imageView);
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }
                                                    } else {

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                                                    // Log error here since request failed
                                                    Log.e("Error", t.toString());
                                                }
                                            });
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyOwnerVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            this.pathList = intent.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < pathList.size(); i++) {
                    sb.append("Photo" + (i + 1) + ":" + pathList.get(i));
                    sb.append("\n");
                    final File file = new File(pathList.get(i));
                    long l = file.length();
                    String fileName = file.getName();
                    final String mimeType = "image/jpeg";
                    getUploadUrl(fileName, mimeType, file);
                }
                Log.d("pathList", sb.toString());
            }
        }
    }

    Dialog loadingDialog;

    private void getUploadUrl(String fileName, final String mimeType, final File file) {
        loadingDialog = Utility.loadingDialog(PropertyActivity.this);
        ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<UploadVo>> call = apiService.getUploadUrl(UploadType.PROPERTY, fileName, mimeType, authenticateVoLocal.getId());
        call.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                UploadVo uploadVo = response.body().getData();
                                new uploadImageTask().execute(uploadVo.getUrl(), uploadVo, file, mimeType);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
            }
        });
    }

    String imageUrl = null;
    StringBuilder stringBuilder = new StringBuilder();

    private class uploadImageTask extends AsyncTask {

        UploadVo uploadVo = new UploadVo();
        String mimeType;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Integer doInBackground(Object[] params) {
            int responseCode = 0;
            this.uploadVo = (UploadVo) params[1];
            this.mimeType = (String) params[3];
            try {
                String decodeUrl = java.net.URLDecoder.decode((String) params[0], "UTF-8");
                URL url = new URL(decodeUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("content-type", (String) params[3]);
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                FileInputStream fileInputStream = new FileInputStream((File) params[2]);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    out.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                out.flush();
                out.close();
                responseCode = connection.getResponseCode();
                System.out.println("Service returned response code " + responseCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onPostExecute(responseCode);
            return responseCode;
        }

        protected void onPostExecute(Integer result) {
            loadingDialog.dismiss();
            if (result.equals(200)) {
                ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
                Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROPERTY, uploadVo.getPhysicalFilename(), mimeType, authenticateVoLocal.getId());
                call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
                                        UploadVo uploadVo1 = response.body().getData();
                                        String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                        Log.d("DownloadUrl", decodeUrl);
                                        stringBuilder.append(uploadVo1.getPhysicalFilename());
                                        stringBuilder.append(",");
                                        imageUrl = stringBuilder.length() > 0 ? stringBuilder.substring(0, stringBuilder.length() - 1) : "";
                                        updateProperty(imageUrl);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                    }
                });
            } else {
            }
        }
    }

    private void updateProperty(String imageUrl) {
        propertyVo.setImage(imageUrl);
        final Dialog loadingDialog = Utility.loadingDialog(PropertyActivity.this);
        ApiInterface apiService = ApiClient.getClient(PropertyActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PropertyVo>> call = apiService.updateProperty(propertyVo);
        call.enqueue(new Callback<ResponseWrapper<PropertyVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PropertyVo>> call, Response<ResponseWrapper<PropertyVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            try {
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PropertyVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(collapsingToolbarLayout, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}

package com.itrustconcierge.atlantahome;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentType;
import com.itrustconcierge.atlantahome.API_Manager.enums.QuoteType;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.vo.CardDetail;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IdListVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.QuoteVo;
import com.itrustconcierge.atlantahome.Fragments.CardFragment;
import com.itrustconcierge.atlantahome.Fragments.DonePaymentFragment;
import com.itrustconcierge.atlantahome.RequestFlow.CreateRequestFinalActivity;
import com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity;
import com.itrustconcierge.atlantahome.RequestFlow.RequestMessagesActivity;
import com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.CardItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardAdapter;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableCardViewHolder;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Fragments.CardFragment.cardDetail;
import static com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity.paymentVo;
import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.ReviewPackagesActivity.propertyVoPackage;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.formatter;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class PaymentNewActivity extends AppCompatActivity {

    public static CardDetail cardDetail;
    RecyclerView recyclerView;
    List<CardDetail> cardDetails = new ArrayList<>();
    SelectableCardViewHolder.OnItemSelectedListener cardListener;
    SelectableCardAdapter selectableCardAdapter;
    List<CardItem> itemsCardDetails = new ArrayList<CardItem>();
    TextView priceTextView, description, chatTextView;
    Button addCardButton, confirmPaymentButton, detailsButton;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        paymentVo = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_new);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(Color.BLACK);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        priceTextView = findViewById(R.id.priceTextView);
        description = findViewById(R.id.description);
        chatTextView = findViewById(R.id.toolBarButton);
        addCardButton = findViewById(R.id.addCardButton);
        confirmPaymentButton = findViewById(R.id.payButton);
        detailsButton = findViewById(R.id.detailsButton);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(PaymentNewActivity.this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        cardListener = new SelectableCardViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableCardItem item) {
                for (CardDetail cardDetailTemp : cardDetails) {
                    if (item.getId().equals(cardDetailTemp.getId()) && item.getCardNumber().equals(cardDetailTemp.getCardNumber())) {
                        cardDetail = cardDetailTemp;
                    }
                }
            }
        };

        if (paymentVo != null) {
            priceTextView.setText("$" + String.valueOf(formatter.format(paymentVo.getAmount())));
            description.setText(paymentVo.getTitle());

            if (paymentVo.getPaymentType().equals(PaymentType.PACKAGE)) {
                detailsButton.setVisibility(View.GONE);
            } else {
                detailsButton.setVisibility(View.VISIBLE);
            }
        }

        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentNewActivity.this, AddCardActivity.class);
                startActivity(intent);
            }
        });

        cardListener = new SelectableCardViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(SelectableCardItem item) {
                for (CardDetail cardDetailTemp : cardDetails) {
                    if (item.getId().equals(cardDetailTemp.getId()) && item.getCardNumber().equals(cardDetailTemp.getCardNumber())) {
                        cardDetail = cardDetailTemp;
                    }
                }
            }
        };

        detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentVo.getPaymentType().equals(PaymentType.INVOICE)) {
                    if(homeOwnerRequestVoStatic == null) {
                        final Dialog loadingDialog = Utility.loadingDialog(PaymentNewActivity.this);
                        ApiInterface apiService = ApiClient.getClient(PaymentNewActivity.this).create(ApiInterface.class);
                        Call<ResponseWrapper<QuoteVo>> call = apiService.getQuote(paymentVo.getSubject().toString());
                        call.enqueue(new Callback<ResponseWrapper<QuoteVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<QuoteVo>> call, Response<ResponseWrapper<QuoteVo>> response) {
                                loadingDialog.dismiss();
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                QuoteVo quoteVoStatic = new QuoteVo();
                                                quoteVoStatic = response.body().getData();
                                                if (quoteVoStatic != null) {
                                                    Intent i = new Intent(PaymentNewActivity.this, WorkHistoryActivity.class);
                                                    try {
                                                        i.putExtra("requestId", quoteVoStatic.getRequest().getId());
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    startActivity(i);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<QuoteVo>> call, Throwable t) {
                                // Log error here since request failed
                                loadingDialog.dismiss();
                                Log.e("Error", t.toString());
                                Snackbar.make(chatTextView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Intent i = new Intent(PaymentNewActivity.this, WorkHistoryActivity.class);
                        try {
                            i.putExtra("requestId", homeOwnerRequestVoStatic.getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                }
            }
        });

        chatTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentVo.getPaymentType().equals(PaymentType.INVOICE)) {
                    if(homeOwnerRequestVoStatic == null) {
                        final Dialog loadingDialog = Utility.loadingDialog(PaymentNewActivity.this);
                        ApiInterface apiService = ApiClient.getClient(PaymentNewActivity.this).create(ApiInterface.class);
                        Call<ResponseWrapper<QuoteVo>> call = apiService.getQuote(paymentVo.getSubject().toString());
                        call.enqueue(new Callback<ResponseWrapper<QuoteVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<QuoteVo>> call, Response<ResponseWrapper<QuoteVo>> response) {
                                loadingDialog.dismiss();
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                QuoteVo quoteVoStatic = new QuoteVo();
                                                quoteVoStatic = response.body().getData();
                                                if (quoteVoStatic != null) {
                                                    final Dialog loadingDialog = Utility.loadingDialog(PaymentNewActivity.this);
                                                    ApiInterface apiService = ApiClient.getClient(PaymentNewActivity.this).create(ApiInterface.class);
                                                    Call<ResponseWrapper<HomeOwnerRequestVo>> call1 = apiService.getRequest(quoteVoStatic.getRequest().getId());
                                                    call1.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                                                            loadingDialog.dismiss();
                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if (response.body().getData() != null) {
                                                                        try {
                                                                            homeOwnerRequestVoStatic = new HomeOwnerRequestVo();
                                                                            homeOwnerRequestVoStatic = response.body().getData();
                                                                            Intent i = new Intent(PaymentNewActivity.this, RequestMessagesActivity.class);
                                                                            try {
                                                                                i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                                                                            } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                            startActivity(i);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else {
                                                                        Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                                                    }
                                                                } else {
                                                                    Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                                                }
                                                            } else {
                                                                Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                                                            // Log error here since request failed
                                                            loadingDialog.dismiss();
                                                            Log.e("Error", t.toString());
                                                            Snackbar.make(chatTextView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                                        }
                                                    });
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<QuoteVo>> call, Throwable t) {
                                // Log error here since request failed
                                loadingDialog.dismiss();
                                Log.e("Error", t.toString());
                                Snackbar.make(chatTextView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Intent i = new Intent(PaymentNewActivity.this, RequestMessagesActivity.class);
                        try {
                            i.putExtra("requestId", homeOwnerRequestVoStatic.getId().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                } else {
                    if (checkInternetConenction(PaymentNewActivity.this)) {
                        Intent i = new Intent(PaymentNewActivity.this, RequestHistory.class);
                        try {
                            i.putExtra("targetId", authenticateVoLocal.getFranchise().getId());
                            i.putExtra("HO_Name", authenticateVoLocal.getTenantSetting().getDisplayName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    } else {
                        Snackbar.make(chatTextView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });


        confirmPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cardDetail != null) {
                    try {
                        ApiInterface apiService = ApiClient.getClient(PaymentNewActivity.this).create(ApiInterface.class);
                        IdListVo idListVo = new IdListVo();
                        List<Long> list = new ArrayList<>();
                        list.add(paymentVo.getId());
                        idListVo.setList(list);
                        final Dialog loadingDialog = Utility.loadingDialog(PaymentNewActivity.this);
                        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.makePayments(idListVo, cardDetail.getId());
                        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                loadingDialog.dismiss();
                                if (response.isSuccessful()) {
                                    try {
                                        paymentVo = null;
                                        cardDetail = null;
                                        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(PaymentNewActivity.this, R.style.dialogAnimation));
                                        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        requestDialog.setContentView(R.layout.fragment_done_payment);
                                        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                        requestDialog.setCancelable(false);
                                        requestDialog.getWindow().setGravity(Gravity.CENTER);
                                        Button done_button = (Button) requestDialog.findViewById(R.id.done_button);
                                        TextView priceTextView1 = (TextView) requestDialog.findViewById(R.id.price);

                                        priceTextView1.setText(priceTextView.getText().toString());
                                        done_button.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                requestDialog.dismiss();
                                                Intent intent = new Intent(PaymentNewActivity.this, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                                startActivity(intent);
                                                finishAffinity();
                                            }
                                        });
                                        requestDialog.show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                loadingDialog.dismiss();
                                // Log error here since request failed
                                Log.e("Error", t.toString());
                                Snackbar.make(chatTextView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Snackbar.make(chatTextView, "Please select card for payment.", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkInternetConenction(PaymentNewActivity.this)) {
            getCards();
        } else {
            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }
    }

    private void getCards() {
        ApiInterface apiService = ApiClient.getClient(PaymentNewActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<CardDetail>> call = apiService.getCardDetails();
        call.enqueue(new Callback<ResponseWrapper<CardDetail>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<CardDetail>> call, Response<ResponseWrapper<CardDetail>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    cardDetails.clear();
                                    cardDetail = null;
                                    recyclerView.setVisibility(View.VISIBLE);
                                    cardDetails = response.body().getList();
                                    itemsCardDetails = new ArrayList<>();
                                    int i = 0;
                                    for (CardDetail cardDetailTemp : cardDetails) {
                                        if (i == 0) {
                                            CardItem cardItem = new CardItem(cardDetailTemp.getId(), cardDetailTemp.getCardType(), cardDetailTemp.getCardNumber());
                                            SelectableCardItem selectableCardItem = new SelectableCardItem(cardItem, true);
                                            itemsCardDetails.add(selectableCardItem);
                                            cardDetail = cardDetailTemp;
                                        } else {
                                            itemsCardDetails.add(new CardItem(cardDetailTemp.getId(), cardDetailTemp.getCardType(), cardDetailTemp.getCardNumber()));
                                        }
                                        i++;
                                    }
                                    selectableCardAdapter = new SelectableCardAdapter(cardListener, itemsCardDetails, false);
                                    recyclerView.setAdapter(selectableCardAdapter);
                                } else {

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<CardDetail>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}

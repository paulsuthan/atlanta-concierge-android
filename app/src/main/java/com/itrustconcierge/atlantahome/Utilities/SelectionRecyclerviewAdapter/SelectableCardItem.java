package com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter;

public class SelectableCardItem extends CardItem{

    private boolean isSelected = false;

    public SelectableCardItem(CardItem item, boolean isSelected) {
        super(item.getId(), item.getCardType(), item.getCardNumber());
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

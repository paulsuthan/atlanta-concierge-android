package com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itrustconcierge.atlantahome.R;

import java.util.ArrayList;
import java.util.List;

public class SelectableCardAdapter extends RecyclerView.Adapter implements SelectableCardViewHolder.OnItemSelectedListener {

    private List<SelectableCardItem> mValues;
    private boolean isMultiSelectionEnabled = false;
    SelectableCardViewHolder.OnItemSelectedListener listener;

    public SelectableCardAdapter() {
    }

    public SelectableCardAdapter(SelectableCardViewHolder.OnItemSelectedListener listener,
                                 List<CardItem> items, boolean isMultiSelectionEnabled) {
        this.listener = listener;
        this.isMultiSelectionEnabled = isMultiSelectionEnabled;

        mValues = new ArrayList<>();

        for (CardItem item : items) {
            if (!(item instanceof SelectableCardItem)) {
                mValues.add(new SelectableCardItem(item, false));
            } else {
                mValues.add(new SelectableCardItem(item, true));
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selectable_card_item, parent, false);

        return new SelectableCardViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SelectableCardViewHolder holder = (SelectableCardViewHolder) viewHolder;
        SelectableCardItem selectableItem = mValues.get(position);
//        String name = selectableItem.getName();
        if (selectableItem.getCardType().contains("Visa")) {
            holder.cardType.setBackground(holder.cardType.getContext().getDrawable(R.drawable.visa_card));
        } else if (selectableItem.getCardType().contains("Master")) {
            holder.cardType.setBackground(holder.cardType.getContext().getDrawable(R.drawable.master_card));
        } else if (selectableItem.getCardType().contains("American")) {
            holder.cardType.setBackground(holder.cardType.getContext().getDrawable(R.drawable.american_card));
        } else {
            holder.cardType.setBackground(holder.cardType.getContext().getDrawable(R.drawable.american_card));
        }
        holder.cardType.setText(selectableItem.getCardType());
        holder.textView.setText(" Ending - " + selectableItem.getCardNumber());
        if (isMultiSelectionEnabled) {
            TypedValue value = new TypedValue();
            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
//            int checkMarkDrawableResId = value.resourceId;
//            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId);
        } else {
            TypedValue value = new TypedValue();
            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
//            int checkMarkDrawableResId = value.resourceId;
//            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId);
        }

        holder.mItem = selectableItem;
        holder.setChecked(holder.mItem.isSelected());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<CardItem> getSelectedItems() {
        List<CardItem> selectedItems = new ArrayList<>();
        for (SelectableCardItem item : mValues) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (isMultiSelectionEnabled) {
            return SelectableViewHolder.MULTI_SELECTION;
        } else {
            return SelectableViewHolder.SINGLE_SELECTION;
        }
    }

    @Override
    public void onItemSelected(SelectableCardItem item) {
        if (!isMultiSelectionEnabled) {
            for (SelectableCardItem selectableItem : mValues) {
                if (!selectableItem.equals(item)
                        && selectableItem.isSelected()) {
                    selectableItem.setSelected(false);
                } else if (selectableItem.equals(item)
                        && item.isSelected()) {
                    selectableItem.setSelected(true);
                }
            }
            notifyDataSetChanged();
        }
        listener.onItemSelected(item);
    }
}

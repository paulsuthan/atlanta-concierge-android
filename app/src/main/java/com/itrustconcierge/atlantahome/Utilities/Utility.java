package com.itrustconcierge.atlantahome.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.itrustconcierge.atlantahome.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Paul on 8/21/2017.
 */

public class Utility {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";
    public static int SPLASH_TIME_OUT = 3000;
    public static String NO_INTERNET = "No Internet Connection";
    public static String SERVER_MAINTENANCE = "We're sorry for the inconvenience. Please try again later.";
    public static String SOMETHING_WENT_WRONG = "Oops, something went wrong. Please try again.";
    public static String PERMISSION_IMAGE_TITTLE = "Share Your View";
    public static String PERMISSION_IMAGE_TEXT = "Instant Sharing Add photos of your property information directly from your phone. Select \"ALLOW\" on the next screen so iTrust Homeowners can access your camera and image files.";
    public static String PERMISSION_LOCATION_TITTLE = "Promote Your Business Locally";
    public static String PERMISSION_LOCATION_TEXT = "iTrustConcierge needs to access your location to broadcast and reach your customer’s neighbors. Please select \"ALLOW\" so iTrust Homeowners can promote your business locally and access your devices.";
    public static String WELCOME_TEXT = "Congratulations! You will get a notification when a new request is submitted by homeowners.";
    public static DecimalFormat formatter = new DecimalFormat("#.00");
    //Deeplinking variables
    public static boolean gotoHome = false;
    public static boolean gotoMyJobs = false;
    public static boolean gotoMessages = false;
    public static boolean gotoNotifications = false;
    public static boolean gotoMore = false;
    public static boolean gotoLeads = false;
    public static boolean gotoCreatedQuotes = false;
    public static boolean gotoQuotesUnderRevision = false;
    public static boolean gotoConfirmedQuotes = false;
    public static boolean gotoPendingJobs = false;
    public static boolean gotoInprogressJobs = false;
    public static boolean gotoCompletedJobs = false;
    public static boolean gotoInvoices = false;
    public static boolean gotoPendingPayment = false;
    public static boolean gotoClosedJobs = false;
    public static String LOGIN_PREF_NAME = "login_credentials";
    public static String LOGIN_USERNAME = "login_username";
    public static String LOGIN_PASSWORD = "login_password";
    public static String ROLE = "role";
    public static String AUTHTOKEN = "authtoken";
    public static String COMMON_PREF_NAME = "common_pref";
    public static String state_code = "{\"stateList\":[\n" +
            "    {\n" +
            "        \"name\": \"Alabama\",\n" +
            "        \"abbreviation\": \"AL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Alaska\",\n" +
            "        \"abbreviation\": \"AK\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"American Samoa\",\n" +
            "        \"abbreviation\": \"AS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Arizona\",\n" +
            "        \"abbreviation\": \"AZ\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Arkansas\",\n" +
            "        \"abbreviation\": \"AR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"California\",\n" +
            "        \"abbreviation\": \"CA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Colorado\",\n" +
            "        \"abbreviation\": \"CO\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Connecticut\",\n" +
            "        \"abbreviation\": \"CT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Delaware\",\n" +
            "        \"abbreviation\": \"DE\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"District Of Columbia\",\n" +
            "        \"abbreviation\": \"DC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Federated States Of Micronesia\",\n" +
            "        \"abbreviation\": \"FM\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Florida\",\n" +
            "        \"abbreviation\": \"FL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Georgia\",\n" +
            "        \"abbreviation\": \"GA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Guam\",\n" +
            "        \"abbreviation\": \"GU\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Hawaii\",\n" +
            "        \"abbreviation\": \"HI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Idaho\",\n" +
            "        \"abbreviation\": \"ID\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Illinois\",\n" +
            "        \"abbreviation\": \"IL\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Indiana\",\n" +
            "        \"abbreviation\": \"IN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Iowa\",\n" +
            "        \"abbreviation\": \"IA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Kansas\",\n" +
            "        \"abbreviation\": \"KS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Kentucky\",\n" +
            "        \"abbreviation\": \"KY\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Louisiana\",\n" +
            "        \"abbreviation\": \"LA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Maine\",\n" +
            "        \"abbreviation\": \"ME\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Marshall Islands\",\n" +
            "        \"abbreviation\": \"MH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Maryland\",\n" +
            "        \"abbreviation\": \"MD\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Massachusetts\",\n" +
            "        \"abbreviation\": \"MA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Michigan\",\n" +
            "        \"abbreviation\": \"MI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Minnesota\",\n" +
            "        \"abbreviation\": \"MN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Mississippi\",\n" +
            "        \"abbreviation\": \"MS\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Missouri\",\n" +
            "        \"abbreviation\": \"MO\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Montana\",\n" +
            "        \"abbreviation\": \"MT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Nebraska\",\n" +
            "        \"abbreviation\": \"NE\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Nevada\",\n" +
            "        \"abbreviation\": \"NV\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Hampshire\",\n" +
            "        \"abbreviation\": \"NH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Jersey\",\n" +
            "        \"abbreviation\": \"NJ\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New Mexico\",\n" +
            "        \"abbreviation\": \"NM\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"New York\",\n" +
            "        \"abbreviation\": \"NY\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"North Carolina\",\n" +
            "        \"abbreviation\": \"NC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"North Dakota\",\n" +
            "        \"abbreviation\": \"ND\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Northern Mariana Islands\",\n" +
            "        \"abbreviation\": \"MP\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Ohio\",\n" +
            "        \"abbreviation\": \"OH\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Oklahoma\",\n" +
            "        \"abbreviation\": \"OK\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Oregon\",\n" +
            "        \"abbreviation\": \"OR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Palau\",\n" +
            "        \"abbreviation\": \"PW\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Pennsylvania\",\n" +
            "        \"abbreviation\": \"PA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Puerto Rico\",\n" +
            "        \"abbreviation\": \"PR\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Rhode Island\",\n" +
            "        \"abbreviation\": \"RI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"South Carolina\",\n" +
            "        \"abbreviation\": \"SC\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"South Dakota\",\n" +
            "        \"abbreviation\": \"SD\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Tennessee\",\n" +
            "        \"abbreviation\": \"TN\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Texas\",\n" +
            "        \"abbreviation\": \"TX\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Utah\",\n" +
            "        \"abbreviation\": \"UT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Vermont\",\n" +
            "        \"abbreviation\": \"VT\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Virgin Islands\",\n" +
            "        \"abbreviation\": \"VI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Virginia\",\n" +
            "        \"abbreviation\": \"VA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Washington\",\n" +
            "        \"abbreviation\": \"WA\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"West Virginia\",\n" +
            "        \"abbreviation\": \"WV\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Wisconsin\",\n" +
            "        \"abbreviation\": \"WI\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"name\": \"Wyoming\",\n" +
            "        \"abbreviation\": \"WY\"\n" +
            "    }\n" +
            "]}";
    static boolean wifi = false, internet = false;
    private static ConnectivityManager connectivityManager;
    private static String IS_LOGGEDIN = "is_logged_in";
    private static String IS_NOTFIRSTTIME = "is_not_first_time";
    private static String IS_CALENDAR_PERMISSION_ENABLED = "IS_CALENDAR_PERMISSION_ENABLED";

    public static void clearDeepLinkingRecord() {
        gotoHome = false;
        gotoMyJobs = false;
        gotoMessages = false;
        gotoNotifications = false;
        gotoMore = false;
        gotoLeads = false;
        gotoCreatedQuotes = false;
        gotoQuotesUnderRevision = false;
        gotoConfirmedQuotes = false;
        gotoPendingJobs = false;
        gotoInprogressJobs = false;
        gotoCompletedJobs = false;
        gotoInvoices = false;
        gotoPendingPayment = false;
        gotoClosedJobs = false;
    }

    public static boolean checkInternetConenction(Context context) {
        wifi = isWifiNetAvailable(context);
        internet = isInternetAvailable(context);
        if (wifi) {
            return true;
        } else if (internet) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isWifiNetAvailable(Context context) {
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiInfo.isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isInternetAvailable(Context context) {
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isValidEmail(String email) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static Dialog loadingDialog(Context context) {
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.loadingDialogTheme);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static Dialog loadingDialogWithMessage(Context context, String loadingMessage) {
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.loadingDialogTheme);
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void setIsLoggedin(Context context, boolean b, String username, String password, String role, String authToken) {
        SharedPreferences pref = context.getSharedPreferences(LOGIN_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_LOGGEDIN, b);
        editor.putString(LOGIN_USERNAME, username);
        editor.putString(LOGIN_PASSWORD, password);
        editor.putString(ROLE, role);
        editor.putString(AUTHTOKEN, authToken);
        editor.commit();
    }

    public static boolean getIsLoggedin(Context context) {
        SharedPreferences pref = context.getSharedPreferences(LOGIN_PREF_NAME, MODE_PRIVATE);
        return pref.getBoolean(IS_LOGGEDIN, false);
    }

    public static void clearLogincredentials(Context context) {
        SharedPreferences pref = context.getSharedPreferences(LOGIN_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public static void setNotFirstTime(Context context, boolean b) {
        SharedPreferences pref = context.getSharedPreferences(COMMON_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_NOTFIRSTTIME, b);
        editor.commit();
    }

    public static boolean isNotFirstTime(Context context) {
        SharedPreferences pref = context.getSharedPreferences(COMMON_PREF_NAME, MODE_PRIVATE);
        return pref.getBoolean(IS_NOTFIRSTTIME, false);
    }

    public static String getDate(long timpstamp) {
        String date = null;
        try {
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timpstamp);
            date = DateFormat.format("MMM dd, yyyy hh:mm a", cal).toString();
            return date;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    public static String getDateOnly(long timpstamp) {
        String date = null;
        try {
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timpstamp);
            date = DateFormat.format("MMM dd, yyyy", cal).toString();
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    public static String getTime(long timpstamp) {
        String date = null;
        try {
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timpstamp);
            date = DateFormat.format("hh:mm a", cal).toString();
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    public static void setCalendarEnabled(Context context, boolean b) {
        SharedPreferences pref = context.getSharedPreferences(COMMON_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_CALENDAR_PERMISSION_ENABLED, b);
        editor.commit();
    }

    public static boolean isCalendarEnabled(Context context) {
        SharedPreferences pref = context.getSharedPreferences(COMMON_PREF_NAME, MODE_PRIVATE);
        return pref.getBoolean(IS_CALENDAR_PERMISSION_ENABLED, false);
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("EEE d, MMM yyyy hh:mm a");
        String strDate = mdformat.format(calendar.getTime());
        return strDate;
    }

    public static class FourDigitsCardTextWatcher implements TextWatcher {

        private static final char SPACE = ' ';
        private EditText mEditText;

        public FourDigitsCardTextWatcher(EditText editText) {
            mEditText = editText;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mEditText.setError(null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (SPACE == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(SPACE)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(SPACE));
                }
            }
        }
    }

    public static class TwoDigitsCardTextWatcher implements TextWatcher {

        private static final String INITIAL_MONTH_ADD_ON = "0";
        private static final String DEFAULT_MONTH = "01";
        private static final String SPACE = "/";
        private EditText mEditText;
        private int mLength;

        public TwoDigitsCardTextWatcher(EditText editText) {
            mEditText = editText;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mEditText.setError(null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mLength = mEditText.getText().length();
        }

        @Override
        public void afterTextChanged(Editable s) {
            int currentLength = mEditText.getText().length();
            boolean ignoreBecauseIsDeleting = false;
            if (mLength > currentLength) {
                ignoreBecauseIsDeleting = true;
            }

            if (ignoreBecauseIsDeleting && s.toString().startsWith(SPACE)) {
                return;
            }

            if (s.length() == 1 && !isNumberChar(String.valueOf(s.charAt(0)))) {
                mEditText.setText(DEFAULT_MONTH + SPACE);
            } else if (s.length() == 1 && !isCharValidMonth(s.charAt(0))) {
                mEditText.setText(INITIAL_MONTH_ADD_ON + String.valueOf(s.charAt(0)) + SPACE);
            } else if (s.length() == 2 && String.valueOf(s.charAt(s.length() - 1)).equals(SPACE)) {
                mEditText.setText(INITIAL_MONTH_ADD_ON + String.valueOf(s));
            } else if (!ignoreBecauseIsDeleting &&
                    (s.length() == 2 && !String.valueOf(s.charAt(s.length() - 1)).equals(SPACE))) {
                mEditText.setText(mEditText.getText().toString() + SPACE);
            } else if (s.length() == 3 && !String.valueOf(s.charAt(s.length() - 1)).equals(SPACE) && !ignoreBecauseIsDeleting) {
                s.insert(2, SPACE);
                mEditText.setText(String.valueOf(s));
            } else if (s.length() > 3 && !isCharValidMonth(s.charAt(0))) {
                mEditText.setText(INITIAL_MONTH_ADD_ON + s);
            }

            if (!ignoreBecauseIsDeleting) {
                mEditText.setSelection(mEditText.getText().toString().length());
            }
        }

        private boolean isCharValidMonth(char charFromString) {
            int month = 0;
            if (Character.isDigit(charFromString)) {
                month = Integer.parseInt(String.valueOf(charFromString));
            }
            return month <= 1;
        }

        private boolean isNumberChar(String string) {
            return string.matches(".*\\d.*");
        }
    }

}

package com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter;

public class CardItem {

    private String id;
    private String cardType;
    private String cardNumber;

    public CardItem(String id, String cardType, String cardNumber) {
        this.id = id;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        CardItem itemCompare = (CardItem) obj;
        if (itemCompare.getId().equals(this.getId()) && itemCompare.getCardNumber().equals(this.getCardNumber()))
            return true;

        return false;
    }
}

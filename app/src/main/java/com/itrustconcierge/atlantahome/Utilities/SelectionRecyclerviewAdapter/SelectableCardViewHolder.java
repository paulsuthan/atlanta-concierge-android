package com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.R;

public class SelectableCardViewHolder extends RecyclerView.ViewHolder{

    public static final int MULTI_SELECTION = 2;
    public static final int SINGLE_SELECTION = 1;
    public CheckedTextView textView;
    public TextView cardType;
    public SelectableCardItem mItem;
    OnItemSelectedListener itemSelectedListener;


    public SelectableCardViewHolder(View view, OnItemSelectedListener listener) {
        super(view);
        itemSelectedListener = listener;
        textView = (CheckedTextView) view.findViewById(R.id.checked_text_item);
        cardType = (TextView) view.findViewById(R.id.cardType);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItem.isSelected() && getItemViewType() == MULTI_SELECTION) {
                    setChecked(false);
                } else {
                    setChecked(true);
                }
                itemSelectedListener.onItemSelected(mItem);
            }
        });
    }

    public void setChecked(boolean value) {
//        if (value) {
//            textView.setBackgroundColor(Color.LTGRAY);
//        } else {
//            textView.setBackground(null);
//        }
        mItem.setSelected(value);
        textView.setChecked(value);
    }

    public interface OnItemSelectedListener {

        void onItemSelected(SelectableCardItem item);
    }

}

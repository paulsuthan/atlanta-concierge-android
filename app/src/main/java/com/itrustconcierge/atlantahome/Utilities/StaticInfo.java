package com.itrustconcierge.atlantahome.Utilities;

import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.NotificationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;

import java.util.List;

/**
 * Created by Paul on 8/21/2017.
 */

public class StaticInfo {
    public static AuthenticateVo authenticateVoLocal = null;
    public static PropertyVo propertyVoLocal = null;
    public static List<NotificationVo> unReadNotificationVos = null;
    public static List<NotificationVo> unReadJobsNotificationVos = null;
    public static List<NotificationVo> unReadMessagesNotificationVos = null;
    public static boolean isAlreadyLoaded = false;
//    public static List<PropertyVo> propertyVos = null;
}

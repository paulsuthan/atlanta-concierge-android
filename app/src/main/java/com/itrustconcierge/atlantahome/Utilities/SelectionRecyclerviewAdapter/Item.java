package com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter;

/**
 * Created by Paul on 11/15/2017.
 */

public class Item {
    private long id;
    private String name;
    private String surname;

    public Item(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        Item itemCompare = (Item) obj;
        if (itemCompare.getName().equals(this.getName()))
            return true;

        return false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
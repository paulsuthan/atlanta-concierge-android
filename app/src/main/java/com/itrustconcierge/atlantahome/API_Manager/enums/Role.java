package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum Role {
    HO, GC, FRANCHISE, SC, SP;

    public static Role fromString(String value) {
        Role role = null;
        for (Role r : Role.values()) {
            if (r.name().equalsIgnoreCase(value)) {
                role = r;
                break;
            }
        }
        return role;
    }
}




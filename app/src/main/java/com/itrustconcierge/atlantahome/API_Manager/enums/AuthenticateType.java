package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum AuthenticateType {
    TOKEN, FACEBOOK, TWITTER, GPLUS;

    public static AuthenticateType fromString(String value) {
        AuthenticateType authenticateType = null;
        for (AuthenticateType type : AuthenticateType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                authenticateType = type;
                break;
            }
        }
        return authenticateType;
    }
}

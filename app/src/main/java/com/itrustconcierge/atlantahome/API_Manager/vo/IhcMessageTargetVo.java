package com.itrustconcierge.atlantahome.API_Manager.vo;

public class IhcMessageTargetVo extends BaseVo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IhcMessageVo message;
	
	private UserVo target;
	
	private Boolean read;
	
	private Boolean consumed;

	public IhcMessageVo getMessage() {
		return message;
	}

	public void setMessage(IhcMessageVo message) {
		this.message = message;
	}
	
	public UserVo getTarget() {
		return target;
	}

	public void setTarget(UserVo target) {
		this.target = target;
	}
	
	public Boolean isRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean isConsumed() {
		return consumed;
	}

	public void setConsumed(Boolean consumed) {
		this.consumed = consumed;
	}

}

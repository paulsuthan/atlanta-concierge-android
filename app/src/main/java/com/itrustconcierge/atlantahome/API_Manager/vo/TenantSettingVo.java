package com.itrustconcierge.atlantahome.API_Manager.vo;

public class TenantSettingVo extends BaseVo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String displayName;
	
	private String description;
	
	private String logoPath;
	
	private String appLogoPath;
	
	private Double chargePerProperty;
	
	private String websiteUrl;
	
	private String contactEmail;
	
	private String contactMobileNumber;
	
	private String contactPersonName;
	
	private TenantVo tenant;
	
	private Double itrustMargin;
	
	private String tenantPath;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public Double getChargePerProperty() {
		return chargePerProperty;
	}

	public void setChargePerProperty(Double chargePerProperty) {
		this.chargePerProperty = chargePerProperty;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactMobileNumber() {
		return contactMobileNumber;
	}

	public void setContactMobileNumber(String contactMobileNumber) {
		this.contactMobileNumber = contactMobileNumber;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public TenantVo getTenant() {
		return tenant;
	}

	public void setTenant(TenantVo tenant) {
		this.tenant = tenant;
	}

	public Double getItrustMargin() {
		return itrustMargin;
	}

	public void setItrustMargin(Double itrustMargin) {
		this.itrustMargin = itrustMargin;
	}

	public String getAppLogoPath() {
		return appLogoPath;
	}

	public void setAppLogoPath(String appLogoPath) {
		this.appLogoPath = appLogoPath;
	}

	public String getTenantPath() {
		return tenantPath;
	}

	public void setTenantPath(String tenantPath) {
		this.tenantPath = tenantPath;
	}

}

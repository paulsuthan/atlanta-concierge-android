package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.QuoteType;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;

import java.util.List;

public class QuoteVo extends BaseVo {

    private HomeOwnerRequestVo request;

    private UserVo sender;

    private QuoteType type;

    private String description;

    private List<QuoteItemVo> items;

    private Double total;

    private Double discount;

    private Double discountedTotal;

    private Double tax;

    private Double finalTotal;

    private String estimateNumber;

    private String taxType;

    private RequestHistoryType status;

    private Double amountPaid;

    private Double balanceAmount;

    private Double taxAmount;

    private Double homeOwnerPayableAmount;

    private Long paymentId;

    public HomeOwnerRequestVo getRequest() {
        return request;
    }

    public void setRequest(HomeOwnerRequestVo request) {
        this.request = request;
    }

    public UserVo getSender() {
        return sender;
    }

    public void setSender(UserVo sender) {
        this.sender = sender;
    }

    public QuoteType getType() {
        return type;
    }

    public void setType(QuoteType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuoteItemVo> getItems() {
        return items;
    }

    public void setItems(List<QuoteItemVo> items) {
        this.items = items;
    }

    public Double getTotal() {
        return total;
    }


    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getDiscountedTotal() {
        return discountedTotal;
    }

    public void setDiscountedTotal(Double discountedTotal) {
        this.discountedTotal = discountedTotal;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getFinalTotal() {
        return finalTotal;
    }

    public void setFinalTotal(Double finalTotal) {
        this.finalTotal = finalTotal;
    }

//	public static List<QuoteVo> toVos(List<Quote> list){
//		List<QuoteVo> vos =  new ArrayList<QuoteVo>();
//		for(Quote quote : list){
//			vos.add(QuoteVo.toVo(quote, false, true));
//		}
//		return vos;
//	}

    public String getEstimateNumber() {
        return estimateNumber;
    }

    public void setEstimateNumber(String estimateNumber) {
        this.estimateNumber = estimateNumber;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public RequestHistoryType getStatus() {
        return status;
    }

    public void setStatus(RequestHistoryType status) {
        this.status = status;
    }

    public Double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getHomeOwnerPayableAmount() {
        return homeOwnerPayableAmount;
    }

    public void setHomeOwnerPayableAmount(Double homeOwnerPayableAmount) {
        this.homeOwnerPayableAmount = homeOwnerPayableAmount;
    }

//	public static QuoteVo toVo(Quote source, boolean loadChildren, boolean loadRequest){
//		QuoteVo dest = null;
//
//		if(source != null){
//			dest = new QuoteVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setDescription(source.getDescription());
//			dest.setDiscount(source.getDiscount());
//			dest.setDiscountedTotal(source.getDiscountedTotal());
//			dest.setFinalTotal(source.getFinalTotal());
//			dest.setSender(UserVo.toVo(source.getSender(), false));
//			dest.setTax(source.getTax());
//			dest.setTotal(source.getTotal());
//			dest.setType(source.getType());
//			dest.setEstimateNumber(source.getEstimateNumber());
//			dest.setStatus(source.getStatus());
//			dest.setTaxType(source.getTaxType());
//			dest.setAmountPaid(source.getAmountPaid());
//			dest.setBalanceAmount(source.getBalanceAmount());
//			dest.setTaxAmount(source.getTaxAmount());
//			dest.setHomeOwnerPayableAmount(source.getHomeOwnerPayableAmount());
//			dest.setPaymentId(source.getPaymentId());
//			if(loadRequest){
//				dest.setRequest(HomeOwnerRequestVo.toVo(source.getRequest(), false));
//			}
//			if(loadChildren){
//				List<QuoteItemVo> quoteItemVos = new ArrayList<QuoteItemVo>();
//				for(QuoteItem item : source.getItems()){
//					quoteItemVos.add(QuoteItemVo.toVo(item));
//				}
//				dest.setItems(quoteItemVos);
//
//			}
//
//		}
//
//		return dest;
//	}
//
//	public static void merge(Quote dest, QuoteVo source){
//		dest.setDescription(source.getDescription());
//		dest.setDiscount(source.getDiscount());
//		dest.setDiscountedTotal(source.getDiscountedTotal());
//		dest.setFinalTotal(source.getFinalTotal());
//		dest.setTax(source.getTax());
//		dest.setTotal(source.getTotal());
//		dest.setType(source.getType());
//		dest.setEstimateNumber(source.getEstimateNumber());
//		dest.setStatus(source.getStatus());
//		dest.setTaxType(source.getTaxType());
//		dest.setAmountPaid(source.getAmountPaid());
//		dest.setBalanceAmount(source.getBalanceAmount());
//		dest.setTaxAmount(source.getTaxAmount());
//		dest.setHomeOwnerPayableAmount(source.getHomeOwnerPayableAmount());
//	}

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

}

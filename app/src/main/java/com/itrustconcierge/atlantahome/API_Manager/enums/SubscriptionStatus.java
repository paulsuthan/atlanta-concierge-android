package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum SubscriptionStatus {
    NOT_SUBSCRIBED, SUBSCRIBED, EXPIRED;

    public static SubscriptionStatus fromString(String value) {
        SubscriptionStatus subscriptionStatus = null;
        for (SubscriptionStatus status : SubscriptionStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                subscriptionStatus = status;
                break;
            }
        }
        return subscriptionStatus;
    }
}

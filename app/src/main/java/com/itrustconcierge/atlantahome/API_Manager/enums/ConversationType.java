package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum ConversationType {

    CHAT, GROUP_CHAT, REQUEST_CHAT;

    public static ConversationType fromString(String value) {
        ConversationType messageType = null;
        for (ConversationType type : ConversationType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                messageType = type;
                break;
            }
        }
        return messageType;
    }

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.ArrayList;
import java.util.List;

public class PropertyFloorSizeVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private long minimum;

    private long maximum;

    private String value;

    private String displayName;

    public long getMinimum() {
        return minimum;
    }

    public void setMinimum(long minimum) {
        this.minimum = minimum;
    }

    public long getMaximum() {
        return maximum;
    }

    public void setMaximum(long maximum) {
        this.maximum = maximum;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

//	public static PropertyFloorSizeVo toVo(PropertyFloorSize source){
//		PropertyFloorSizeVo dest = null;
//		if(source != null){
//			dest = new PropertyFloorSizeVo();
//			dest.setId(source.getId());
//			dest.setDisplayName(source.getDisplayName());
//			dest.setMaximum(source.getMaximum());
//			dest.setMinimum(source.getMinimum());
//			dest.setValue(source.getValue());
//		}
//
//		return dest;
//	}
//
//	public static List<PropertyFloorSizeVo> toVos(List<PropertyFloorSize> list){
//		List<PropertyFloorSizeVo> vos = new ArrayList<PropertyFloorSizeVo>();
//		for(PropertyFloorSize fS : list){
//			vos.add(PropertyFloorSizeVo.toVo(fS));
//		}
//		return vos;
//	}

}

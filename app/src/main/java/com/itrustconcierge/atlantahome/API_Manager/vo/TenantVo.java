package com.itrustconcierge.atlantahome.API_Manager.vo;

public class TenantVo extends BaseVo{
	/**
	 * 
	 */
	
	private String name;
	private String companyName;
	private String address;	
	private String portalUri;
	private Integer numberOfUsers;
	private String tenantCode;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPortalUri() {
		return portalUri;
	}
	public void setPortalUri(String portalUri) {
		this.portalUri = portalUri;
	}

	public Integer getNumberOfUsers() {
		return numberOfUsers;
	}
	public void setNumberOfUsers(Integer numberOfUsers) {
		this.numberOfUsers = numberOfUsers;
	}
	
	@Override
	public String toString() {
		return "TenantVo [name=" + name + ", companyName=" + companyName + ", address=" + address + ", portalUri="
				+ portalUri + ", numberOfUsers=" + numberOfUsers + "]";
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
}

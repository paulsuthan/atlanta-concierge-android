package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum UploadType {
    SP_DOCUMENTS, GALLERY, CHAT, REQUEST, PROFILE_PIC, PROPERTY, STRIPE_DOCUMENTS;

    public static UploadType fromString(String value) {
        UploadType authenticateType = null;
        for (UploadType type : UploadType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                authenticateType = type;
                break;
            }
        }
        return authenticateType;
    }
}

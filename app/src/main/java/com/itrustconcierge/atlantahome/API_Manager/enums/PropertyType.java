package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum PropertyType {
    SINGLE_FAMILY_RESIDENCE, CONDO, APARTMENT, TOWNHOME;

    public static PropertyType fromString(String value) {
        PropertyType propertyType = null;
        for (PropertyType type : PropertyType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                propertyType = type;
                break;
            }
        }
        return propertyType;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.List;

public class IhcMessageVo extends BaseVo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserVo sender;
	
	private String message;
	
	private List<IhcMessageTargetVo> targets;
	
	private List<IhcMessageFileVo> files;

	public UserVo getSender() {
		return sender;
	}

	public void setSender(UserVo sender) {
		this.sender = sender;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<IhcMessageTargetVo> getTargets() {
		return targets;
	}

	public void setTargets(List<IhcMessageTargetVo> targets) {
		this.targets = targets;
	}

	public List<IhcMessageFileVo> getFiles() {
		return files;
	}

	public void setFiles(List<IhcMessageFileVo> files) {
		this.files = files;
	}
}

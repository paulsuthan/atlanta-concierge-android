package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.List;

public class IdListVo {

    private List<Long> list;

    public List<Long> getList() {
        return list;
    }

    public void setList(List<Long> list) {
        this.list = list;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;

public class RequestHistoryVo extends BaseVo implements Comparable<RequestHistoryVo> {

    private static final long serialVersionUID = 1L;

    private HomeOwnerRequestVo request;

    private UserVo sender;

    private UserVo assignedTo;

    private UserVo assignedBy;

    private RequestHistoryType type;

    private String description;

    private QuoteVo quote;

    private String image;

    private Long scheduleId;

    public HomeOwnerRequestVo getRequest() {
        return request;
    }

    public void setRequest(HomeOwnerRequestVo request) {
        this.request = request;
    }

    public UserVo getSender() {
        return sender;
    }

    public void setSender(UserVo sender) {
        this.sender = sender;
    }

    public RequestHistoryType getType() {
        return type;
    }

    public void setType(RequestHistoryType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public QuoteVo getQuote() {
        return quote;
    }

    public void setQuote(QuoteVo quote) {
        this.quote = quote;
    }

//	public static RequestHistoryVo toVo(RequestHistory source, boolean loadParent){
//		RequestHistoryVo dest = null;
//
//		if(source != null){
//			dest = new RequestHistoryVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setQuote(QuoteVo.toVo(source.getQuote(), false, true));
//			dest.setSender(UserVo.toVo(source.getSender(), false));
//			dest.setDescription(source.getDescription());
//			dest.setType(source.getType());
//			dest.setImage(source.getImage());
//			if(loadParent){
//				dest.setRequest(HomeOwnerRequestVo.toVo(source.getRequest(), false));
//			}
//
//
//		}
//
//		return dest;
//	}
//
//	public static List<RequestHistoryVo> toVOs(List<RequestHistory> list, boolean loadParent){
//		List<RequestHistoryVo> vos = new ArrayList<RequestHistoryVo>();
//		if(list != null){
//			for(RequestHistory history : list){
//				vos.add(RequestHistoryVo.toVo(history, loadParent));
//			}
//		}
//		Collections.sort(vos);
//		return vos;
//	}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int compareTo(RequestHistoryVo o) {

        if (o.getCreatedOn() == null || this.getCreatedOn() == null) {
            return 0;
        }

        return o.getCreatedOn().compareTo(this.getCreatedOn());
    }


    public UserVo getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(UserVo assignedTo) {
        this.assignedTo = assignedTo;
    }

    public UserVo getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(UserVo assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }
}

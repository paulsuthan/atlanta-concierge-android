package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.ConversationType;

import java.util.List;

public class ConversationVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String name;

    private String description;

    private ConversationType type = ConversationType.CHAT;

    private Long typeId;

    private UserVo source;

    private UserVo target;

    private MessageVo lastMessage;

    private HomeOwnerRequestVo request;

    private Boolean consumed = false;

    private List<ConversationParticipantVo> participants;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public UserVo getSource() {
        return source;
    }

    public void setSource(UserVo source) {
        this.source = source;
    }

    public UserVo getTarget() {
        return target;
    }

    public void setTarget(UserVo target) {
        this.target = target;
    }

//	public static ConversationVo toVo(Conversation source){
//		ConversationVo dest = null;
//		if(source != null){
//			dest = new ConversationVo();
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setId(source.getId());
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//			dest.setSource(UserVo.toVo(source.getSource(), false));
//			dest.setTarget(UserVo.toVo(source.getTarget(), false));
//			dest.setLastMessage(MessageVo.toVo(source.getLastMessage(), false));
//		}
//		return dest;
//	}
//
//	public static List<ConversationVo> toVos(List<Conversation> list){
//		List<ConversationVo> vos = new ArrayList<ConversationVo>();
//		for(Conversation con : list){
//			vos.add(ConversationVo.toVo(con));
//		}
//		return vos;
//	}

    public MessageVo getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessageVo lastMessage) {
        this.lastMessage = lastMessage;
    }

    public HomeOwnerRequestVo getRequest() {
        return request;
    }

    public void setRequest(HomeOwnerRequestVo request) {
        this.request = request;
    }

    public Boolean getConsumed() {
        return consumed;
    }

    public void setConsumed(Boolean consumed) {
        this.consumed = consumed;
    }

    public List<ConversationParticipantVo> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ConversationParticipantVo> participants) {
        this.participants = participants;
    }
}

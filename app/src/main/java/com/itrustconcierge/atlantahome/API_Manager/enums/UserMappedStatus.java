package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum UserMappedStatus {
    NOT_MAPPED, PENDING, APPROVED;

    public static UserMappedStatus fromString(String value) {
        UserMappedStatus userMappedStatus = null;
        for (UserMappedStatus status : UserMappedStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                userMappedStatus = status;
                break;
            }
        }
        return userMappedStatus;
    }
}

package com.itrustconcierge.atlantahome.API_Manager;

import java.util.List;
import java.util.Map;

/**
 * Created by paul on 8/3/2017.
 */

public class ResponseWrapper<T> {
    private List<T> list;
    private T data;
    private String appName;
    private String version;
    private Map<String, T> error;

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseWrapper(List<T> list) {
        this.list = list;
    }

    public ResponseWrapper(T obj) {
        this.data = obj;
    }

    public List<T> getList() {
        return list;
    }

    public T getData() {
        return data;
    }

    public String getAppName() {
        return appName;
    }

    public String getVersion() {
        return version;
    }

    public Map<String, T> getError() {
        return error;
    }

    public void setError(Map<String, T> error) {
        this.error = error;
    }

}
package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;

import java.util.ArrayList;
import java.util.List;

public class FranchisePackageVo extends BaseVo {
    private String name;
    private String description;
    private Double amount;
    private Integer time;
    private TimeUnit timeUnit;
    private Boolean active;
    private UserVo franchise;
    private String serviceIdCsv;

    private List<PackageServiceVo> services;

    private Double discount;

    private Double yearlyAmount;

    private Double monthlyAdditionalMargin;

    private Double monthlyAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public UserVo getFranchise() {
        return franchise;
    }

    public void setFranchise(UserVo franchise) {
        this.franchise = franchise;
    }

    public String getServiceIdCsv() {
        return serviceIdCsv;
    }

    public void setServiceIdCsv(String serviceIdCsv) {
        this.serviceIdCsv = serviceIdCsv;
    }

    //	public static List<FranchisePackageVo> toVoList(List<FranchisePackage> list){
//		final List<FranchisePackageVo> voList = new ArrayList<>();
//		if (list != null) {
//			list.forEach(franchisePackage->{
//				voList.add(toVo(franchisePackage, null));
//			});
//		}
//		return voList;
//	}
//
//	public static FranchisePackageVo toVo(FranchisePackage source, List<FranchiseCategoryVo> franchiseCategoryList ) {
//		FranchisePackageVo dest =null;
//		if ( source != null ) {
//			dest = new FranchisePackageVo();
//			dest.setId(source.getId());
//			dest.setActive(source.getActive());
//			dest.setAmount(source.getAmount());
//			dest.setDescription(source.getDescription());
//			dest.setName(source.getName());
//			dest.setTime(source.getTime());
//			dest.setTimeUnit(source.getTimeUnit());
//			dest.setDiscount(source.getDiscount());
//			dest.setMonthlyAdditionalMargin(source.getMonthlyAdditionalMargin());
//			dest.setMonthlyAmount(source.getMonthlyAmount());
//			dest.setYearlyAmount(source.getYearlyAmount());
//			List<PackageServiceVo> services = new ArrayList<PackageServiceVo>();
//			for(PackageService pS : source.getServices()){
//				services.add(PackageServiceVo.toVo(pS));
//
//			}
//			dest.setServices(services);
//		}
//		return dest;
//	}
//
//	public static List<FranchisePackage> toDaoList(List<FranchisePackageVo> list){
//		final List<FranchisePackage> daoList = new ArrayList<>();
//		if (list != null) {
//			list.forEach(franchisePackage->{
//				daoList.add(toDao(franchisePackage));
//			});
//		}
//		return daoList;
//	}
//
//	public static void merge(FranchisePackage dest, FranchisePackageVo source){
//
//		dest.setActive(source.getActive());
//		dest.setAmount(source.getAmount());
//		dest.setDescription(source.getDescription());
//		dest.setName(source.getName());
//		dest.setTime(source.getTime());
//		dest.setTimeUnit(source.getTimeUnit());
//		dest.setDiscount(source.getDiscount());
//		dest.setMonthlyAdditionalMargin(source.getMonthlyAdditionalMargin());
//		dest.setMonthlyAmount(source.getMonthlyAmount());
//		dest.setYearlyAmount(source.getYearlyAmount());
//
//	}
//
//	public static FranchisePackage toDao(FranchisePackageVo franchisePackage) {
//		FranchisePackage dao =null;
//		if ( franchisePackage != null ) {
//			dao = new FranchisePackage();
//			dao.setActive(franchisePackage.getActive());
//			dao.setAmount(franchisePackage.getAmount());
//			dao.setDescription(franchisePackage.getDescription());
//			dao.setServiceIdsCsv(franchisePackage.getServiceIdCsv());
//			dao.setName(franchisePackage.getName());
//			dao.setTime(franchisePackage.getTime());
//			dao.setTimeUnit(franchisePackage.getTimeUnit());
//		}
//		return dao;
//	}
    public List<PackageServiceVo> getServices() {
        return services;
    }

    public void setServices(List<PackageServiceVo> services) {
        this.services = services;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getYearlyAmount() {
        return yearlyAmount;
    }

    public void setYearlyAmount(Double yearlyAmount) {
        this.yearlyAmount = yearlyAmount;
    }

    public Double getMonthlyAdditionalMargin() {
        return monthlyAdditionalMargin;
    }

    public void setMonthlyAdditionalMargin(Double monthlyAdditionalMargin) {
        this.monthlyAdditionalMargin = monthlyAdditionalMargin;
    }

    public Double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(Double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }
}

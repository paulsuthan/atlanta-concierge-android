package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.ArrayList;
import java.util.List;

public class ConversationParticipantVo extends BaseVo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserVo participant;
	
	private boolean owner;
	
	private boolean active = true;
	
	private String lastMessage;
	
	private Boolean consumed = false;
	
	private UserVo source;

	public UserVo getParticipant() {
		return participant;
	}

	public void setParticipant(UserVo participant) {
		this.participant = participant;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(String lastMessage) {
		this.lastMessage = lastMessage;
	}

	public Boolean getConsumed() {
		return consumed;
	}

	public void setConsumed(Boolean consumed) {
		this.consumed = consumed;
	}

	public UserVo getSource() {
		return source;
	}

	public void setSource(UserVo source) {
		this.source = source;
	}
 }

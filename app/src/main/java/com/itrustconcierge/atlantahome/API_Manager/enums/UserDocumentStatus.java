package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum UserDocumentStatus {
    AVAILABLE, REQUEST;

    public static UserDocumentStatus fromString(String value) {
        UserDocumentStatus status = null;
        for (UserDocumentStatus type : UserDocumentStatus.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                status = type;
                break;
            }
        }
        return status;
    }

}

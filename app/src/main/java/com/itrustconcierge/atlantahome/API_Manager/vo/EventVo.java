package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.CalendarType;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventType;

import java.util.List;


public class EventVo extends BaseVo {

    /**
     *
     */
    private String title;
    private AddressVo address;
    private long startTime;
    private long endTime;
    private String description;
    private List<ParticipantVo> participants;
    private EventStatus status;
    private EventType eventType;
    private Boolean showAsBusy;
    private String eventId;
    private CalendarType calendarType;
    private Long subject;
    private ScheduleVo schedule;

    public EventVo(long id, String eventId, String description, long startTime, long endTime, String title, List<ParticipantVo> participants, AddressVo address) {
        super();
        this.setId(id);
        this.eventId = eventId;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.title = title;
        this.participants = participants;
        this.address = address;
    }

    public EventVo() {

    }


    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AddressVo getAddress() {
        return address;
    }

    public void setAddress(AddressVo address) {
        this.address = address;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ParticipantVo> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantVo> participants) {
        this.participants = participants;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Boolean getShowAsBusy() {
        return showAsBusy;
    }

    public void setShowAsBusy(Boolean showAsBusy) {
        this.showAsBusy = showAsBusy;
    }

    public CalendarType getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(CalendarType calendarType) {
        this.calendarType = calendarType;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }

    public ScheduleVo getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleVo schedule) {
        this.schedule = schedule;
    }
}

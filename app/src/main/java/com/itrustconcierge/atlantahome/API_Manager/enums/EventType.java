package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum EventType {
    ALL_DAY, FIXED, RECURRING;

    public static EventType fromString(String value) {
        EventType eventType = null;
        for (EventType type : EventType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                eventType = type;
                break;
            }
        }
        return eventType;
    }
}

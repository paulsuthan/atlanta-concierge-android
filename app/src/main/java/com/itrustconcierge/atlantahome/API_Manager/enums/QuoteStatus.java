package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum QuoteStatus {
    QUOTE, APPROVED, REJECTED, INVOICE, PART_PAYMENT, PAID, CANCELLED;

    public static QuoteStatus fromString(String value) {
        QuoteStatus quoteItemType = null;
        for (QuoteStatus type : QuoteStatus.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                quoteItemType = type;
                break;
            }
        }
        return quoteItemType;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum TimeUnit {
    SECONS, MINUTES, HOUR, DAY, WEEK, MONTH, QUARTER, SEMI_ANNUAL, ANNUAL;

    public static TimeUnit fromString(String value) {
        TimeUnit timeUnit = null;
        for (TimeUnit unit : TimeUnit.values()) {
            if (unit.name().equalsIgnoreCase(value)) {
                timeUnit = unit;
                break;
            }
        }
        return timeUnit;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.ArrayList;
import java.util.List;

public class ServiceVo extends BaseVo {

    /**
     *
     */
    private String name;

    private String description;

    private String iconUrl;

    private FranchiseCategoryVo franchiseCategory;

    private boolean selected;

    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public FranchiseCategoryVo getFranchiseCategory() {
        return franchiseCategory;
    }

    public void setFranchiseCategory(FranchiseCategoryVo franchiseCategory) {
        this.franchiseCategory = franchiseCategory;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}

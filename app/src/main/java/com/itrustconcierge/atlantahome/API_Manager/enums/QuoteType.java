package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum QuoteType {

    QUOTE, INVOICE;

    public static QuoteType fromString(String value) {
        QuoteType quoteType = null;
        for (QuoteType type : QuoteType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                quoteType = type;
                break;
            }
        }
        return quoteType;
    }

}

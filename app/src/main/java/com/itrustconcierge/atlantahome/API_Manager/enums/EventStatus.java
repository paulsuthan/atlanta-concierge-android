package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum EventStatus {
    PENDING, STARTED, CLOSED, CANCELLED, PROCESSING, MOVED;

    public static EventStatus fromString(String value) {
        EventStatus eventStatus = null;
        for (EventStatus status : EventStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                eventStatus = status;
                break;
            }
        }
        return eventStatus;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.UserDocumentStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDocumentVo extends BaseVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -792816766297533037L;

    private DocumentTypeVo documentType;

    private String name;

    private String description;

    private Long expiryDate;

    private String authority;

    private String policyNumber;

    private Double value;

    private String status;

    private String state;

    private String city;

    private Long userId;

    private UserDocumentStatus documentStatus;

    private List<DocAttachmentVo> docAttachmentList;

    public DocumentTypeVo getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeVo documentType) {
        this.documentType = documentType;
    }

    public Long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<DocAttachmentVo> getDocAttachmentList() {
        return docAttachmentList;
    }

    public void setDocAttachmentList(List<DocAttachmentVo> docAttachmentList) {
        this.docAttachmentList = docAttachmentList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserDocumentStatus getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(UserDocumentStatus documentStatus) {
        this.documentStatus = documentStatus;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

}

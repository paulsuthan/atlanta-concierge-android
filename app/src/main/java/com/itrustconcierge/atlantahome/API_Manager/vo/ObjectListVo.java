package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.List;

public class ObjectListVo<T> {

    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}

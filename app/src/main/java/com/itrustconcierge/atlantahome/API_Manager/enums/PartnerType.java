package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum PartnerType {
    HOME_DEPO;

    public static PartnerType fromString(String value) {
        PartnerType partnerType = null;
        for (PartnerType type : PartnerType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                partnerType = type;
                break;
            }
        }
        return partnerType;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.ArrayList;
import java.util.List;

public class FranchiseServiceVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private FranchiseCategoryVo franchiseCategory;

    private ServiceVo service;

    private Double minHourlyRate;

    private Double fifteenMinIncrementRate;

    private String description;

    public FranchiseCategoryVo getFranchiseCategory() {
        return franchiseCategory;
    }

    public void setFranchiseCategory(FranchiseCategoryVo franchiseCategory) {
        this.franchiseCategory = franchiseCategory;
    }

    public ServiceVo getService() {
        return service;
    }

    public void setService(ServiceVo service) {
        this.service = service;
    }

    public Double getFifteenMinIncrementRate() {
        return fifteenMinIncrementRate;
    }

    public void setFifteenMinIncrementRate(Double fifteenMinIncrementRate) {
        this.fifteenMinIncrementRate = fifteenMinIncrementRate;
    }

    //	public static List<FranchiseServiceVo> toVoList( List<FranchiseService> list ){
//		final List<FranchiseServiceVo> voList = new ArrayList<>();
//		if (list != null ) {
//			list.forEach(franchiseService->{
//				voList.add(toVo(franchiseService));
//			});
//		}
//		return voList;
//	}
//
//	public static FranchiseServiceVo toVo( FranchiseService source){
//		FranchiseServiceVo dest = null;
//		if (source != null ) {
//			dest = new FranchiseServiceVo();
//			dest.setId(source.getId());
//			dest.setService(ServiceVo.toVo(source.getService()));
//			dest.setFifteenMinIncrementRate(source.getFifteenMinIncrementRate());
//			dest.setMinHourlyRate(source.getMinHourlyRate());
//			dest.setDescription(source.getDescription());
//		}
//		return dest;
//	}
    public Double getMinHourlyRate() {
        return minHourlyRate;
    }

    public void setMinHourlyRate(Double minHourlyRate) {
        this.minHourlyRate = minHourlyRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

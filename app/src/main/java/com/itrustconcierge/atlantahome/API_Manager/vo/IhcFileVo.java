package com.itrustconcierge.atlantahome.API_Manager.vo;

public class IhcFileVo extends BaseVo{
	
	/**
	 * 
	 */

	private String name;
	
	private String physicalFileName;
	
	private String mimeType;
	
	private String description;
	
	private String docType;
	
	private String type;
	
	private Long typeId;
	
	private UserVo uploadedBy;
	
	private boolean deleted;
	
	private UserVo lastUpdatedBy;
	
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhysicalFileName() {
		return physicalFileName;
	}

	public void setPhysicalFileName(String physicalFileName) {
		this.physicalFileName = physicalFileName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public UserVo getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(UserVo uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public UserVo getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(UserVo lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

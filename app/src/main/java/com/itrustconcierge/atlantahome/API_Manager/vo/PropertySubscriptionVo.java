package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;

public class PropertySubscriptionVo extends BaseVo {

    private PropertyPackageVo propertyPackage;

    private Integer duaration;
    private Double amount;
    private Long startTime;
    private Long expiryTime;

    private Long nextMaintenceDate;

    private PropertyVo property;

    private TimeUnit durationType;

    private Long lastReminder;
    private Long nextReminder;

    private PaymentVo payment;

    public Integer getDuaration() {
        return duaration;
    }

    public void setDuaration(Integer duaration) {
        this.duaration = duaration;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Long expiryTime) {
        this.expiryTime = expiryTime;
    }

    public PropertyVo getProperty() {
        return property;
    }

    public void setProperty(PropertyVo property) {
        this.property = property;
    }

    public TimeUnit getDurationType() {
        return durationType;
    }

    public void setDurationType(TimeUnit durationType) {
        this.durationType = durationType;
    }

//	public static PropertySubscriptionVo toVo(PropertySubscription source){
//		PropertySubscriptionVo dest = null;
//		if(source != null){
//			dest = new PropertySubscriptionVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setAmount(source.getAmount());
//			dest.setDuaration(source.getDuaration());
//			dest.setDurationType(source.getDurationType());
//			dest.setPropertyPackage(PropertyPackageVo.toVo(source.getPropertyPackage(), null));
//			dest.setStartTime(source.getStartTime());
//			dest.setExpiryTime(source.getExpiryTime());
//		}
//
//		return dest;
//	}
//
//	public static List<PropertySubscriptionVo>  toVos(List<PropertySubscription> sourceList){
//		List<PropertySubscriptionVo> destList = new ArrayList<PropertySubscriptionVo>();
//		PropertySubscriptionVo dest = null;
//		for(PropertySubscription source: sourceList){
//			dest = new PropertySubscriptionVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setAmount(source.getAmount());
//			dest.setDuaration(source.getDuaration());
//			dest.setDurationType(source.getDurationType());
//			dest.setPropertyPackage(PropertyPackageVo.toVo(source.getPropertyPackage(), null));
//			dest.setStartTime(source.getStartTime());
//			dest.setExpiryTime(source.getExpiryTime());
//			destList.add(dest);
//		}
//
//		return destList;
//	}
//
//	public static void merge(PropertySubscription dest, PropertySubscriptionVo source){
//		dest.setAmount(source.getAmount());
//		dest.setDuaration(source.getDuaration());
//		dest.setDurationType(source.getDurationType());
//		dest.setStartTime(source.getStartTime());
//		dest.setExpiryTime(source.getExpiryTime());
//	}

    public PropertyPackageVo getPropertyPackage() {
        return propertyPackage;
    }

    public void setPropertyPackage(PropertyPackageVo propertyPackage) {
        this.propertyPackage = propertyPackage;
    }


    public Long getNextMaintenceDate() {
        return nextMaintenceDate;
    }

    public void setNextMaintenceDate(Long nextMaintenceDate) {
        this.nextMaintenceDate = nextMaintenceDate;
    }

    public Long getLastReminder() {
        return lastReminder;
    }

    public void setLastReminder(Long lastReminder) {
        this.lastReminder = lastReminder;
    }

    public Long getNextReminder() {
        return nextReminder;
    }

    public void setNextReminder(Long nextReminder) {
        this.nextReminder = nextReminder;
    }

    public PaymentVo getPayment() {
        return payment;
    }

    public void setPayment(PaymentVo payment) {
        this.payment = payment;
    }
}

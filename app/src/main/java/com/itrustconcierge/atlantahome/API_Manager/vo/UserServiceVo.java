package com.itrustconcierge.atlantahome.API_Manager.vo;

public class UserServiceVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private Long userId;

    private ServiceVo service;

    private boolean active;


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

//	public static UserServiceVo toVo(FranchiseUserService source){
//
//		UserServiceVo dest = null;
//		if(source != null){
//			dest = new UserServiceVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setActive(source.isActive());
//			dest.setService(ServiceVo.toVo(source.getService()));
//		}
//		return dest;
//	}
//
//	public static void merge(FranchiseUserService dest, UserServiceVo source){
//		dest.setActive(source.isActive());
//	}

    public ServiceVo getService() {
        return service;
    }

    public void setService(ServiceVo service) {
        this.service = service;
    }

}

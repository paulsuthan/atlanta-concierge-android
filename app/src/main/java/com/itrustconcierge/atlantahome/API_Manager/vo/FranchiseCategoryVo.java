package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.List;

public class FranchiseCategoryVo extends BaseVo {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String name;
    private String icon;
    private UserVo franchise;
    private List<ServiceVo> services;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public UserVo getFranchise() {
        return franchise;
    }

    public void setFranchise(UserVo franchise) {
        this.franchise = franchise;
    }


//	public static List<FranchiseCategoryVo> toVoList(List<FranchiseCategory> franchiseCategoryList, Boolean includeService){
//		final List<FranchiseCategoryVo> voList = new ArrayList<>();
//		if (franchiseCategoryList!=null) {
//			franchiseCategoryList.forEach(franchiseCategory->{
//				voList.add(toVo(franchiseCategory, includeService));
//			});
//		}
//		return voList;
//	}
//
//	public static FranchiseCategoryVo toVo(FranchiseCategory franchiseCategory, Boolean includeService){
//		FranchiseCategoryVo vo = new FranchiseCategoryVo();
//		if (franchiseCategory!=null) {
//			vo = new FranchiseCategoryVo();
//			vo.setIcon(franchiseCategory.getIcon());
//			vo.setId(franchiseCategory.getId());
//			vo.setName(franchiseCategory.getName());
//			if (includeService) {
//				vo.setServices(ServiceVo.toVoList(franchiseCategory.getServices()));
//			}
//		}
//		return vo;
//	}

    public List<ServiceVo> getServices() {
        return services;
    }

    public void setServices(List<ServiceVo> services) {
        this.services = services;
    }
}

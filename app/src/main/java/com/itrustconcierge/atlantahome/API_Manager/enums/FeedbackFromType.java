package com.itrustconcierge.atlantahome.API_Manager.enums;

/**
 * Created by Paul on 1/31/2018.
 */

public enum FeedbackFromType {
    HO, GC, REALTOR, SP, ANONYMOUS;

    public static FeedbackType fromString(String value) {
        FeedbackType feedbackType = null;
        for (FeedbackType type : FeedbackType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                feedbackType = type;
                break;
            }
        }
        return feedbackType;
    }

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.RateIncrementType;

public class PackageServiceVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private FranchisePackageVo franchisePackage;

    private FranchiseServiceVo service;

    private RateIncrementType rateIncrementType;

    private Double incrementRate;

    private Double increment;

    private Double total;
    private boolean active;

    public FranchisePackageVo getFranchisePackage() {
        return franchisePackage;
    }

    public void setFranchisePackage(FranchisePackageVo franchisePackage) {
        this.franchisePackage = franchisePackage;
    }

    public FranchiseServiceVo getService() {
        return service;
    }

    public void setService(FranchiseServiceVo service) {
        this.service = service;
    }

    public RateIncrementType getRateIncrementType() {
        return rateIncrementType;
    }

    public void setRateIncrementType(RateIncrementType rateIncrementType) {
        this.rateIncrementType = rateIncrementType;
    }

    public Double getIncrementRate() {
        return incrementRate;
    }

    public void setIncrementRate(Double incrementRate) {
        this.incrementRate = incrementRate;
    }

    public Double getIncrement() {
        return increment;
    }

    public void setIncrement(Double increment) {
        this.increment = increment;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

//	public static PackageServiceVo toVo(PackageService source){
//		PackageServiceVo dest = null;
//		if(source != null){
//			dest = new PackageServiceVo();
//			dest.setId(source.getId());
//			dest.setService(FranchiseServiceVo.toVo(source.getService()));
//			dest.setIncrement(source.getIncrement());
//			dest.setIncrementRate(source.getIncrementRate());
//			dest.setRateIncrementType(source.getRateIncrementType());
//			dest.setTotal(source.getTotal());
//			dest.setActive(source.isActive());
//		}
//
//		return dest;
//	}
//
//	public static void merge(PackageService dest, PackageServiceVo source){
//		dest.setIncrement(source.getIncrement());
//		dest.setIncrementRate(source.getIncrementRate());
//		dest.setRateIncrementType(source.getRateIncrementType());
//		dest.setTotal(source.getTotal());
//		dest.setActive(source.isActive());
//	}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


}

package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum ParticipantType {
    HO, SP;

    public static ParticipantType fromString(String value) {
        ParticipantType participantType = null;
        for (ParticipantType type : ParticipantType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                participantType = type;
                break;
            }
        }
        return participantType;
    }
}

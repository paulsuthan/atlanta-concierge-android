package com.itrustconcierge.atlantahome.API_Manager.enums;


public enum NotificationType {

    FRANCHISE_MESSAGE, BLVI_DOCUMENT_REQUESTED, BLVI_DOCUMENT_CREATED, BLVI_DOCUMENT_UPDATED, BLVI_DOCUMENT_EXPIRED, VIDEO_CHAT,
    QUOTE, DENY_REQUEST, MESSAGE, REQUEST, JOB_STARTED, JOB_COMPLETED, QUOTE_APPROVED, INVOICE, PAYMENT, REQUEST_MESSAGE, LEAD,
    INVOICE_APPROVED, USER_CREATED, VISIT_REQUESTED, VISIT_SCHEDULED, JOB_SCHEDULE_REQUESTED, JOB_SCHEDULE_CONFIRMED, HO_SCHEDULE, PACKAGE, INVENTORY_REVIEWED, REQUEST_CHAT, REQUEST_CLOSE, REQUEST_DELETED;;

    public static NotificationType fromString(String value) {
        NotificationType notificationType = null;
        for (NotificationType type : NotificationType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                notificationType = type;
                break;
            }
        }
        return notificationType;
    }

}

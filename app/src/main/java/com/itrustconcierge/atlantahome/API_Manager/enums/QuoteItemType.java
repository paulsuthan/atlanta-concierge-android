package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum QuoteItemType {
    PRODUCT, SERVICE;

    public static QuoteItemType fromString(String value) {
        QuoteItemType quoteItemType = null;
        for (QuoteItemType type : QuoteItemType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                quoteItemType = type;
                break;
            }
        }
        return quoteItemType;
    }


}

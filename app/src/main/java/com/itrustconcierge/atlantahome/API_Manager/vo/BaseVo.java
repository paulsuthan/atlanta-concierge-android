package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.io.Serializable;
import java.util.Date;

public class BaseVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8065313814994098641L;

    private Long id;

    private Long lastUpdatedOn;

    private Long createdOn;

    private Long userTenantId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Long lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public Long getUserTenantId() {
        return userTenantId;
    }

    public void setUserTenantId(Long userTenantId) {
        this.userTenantId = userTenantId;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

public class ServiceDetailVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    private String name;
    private String description;
    private String iconUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

}

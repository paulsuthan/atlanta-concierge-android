package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.io.Serializable;

public class DocAttachmentVo extends BaseVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8919935789068987293L;

    private String name;
    private String filename;
    private String physicalFilename;
    private String mimeType;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPhysicalFilename() {
        return physicalFilename;
    }

    public void setPhysicalFilename(String physicalFilename) {
        this.physicalFilename = physicalFilename;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

//	public static DocAttachmentVo toVo(Attachment source) {
//		DocAttachmentVo dest = null;
//		if (source != null) {
//			dest = new DocAttachmentVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setName(source.getName());
//			dest.setFilename(source.getFilename());
//			dest.setPhysicalFilename(source.getPhysicalFilename());
//			dest.setMimeType(source.getPhysicalFilename());
//		}
//		return dest;
//
//	}
//
//	public static Attachment toDao(DocAttachmentVo source, Document document) {
//
//		Attachment dest = null;
//		if (source != null) {
//			dest = new Attachment();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setName(source.getName());
//			dest.setFilename(source.getFilename());
//			dest.setPhysicalFilename(source.getPhysicalFilename());
//			dest.setMimeType(source.getPhysicalFilename());
//			dest.setDocument(document);
//		}
//		return dest;
//
//	}
//
//	public static List<DocAttachmentVo> toVoList(List<Attachment> sourceList) {
//		List<DocAttachmentVo> destList = new ArrayList<>();
//		if (sourceList != null && (!sourceList.isEmpty())) {
//			sourceList.forEach(source -> {
//				destList.add(toVo(source));
//			});
//		}
//		return destList;
//	}
//
//	public static List<Attachment> toDaoList(List<DocAttachmentVo> sourceList, Document document) {
//
//		List<Attachment> destList = new ArrayList<>();
//		if (sourceList != null && (!sourceList.isEmpty())) {
//			sourceList.forEach(source -> {
//				destList.add(toDao(source, document));
//			});
//		}
//		return destList;
//	}

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

public class CompanyVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String name;

    private String description;

    private String fax;

    private String mobileNumber;

    private String email;

    private String website;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

//	public static CompanyVo toVo(Company source){
//		CompanyVo dest = null;
//		if(source != null){
//			dest = new CompanyVo();
//			dest.setDescription(source.getDescription());
//			dest.setEmail(source.getEmail());
//			dest.setFax(source.getFax());
//			dest.setId(source.getId());
//			dest.setMobileNumber(source.getMobileNumber());
//			dest.setName(source.getName());
//			dest.setWebsite(source.getWebsite());
//		}
//
//		return dest;
//
//	}
//
//	public static void merge(Company dest, CompanyVo source){
//			dest.setDescription(source.getDescription());
//			dest.setEmail(source.getEmail());
//			dest.setFax(source.getFax());
//			dest.setMobileNumber(source.getMobileNumber());
//			dest.setName(source.getName());
//			dest.setWebsite(source.getWebsite());
//	}

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

public class RequestMessageVo extends BaseVo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HomeOwnerRequestVo request;
	
	private IhcMessageVo message;

	public HomeOwnerRequestVo getRequest() {
		return request;
	}

	public void setRequest(HomeOwnerRequestVo request) {
		this.request = request;
	}
	
	public IhcMessageVo getMessage() {
		return message;
	}

	public void setMessage(IhcMessageVo message) {
		this.message = message;
	}

}

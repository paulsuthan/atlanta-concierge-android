package com.itrustconcierge.atlantahome.API_Manager.vo;

public enum AccountType {
    INDIVIDUAL, COMPANY;

    public static AccountType fromString(String value) {
        AccountType accountType = null;
        for (AccountType type : AccountType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                accountType = type;
                break;
            }
        }
        return accountType;
    }

    public String stringify() {
        String retVal = null;

        switch (this) {
            case COMPANY:
                retVal = "company";
                break;
            case INDIVIDUAL:
                retVal = "individual";
                break;
        }

        return retVal;
    }
}

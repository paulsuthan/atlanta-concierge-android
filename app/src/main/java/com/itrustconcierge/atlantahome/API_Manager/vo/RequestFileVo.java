package com.itrustconcierge.atlantahome.API_Manager.vo;

public class RequestFileVo extends BaseVo{

	/**
	 * 
	 */
	
	private HomeOwnerRequestVo request;
	
	private IhcFileVo file;
	
	private String type;

	public HomeOwnerRequestVo getRequest() {
		return request;
	}

	public void setRequest(HomeOwnerRequestVo request) {
		this.request = request;
	}

	public IhcFileVo getFile() {
		return file;
	}

	public void setFile(IhcFileVo file) {
		this.file = file;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

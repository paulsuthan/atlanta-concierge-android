package com.itrustconcierge.atlantahome.API_Manager.vo;


import com.itrustconcierge.atlantahome.API_Manager.enums.CalendarType;
import com.itrustconcierge.atlantahome.API_Manager.enums.ScheduleStatus;

import java.util.List;

public class ScheduleVo extends BaseVo {

    //	private static final long serialVersionUID = APP_SERIAL_ID;
    private static final long serialVersionUID = 1L;

    private String title;

    private AddressVo address;

    private String description;

    private List<ParticipantVo> participants;

    private ScheduleStatus status;

    private CalendarType type;

    private Long subject;

    private UserVo scheduledBy;

    private List<EventVo> events;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AddressVo getAddress() {
        return address;
    }

    public void setAddress(AddressVo address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ParticipantVo> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantVo> participants) {
        this.participants = participants;
    }

    public ScheduleStatus getStatus() {
        return status;
    }

    public void setStatus(ScheduleStatus status) {
        this.status = status;
    }

    public CalendarType getType() {
        return type;
    }

    public void setType(CalendarType type) {
        this.type = type;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }

    public UserVo getScheduledBy() {
        return scheduledBy;
    }

    public void setScheduledBy(UserVo scheduledBy) {
        this.scheduledBy = scheduledBy;
    }

    public List<EventVo> getEvents() {
        return events;
    }

    public void setEvents(List<EventVo> events) {
        this.events = events;
    }

//	public static ScheduleVo toVo(Schedule source){
//		ScheduleVo dest = null;
//		if(source != null){
//			dest = new ScheduleVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setDescription(source.getDescription());
//			if(source.getEvents() !=null){
//				dest.setEvents(EventVo.toVoList(source.getEvents()));
//			}
//			if(source.getParticipants()!=null){
//				dest.setParticipants(ParticipantVo.toVoList(source.getParticipants()));
//			}
//
//			dest.setScheduledBy(UserVo.toVo(source.getScheduledBy(), false));
//			dest.setStatus(source.getStatus());
//			dest.setSubject(source.getSubject());
//			dest.setTitle(source.getTitle());
//			dest.setType(source.getType());
//		}
//
//		return dest;
//	}
//
//	public static List<ScheduleVo> toVos(List<Schedule> list){
//		List<ScheduleVo> vos = new ArrayList<ScheduleVo>();
//
//		if (list != null){
//			for(Schedule schedule: list){
//				vos.add(ScheduleVo.toVo(schedule));
//			}
//		}
//		return vos;
//	}

}

package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum InviteType {
    SP, HO;

    public static InviteType fromString(String value) {
        InviteType inviteType = null;
        for (InviteType type : InviteType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                inviteType = type;
                break;
            }
        }
        return inviteType;
    }

}

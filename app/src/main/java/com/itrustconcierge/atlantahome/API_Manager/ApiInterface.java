package com.itrustconcierge.atlantahome.API_Manager;

import com.itrustconcierge.atlantahome.API_Manager.enums.DeviceType;
import com.itrustconcierge.atlantahome.API_Manager.enums.FeedbackType;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserType;
import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.CardDetail;
import com.itrustconcierge.atlantahome.API_Manager.vo.ContactVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ConversationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.FeedbackParameterVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.FeedbackVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.FranchiseCategoryVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IdListVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.MessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.NotificationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ObjectListVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyFloorSizeVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyOwnerVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyPackageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.QuoteVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestFileVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestHistoryVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestMessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ScheduleVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.SubscriptionBenefitVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserServiceVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserVo;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by paul on 7/27/2017.
 */

public interface ApiInterface {

    @GET("signup/invite/{code}")
    Call<ResponseWrapper<UserVo>> getUserByCode(@Path("code") String code);

    @POST("signup/activate")
    Call<ResponseWrapper<AuthenticateVo>> activateUser(@Body AuthenticateVo authenticateVo);

    @POST("signup")
    Call<ResponseWrapper<Map<String, Boolean>>> signUp(@Body AuthenticateVo authenticateVo);

    @PUT("signin")
    Call<AuthenticateVo> signIn(@Body Map<String, Object> input);

    @GET("signup/userExist")
    Call<ResponseWrapper<Map<String, Boolean>>> checkUserExist(@Query("email") String email, @Query("mobileNumber") String mobileNumber);

    @GET("signup/tac/{inviteCode}")
    Call<ResponseWrapper<Map<String, Object>>> tacBasedOnInviteCode(@Path("inviteCode") String inviteCode);

    @POST("signup/contact")
    Call<ResponseWrapper<Map<String, Boolean>>> sendContact(@Body ContactVo authenticateVo);

    @POST("forgotPassword")
    Call<ResponseWrapper<Map<String, Boolean>>> initiateForgotPassword(@Body Map<String, String> map);

    @GET("franchise/category/services")
    Call<ResponseWrapper<FranchiseCategoryVo>> getCategories();

    @POST("userservice")
    Call<ResponseWrapper<UserServiceVo>> updateServices(@Body IdListVo userServiceVoList);

    @GET("userdocument/uploadUrl")
    Call<ResponseWrapper<UploadVo>> getUploadUrl(@Query("uploadType") UploadType uploadType, @Query("filename") String fileName, @Query("mimeType") String mimeType, @Query("id") Long id);

    @GET("userdocument/uploadUrl")
    Call<ResponseWrapper<UploadVo>> getUploadUrl(@Query("uploadType") UploadType uploadType, @Query("filename") String fileName, @Query("mimeType") String mimeType);

    @GET("userdocument/downloadUrl")
    Call<ResponseWrapper<UploadVo>> getDownloadUrl(@Query("uploadType") UploadType uploadType, @Query("filename") String fileName, @Query("mimeType") String mimeType, @Query("id") Long id);

    @GET("user/notifications")
    Call<ResponseWrapper<NotificationVo>> getNotifications();

    @GET("user/unread-notifications")
    Call<ResponseWrapper<NotificationVo>> getUnReadNotifications();

    @PUT("user/notifications/mark-as-read")
    Call<ResponseWrapper<Map<String, Boolean>>> readNotifications(@Body IdListVo userNotificationVoList);

    @PUT("user/notifications/mark-as-consumed")
    Call<ResponseWrapper<Map<String, Boolean>>> consumeNotification(@Body NotificationVo notificationVo);

    @POST("request")
    Call<ResponseWrapper<HomeOwnerRequestVo>> createHomeOwnerRequest(@Body HomeOwnerRequestVo homeOwnerRequestVo);

    @GET("request/{id}")
    Call<ResponseWrapper<HomeOwnerRequestVo>> getRequest(@Path("id") Long requestId);

    @GET("requests/status/{type}")
    Call<ResponseWrapper<HomeOwnerRequestVo>> getRequestHistories(@Path("type") RequestHistoryType requestHistoryType);

    @GET("requests")
    Call<ResponseWrapper<HomeOwnerRequestVo>> getRequestHistoriesWithoutStatus();

    @PUT("requests/{requestId}/needVisitForQuote")
    Call<ResponseWrapper<Map<String, Boolean>>> confirmVisitForQuote(@Path("requestId") String requestId, @Body Map<String, Long> dateMap);

    @PUT("requests/{requestId}/scheduleJob")
    Call<ResponseWrapper<Map<String, Boolean>>> confirmScheduleForJob(@Path("requestId") String requestId, @Body Map<String, Long> dateMap);

    @GET("quotes/status/{type}")
    Call<ResponseWrapper<QuoteVo>> getQuotes(@Path("type") RequestHistoryType requestHistoryType);

    @POST("quotes/approve-quote")
    Call<ResponseWrapper<Map<String, Boolean>>> approveQuote(@Body QuoteVo quoteVo);

    @POST("requests/quote")
    Call<ResponseWrapper<QuoteVo>> createQuote(@Body QuoteVo quoteVo);

    @GET("quote/{id}")
    Call<ResponseWrapper<QuoteVo>> getQuote(@Path("id") String requestId);

    @GET("conversation")
    Call<ResponseWrapper<ConversationVo>> getConversations();

    @GET("messages-by-users")
    Call<ResponseWrapper<MessageVo>> getMessages(@Query("source") long sourceId, @Query("target") long targetId);

    @GET("properties")
    Call<ResponseWrapper<PropertyVo>> getPropertiesByUser();

    @GET("user/unmapped/property")
    Call<ResponseWrapper<PropertyVo>> getUnmappedPropertiesByUser();

    @GET("property/{id}")
    Call<ResponseWrapper<PropertyOwnerVo>> getPropertyById(@Path("id") Long id);

    @POST("property")
    Call<ResponseWrapper<PropertyVo>> createProperty(@Body PropertyVo propertyVo);

    @PUT("property")
    Call<ResponseWrapper<PropertyVo>> updateProperty(@Body PropertyVo propertyVo);

    @POST("user/unmapped/property")
    Call<ResponseWrapper<PropertyVo>> createUnmappedProperty(@Body PropertyVo propertyVo);

    @GET("user/tenants")
    Call<ResponseWrapper<AuthenticateVo>> getTenants(@Query("zipcode") String pincode);

    @PUT("user/mapToFranchise/{tenantId}")
    Call<ResponseWrapper<Map<String, Boolean>>> mapToFranchise(@Path("tenantId") String tenantId);

    @POST("send-message")
    Call<ResponseWrapper<MessageVo>> sendMessage(@Body MessageVo messageVo);

    @GET("schedule")
    Call<ResponseWrapper<EventVo>> getEvents();

    @GET("schedule/get-schedule/{id}")
    Call<ResponseWrapper<ScheduleVo>> getScheduleById(@Path("id") Long Type);

    @POST("schedule")
    Call<ResponseWrapper<Map<String, Boolean>>> addEvent(@Body EventVo eventInfo);

    @PUT("schedule/confirm")
    Call<ResponseWrapper<Map<String, Boolean>>> confirmEvent(@Body EventVo eventInfo);

    @GET("payment")
    Call<ResponseWrapper<PaymentVo>> getPayments(@Query("status") PaymentStatus paymentStatus);

    @POST("payment/{cardId}")
    Call<ResponseWrapper<Map<String, Boolean>>> makePayments(@Body IdListVo idListVo, @Path("cardId") String cardId);

    @PUT("payment")
    Call<ResponseWrapper<Map<String, Boolean>>> payInvoice(@Body ObjectListVo<PaymentVo> paymentVoObjectListVo);

    @POST("subscription/card")
    Call<ResponseWrapper<Map<String, Boolean>>> addCard(@Body CardDetail cardDetail);

    @GET("subscription/card")
    Call<ResponseWrapper<CardDetail>> getCardDetails();

    @GET("feedback/parameters/{type}")
    Call<ResponseWrapper<FeedbackParameterVo>> getFeedbackParameters(@Path("type") FeedbackType type);

    @POST("feedback")
    Call<ResponseWrapper<Map<String, Boolean>>> addFeedback(@Body FeedbackVo feedbackVo);

    @POST("signup/contact")
    Call<ResponseWrapper<Map<String, Boolean>>> requestCode(@Body ContactVo contactVo);

    @GET("signup/appsettings/version-check")
    Call<ResponseWrapper<Map<String, String>>> versionCheck(@Query("deviceType") DeviceType deviceType, @Query("userType") UserType userType);

    @POST("user/profile-image")
    Call<ResponseWrapper<UserVo>> updateProfilePic(@Body UserVo userVo);

    @GET("property/{id}/package")
    Call<ResponseWrapper<PropertyPackageVo>> getPropertyPackage(@Path("id") long propertyId);

    @GET("property-subscription/{userId}/{propertyId}")
    Call<ResponseWrapper<PaymentVo>> getPropertySubscriptionPayment(@Path("userId") long userId, @Path("propertyId") long propertyId);

    @POST("addPackages")
    Call<ResponseWrapper<Map<String, Object>>> addPackages(@Body PropertyVo propertyVo);

    @GET("property-floor-size")
    Call<ResponseWrapper<PropertyFloorSizeVo>> getPropertyFloorSizes();

    @GET("subscription/subscriptionbenefits")
    Call<ResponseWrapper<SubscriptionBenefitVo>> getSubscriptionbenefits();

    @DELETE("signout")
    Call<ResponseBody> signOut();

    @POST("ihcfile/request")
    Call<ResponseWrapper<RequestFileVo>> addRequestPhotos(@Body RequestFileVo requestFileVo);

    @GET("ihcfile/request/{id}")
    Call<ResponseWrapper<RequestFileVo>> getRequestPhotos(@Path("id") Long id);

    @GET("ihcmessage/request/{id}/{userId}")
    Call<ResponseWrapper<RequestMessageVo>> getRequestMessages(@Path("id") String id, @Path("userId") Long userId);

    @POST("ihcmessage/request")
    Call<ResponseWrapper<RequestMessageVo>> saveRequestMessage(@Body RequestMessageVo requestMessageVo);

    @PUT("ihcmessage/conversation/{participantId}/markAsConsumed")
    Call<ResponseWrapper<Map<String, Boolean>>> markConversationConsumed(@Path("participantId") Long id);

    @HTTP(method = "DELETE", path = "request/{requestId}", hasBody = true)
    Call<ResponseWrapper<Map<String, Boolean>>> deleteJob(@Path("requestId") Long id, @Body Map<String, String> input);

}

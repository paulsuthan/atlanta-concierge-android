package com.itrustconcierge.atlantahome.API_Manager.vo;

public class QuestionVo {

    private HomeOwnerRequestVo request;

    private MessageVo message;

    public HomeOwnerRequestVo getRequest() {
        return request;
    }

    public void setRequest(HomeOwnerRequestVo request) {
        this.request = request;
    }

    public MessageVo getMessage() {
        return message;
    }

    public void setMessage(MessageVo message) {
        this.message = message;
    }

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.ConatctType;

import java.util.Date;
import java.util.List;

public class ContactVo {

    private String firstName;
    private String lastName;
    private String companyName;
    private String mobileNumber;
    private String email;
    private List<Long> preferredTimeToContact;
    private String preferredMediumToContact;
    private Boolean notifyMe;
    private ContactVo refferedContact;
    private ConatctType contactType;
    private Long itcId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Long> getPreferredTimeToContact() {
        return preferredTimeToContact;
    }

    public void setPreferredTimeToContact(List<Long> preferredTimeToContact) {
        this.preferredTimeToContact = preferredTimeToContact;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPreferredMediumToContact() {
        return preferredMediumToContact;
    }

    public void setPreferredMediumToContact(String preferredMediumToContact) {
        this.preferredMediumToContact = preferredMediumToContact;
    }

    public ContactVo getRefferedContact() {
        return refferedContact;
    }

    public void setRefferedContact(ContactVo refferedContact) {
        this.refferedContact = refferedContact;
    }

    public ConatctType getContactType() {
        return contactType;
    }

    public void setContactType(ConatctType contactType) {
        this.contactType = contactType;
    }

    public Long getItcId() {
        return itcId;
    }

    public void setItcId(Long itcId) {
        this.itcId = itcId;
    }

    public Boolean getNotifyMe() {
        return notifyMe;
    }

    public void setNotifyMe(Boolean notifyMe) {
        this.notifyMe = notifyMe;
    }
}

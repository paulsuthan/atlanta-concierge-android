package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;

import java.io.Serializable;
import java.util.List;

public class UserVo extends BaseVo implements Serializable {

    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String email;
    private List<UserDocumentVo> documents;
    private AddressVo address;
    private String status;
    private UserMappedStatus userMappedStatus;
    private String password;

    private boolean fbUser;

    private String inviteCode;

    List<UserServiceVo> services;

    private CompanyVo company;

    private boolean companyOwner;

    private String profileImage;

    private String tenantCode;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public static UserVo toVo(User source, boolean loadChildren) {
//        UserVo dest = null;
//        if (source != null) {
//            dest = new UserVo();
//            dest.setId(source.getId());
//            dest.setEmail(source.getEmail());
//            dest.setFirstName(source.getFirstName());
//            dest.setLastName(source.getLastName());
//            dest.setMobileNumber(source.getMobileNumber());
//            dest.setProfileImage(source.getProfileImage());
//            if (loadChildren) {
//                if (source.getDocuments() != null) {
//                    List<UserDocumentVo> documents = new ArrayList<>();
//                    for (UserDocument userDoc : source.getDocuments()) {
//                        documents.add(UserDocumentVo.toVo(userDoc.getDocument()));
//                    }
//                    dest.setDocuments(documents);
//                }
//
//                if (source.getServices() != null) {
//                    List<UserServiceVo> services = new ArrayList<UserServiceVo>();
//                    for (FranchiseUserService ser : source.getServices()) {
//                        services.add(UserServiceVo.toVo(ser));
//                    }
//                    dest.setServices(services);
//                }
//            }
//            dest.setStatus(source.getStatus());
//            dest.setFbUser(source.isFbUser());
//            dest.setInviteCode(source.getInviteCode());
//
//        }
//        return dest;
//    }
//
//    public static void merge(User dest, UserVo source) {
//        dest.setEmail(source.getEmail());
//        dest.setFirstName(source.getFirstName());
//        dest.setLastName(source.getLastName());
//        dest.setMobileNumber(source.getMobileNumber());
//        dest.setStatus(source.getStatus());
//        dest.setFbUser(source.isFbUser());
//        dest.setInviteCode(source.getInviteCode());
//        dest.setProfileImage(source.getProfileImage());
//    }

    public List<UserDocumentVo> getDocuments() {
        return documents;
    }

    public void setDocuments(List<UserDocumentVo> documents) {
        this.documents = documents;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isFbUser() {
        return fbUser;
    }

    public void setFbUser(boolean fbUser) {
        this.fbUser = fbUser;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public List<UserServiceVo> getServices() {
        return services;
    }

    public void setServices(List<UserServiceVo> services) {
        this.services = services;
    }

    public CompanyVo getCompany() {
        return company;
    }

    public void setCompany(CompanyVo company) {
        this.company = company;
    }

    public boolean isCompanyOwner() {
        return companyOwner;
    }

    public void setCompanyOwner(boolean companyOwner) {
        this.companyOwner = companyOwner;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public AddressVo getAddress() {
        return address;
    }

    public void setAddress(AddressVo address) {
        this.address = address;
    }

    public UserMappedStatus getUserMappedStatus() {
        return userMappedStatus;
    }

    public void setUserMappedStatus(UserMappedStatus userMappedStatus) {
        this.userMappedStatus = userMappedStatus;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}
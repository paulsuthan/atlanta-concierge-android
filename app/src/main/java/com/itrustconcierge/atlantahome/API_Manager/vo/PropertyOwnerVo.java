package com.itrustconcierge.atlantahome.API_Manager.vo;

public class PropertyOwnerVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private PropertyVo property;

    private UserVo user;

    private boolean currentOwner;

    public PropertyVo getProperty() {
        return property;
    }

    public void setProperty(PropertyVo property) {
        this.property = property;
    }

    public UserVo getUser() {
        return user;
    }

    public void setUser(UserVo user) {
        this.user = user;
    }

    public boolean isCurrentOwner() {
        return currentOwner;
    }

    public void setCurrentOwner(boolean currentOwner) {
        this.currentOwner = currentOwner;
    }

//	public static PropertyOwnerVo toVo(PropertyOwner source){
//		PropertyOwnerVo dest = null;
//		if(source != null){
//			dest = new PropertyOwnerVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setCurrentOwner(source.isCurrentOwner());
//			dest.setUser(UserVo.toVo(source.getUser(), false));
//			dest.setProperty(PropertyVo.toVo(source.getProperty(), true));
//
//		}
//		return dest;
//	}

}

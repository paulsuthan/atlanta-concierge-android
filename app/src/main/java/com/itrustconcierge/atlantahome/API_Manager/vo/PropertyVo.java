package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.FloorSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.HomeAccessKey;
import com.itrustconcierge.atlantahome.API_Manager.enums.LotSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.PropertyType;
import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;

import java.util.List;

public class PropertyVo extends BaseVo {

    private static final long serialVersionUID = 1L;
    private AuthenticateVo owner;
    private UserVo managedBy;
    private UserVo refferedBy;

    private AddressVo address;

    private List<PropertySubscriptionVo> subscriptions;

    private String propertyType;
    private String floorSize;
    private String lotSize;
    private String code;

    private String image;

    private Boolean subscriptionRequired=false;

    private Double subscriptionDiscount;

    private Double subscriptionAmount;

    private Boolean vacationRental;

    private Boolean homeDash;

    private Boolean reviewPackageAvailable = false;

    private Boolean secondaryHomeOwner;

    private TimeUnit durationType;

    private Long subscriptionExpiresOn;

    private Long subscribedOn;

    private String alarmCode;

    private HomeAccessKey accessKey;

    private long floorSizeInSqFt;

    private Double lotSizeInAcres = new Double(0);

    private String propertyTypeValue;

    private String houseCanaryProperty;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<PropertySubscriptionVo> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<PropertySubscriptionVo> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getSubscriptionDiscount() {
        return subscriptionDiscount;
    }

    public void setSubscriptionDiscount(Double subscriptionDiscount) {
        this.subscriptionDiscount = subscriptionDiscount;
    }

    public Double getSubscriptionAmount() {
        return subscriptionAmount;
    }

    public void setSubscriptionAmount(Double subscriptionAmount) {
        this.subscriptionAmount = subscriptionAmount;
    }

    public Boolean getVacationRental() {
        return vacationRental;
    }

    public void setVacationRental(Boolean vacationRental) {
        this.vacationRental = vacationRental;
    }

    public Boolean getHomeDash() {
        return homeDash;
    }

    public void setHomeDash(Boolean homeDash) {
        this.homeDash = homeDash;
    }

    public Boolean getSecondaryHomeOwner() {
        return secondaryHomeOwner;
    }

    public void setSecondaryHomeOwner(Boolean secondaryHomeOwner) {
        this.secondaryHomeOwner = secondaryHomeOwner;
    }

    public TimeUnit getDurationType() {
        return durationType;
    }

    public void setDurationType(TimeUnit durationType) {
        this.durationType = durationType;
    }

    public Long getSubscriptionExpiresOn() {
        return subscriptionExpiresOn;
    }

    public void setSubscriptionExpiresOn(Long subscriptionExpiresOn) {
        this.subscriptionExpiresOn = subscriptionExpiresOn;
    }

    public Long getSubscribedOn() {
        return subscribedOn;
    }

    public void setSubscribedOn(Long subscribedOn) {
        this.subscribedOn = subscribedOn;
    }

    public String getAlarmCode() {
        return alarmCode;
    }

    public void setAlarmCode(String alarmCode) {
        this.alarmCode = alarmCode;
    }

    public HomeAccessKey getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(HomeAccessKey accessKey) {
        this.accessKey = accessKey;
    }

    public AuthenticateVo getOwner() {
        return owner;
    }


    public void setOwner(AuthenticateVo owner) {
        this.owner = owner;
    }


    public UserVo getManagedBy() {
        return managedBy;
    }


    public void setManagedBy(UserVo managedBy) {
        this.managedBy = managedBy;
    }


    public UserVo getRefferedBy() {
        return refferedBy;
    }


    public void setRefferedBy(UserVo refferedBy) {
        this.refferedBy = refferedBy;
    }


    public AddressVo getAddress() {
        return address;
    }


    public void setAddress(AddressVo address) {
        this.address = address;
    }


    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }


    public String getPropertyType() {
        return propertyType;
    }


    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }


    public String getFloorSize() {
        return floorSize;
    }


    public void setFloorSize(String floorSize) {
        this.floorSize = floorSize;
    }


    public String getLotSize() {
        return lotSize;
    }


    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }


    public long getFloorSizeInSqFt() {
        return floorSizeInSqFt;
    }


    public void setFloorSizeInSqFt(long floorSizeInSqFt) {
        this.floorSizeInSqFt = floorSizeInSqFt;
    }


    public Double getLotSizeInAcres() {
        return lotSizeInAcres;
    }


    public void setLotSizeInAcres(Double lotSizeInAcres) {
        this.lotSizeInAcres = lotSizeInAcres;
    }


    public String getPropertyTypeValue() {
        return propertyTypeValue;
    }


    public void setPropertyTypeValue(String propertyTypeValue) {
        this.propertyTypeValue = propertyTypeValue;
    }

    public Boolean getSubscriptionRequired() {
        return subscriptionRequired;
    }

    public void setSubscriptionRequired(Boolean subscriptionRequired) {
        this.subscriptionRequired = subscriptionRequired;
    }

    public Boolean getReviewPackageAvailable() {
        return reviewPackageAvailable;
    }

    public void setReviewPackageAvailable(Boolean reviewPackageAvailable) {
        this.reviewPackageAvailable = reviewPackageAvailable;
    }

    public String getHouseCanaryProperty() {
        return houseCanaryProperty;
    }

    public void setHouseCanaryProperty(String houseCanaryProperty) {
        this.houseCanaryProperty = houseCanaryProperty;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;

import java.util.List;

public class PropertyPackageVo extends BaseVo {

    private String name;
    private String description;
    private Double amount;
    private Integer time;
    private TimeUnit timeUnit;
    private Boolean active;
    private PropertyVo property;
    private String serviceIdCsv;
    private boolean selected;
    private List<PackageServiceVo> services;
    private Double discount;
    private Double yearlyAmount;
    private Double monthlyAdditionalMargin;
    private Double monthlyAmount;
    private Long startTime;
    private TimeUnit paymentFrequency;
    private Double franchiseMarkup;
    private List<PropertySubscriptionVo> propertySubscriptions;
    private String preferredTimeToCompletion;
    private String frequency;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getServiceIdCsv() {
        return serviceIdCsv;
    }

    public void setServiceIdCsv(String serviceIdCsv) {
        this.serviceIdCsv = serviceIdCsv;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<PackageServiceVo> getServices() {
        return services;
    }

    public void setServices(List<PackageServiceVo> services) {
        this.services = services;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getYearlyAmount() {
        return yearlyAmount;
    }

    public void setYearlyAmount(Double yearlyAmount) {
        this.yearlyAmount = yearlyAmount;
    }

    public Double getMonthlyAdditionalMargin() {
        return monthlyAdditionalMargin;
    }

    public void setMonthlyAdditionalMargin(Double monthlyAdditionalMargin) {
        this.monthlyAdditionalMargin = monthlyAdditionalMargin;
    }

    public Double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(Double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }


    public PropertyVo getProperty() {
        return property;
    }

    public void setProperty(PropertyVo property) {
        this.property = property;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public TimeUnit getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(TimeUnit paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public Double getFranchiseMarkup() {
        return franchiseMarkup;
    }

    public void setFranchiseMarkup(Double franchiseMarkup) {
        this.franchiseMarkup = franchiseMarkup;
    }

    public List<PropertySubscriptionVo> getPropertySubscriptions() {
        return propertySubscriptions;
    }

    public void setPropertySubscriptions(List<PropertySubscriptionVo> propertySubscriptions) {
        this.propertySubscriptions = propertySubscriptions;
    }

    public String getPreferredTimeToCompletion() {
        return preferredTimeToCompletion;
    }

    public void setPreferredTimeToCompletion(String preferredTimeToCompletion) {
        this.preferredTimeToCompletion = preferredTimeToCompletion;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

//	public static List<PropertyPackageVo> toVoList(List<PropertyPackage> list){
//		final List<PropertyPackageVo> voList = new ArrayList<>();
//		if (list != null) {
//			list.forEach(franchisePackage->{
//				voList.add(toVo(franchisePackage, null));
//			});
//		}
//		return voList;
//	}
//
//	public static PropertyPackageVo toVo(PropertyPackage source, List<FranchiseCategoryVo> franchiseCategoryList ) {
//		PropertyPackageVo dest =null;
//		if ( source != null ) {
//			dest = new PropertyPackageVo();
//			dest.setId(source.getId());
//			dest.setActive(source.getActive());
//			dest.setAmount(source.getAmount());
//			dest.setDescription(source.getDescription());
//			dest.setName(source.getName());
//			dest.setTime(source.getTime());
//			dest.setTimeUnit(source.getTimeUnit());
//			dest.setDiscount(source.getDiscount());
//			dest.setMonthlyAdditionalMargin(source.getMonthlyAdditionalMargin());
//			dest.setMonthlyAmount(source.getMonthlyAmount());
//			dest.setYearlyAmount(source.getYearlyAmount());
//			List<PackageServiceVo> services = new ArrayList<PackageServiceVo>();
//			for(PackageService pS : source.getServices()){
//				services.add(PackageServiceVo.toVo(pS));
//			}
//			dest.setServices(services);
//			dest.setStartTime(source.getStartTime());
//			dest.setPaymentFrequency(source.getPaymentFrequency());
//			dest.setFranchiseMarkup(source.getFranchiseMarkup());
//		}
//		return dest;
//	}
//
//
//	public static List<PropertyPackage> toDaoList(List<PropertyPackageVo> list){
//		final List<PropertyPackage> daoList = new ArrayList<>();
//		if (list != null) {
//			list.forEach(franchisePackage->{
//				daoList.add(toDao(franchisePackage));
//			});
//		}
//		return daoList;
//	}
//
//	public static void merge(PropertyPackage dest, PropertyPackageVo source){
//
//		dest.setActive(source.getActive());
//		dest.setAmount(source.getAmount());
//		dest.setDescription(source.getDescription());
//		dest.setName(source.getName());
//		dest.setTime(source.getTime());
//		dest.setTimeUnit(source.getTimeUnit());
//		dest.setDiscount(source.getDiscount());
//		dest.setMonthlyAdditionalMargin(source.getMonthlyAdditionalMargin());
//		dest.setMonthlyAmount(source.getMonthlyAmount());
//		dest.setYearlyAmount(source.getYearlyAmount());
//		Property property = new Property();
//		property.setId(source.getProperty().getId());
//		dest.setProperty(property);
//		dest.setStartTime(source.getStartTime());
//		dest.setPaymentFrequency(source.getPaymentFrequency());
//		dest.setFranchiseMarkup(source.getFranchiseMarkup());
//
//	}
//
//	public static PropertyPackage toDao(PropertyPackageVo franchisePackage) {
//		PropertyPackage dao =null;
//		if ( franchisePackage != null ) {
//			dao = new PropertyPackage();
//			dao.setActive(franchisePackage.getActive());
//			dao.setAmount(franchisePackage.getAmount());
//			dao.setDescription(franchisePackage.getDescription());
//			dao.setServiceIdsCsv(franchisePackage.getServiceIdCsv());
//			dao.setName(franchisePackage.getName());
//			dao.setTime(franchisePackage.getTime());
//			dao.setTimeUnit(franchisePackage.getTimeUnit());
//			dao.setStartTime(franchisePackage.getStartTime());
//			dao.setPaymentFrequency(franchisePackage.getPaymentFrequency());
//			dao.setFranchiseMarkup(franchisePackage.getFranchiseMarkup());
//		}
//		return dao;
//	}
}

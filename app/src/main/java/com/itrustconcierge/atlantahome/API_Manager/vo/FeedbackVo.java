package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.FeedbackFromType;
import com.itrustconcierge.atlantahome.API_Manager.enums.FeedbackType;

import java.util.List;

public class FeedbackVo extends BaseVo {

    private FeedbackType feedbackType;
    private FeedbackFromType feedbackFromType;
    private String feedbackTo;
    private String feedbackFrom;
    private Boolean isExternalUser;
    private Double finalRating;
    private String description;
    private List<FeedbackParameterVo> parameters;
    private FeedbackCommentVo comments;

    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public Double getFinalRating() {
        return finalRating;
    }

    public void setFinalRating(Double finalRating) {
        this.finalRating = finalRating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FeedbackParameterVo> getParameters() {
        return parameters;
    }

    public void setParameters(List<FeedbackParameterVo> parameters) {
        this.parameters = parameters;
    }

    public FeedbackCommentVo getComments() {
        return comments;
    }

    public void setComments(FeedbackCommentVo comments) {
        this.comments = comments;
    }

    public String getFeedbackTo() {
        return feedbackTo;
    }

    public void setFeedbackTo(String feedbackTo) {
        this.feedbackTo = feedbackTo;
    }

    public String getFeedbackFrom() {
        return feedbackFrom;
    }

    public void setFeedbackFrom(String feedbackFrom) {
        this.feedbackFrom = feedbackFrom;
    }

    public Boolean getIsExternalUser() {
        return isExternalUser;
    }

    public void setIsExternalUser(Boolean isExternalUser) {
        this.isExternalUser = isExternalUser;
    }

    @Override
    public String toString() {
        return "FeedbackVo [feedbackType=" + feedbackType + ", feedbackTo=" + feedbackTo + ", feedbackFrom="
                + feedbackFrom + ", isExternalUser=" + isExternalUser + ", finalRating=" + finalRating
                + ", description=" + description + ", parameters=" + parameters + ", comments=" + comments
                + "]";
    }


    public FeedbackFromType getFeedbackFromType() {
        return feedbackFromType;
    }

    public void setFeedbackFromType(FeedbackFromType feedbackFromType) {
        this.feedbackFromType = feedbackFromType;
    }
}

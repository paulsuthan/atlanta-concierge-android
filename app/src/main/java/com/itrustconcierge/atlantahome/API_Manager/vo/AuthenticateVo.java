package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.AuthenticateType;
import com.itrustconcierge.atlantahome.API_Manager.enums.Role;
import com.itrustconcierge.atlantahome.API_Manager.enums.SubscriptionStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthenticateVo extends UserVo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8313487661159671233L;
    private String userName;
    private String token;
    private AuthenticateType type;
    private DeviceInfoVo deviceInfo;
    private List<Role> roles;
    private String tenantId;
    private UserVo franchise;
    private Boolean subscriptionRequired = true;
    private SubscriptionStatus subscriptionStatus = SubscriptionStatus.NOT_SUBSCRIBED;
    private Long subscriptionExpiry;
    private TenantSettingVo tenantSetting;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AuthenticateType getType() {
        return type;
    }

    public void setType(AuthenticateType type) {
        this.type = type;
    }

    public DeviceInfoVo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfoVo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        if (this.roles == null) {
            this.roles = new ArrayList<>();
        }
        this.roles.add(role);
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

//	public static TenantUser toTenantUser(AuthenticateVo vo) {
//		TenantUser tenantUser = new TenantUser();
//		if ( vo != null ) {
//			tenantUser.setEmail(vo.getEmail());
//			tenantUser.setFirstName(vo.getFirstName());
//			tenantUser.setInviteCode(vo.getInviteCode());
//			tenantUser.setLastName(vo.getLastName());
//			tenantUser.setMobileNumber(vo.getMobileNumber());
//			tenantUser.setUserName(vo.getUserName());
//			tenantUser.setTenantId(vo.getTenantId());
//			tenantUser.setPassword(vo.getToken());
//			if ( vo.getCompany() != null) {
//				tenantUser.setCompanyName(vo.getCompany().getName());
//			}
//			tenantUser.setRole(vo.getRoles().get(0).name());
//			vo.setTenantId(vo.getTenantId());
//		}
//		return tenantUser;
//	}
//
//	public static  AuthenticateVo toVo(TenantUser tenantUser) {
//		AuthenticateVo vo = new AuthenticateVo();
//		vo.setEmail(tenantUser.getEmail());
//		vo.setFirstName(tenantUser.getFirstName());
//		vo.setInviteCode(tenantUser.getInviteCode());
//		vo.setLastName(tenantUser.getLastName());
//		vo.setMobileNumber(tenantUser.getMobileNumber());
//		vo.setUserName(tenantUser.getUserName());
//		vo.setTenantId(tenantUser.getTenantId());
//		vo.setId(tenantUser.getId());
//		if ( StringUtils.hasText(tenantUser.getRole())) {
//			List<Role> list = new ArrayList<>(1);
//			list.add(Role.fromString(tenantUser.getRole()));
//			vo.setRoles(list);
//		}
//		if ( StringUtils.hasText(tenantUser.getCompanyName())) {
//			CompanyVo  company = new CompanyVo();
//			company.setName(tenantUser.getCompanyName());
//			vo.setCompany(company);
//		}
//		vo.setToken(tenantUser.getPassword());
//		vo.setType(AuthenticateType.TOKEN);
//		return vo;
//	}

    public Boolean getSubscriptionRequired() {
        return subscriptionRequired;
    }

    public void setSubscriptionRequired(Boolean subscriptionRequired) {
        this.subscriptionRequired = subscriptionRequired;
    }

    public SubscriptionStatus getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(SubscriptionStatus subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public Long getSubscriptionExpiry() {
        return subscriptionExpiry;
    }

    public void setSubscriptionExpiry(Long subscriptionExpiry) {
        this.subscriptionExpiry = subscriptionExpiry;
    }

    public UserVo getFranchise() {
        return franchise;
    }

    public void setFranchise(UserVo franchise) {
        this.franchise = franchise;
    }


    public TenantSettingVo getTenantSetting() {
        return tenantSetting;
    }

    public void setTenantSetting(TenantSettingVo tenantSetting) {
        this.tenantSetting = tenantSetting;
    }
}

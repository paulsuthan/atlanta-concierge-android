package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.InviteType;

import java.util.List;

public class InviteVo extends BaseVo {

    private UserVo user;

    private String code;

    private String firstName;

    private String lastName;

    private String email;

    private String mobile;

    private InviteType type;

    private UserVo invitedUser;

    private List<UserDocumentVo> documents;

    public UserVo getUser() {
        return user;
    }

    public void setUser(UserVo user) {
        this.user = user;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public InviteType getType() {
        return type;
    }

    public void setType(InviteType type) {
        this.type = type;
    }

    public UserVo getInvitedUser() {
        return invitedUser;
    }

    public void setInvitedUser(UserVo invitedUser) {
        this.invitedUser = invitedUser;
    }

//	public static InviteVo toVo(Invite source){
//		InviteVo dest = null;
//
//		if (source != null){
//			dest = new InviteVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setFirstName(source.getFirstName());
//			dest.setLastName(source.getLastName());
//			dest.setMobile(source.getMobile());
//			dest.setEmail(source.getEmail());
//			dest.setInvitedUser(UserVo.toVo(source.getInvitedUser(), false));
//		}
//
//		return dest;
//	}
//
//	public static void merge(Invite source, InviteVo dest){
//		dest.setFirstName(source.getFirstName());
//		dest.setLastName(source.getLastName());
//		dest.setMobile(source.getMobile());
//		dest.setEmail(source.getEmail());
//	}

    public List<UserDocumentVo> getDocuments() {
        return documents;
    }

    public void setDocuments(List<UserDocumentVo> documents) {
        this.documents = documents;
    }

}

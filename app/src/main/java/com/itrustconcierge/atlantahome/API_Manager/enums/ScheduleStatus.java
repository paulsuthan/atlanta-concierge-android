package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum ScheduleStatus {
    PENDING, STARTED, CLOSED, CANCELLED, PROCESSING, MOVED;

    public static ScheduleStatus fromString(String value) {
        ScheduleStatus scheduleStatus = null;
        for (ScheduleStatus status : ScheduleStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                scheduleStatus = status;
                break;
            }
        }
        return scheduleStatus;
    }
}


package com.itrustconcierge.atlantahome.API_Manager.enums;

/**
 * Created by Paul on 11/21/2017.
 */

public enum ConatctType {
    NOTIFY_ME, REFER_FRACHISE;

    public static ConatctType fromString(String value) {
        ConatctType conatctType = null;
        for (ConatctType type : ConatctType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                conatctType = type;
                break;
            }
        }
        return conatctType;
    }
}

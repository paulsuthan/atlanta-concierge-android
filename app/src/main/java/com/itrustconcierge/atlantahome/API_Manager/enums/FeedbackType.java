package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum FeedbackType {
    JOB, SUBS, FRANCHISE, COMPANY;

    public static FeedbackType fromString(String value) {
        FeedbackType feedbackType = null;
        for (FeedbackType type : FeedbackType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                feedbackType = type;
                break;
            }
        }
        return feedbackType;
    }
}
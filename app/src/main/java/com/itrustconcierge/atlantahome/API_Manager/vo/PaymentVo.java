package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentType;

import java.util.Date;

public class PaymentVo extends BaseVo {

    private String title;
    private Double amount;
    private PaymentType paymentType;
    private Long subject;
    private PaymentStatus paymentStatus;
    private Long paidOn;
    private PropertyVo property;
    private UserVo user;
    private Long startTime;
    private Long endTime;
    private String cardNumber;
    private String cardType;
    private String description;
    private Long expiresOn;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

//	public static Payment toDao(PaymentVo source) {
//		Payment target = new Payment();
//		target.setId(source.getId());
//		target.setAmount(source.getAmount());
//		target.setPaidOn(source.getPaidOn());
//		target.setPaymentStatus(source.getPaymentStatus());
//		target.setPaymentType(source.getPaymentType());
//		target.setTitle(source.getTitle());
//		target.setSubject(source.getSubject());
//		return target;
//	}
//
//	public static PaymentVo toVo(Payment source) {
//		PaymentVo target = new PaymentVo();
//		target.setId(source.getId());
//		target.setAmount(source.getAmount());
//		target.setPaidOn(source.getPaidOn());
//		target.setPaymentStatus(source.getPaymentStatus());
//		target.setPaymentType(source.getPaymentType());
//		target.setTitle(source.getTitle());
//		target.setSubject(source.getSubject());
//		target.setUser(UserVo.toVo(source.getUser(), false));
//		return target;
//	}
//
//	public static List<PaymentVo> toVoList(List<Payment> sourceList) {
//		final List<PaymentVo>  targetList = new ArrayList<>();
//		sourceList.forEach((source)->{
//			targetList.add(toVo(source));
//		});
//		return targetList;
//	}

    public PropertyVo getProperty() {
        return property;
    }

    public void setProperty(PropertyVo property) {
        this.property = property;
    }

    public UserVo getUser() {
        return user;
    }

    public void setUser(UserVo user) {
        this.user = user;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Long expiresOn) {
        this.expiresOn = expiresOn;
    }

    public Long getPaidOn() {
        return paidOn;
    }

    public void setPaidOn(Long paidOn) {
        this.paidOn = paidOn;
    }
}

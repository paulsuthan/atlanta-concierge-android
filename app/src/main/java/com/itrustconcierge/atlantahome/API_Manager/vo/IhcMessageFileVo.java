package com.itrustconcierge.atlantahome.API_Manager.vo;

public class IhcMessageFileVo extends BaseVo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private IhcMessageVo message;
	
	private IhcFileVo file;

	public IhcMessageVo getMessage() {
		return message;
	}

	public void setMessage(IhcMessageVo message) {
		this.message = message;
	}

	public IhcFileVo getFile() {
		return file;
	}

	public void setFile(IhcFileVo file) {
		this.file = file;
	}

}

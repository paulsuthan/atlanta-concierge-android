package com.itrustconcierge.atlantahome.API_Manager;

/**
 * Created by paul on 7/27/2017.
 */

import android.content.Context;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;


public class ApiClient {

    public static final int PORT = 443; // production server
    public static final String SCHEME = "https"; // production server
    public static final String HOST = "www.itrustconcierge.com"; // production serverAn
    public static final String API_PATH = "/api/v1/"; // production server

//    public static final int PORT = 8080; // UAT server
//    public static final String SCHEME = "http"; // UAT server
//    public static final String HOST = "uat.itrustconcierge.com"; // UAT server
//    public static final String API_PATH = "/ihc/api/v1/"; // UAT server

//    public static final int PORT = 8080; // local server
//    public static final String SCHEME = "http"; // local server
//    public static final String HOST = "192.168.0.14"; // local server
//    public static final String API_PATH = "/ihc/api/v1/"; // local server

    private static final String BASE_URL = SCHEME + "://" + HOST + ":" + PORT + API_PATH;
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder defaultHttpClient = new OkHttpClient().newBuilder();
    private static String authToken;

    public static Retrofit getClient(Context context) {
//        int cacheSize = 10 * 1024 * 1024; // 10 MB
//        Cache cache = new Cache(getCacheDir(context), cacheSize);
//
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .cache(cache)
//                .build();

        if (authenticateVoLocal != null) {
            authToken = authenticateVoLocal.getToken();
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        defaultHttpClient.addInterceptor(interceptor).build();
        defaultHttpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                if (originalRequest.url().toString().contains("/signin")) {
                    return chain.proceed(originalRequest);
                } else if (originalRequest.url().toString().contains("/signup")) {
                    return chain.proceed(originalRequest);
                } else if (originalRequest.url().toString().contains("/forgotPassword")) {
                    return chain.proceed(originalRequest);
                } else if (originalRequest.url().toString().contains("/signup/contact")) {
                    String token = authToken;
                    Request newRequest = originalRequest.newBuilder()
                            .header("Authorization", "Bearer " + token)
                            .build();

                    return chain.proceed(newRequest);
                }

                String token = authToken;
                Request newRequest = originalRequest.newBuilder()
                        .header("Authorization", "Bearer " + token)
                        .build();

                return chain.proceed(newRequest);
//                    Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + authToken).build();
//                    return chain.proceed(request);
            }
        });

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(defaultHttpClient.build())
                    .build();
        }
        return retrofit;
    }

    private static File getCacheDir(Context context) {
        File file = new File(String.valueOf(new File(context.getCacheDir(), "okHttpCache")));
        return file;
    }

    public static class JsonDateDeserializer implements JsonDeserializer<Date> {
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            String s = json.getAsJsonPrimitive().getAsString();
            long l = Long.parseLong(s.substring(6, s.length() - 2));
            Date d = new Date(l);
            return d;
        }
    }
}

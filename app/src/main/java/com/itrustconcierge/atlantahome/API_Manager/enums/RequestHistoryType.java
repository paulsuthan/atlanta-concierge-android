package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum RequestHistoryType {

    REQUEST, LEAD, DENY_REQUEST, QUESTION, VISIT_REQUESTED, VISIT_SCHEDULED, QUOTE_REQUESTED, QUOTE, QUOTE_UNDER_REVISION, AUTO_APPROVED_QUOTE,
    APPROVED_QUOTE, ASSIGNED, PENDING, JOB_SCHEDULE_REQUESTED,JOB_SCHEDULE_CONFIRMED, SCHEDULED,STARTED, COMPLETED, INVOICE, APPROVED_INVOICE, CLOSED, ACCEPTED,
    CANCELLED, EMP_ACCEPTED, EMP_DECLINED, EMP_CANCELLED, JOB_CLOSED;

    private String name;

    public static RequestHistoryType fromString(String value) {
        RequestHistoryType requestHistoryType = null;
        for (RequestHistoryType type : RequestHistoryType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                requestHistoryType = type;
                break;
            }
        }
        return requestHistoryType;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        this.name = this.name();
        for (RequestHistoryType type : RequestHistoryType.values()) {
            if (this.name().equalsIgnoreCase("REQUEST")) {
                this.name = "JOB REQUEST MADE";
                break;
            } else if (this.name().equalsIgnoreCase("LEAD")) {
                this.name = "JOB REQUEST MADE";
                break;
            } else if (this.name().equalsIgnoreCase("ACCEPTED")) {
                this.name = "JOB REQUEST MADE";
                break;
            } else if (this.name().equalsIgnoreCase("VISIT_REQUESTED")) {
                this.name = "QUOTE VISIT REQUESTED";
                break;
            } else if (this.name().equalsIgnoreCase("VISIT_SCHEDULED")) {
                this.name = "QUOTE VISIT SCHEDULED";
                break;
            } else if (this.name().equalsIgnoreCase("QUOTE")) {
                this.name = "QUOTE RECEIVED";
                break;
            } else if (this.name().equalsIgnoreCase("QUOTE_UNDER_REVISION")) {
                this.name = "REVISE QUOTE";
                break;
            } else if (this.name().equalsIgnoreCase("APPROVED_QUOTE")) {
                this.name = "QUOTE APPROVED";
                break;
            } else if (this.name().equalsIgnoreCase("AUTO_APPROVED_QUOTE")) {
                this.name = "QUOTE APPROVED";
                break;
            } else if (this.name().equalsIgnoreCase("JOB_SCHEDULE_REQUESTED")) {
                this.name = "JOB SCHEDULE REQUESTED";
                break;
            } else if (this.name().equalsIgnoreCase("JOB_SCHEDULE_CONFIRMED")) {
                this.name = "JOB SCHEDULE CONFIRMED";
                break;
            } else if (this.name().equalsIgnoreCase("STARTED")) {
                this.name = "WORK STARTED";
                break;
            } else if (this.name().equalsIgnoreCase("COMPLETED")) {
                this.name = "CONCIERGE REVIEWING WORK";
                break;
            } else if (this.name().equalsIgnoreCase("INVOICE")) {
                this.name = "CONCIERGE REVIEWING WORK";
                break;
            } else if (this.name().equalsIgnoreCase("APPROVED_INVOICE")) {
                this.name = "PAYMENT REQUESTED";
                break;
            } else if (this.name().equalsIgnoreCase("CLOSED")) {
                this.name = "JOB COMPLETED AND CLOSED";
                break;
            } else if (this.name().equalsIgnoreCase("JOB_CLOSED")) {
                this.name = "JOB CLOSED";
                break;
            }
        }
        return this.name;
    }
}

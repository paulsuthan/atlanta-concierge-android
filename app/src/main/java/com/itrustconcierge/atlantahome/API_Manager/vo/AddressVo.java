package com.itrustconcierge.atlantahome.API_Manager.vo;

public class AddressVo extends BaseVo {

    private String line1;
    private String line2;
    private String displayAddress;
    private String city;
    private String state;
    private String zipCode;
    private String subDivision;
    private LocationVo location;

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getDisplayAddress() {
        return displayAddress;
    }

    public void setDisplayAddress(String displayAddress) {
        this.displayAddress = displayAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getSubDivision() {
        return subDivision;
    }

    public void setSubDivision(String subDivision) {
        this.subDivision = subDivision;
    }

    public LocationVo getLocation() {
        return location;
    }

    public void setLocation(LocationVo location) {
        this.location = location;
    }

//	public static AddressVo toVo(Address source){
//
//		AddressVo dest = null;
//		if(source != null){
//			dest = new AddressVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setCity(source.getCity());
//			dest.setDisplayAddress(source.getDisplayAddress());
//			dest.setLine1(source.getLine1());
//			dest.setLine2(source.getLine2());
//			dest.setState(source.getState());
//			dest.setSubDivision(source.getSubDivision());
//			dest.setZipCode(source.getZipCode());
//
//		}
//
//		return dest;
//
//
//	}
//
//	public static void merge(Address dest, AddressVo source){
//		if(dest != null && source != null){
//			dest.setCity(source.getCity());
//			dest.setDisplayAddress(source.getDisplayAddress());
//			dest.setLine1(source.getLine1());
//			dest.setLine2(source.getLine2());
//			dest.setState(source.getState());
//			dest.setSubDivision(source.getSubDivision());
//			dest.setZipCode(source.getZipCode());
//		}
//	}

}

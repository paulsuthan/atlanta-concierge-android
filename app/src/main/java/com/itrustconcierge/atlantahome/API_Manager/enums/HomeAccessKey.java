package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum HomeAccessKey {
    CONTACT_ME_FOR_ACCESS_KEY, CONTACT_MASTER_CONCIERGE, NOT_APPLICABLE;

    public static HomeAccessKey fromString(String value) {
        HomeAccessKey accessKey = null;
        for (HomeAccessKey key : HomeAccessKey.values()) {
            if (key.name().equalsIgnoreCase(value)) {
                accessKey = key;
                break;
            }
        }
        return accessKey;
    }
}

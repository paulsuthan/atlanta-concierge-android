package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.NotificationType;

public class NotificationVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private String message;

    private NotificationType type;

    private UserVo source;

    private UserVo target;

    private Long typeId;

    private Boolean read;

    private Boolean consumed;

    private String extra;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public UserVo getSource() {
        return source;
    }

    public void setSource(UserVo source) {
        this.source = source;
    }

    public UserVo getTarget() {
        return target;
    }

    public void setTarget(UserVo target) {
        this.target = target;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getConsumed() {
        return consumed;
    }

    public void setConsumed(Boolean consumed) {
        this.consumed = consumed;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

//	public static NotificationVo toVo(Notification source){
//		NotificationVo dest = null;
//		if(source != null){
//			dest = new NotificationVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setMessage(source.getMessage());
//			dest.setTypeId(source.getTypeId());
//			dest.setSource(UserVo.toVo(source.getSource(), false));
//			dest.setTarget(UserVo.toVo(source.getTarget(), false));
//			dest.setType(source.getType());
//		}
//
//		return dest;
//	}
//
//	public static List<NotificationVo> toVos(List<Notification> list){
//		List<NotificationVo> vos = new ArrayList<NotificationVo>();
//		for(Notification noti : list){
//			vos.add(NotificationVo.toVo(noti));
//		}
//		return vos;
//	}

}

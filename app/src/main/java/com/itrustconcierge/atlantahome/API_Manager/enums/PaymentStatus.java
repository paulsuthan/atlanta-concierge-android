package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum PaymentStatus {
    PENDING, COMPLETED;

    public static PaymentStatus fromString(String value) {
        PaymentStatus paymentStatus = null;
        for (PaymentStatus status : PaymentStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                paymentStatus = status;
                break;
            }
        }
        return paymentStatus;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum DeviceType {
    WEB, IOS, ANDROID, IPAD, ANDROID_TABLET;

    public static DeviceType fromString(String value) {
        DeviceType deviceType = null;
        for (DeviceType type : DeviceType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                deviceType = type;
                break;
            }
        }
        return deviceType;
    }
}

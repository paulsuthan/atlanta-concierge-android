package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;

public class JobRequestVO extends BaseVo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserVo user;
	
	private HomeOwnerRequestVo request;
	
	private RequestHistoryType status;

	public UserVo getUser() {
		return user;
	}

	public void setUser(UserVo user) {
		this.user = user;
	}

	public HomeOwnerRequestVo getRequest() {
		return request;
	}

	public void setRequest(HomeOwnerRequestVo request) {
		this.request = request;
	}

	public RequestHistoryType getStatus() {
		return status;
	}

	public void setStatus(RequestHistoryType status) {
		this.status = status;
	}
}

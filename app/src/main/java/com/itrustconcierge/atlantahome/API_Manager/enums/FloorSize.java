package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum FloorSize {
    SQFT_LESS_THEN_2000, SQFT_2000_TO_4000, SQFT_4000_8000, SQFT_ABOVE_8000, SQFT_0_TO_3000, SQFT_3000_TO_5000, SQFT_ABOVE_5000;

    public static FloorSize fromString(String value) {
        FloorSize floorSize = null;
        for (FloorSize size : FloorSize.values()) {
            if (size.name().equalsIgnoreCase(value)) {
                floorSize = size;
                break;
            }
        }
        return floorSize;
    }

    private String name;

    public String getName() {
        this.name = this.name();
        for (RequestHistoryType type : RequestHistoryType.values()) {
            if (this.name().equalsIgnoreCase("SQFT_LESS_THEN_2000")) {
                this.name = "Less than 2000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_2000_TO_4000")) {
                this.name = "2000 to 4000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_4000_8000")) {
                this.name = "4000 to 8000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_ABOVE_8000")) {
                this.name = "above 8000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_0_TO_3000")) {
                this.name = "less than 3000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_3000_TO_5000")) {
                this.name = "3000 to 5000";
                break;
            } else if (this.name().equalsIgnoreCase("SQFT_ABOVE_5000")) {
                this.name = "above 5000";
                break;
            }
        }
        return this.name;
    }

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

public class FeedbackParameterVo extends BaseVo {

    private Double rating;
    private String name;
    private Float weightage;
    private String toolTip;
    private String definition;
    private String importance;
    private String priority;
    private String iconUrl;

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getWeightage() {
        return weightage;
    }

    public void setWeightage(Float weightage) {
        this.weightage = weightage;
    }

    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @Override
    public String toString() {
        return "FeedbackParameterVo [rating=" + rating + ", name=" + name + ", weightage=" + weightage + ", toolTip="
                + toolTip + ", definition=" + definition + ", importance=" + importance + ", priority=" + priority
                + ", iconUrl=" + iconUrl + "]";
    }


}

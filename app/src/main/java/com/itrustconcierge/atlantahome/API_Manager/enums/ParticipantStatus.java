package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum ParticipantStatus {
    APPROVED, DENIED, PENDING;

    public static ParticipantStatus fromString(String value) {
        ParticipantStatus participantStatus = null;
        for (ParticipantStatus status : ParticipantStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                participantStatus = status;
                break;
            }
        }
        return participantStatus;
    }
}

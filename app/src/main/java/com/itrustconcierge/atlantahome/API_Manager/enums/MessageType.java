package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum MessageType {

    MESSAGE, IMAGE, CONTACT, LOCATION, IMAGE_MESSAGE;

    public static MessageType fromString(String value) {
        MessageType messageType = null;
        for (MessageType type : MessageType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                messageType = type;
                break;
            }
        }
        return messageType;
    }

}

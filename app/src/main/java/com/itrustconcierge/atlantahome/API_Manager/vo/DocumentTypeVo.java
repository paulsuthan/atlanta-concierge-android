package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DocumentTypeVo extends BaseVo implements Serializable {

    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//	public static DocumentTypeVo toVo(DocumentType source) {
//		DocumentTypeVo dest = null;
//
//		if (source != null) {
//			dest = new DocumentTypeVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//		}
//
//		return dest;
//	}
//
//	public static void merge(DocumentType dest, DocumentTypeVo source) {
//		dest.setName(source.getName());
//		dest.setDescription(source.getDescription());
//	}
//
//	public static DocumentTypeVo toVo(DocType source) {
//		DocumentTypeVo dest = null;
//
//		if (source != null) {
//			dest = new DocumentTypeVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//		}
//
//		return dest;
//	}
//
//	public static List<DocumentTypeVo> toVoList(List<DocType> sourceList) {
//		final List<DocumentTypeVo> destList = new ArrayList<>();
//		if (sourceList != null && (!sourceList.isEmpty())) {
//			sourceList.forEach(source->{
//				destList.add(toVo(source));
//			});
//		}
//		return destList;
//	}
//
//	public static DocType toDao(DocumentTypeVo source) {
//		DocType dest = null;
//		if (source != null) {
//			dest = new DocType();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//		}
//		return dest;
//	}


}

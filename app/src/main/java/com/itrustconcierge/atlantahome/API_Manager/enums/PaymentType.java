package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum PaymentType {

    SUBSCRIBE, PACKAGE, INVOICE, SP_PAYMENT;

    public static PaymentType fromString(String value) {
        PaymentType paymentType = null;
        for (PaymentType type : PaymentType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                paymentType = type;
                break;
            }
        }
        return paymentType;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum LotSize {
    ACRE_LESS_THAN_1, ACRE_1_TO_3, ACRE_ABOVE_3;

    public static LotSize fromString(String value) {
        LotSize lotSize = null;
        for (LotSize size : LotSize.values()) {
            if (size.name().equalsIgnoreCase(value)) {
                lotSize = size;
                break;
            }
        }
        return lotSize;
    }
}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import java.util.List;

public class FeedbackCommentVo extends BaseVo {

    private String comment;
    private Long date;
    private String user;
    private Boolean isExternalUser;
    private Long parentId;
    private List<FeedbackCommentVo> feedbackComments;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<FeedbackCommentVo> getFeedbackComments() {
        return feedbackComments;
    }

    public void setFeedbackComments(List<FeedbackCommentVo> feedbackComments) {
        this.feedbackComments = feedbackComments;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Boolean getIsExternalUser() {
        return isExternalUser;
    }

    public void setIsExternalUser(Boolean isExternalUser) {
        this.isExternalUser = isExternalUser;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }


}

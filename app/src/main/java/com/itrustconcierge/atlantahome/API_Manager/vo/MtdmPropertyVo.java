package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.FloorSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.LotSize;
import com.itrustconcierge.atlantahome.API_Manager.enums.PropertyType;

public class MtdmPropertyVo extends BaseVo{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private AuthenticateVo owner;
    private UserVo managedBy;
    private UserVo refferedBy;

    private AddressVo address;

    private PropertyType propertyType;
    private FloorSize floorSize;
    private LotSize lotSize;

    private String image;

    private long floorSizeInSqFt;

    private Double lotSizeInAcres = new Double(0);

    private String propertyTypeValue;


    public AuthenticateVo getOwner() {
        return owner;
    }


    public void setOwner(AuthenticateVo owner) {
        this.owner = owner;
    }


    public UserVo getManagedBy() {
        return managedBy;
    }


    public void setManagedBy(UserVo managedBy) {
        this.managedBy = managedBy;
    }


    public UserVo getRefferedBy() {
        return refferedBy;
    }


    public void setRefferedBy(UserVo refferedBy) {
        this.refferedBy = refferedBy;
    }


    public AddressVo getAddress() {
        return address;
    }


    public void setAddress(AddressVo address) {
        this.address = address;
    }




    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }


    public PropertyType getPropertyType() {
        return propertyType;
    }


    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }


    public FloorSize getFloorSize() {
        return floorSize;
    }


    public void setFloorSize(FloorSize floorSize) {
        this.floorSize = floorSize;
    }


    public LotSize getLotSize() {
        return lotSize;
    }


    public void setLotSize(LotSize lotSize) {
        this.lotSize = lotSize;
    }

    public long getFloorSizeInSqFt() {
        return floorSizeInSqFt;
    }


    public void setFloorSizeInSqFt(long floorSizeInSqFt) {
        this.floorSizeInSqFt = floorSizeInSqFt;
    }


    public Double getLotSizeInAcres() {
        return lotSizeInAcres;
    }


    public void setLotSizeInAcres(Double lotSizeInAcres) {
        this.lotSizeInAcres = lotSizeInAcres;
    }


    public String getPropertyTypeValue() {
        return propertyTypeValue;
    }


    public void setPropertyTypeValue(String propertyTypeValue) {
        this.propertyTypeValue = propertyTypeValue;
    }

}


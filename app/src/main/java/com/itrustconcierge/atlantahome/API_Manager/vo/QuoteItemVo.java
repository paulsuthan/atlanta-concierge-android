package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.QuoteItemType;

public class QuoteItemVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String name;

    private QuoteItemType type;

    private String description;

    private Double amount;

    private Double quantity;

    private Double total;

    private Double tax;

    private Double taxAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuoteItemType getType() {
        return type;
    }

    public void setType(QuoteItemType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

//	public static QuoteItemVo toVo(QuoteItem source){
//
//		QuoteItemVo dest = null;
//
//		if (source != null){
//			dest = new QuoteItemVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//			dest.setAmount(source.getAmount());
//			dest.setQuantity(source.getQuantity());
//			dest.setTotal(source.getTotal());
//			dest.setType(source.getType());
//		}
//
//		return dest;
//	}
//
//	public static void merge(QuoteItem dest, QuoteItemVo source){
//		if(source != null && dest != null){
//			dest.setName(source.getName());
//			dest.setDescription(source.getDescription());
//			dest.setAmount(source.getAmount());
//			dest.setQuantity(source.getQuantity());
//			dest.setTotal(source.getTotal());
//			dest.setType(source.getType());
//		}
//	}

}

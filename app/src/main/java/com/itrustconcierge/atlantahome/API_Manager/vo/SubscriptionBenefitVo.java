package com.itrustconcierge.atlantahome.API_Manager.vo;

public class SubscriptionBenefitVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private String description;

    private Boolean active;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}

package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.MessageType;

public class MessageVo extends BaseVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ConversationVo conversation;

    private UserVo sender;

    private String message;

    private MessageType type;

    private String image;

    public ConversationVo getConversation() {
        return conversation;
    }

    public void setConversation(ConversationVo conversation) {
        this.conversation = conversation;
    }

    public UserVo getSender() {
        return sender;
    }

    public void setSender(UserVo sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//	public static MessageVo toVo(Message source, boolean loadParent){
//		MessageVo dest = null;
//
//		if(source != null){
//			dest = new MessageVo();
//			dest.setId(source.getId());
//			if(loadParent){
//				dest.setConversation(ConversationVo.toVo(source.getConversation()));
//			}
//			dest.setImage(source.getImage());
//			dest.setMessage(source.getMessage());
//			dest.setType(source.getType());
//			dest.setSender(UserVo.toVo(source.getSender(), false));
//			dest.setId(source.getId());
//		}
//
//		return dest;
//	}
//
//	public static List<MessageVo> toVos(List<Message> list){
//
//		List<MessageVo> vos = new ArrayList<MessageVo>();
//
//		for(Message message : list){
//			vos.add(MessageVo.toVo(message, false));
//		}
//
//		return vos;
//
//	}

}

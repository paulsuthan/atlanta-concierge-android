package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum RateIncrementType {

    HOURLY, FIFTEEN_MINUTES;

    public static RateIncrementType fromString(String value) {
        RateIncrementType rateIncrementType = null;
        for (RateIncrementType type : RateIncrementType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                rateIncrementType = type;
                break;
            }
        }
        return rateIncrementType;
    }

}

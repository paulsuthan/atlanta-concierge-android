package com.itrustconcierge.atlantahome.API_Manager.enums;

public enum CalendarType {
    JOB, DISCUESS, VISIT_FOR_QUOTE, SCHEDULE, HOME_INVENTORY, HOME_WATCH, HOME_INSPECTION, DISCUSSION;

    private String name;

    public static CalendarType fromString(String value) {
        CalendarType calendarType = null;
        for (CalendarType type : CalendarType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                calendarType = type;
                break;
            }
        }
        return calendarType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        this.name = this.name();
        return this.name;
    }
}

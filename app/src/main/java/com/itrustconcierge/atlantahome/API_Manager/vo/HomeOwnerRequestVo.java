package com.itrustconcierge.atlantahome.API_Manager.vo;

import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;

import java.util.List;

public class HomeOwnerRequestVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private UserVo homeOwner;

    private ServiceVo service;

    private boolean urgent;

    private String preferredTimeToCompletion;

    private String description;

    private PropertyVo property;

    private List<RequestHistoryVo> historyItems;

    private RequestHistoryType status;

    private String image;

    private String accessKey;

    private ConversationVo conversation;

    private QuoteVo approvedQuote;

    private QuoteVo invoice;

    private List<Long> visitSchedule;

    private Long visitForQuoteScheduledOn;

    private List<Long> availableScheduleDates;

    private Long scheduledOn;

    private Integer estimatedTime;

    private TimeUnit estimatedTimeUnit;

    private Boolean fromPackage;

    private Long packageId;

    private UserVo assignedTo;

    private UserVo assignedBy;

    private Boolean isAssigned;

    private UserVo tenant;

    private Long scheduleId;

    private List<JobRequestVO> jobRequests;

    private UserVo createdBy;

    private Boolean deleted = false;

    private List<NotificationVo> notification;

    public UserVo getHomeOwner() {
        return homeOwner;
    }

    public void setHomeOwner(UserVo homeOwner) {
        this.homeOwner = homeOwner;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public String getPreferredTimeToCompletion() {
        return preferredTimeToCompletion;
    }

    public void setPreferredTimeToCompletion(String preferredTimeToCompletion) {
        this.preferredTimeToCompletion = preferredTimeToCompletion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//	public static HomeOwnerRequestVo toVo(HomeOwnerRequest source, boolean loadChildren){
//		HomeOwnerRequestVo dest = null;
//		if(source != null){
//			dest = new HomeOwnerRequestVo();
//			dest.setId(source.getId());
//			dest.setCreatedOn(source.getCreatedOn());
//			dest.setLastUpdatedOn(source.getLastUpdatedOn());
//			dest.setDescription(source.getDescription());
//			dest.setHomeOwner(UserVo.toVo(source.getHomeOwner(), false));
//			dest.setProperty(PropertyVo.toVo(source.getProperty(), false));
//			dest.setService(ServiceVo.toVo(source.getService()));
//			dest.setStatus(source.getStatus());
//			dest.setConversation(ConversationVo.toVo(source.getConversation()));
//			if(source.getApprovedQuote() != null){
//				dest.setApprovedQuote(QuoteVo.toVo(source.getApprovedQuote(), false, false));
//			}
//			if(loadChildren){
//				dest.setHistoryItems(RequestHistoryVo.toVOs(source.getHistoryItems(), false));
//			}
//		}
//
//
//		return dest;
//	}

    public PropertyVo getProperty() {
        return property;
    }

    public void setProperty(PropertyVo property) {
        this.property = property;
    }

    public ServiceVo getService() {
        return service;
    }

    public void setService(ServiceVo service) {
        this.service = service;
    }

    public List<RequestHistoryVo> getHistoryItems() {
        return historyItems;
    }

    public void setHistoryItems(List<RequestHistoryVo> historyItems) {
        this.historyItems = historyItems;
    }

    public RequestHistoryType getStatus() {
        return status;
    }

    public void setStatus(RequestHistoryType status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ConversationVo getConversation() {
        return conversation;
    }

    public void setConversation(ConversationVo conversation) {
        this.conversation = conversation;
    }

    public QuoteVo getApprovedQuote() {
        return approvedQuote;
    }

    public void setApprovedQuote(QuoteVo approvedQuote) {
        this.approvedQuote = approvedQuote;
    }

    public QuoteVo getInvoice() {
        return invoice;
    }

    public void setInvoice(QuoteVo invoice) {
        this.invoice = invoice;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public Long getScheduledOn() {
        return scheduledOn;
    }

    public void setScheduledOn(Long scheduledOn) {
        this.scheduledOn = scheduledOn;
    }

    public List<Long> getAvailableScheduleDates() {
        return availableScheduleDates;
    }

    public void setAvailableScheduleDates(List<Long> availableScheduleDates) {
        this.availableScheduleDates = availableScheduleDates;
    }

    public Long getVisitForQuoteScheduledOn() {
        return visitForQuoteScheduledOn;
    }

    public void setVisitForQuoteScheduledOn(Long visitForQuoteScheduledOn) {
        this.visitForQuoteScheduledOn = visitForQuoteScheduledOn;
    }

    public List<Long> getVisitSchedule() {
        return visitSchedule;
    }

    public void setVisitSchedule(List<Long> visitSchedule) {
        this.visitSchedule = visitSchedule;
    }

    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public TimeUnit getEstimatedTimeUnit() {
        return estimatedTimeUnit;
    }

    public void setEstimatedTimeUnit(TimeUnit estimatedTimeUnit) {
        this.estimatedTimeUnit = estimatedTimeUnit;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public Boolean getFromPackage() {
        return fromPackage;
    }

    public void setFromPackage(Boolean fromPackage) {
        this.fromPackage = fromPackage;
    }

    public UserVo getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(UserVo assignedTo) {
        this.assignedTo = assignedTo;
    }

    public UserVo getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(UserVo assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Boolean getAssigned() {
        return isAssigned;
    }

    public void setAssigned(Boolean assigned) {
        isAssigned = assigned;
    }

    public void setTenant(UserVo tenant) {
        this.tenant = tenant;
    }

    public UserVo getTenant() {
        return tenant;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public List<JobRequestVO> getJobRequests() {
        return jobRequests;
    }

    public void setJobRequests(List<JobRequestVO> jobRequests) {
        this.jobRequests = jobRequests;
    }

    public UserVo getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserVo createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<NotificationVo> getNotification() {
        return notification;
    }

    public void setNotification(List<NotificationVo> notification) {
        this.notification = notification;
    }
}

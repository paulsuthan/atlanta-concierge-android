package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.MessageType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.ConversationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.MessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UserVo;
import com.itrustconcierge.atlantahome.Adapters.MessageAdapter;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TEXT;
import static com.itrustconcierge.atlantahome.Utilities.Utility.PERMISSION_IMAGE_TITTLE;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

/**
 * Created by Paul on 11/16/2017.
 */

public class RequestHistory extends AppCompatActivity {

    LinearLayout loading_layout;
    ProgressBar progressBar;
    List<MessageVo> messages = new ArrayList<>();
    MessageAdapter mAdapter;
    RecyclerView recyclerView;
    ImageButton chatSendButton, takeImageButton;
    EditText messageEdit;
    Button interestedButton;
    SwipeRefreshLayout swipeRefreshLayout;
    private String mimeType;
    private String fileName;
    private File filetoUpload;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    TextView myImageViewText, toolBarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);

        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loading_layout = (LinearLayout) findViewById(R.id.loading_layout);
        loading_layout.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        recyclerView = (RecyclerView) findViewById(R.id.messagesContainer);
        chatSendButton = (ImageButton) findViewById(R.id.chatSendButton);
        takeImageButton = (ImageButton) findViewById(R.id.imageButton4);
        messageEdit = (EditText) findViewById(R.id.messageEdit);
        interestedButton = (Button) findViewById(R.id.interested);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        toolBarButton.setVisibility(View.GONE);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(RequestHistory.this);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.scrollToPosition(messages.size() - 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getIntent().getExtras() != null) {
                    if (getIntent().getExtras().getLong("targetId") != 0) {
                        if (checkInternetConenction(RequestHistory.this)) {
                            getMessages(getIntent().getExtras().getLong("targetId"));
                        } else
                            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getLong("targetId") != 0) {
                if (checkInternetConenction(RequestHistory.this)) {
                    getMessages(getIntent().getExtras().getLong("targetId"));
                } else
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
            if (getIntent().getExtras().getString("HO_Name") != null) {
//                toolbar.setTitle(getIntent().getExtras().getString("HO_Name"));
                myImageViewText.setText(getIntent().getExtras().getString("HO_Name"));
            } else {
//                toolbar.setTitle("Request History");
                myImageViewText.setText("Request History");
            }

            try {
                if (authenticateVoLocal.getUserTenantId() != getIntent().getExtras().getLong("targetTenantId")) {
                    Drawable img = myImageViewText.getContext().getResources().getDrawable(R.drawable.sp_avatar);
                    img.setBounds(0, 0, 80, 80);
                    myImageViewText.setCompoundDrawables(null, img, null, null);
                } else {
                    Drawable img = myImageViewText.getContext().getResources().getDrawable(R.drawable.concierge_chat);
                    img.setBounds(0, 0, 80, 80);
                    myImageViewText.setCompoundDrawables(null, img, null, null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!messageEdit.getText().toString().isEmpty()) {
                    if (checkInternetConenction(RequestHistory.this)) {
                        sendMessage(messageEdit.getText().toString());
                    } else
                        Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            }
        });

        takeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddPicturePopup();
            }
        });

        messageEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (checkInternetConenction(RequestHistory.this)) {
                        sendMessage(messageEdit.getText().toString());
                    } else
                        Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
                return false;
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Utility.REGISTRATION_COMPLETE)) {

                } else if (intent.getAction().equals(Utility.PUSH_NOTIFICATION)) {
                    try {
                        if (!intent.getExtras().getString("type", "").isEmpty()) {
                            String type = intent.getExtras().getString("type", "");
                            String typeId = intent.getExtras().getString("typeId", "");
                            String notifId = intent.getExtras().getString("notifId", "");
                            if (type.equals("MESSAGE")) {
                                MessageVo data = new MessageVo();
                                UserVo sourceUserVo = new UserVo();
                                sourceUserVo.setId(Long.valueOf(intent.getExtras().getString("sourceId", "")));
                                UserVo targetUserVo = new UserVo();
                                targetUserVo.setId(Long.valueOf(intent.getExtras().getString("targetId", "")));
                                data.setSender(sourceUserVo);
                                data.setType(MessageType.MESSAGE);
                                data.setId(Long.valueOf(typeId));
                                data.setMessage(intent.getExtras().getString("message", ""));
                                long time = System.currentTimeMillis();
                                data.setCreatedOn(time);
                                messages.add(data);
                                if (messages != null) {
                                    mAdapter = new MessageAdapter(messages, RequestHistory.this);
                                    recyclerView.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Utility.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Utility.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (getIntent().getExtras().getBoolean("isFromPush")) {
                Utility.gotoMessages = true;
                Intent intent = new Intent(RequestHistory.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                ActivityCompat.finishAffinity(RequestHistory.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(String s) {
        try {
            MessageVo messageVo = new MessageVo();
            messageVo.setMessage(s);
            ConversationVo conversationVo = new ConversationVo();
            conversationVo.setSource(authenticateVoLocal);
            UserVo userVo = new UserVo();
            userVo.setId(getIntent().getExtras().getLong("targetId"));
            conversationVo.setTarget(userVo);
            messageVo.setConversation(conversationVo);
            messageVo.setType(MessageType.MESSAGE);
            final Dialog loadingDialog = Utility.loadingDialog(RequestHistory.this);
            ApiInterface apiService = ApiClient.getClient(RequestHistory.this).create(ApiInterface.class);
            Call<ResponseWrapper<MessageVo>> call = apiService.sendMessage(messageVo);
            call.enqueue(new Callback<ResponseWrapper<MessageVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<MessageVo>> call, Response<ResponseWrapper<MessageVo>> response) {
                    loadingDialog.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    MessageVo data = response.body().getData();
                                    messages.add(data);
                                    if (messages != null) {
                                        mAdapter = new MessageAdapter(messages, RequestHistory.this);
                                        recyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                    } else {

                                    }
                                    messageEdit.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<MessageVo>> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendImage(String physicalFilename, String mimType) {
        try {
            MessageVo messageVo = new MessageVo();
            messageVo.setMessage(mimType);
            messageVo.setImage(physicalFilename);
            ConversationVo conversationVo = new ConversationVo();
            conversationVo.setSource(authenticateVoLocal);
            UserVo userVo = new UserVo();
            userVo.setId(getIntent().getExtras().getLong("targetId"));
            conversationVo.setTarget(userVo);
            messageVo.setConversation(conversationVo);
            messageVo.setConversation(conversationVo);
            messageVo.setType(MessageType.IMAGE);
            ApiInterface apiService = ApiClient.getClient(RequestHistory.this).create(ApiInterface.class);
            Call<ResponseWrapper<MessageVo>> call = apiService.sendMessage(messageVo);
            call.enqueue(new Callback<ResponseWrapper<MessageVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<MessageVo>> call, Response<ResponseWrapper<MessageVo>> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    MessageVo data = response.body().getData();
                                    messages.add(data);
                                    if (messages != null) {
                                        mAdapter = new MessageAdapter(messages, RequestHistory.this);
                                        recyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                    } else {

                                    }
                                    messageEdit.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<MessageVo>> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMessages(long targetId) {
        try {
            final Dialog loadingDialog = Utility.loadingDialog(RequestHistory.this);
            ApiInterface apiService = ApiClient.getClient(RequestHistory.this).create(ApiInterface.class);
            Call<ResponseWrapper<MessageVo>> call = apiService.getMessages(authenticateVoLocal.getId(), targetId);
            call.enqueue(new Callback<ResponseWrapper<MessageVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<MessageVo>> call, Response<ResponseWrapper<MessageVo>> response) {
                    loadingDialog.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getList() != null) {
                                try {
                                    if (!response.body().getList().isEmpty()) {
                                        messages = response.body().getList();
                                        mAdapter = new MessageAdapter(messages, RequestHistory.this);
                                        recyclerView.setAdapter(mAdapter);
                                    } else {

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<MessageVo>> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showAddPicturePopup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(RequestHistory.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.photos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(RequestHistory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else if (checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                final Dialog dialog1 = new Dialog(RequestHistory.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.allowlocation);
                dialog1.setCancelable(false);
                final Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageView imageView;
                TextView tittleTextView, textView, skipTextView;
                Button okTextView;

                imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                textView = (TextView) dialog1.findViewById(R.id.textView);
                skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                okTextView = (Button) dialog1.findViewById(R.id.ok);

                tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                textView.setText(PERMISSION_IMAGE_TEXT);
                imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                okTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        ActivityCompat.requestPermissions(RequestHistory.this, new String[]{android.Manifest.permission.CAMERA},
                                2);
                    }
                });

                skipTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                dialog1.show();

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant

                return;
            } else {
                selectFile();
            }
        } else {
            selectFile();
        }
    }

    String userChoosenTask;
    int REQUEST_CAMERA = 1;
    int SELECT_FILE = 2;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 10;

    private void selectFile() {
        final CharSequence[] items = {"Open Camera and Take Picture", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RequestHistory.this);
        builder.setTitle(null);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera and Take Picture")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri returnUri = data.getData();
        mimeType = getContentResolver().getType(returnUri);
        Cursor returnCursor =
                getContentResolver().query(returnUri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        fileName = returnCursor.getString(nameIndex);

        filetoUpload = destination;
        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(RequestHistory.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_image_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(false);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);
        final ImageView image = (ImageView) requestDialog.findViewById(R.id.image);

        image.post(new Runnable() {
            @Override
            public void run() {
                Bitmap myBitmap = BitmapFactory.decodeFile(filetoUpload.getPath());
                int nh = (int) (myBitmap.getHeight() * (512.0 / myBitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(myBitmap, 512, nh, true);
                image.setImageBitmap(scaled);
            }
        });
        negativeButton.setText("Cancel");
        positiveButton.setText("Send");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                uploadPicture(filetoUpload, fileName, mimeType);
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDialog.dismiss();
            }
        });

        requestDialog.show();
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                "before_work" + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mimeType = "image/jpeg";
        fileName = destination.getName();
        filetoUpload = destination;

        filetoUpload = destination;
        uploadPicture(filetoUpload, fileName, mimeType);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "ITC");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ITC", "Oops! Failed create "
                        + "ITC" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            final Dialog dialog1 = new Dialog(RequestHistory.this);
                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog1.setContentView(R.layout.allowlocation);
                            dialog1.setCancelable(false);
                            final Window window = dialog1.getWindow();
                            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            ImageView imageView;
                            TextView tittleTextView, textView, skipTextView;
                            Button okTextView;

                            imageView = (ImageView) dialog1.findViewById(R.id.imageView3);
                            tittleTextView = (TextView) dialog1.findViewById(R.id.tittle);
                            textView = (TextView) dialog1.findViewById(R.id.textView);
                            skipTextView = (TextView) dialog1.findViewById(R.id.skip);
                            okTextView = (Button) dialog1.findViewById(R.id.ok);

                            tittleTextView.setText(PERMISSION_IMAGE_TITTLE);
                            textView.setText(PERMISSION_IMAGE_TEXT);
                            imageView.setBackgroundResource(R.mipmap.add_images_and_videos);
                            okTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                    ActivityCompat.requestPermissions(RequestHistory.this, new String[]{android.Manifest.permission.CAMERA},
                                            2);
                                }
                            });

                            skipTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                }
                            });

                            dialog1.show();

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant

                            return;
                        } else {
                            selectFile();
                        }
                    } else {
                        selectFile();
                    }
                } else {
                    // permission denied
                }
                return;
            }

            case 2: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    selectFile();
                } else {

                    // permission denied
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    UploadVo uploadVo;

    private void uploadPicture(File filetoUpload, String fileName, String mimeType) {
        try {
            if (filetoUpload != null) {
                final Dialog loadingDialog = Utility.loadingDialog(RequestHistory.this);
                ApiInterface apiService = ApiClient.getClient(RequestHistory.this).create(ApiInterface.class);
                Call<ResponseWrapper<UploadVo>> call = apiService.getUploadUrl(UploadType.CHAT, fileName, mimeType, authenticateVoLocal.getId());
                call.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
                                        uploadVo = response.body().getData();
                                        new uploadImageTask().execute(uploadVo.getUrl());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            Snackbar.make(findViewById(R.id.relativeLayout), SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                        Snackbar.make(findViewById(R.id.relativeLayout), SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class uploadImageTask extends AsyncTask {
        Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            loadingDialog = Utility.loadingDialog(RequestHistory.this);
        }

        @Override
        protected Integer doInBackground(Object[] params) {
            int responseCode = 0;
            try {
                String decodeUrl = java.net.URLDecoder.decode((String) params[0], "UTF-8");
                URL url = new URL(decodeUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("content-type", mimeType);
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                FileInputStream fileInputStream = new FileInputStream(filetoUpload);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    out.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                out.flush();
                out.close();
                responseCode = connection.getResponseCode();
                System.out.println("Service returned response code " + responseCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onPostExecute(responseCode);
            return responseCode;
        }

        protected void onPostExecute(Integer result) {
            if (loadingDialog != null)
                loadingDialog.dismiss();
            if (result.equals(200)) {
                sendImage(uploadVo.getPhysicalFilename(), mimeType);
            } else {
                Snackbar.make(findViewById(R.id.relativeLayout), SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
            }
        }
    }

}


package com.itrustconcierge.atlantahome;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.NotificationType;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.IdListVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.NotificationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PaymentVo;
import com.itrustconcierge.atlantahome.Fragments.MessagesFragment;
import com.itrustconcierge.atlantahome.Fragments.MoreFragment;
import com.itrustconcierge.atlantahome.Fragments.NoInternetFragment;
import com.itrustconcierge.atlantahome.Fragments.NotificationsFragment;
import com.itrustconcierge.atlantahome.Fragments.PropertyListFragment;
import com.itrustconcierge.atlantahome.Fragments.WorkHistoryFragment;
import com.itrustconcierge.atlantahome.RequestFlow.RequestServiceActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.isAlreadyLoaded;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadJobsNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadMessagesNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.unReadNotificationVos;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoHome;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoMessages;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoMore;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoMyJobs;
import static com.itrustconcierge.atlantahome.Utilities.Utility.gotoNotifications;

public class MainActivity extends AppCompatActivity {

    public static AHBottomNavigation navigation;
    private static boolean needTransaction = false;
    TextView toolBarButton;
    ImageView toolbarTittleImageView;
    TextView textViewTittle;
    String name;
    PopupWindow noInternetPopupWindow;
    FrameLayout frameLayout;
    Dialog noInternetDialog;
    private Toolbar toolbar;
    private BroadcastReceiver mRegistrationBroadcastReceiver, noInternetHandlingReceiver;


    private AHBottomNavigation.OnTabSelectedListener mOnNavigationItemSelectedListener
            = new AHBottomNavigation.OnTabSelectedListener() {

        @Override
        public boolean onTabSelected(int position, boolean wasSelected) {
            Fragment selectedFragment = null;
            if (checkInternetConenction(MainActivity.this)) {
                switch (position) {
                    case 0:
//                    toolbar.setTitle(R.string.title_home);
//                    toolbar.setTitle(null);
//                    textViewTittle.setText(R.string.title_home);
                        textViewTittle.setText(name);
                        toolbarTittleImageView.setVisibility(View.VISIBLE);
                        toolBarButton.setVisibility(View.GONE);
//                    toolBarButton.setText("ADD PROPERTY");
                        selectedFragment = PropertyListFragment.newInstance();
                        break;
                    case 1:
//                  toolbar.setTitle(R.string.title_messages);
                        textViewTittle.setText(R.string.title_messages);
                        toolbarTittleImageView.setVisibility(View.VISIBLE);
                        toolBarButton.setVisibility(View.GONE);
                        selectedFragment = MessagesFragment.newInstance(1);
                        break;
                    case 2:
//                    toolbar.setTitle(R.string.title_jobs);
                        textViewTittle.setText(R.string.title_jobs);
                        toolbarTittleImageView.setVisibility(View.VISIBLE);
                        toolBarButton.setVisibility(View.VISIBLE);
//                    toolBarButton.setText("ADD REQUEST");
                        selectedFragment = WorkHistoryFragment.newInstance();
                        break;
//                    case 3:
////                    toolbar.setTitle(R.string.title_notifications);
//                        textViewTittle.setText(R.string.title_notifications);
//                        toolbarTittleImageView.setVisibility(View.VISIBLE);
//                        toolBarButton.setVisibility(View.GONE);
//                        selectedFragment = NotificationsFragment.newInstance();
//                        break;
                    case 3:
//                    toolbar.setTitle(R.string.title_more);
                        textViewTittle.setText(R.string.title_more);
                        toolbarTittleImageView.setVisibility(View.VISIBLE);
                        toolBarButton.setVisibility(View.GONE);
                        selectedFragment = MoreFragment.newInstance();
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().
                        beginTransaction();
                transaction.replace(R.id.content, selectedFragment);
                transaction.commit();
                return true;
            } else {
                if (position == 0) {
                    textViewTittle.setText(name);
                    toolbarTittleImageView.setVisibility(View.VISIBLE);
                    toolBarButton.setVisibility(View.GONE);
                    if (!isAlreadyLoaded) {
                        NoInternetFragment noInternetFragment = new NoInternetFragment();
                        FragmentTransaction transaction = getSupportFragmentManager().
                                beginTransaction();
                        transaction.replace(R.id.content, noInternetFragment);
                        transaction.commit();
                    } else {
                        selectedFragment = PropertyListFragment.newInstance();
                        FragmentTransaction transaction = getSupportFragmentManager().
                                beginTransaction();
                        transaction.replace(R.id.content, selectedFragment);
                        transaction.commit();
                    }
                } else if (position == 1) {
                    textViewTittle.setText(R.string.title_messages);
                    toolbarTittleImageView.setVisibility(View.VISIBLE);
                    toolBarButton.setVisibility(View.GONE);
                    selectedFragment = MessagesFragment.newInstance(1);
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                } else if (position == 2) {
                    textViewTittle.setText(R.string.title_jobs);
                    toolbarTittleImageView.setVisibility(View.VISIBLE);
                    toolBarButton.setVisibility(View.VISIBLE);
                    selectedFragment = WorkHistoryFragment.newInstance();
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                }
//                else if (position == 3) {
//                    textViewTittle.setText(R.string.title_notifications);
//                    toolbarTittleImageView.setVisibility(View.VISIBLE);
//                    toolBarButton.setVisibility(View.GONE);
//                    selectedFragment = NotificationsFragment.newInstance();
//                    FragmentTransaction transaction = getSupportFragmentManager().
//                            beginTransaction();
//                    transaction.replace(R.id.content, selectedFragment);
//                    transaction.commit();
//                }
                else if (position == 3) {
                    textViewTittle.setText(R.string.title_more);
                    toolbarTittleImageView.setVisibility(View.VISIBLE);
                    toolBarButton.setVisibility(View.GONE);
                    selectedFragment = MoreFragment.newInstance();
//                    navigation.setCurrentItem(4);
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                }
                return true;
            }
//        @Override
//        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//
//        }
        }
    };
    private FirebaseAnalytics mFirebaseAnalytics;

    private static void getUnReadNotifications(Activity activity) {
        ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
        Call<ResponseWrapper<NotificationVo>> call = apiService.getUnReadNotifications();
        call.enqueue(new Callback<ResponseWrapper<NotificationVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<NotificationVo>> call, Response<ResponseWrapper<NotificationVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                unReadNotificationVos = new ArrayList<>();
                                unReadJobsNotificationVos = new ArrayList<NotificationVo>();
                                unReadMessagesNotificationVos = new ArrayList<NotificationVo>();
                                unReadNotificationVos = response.body().getList();
                                if (!unReadNotificationVos.isEmpty()) {
                                    for (NotificationVo notificationVoBodyItem : unReadNotificationVos) {
                                        try {
                                            if (notificationVoBodyItem.getType().equals(NotificationType.LEAD) || notificationVoBodyItem.getType().equals(NotificationType.QUOTE)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.INVOICE_APPROVED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.QUOTE_APPROVED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.JOB_STARTED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.JOB_COMPLETED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.JOB_SCHEDULE_REQUESTED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.REQUEST)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.VISIT_REQUESTED)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.REQUEST_CLOSE)) {
                                                unReadJobsNotificationVos.add(notificationVoBodyItem);
                                            } else if (notificationVoBodyItem.getType().equals(NotificationType.MESSAGE)
                                                    || notificationVoBodyItem.getType().equals(NotificationType.REQUEST_CHAT)) {
                                                unReadMessagesNotificationVos.add(notificationVoBodyItem);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    List<NotificationVo> unReadNotificationVosTemp = new ArrayList<>();
                                    for (NotificationVo notificationVo : unReadNotificationVos) {
                                        if (!notificationVo.getType().equals(NotificationType.MESSAGE)
                                                || notificationVo.getType().equals(NotificationType.REQUEST_CHAT)) {
                                            unReadNotificationVosTemp.add(notificationVo);
                                        }
                                    }
                                    unReadNotificationVos = unReadNotificationVosTemp;
                                    if (!unReadJobsNotificationVos.isEmpty())
                                        navigation.setNotification(unReadJobsNotificationVos.size() + "", 2);
                                    if (!unReadMessagesNotificationVos.isEmpty())
                                        navigation.setNotification(unReadMessagesNotificationVos.size() + "", 1);
//                                    if (!unReadNotificationVos.isEmpty())
//                                        navigation.setNotification(unReadNotificationVos.size() + "", 3);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<NotificationVo>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
            }
        });
    }

    public static void readNotifications(final Activity activity, final List<NotificationVo> notificationVos) {
        List<Long> longs = new ArrayList<>();
        for (NotificationVo notificationVo : notificationVos) {
            longs.add(notificationVo.getId());
        }
        IdListVo idListVo = new IdListVo();
        idListVo.setList(longs);
        ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.readNotifications(idListVo);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                if (response.isSuccessful()) {
                    try {
                        if (notificationVos != null) {
                            if (!notificationVos.isEmpty()) {
                                try {
                                    if (notificationVos.get(0).getType().equals(NotificationType.LEAD)
                                            || notificationVos.get(0).getType().equals(NotificationType.QUOTE)
                                            || notificationVos.get(0).getType().equals(NotificationType.INVOICE_APPROVED)
                                            || notificationVos.get(0).getType().equals(NotificationType.QUOTE_APPROVED)
                                            || notificationVos.get(0).getType().equals(NotificationType.JOB_STARTED)
                                            || notificationVos.get(0).getType().equals(NotificationType.JOB_COMPLETED)
                                            || notificationVos.get(0).getType().equals(NotificationType.JOB_SCHEDULE_REQUESTED)
                                            || notificationVos.get(0).getType().equals(NotificationType.REQUEST)
                                            || notificationVos.get(0).getType().equals(NotificationType.VISIT_REQUESTED)
                                            || notificationVos.get(0).getType().equals(NotificationType.REQUEST_CLOSE)) {
                                        navigation.setNotification("", 2);
                                    } else if (notificationVos.get(0).getType().equals(NotificationType.MESSAGE)
                                            || notificationVos.get(0).getType().equals(NotificationType.REQUEST_CHAT)) {
                                        navigation.setNotification("", 1);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
//                        navigation.setNotification("", 3);
                        notificationVos.clear();
                        if (checkInternetConenction(activity)) {
                            getUnReadNotifications(activity);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkInternetConenction(MainActivity.this)) {
            getUnReadNotifications(MainActivity.this);
            getRequests();
            getPayments();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Utility.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Utility.PUSH_NOTIFICATION));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
//        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
//        toolbar.setTitle(R.string.title_home);
        toolbar.setTitle(null);
        toolbarTittleImageView = (ImageView) findViewById(R.id.toolbarTittleImageView);
        textViewTittle = (TextView) findViewById(R.id.textViewTittle);
        frameLayout = (FrameLayout) findViewById(R.id.content);

        ItrustHOApplication application = (ItrustHOApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Main Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        navigation = (AHBottomNavigation) findViewById(R.id.navigation);
        navigation.setOnTabSelectedListener(mOnNavigationItemSelectedListener);
        int[] tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.navigation);
        navigationAdapter.setupWithBottomNavigation(navigation, tabColors);
        navigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        navigation.setDefaultBackgroundColor(Color.parseColor("#f6f6f6"));
        navigation.setAccentColor(Color.parseColor("#449EDE"));
        navigation.setInactiveColor(Color.parseColor("#838383"));
        navigation.setForceTint(true);
        navigation.setNotificationBackgroundColor(Color.parseColor("#FF4081"));

        if (authenticateVoLocal != null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
            mFirebaseAnalytics.setUserProperty("userName", authenticateVoLocal.getFirstName() + " " + authenticateVoLocal.getLastName());
            mFirebaseAnalytics.setUserId(authenticateVoLocal.getId().toString());
        }

//        if (authenticateVoLocal.getFranchise() != null) {
//            String fname = authenticateVoLocal.getFranchise().getFirstName();
//            if (!fname.contains("iTrust Concierge"))
//                authenticateVoLocal.getFranchise().setFirstName("iTrust Concierge of " + fname);
//        }
        toolbarTittleImageView.setVisibility(View.VISIBLE);
        name = "Hello " + authenticateVoLocal.getFirstName() + "!";
        textViewTittle.setText(name);

        if (checkInternetConenction(MainActivity.this)) {
            Fragment selectedFragment = PropertyListFragment.newInstance();
            if (gotoHome) {
                selectedFragment = PropertyListFragment.newInstance();
                navigation.setCurrentItem(0);
            } else if (gotoMyJobs) {
                selectedFragment = WorkHistoryFragment.newInstance();
                navigation.setCurrentItem(2);
            } else if (gotoMessages) {
                selectedFragment = MessagesFragment.newInstance(1);
                navigation.setCurrentItem(1);
            }
//            else if (gotoNotifications) {
//                selectedFragment = NotificationsFragment.newInstance();
//                navigation.setCurrentItem(3);
//            }
            else if (gotoMore) {
                selectedFragment = MoreFragment.newInstance();
                navigation.setCurrentItem(3);
            }

            FragmentTransaction transaction = getSupportFragmentManager().
                    beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.commit();
        } else {
            Fragment selectedFragment = PropertyListFragment.newInstance();
            NoInternetFragment noInternetFragment = new NoInternetFragment();
            textViewTittle.setText(name);
            toolbarTittleImageView.setVisibility(View.VISIBLE);
            toolBarButton.setVisibility(View.GONE);

            if (gotoHome) {
                if (!isAlreadyLoaded) {
                    needTransaction = false;
                    navigation.setCurrentItem(0);
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, noInternetFragment);
                    transaction.commit();
                } else {
                    selectedFragment = PropertyListFragment.newInstance();
                    navigation.setCurrentItem(0);
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                }
            } else if (gotoMessages) {
                textViewTittle.setText(R.string.title_messages);
                toolbarTittleImageView.setVisibility(View.VISIBLE);
                toolBarButton.setVisibility(View.GONE);
                selectedFragment = MessagesFragment.newInstance(1);
                FragmentTransaction transaction = getSupportFragmentManager().
                        beginTransaction();
                transaction.replace(R.id.content, selectedFragment);
                transaction.commit();
            } else if (gotoMyJobs) {
                textViewTittle.setText(R.string.title_jobs);
                toolbarTittleImageView.setVisibility(View.VISIBLE);
                toolBarButton.setVisibility(View.VISIBLE);
                selectedFragment = WorkHistoryFragment.newInstance();
                navigation.setCurrentItem(2);
                FragmentTransaction transaction = getSupportFragmentManager().
                        beginTransaction();
                transaction.replace(R.id.content, selectedFragment);
                transaction.commit();
            }
//            else if (gotoNotifications) {
//                textViewTittle.setText(R.string.title_notifications);
//                toolbarTittleImageView.setVisibility(View.VISIBLE);
//                toolBarButton.setVisibility(View.GONE);
//                selectedFragment = NotificationsFragment.newInstance();
//                navigation.setCurrentItem(3);
//                FragmentTransaction transaction = getSupportFragmentManager().
//                        beginTransaction();
//                transaction.replace(R.id.content, selectedFragment);
//                transaction.commit();
//            }
            else if (gotoMore) {
                textViewTittle.setText(R.string.title_more);
                toolbarTittleImageView.setVisibility(View.VISIBLE);
                toolBarButton.setVisibility(View.GONE);
                selectedFragment = MoreFragment.newInstance();
                navigation.setCurrentItem(3);
                navigation.setCurrentItem(3);
                FragmentTransaction transaction = getSupportFragmentManager().
                        beginTransaction();
                transaction.replace(R.id.content, selectedFragment);
                transaction.commit();
            } else if (!gotoHome) {
                if (!isAlreadyLoaded) {
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, noInternetFragment);
                    transaction.commit();
                } else {
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                }
            }
        }

//        if (checkInternetConenction(MainActivity.this)) {
//            getUnReadNotifications(MainActivity.this);
//        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Utility.REGISTRATION_COMPLETE)) {

                } else if (intent.getAction().equals(Utility.PUSH_NOTIFICATION)) {
                    try {
                        if (!intent.getExtras().getString("type", "").isEmpty()) {
                            String type = intent.getExtras().getString("type", "");
                            String typeId = intent.getExtras().getString("typeId", "");
                            String notifId = intent.getExtras().getString("notifId", "");
                            NotificationVo notificationVo = new NotificationVo();
                            notificationVo.setType(NotificationType.fromString(type));
                            notificationVo.setTypeId(Long.valueOf(typeId));
                            notificationVo.setId(Long.valueOf(notifId));
                            if (type.equals("LEAD") || type.equals("QUOTE")
                                    || type.equals("INVOICE_APPROVED")
                                    || type.equals("QUOTE_APPROVED")
                                    || type.equals("JOB_STARTED")
                                    || type.equals("JOB_COMPLETED")
                                    || type.equals("JOB_SCHEDULE_REQUESTED")
                                    || type.equals("REQUEST")
                                    || type.equals("VISIT_REQUESTED")) {
                                if (unReadJobsNotificationVos != null) {
                                    unReadJobsNotificationVos.add(notificationVo);
                                }
                            } else if (type.equals("MESSAGE")) {
                                if (unReadMessagesNotificationVos != null) {
                                    unReadMessagesNotificationVos.add(notificationVo);
                                }
                            } else if (type.equals("REQUEST_CHAT")) {
                                if (unReadMessagesNotificationVos != null) {
                                    unReadMessagesNotificationVos.add(notificationVo);
                                }
                            }

                            if (!unReadJobsNotificationVos.isEmpty())
                                navigation.setNotification(unReadJobsNotificationVos.size() + "", 2);
                            if (!unReadMessagesNotificationVos.isEmpty())
                                navigation.setNotification(unReadMessagesNotificationVos.size() + "", 1);
//                            if (!unReadNotificationVos.isEmpty())
//                                navigation.setNotification(unReadNotificationVos.size() + "", 3);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        toolBarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (toolBarButton.getText().toString().contains("ADD PROPERTY")) {
//                    Intent intent = new Intent(MainActivity.this, AddPropertyActivity.class);
//                    startActivity(intent);
//                } else if (toolBarButton.getText().toString().contains("ADD REQUEST")) {
//                    Toast.makeText(MainActivity.this, "Click on 'REQUEST SERVICE' for which property you want to create request", Toast.LENGTH_LONG).show();

                if (checkInternetConenction(getApplicationContext())) {
                    Intent intent = new Intent(MainActivity.this, RequestServiceActivity.class);
                    startActivity(intent);
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });
    }

    private void getPayments() {
        unConsumedPaymentsSize = 0;
        ApiInterface apiService = ApiClient.getClient(MainActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<PaymentVo>> call = apiService.getPayments(PaymentStatus.PENDING);
        call.enqueue(new Callback<ResponseWrapper<PaymentVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<PaymentVo>> call, Response<ResponseWrapper<PaymentVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    for(PaymentVo paymentVo : response.body().getList()){
                                        unConsumedPaymentsSize++;
                                    }
                                    if(unConsumedPaymentsSize != 0){
                                        navigation.setNotification(unConsumedPaymentsSize + "", 3);
                                    } else {
                                        navigation.setNotification("", 3);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<PaymentVo>> call, Throwable t) {
                Log.e("Error", t.toString());
            }
        });
    }

    public static int unConsumedReqSize = 0, unConsumedPaymentsSize = 0;

    private void getRequests() {
        unConsumedReqSize = 0;
        ApiInterface apiService = ApiClient.getClient(MainActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequestHistoriesWithoutStatus();
        call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    for (HomeOwnerRequestVo homeOwnerRequestVo : response.body().getList()) {
                                        if (homeOwnerRequestVo.getFromPackage() == null) {
                                            homeOwnerRequestVo.setFromPackage(false);
                                        }
                                        if (!homeOwnerRequestVo.getFromPackage() && !homeOwnerRequestVo.getDeleted()) {
                                            if (homeOwnerRequestVo.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)
                                                    || homeOwnerRequestVo.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)
                                                    || homeOwnerRequestVo.getStatus().equals(RequestHistoryType.QUOTE)
                                                    || homeOwnerRequestVo.getStatus().equals(RequestHistoryType.APPROVED_INVOICE)) {
                                                unConsumedReqSize++;
                                            }
                                        }
                                    }
                                    if(unConsumedReqSize != 0){
                                        navigation.setNotification(unConsumedReqSize + "", 2);
                                    } else {
                                        navigation.setNotification("", 2);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                Log.e("Error", t.toString());
            }
        });
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(this, R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(getApplicationContext())) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

}

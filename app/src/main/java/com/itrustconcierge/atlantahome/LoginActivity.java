package com.itrustconcierge.atlantahome;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.DeviceType;
import com.itrustconcierge.atlantahome.API_Manager.enums.Role;
import com.itrustconcierge.atlantahome.API_Manager.vo.AuthenticateVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.DeviceInfoVo;
import com.itrustconcierge.atlantahome.Utilities.Utility;
import com.itrustconcierge.atlantahome.database.DatabaseHelper;
import com.itrustconcierge.atlantahome.database.model.Json;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SERVER_MAINTENANCE;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.setIsLoggedin;

public class LoginActivity extends AppCompatActivity {

    EditText mEmailView, mPasswordView;
    //    CallbackManager callbackManager;
    Button signInButton;
    Button signupButton;
    TextView forgotPassword;
    CoordinatorLayout relativeLayout;
    private DatabaseHelper db;

    @Override
    public void onStart() {
        super.onStart();

        // Branch init
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", referringParams.toString());
                    String firstName = referringParams.optString("firstName", "");
                    String lastName = referringParams.optString("lastName", "");
                    String email = referringParams.optString("email", "");
                    String mobileNumber = referringParams.optString("mobileNumber", "");
                    String type = referringParams.optString("type", "");
                    String inviteCode = referringParams.optString("inviteCode", "");
                    if (inviteCode != null && !inviteCode.isEmpty()) {
                        Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                        i.putExtra("firstName", firstName);
                        i.putExtra("lastName", lastName);
                        i.putExtra("email", email);
                        i.putExtra("mobileNumber", mobileNumber);
                        i.putExtra("inviteCode", inviteCode);
                        startActivity(i);
                    }
                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(LoginActivity.this);
//        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        signInButton = (Button) findViewById(R.id.sign_in_button);
        signupButton = (Button) findViewById(R.id.signupButton);
        relativeLayout = (CoordinatorLayout) findViewById(R.id.relativeLayout);
        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        db = new DatabaseHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(null);

        ItrustHOApplication application = (ItrustHOApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Login Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        forgotPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog requestDialog = new Dialog(new ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                requestDialog.setContentView(R.layout.code_validation_form);
                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                requestDialog.setCancelable(true);
                requestDialog.getWindow().setGravity(Gravity.BOTTOM);

                final EditText codeEditText;
                Button cancelButton, validateButton;

                codeEditText = (EditText) requestDialog.findViewById(R.id.code);
                cancelButton = (Button) requestDialog.findViewById(R.id.button5);
                validateButton = (Button) requestDialog.findViewById(R.id.button4);

                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                        requestDialog.dismiss();
                    }
                });

                validateButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        codeEditText.setError(null);
                        if (!codeEditText.getText().toString().isEmpty()) {
                            if (TextUtils.isDigitsOnly(codeEditText.getText().toString())) {
                                if (codeEditText.getText().toString().trim().length() == 10) {
                                    if (v != null) {
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    }
                                    sendRequest(codeEditText.getText().toString().trim());
                                    requestDialog.dismiss();
                                } else {
                                    codeEditText.setError("Enter valid mobile number!");
                                }
                            } else {
                                if (isEmailValid(codeEditText.getText().toString())) {
                                    if (v != null) {
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    }
                                    sendRequest(codeEditText.getText().toString().trim());
                                    requestDialog.dismiss();
                                } else {
                                    codeEditText.setError("Enter valid email!");
                                }
                            }
                        } else {
                            codeEditText.setError("Enter email or mobile!");
                        }
                    }
                });
                requestDialog.show();
            }
        });

        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        signupButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//                final Dialog requestDialog = new Dialog(new ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
//                requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                requestDialog.setContentView(R.layout.code_validation_form);
//                requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
//                requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                requestDialog.setCancelable(true);
//                requestDialog.getWindow().setGravity(Gravity.BOTTOM);
//
//                EditText codeEditText;
//                Button cancelButton, validateButton, signUpManuallyButton;
//
//                codeEditText = (EditText) requestDialog.findViewById(R.id.code);
//                cancelButton = (Button) requestDialog.findViewById(R.id.button5);
//                validateButton = (Button) requestDialog.findViewById(R.id.button4);
//                signUpManuallyButton = (Button) requestDialog.findViewById(R.id.button3);
//
//                cancelButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        requestDialog.dismiss();
//                    }
//                });
//
//                validateButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        requestDialog.dismiss();
//                        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//                    }
//                });
//
//                signUpManuallyButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        requestDialog.dismiss();
//                        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//                    }
//                });
//
//                requestDialog.show();
            }
        });
    }

    private void sendRequest(String trim) {
        if (checkInternetConenction(LoginActivity.this)) {
            Map<String, String> map = new HashMap<>();
            map.put("userName", trim);
            map.put("role", "HO");
            final Dialog loadingDialog = Utility.loadingDialog(LoginActivity.this);
            ApiInterface apiService = ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.initiateForgotPassword(map);
            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
                                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    requestDialog.setCancelable(true);
                                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                                    TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                                    TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                                    tittleTextView.setText("Success!");
                                    descriptionTextView.setText("Please click on the link that we sent to your email and text to reset account password.");
                                    negativeButton.setVisibility(View.GONE);
                                    positiveButton.setText("Okay");

                                    positiveButton.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method stub
                                            requestDialog.dismiss();
                                        }
                                    });

                                    requestDialog.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
                        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        requestDialog.setCancelable(true);
                        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                        tittleTextView.setText("Error!");
                        descriptionTextView.setText("We are facing problem in retrieving your account. Please make sure that you have entered the correct email or mobile number associated with your account and try again. Please contact Atlanta Concierge if the problem persist.");
                        negativeButton.setVisibility(View.GONE);
                        positiveButton.setText("Okay");

                        positiveButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                requestDialog.dismiss();
                            }
                        });

                        requestDialog.show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(findViewById(R.id.relativeLayout), SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            });
        } else {
            connectionLostDialogue();
        }
    }

    private void attemptLogin() {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
//        else if (!isEmailValid(email)) {
//            mEmailView.setError(getString(R.string.error_invalid_email));
//            focusView = mEmailView;
//            cancel = true;
//        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (checkInternetConenction(LoginActivity.this)) {
                doLogin();
            } else {
                connectionLostDialogue();
            }
        }
    }

    private void doLogin() {
        Map<String, Object> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("userName", mEmailView.getText().toString().trim());
        stringStringHashMap.put("token", mPasswordView.getText().toString().trim());
        List<Role> roles = new ArrayList<>();
        roles.add(Role.HO);
        stringStringHashMap.put("roles", roles);
        DeviceInfoVo deviceInfo = new DeviceInfoVo();
        deviceInfo.setDeviceType(DeviceType.ANDROID);
        WindowManager wm = (WindowManager) LoginActivity.this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        deviceInfo.setHeight(height);
        deviceInfo.setWidth(width);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Utility.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
//        String LOGIN_PREF_NAME = pref.getString("LOGIN_PREF_NAME", null);
        Log.e("FCM", "Firebase reg id: " + regId);
//        Toast.makeText(this, LOGIN_PREF_NAME, Toast.LENGTH_SHORT).show();
        if (regId != null) {
            deviceInfo.setDeviceToken(regId);
        }
        stringStringHashMap.put("deviceInfo", deviceInfo);
        if (checkInternetConenction(LoginActivity.this)) {
            final Dialog loadingDialog = Utility.loadingDialog(LoginActivity.this);
            ApiInterface apiService = ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
            Call<AuthenticateVo> call = apiService.signIn(stringStringHashMap);
            call.enqueue(new Callback<AuthenticateVo>() {
                @Override
                public void onResponse(Call<AuthenticateVo> call, Response<AuthenticateVo> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                authenticateVoLocal = response.body();
                                Gson gson = new Gson();
                                String jsonString = gson.toJson(authenticateVoLocal);
                                db.deleteAllJson();
                                insertJsoninDB(jsonString);
                                setIsLoggedin(LoginActivity.this, true, mEmailView.getText().toString().trim(), mPasswordView.getText().toString().trim(), "SP", authenticateVoLocal.getToken());
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Snackbar.make(findViewById(R.id.relativeLayout), "User Not Found", Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AuthenticateVo> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    ServerDownDialogue();
                }
            });
        } else {
            connectionLostDialogue();
        }
    }

    private void ServerDownDialogue() {
        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(true);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        tittleTextView.setText("Server Under Maintenance!");
        descriptionTextView.setText(SERVER_MAINTENANCE);

        negativeButton.setVisibility(View.GONE);
        positiveButton.setText("Try Again");

        positiveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                if (checkInternetConenction(LoginActivity.this)) {
                    attemptLogin();
                } else {
                    connectionLostDialogue();
                }
            }
        });

        requestDialog.show();
    }

    private void connectionLostDialogue() {
        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(LoginActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(true);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        tittleTextView.setText("No Internet Connection!");
        descriptionTextView.setText(NO_INTERNET);

        negativeButton.setVisibility(View.GONE);
        positiveButton.setText("Try Again");

        positiveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                if (checkInternetConenction(LoginActivity.this)) {
                    attemptLogin();
                } else {
                    connectionLostDialogue();
                }
            }
        });

        requestDialog.show();
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

    /**
     * Inserting new note in db
     * and refreshing the list
     */
    private void insertJsoninDB(String jsonString) {
        // inserting note in db and getting
        // newly inserted note id
        long id = db.insertJson(jsonString);

        // get the newly inserted json from db
        Json json = db.getJson(id);
        Log.d("LoginJson", "" + json.getId() + " " + json.getJson());
    }
}


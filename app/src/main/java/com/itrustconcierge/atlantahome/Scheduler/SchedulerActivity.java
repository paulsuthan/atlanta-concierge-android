package com.itrustconcierge.atlantahome.Scheduler;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UserMappedStatus;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.RequestFlow.ConfirmScheduleActivity.confirmScheduleActivity;
import static com.itrustconcierge.atlantahome.RequestFlow.ConfirmScheduleActivity.eventVo;
import static com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity.homeOwnerRequestVoStatic;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.isCalendarEnabled;
import static com.itrustconcierge.atlantahome.Utilities.Utility.setCalendarEnabled;

public class SchedulerActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {CalendarScopes.CALENDAR};

    public static com.google.api.services.calendar.Calendar mService = null;
    public static CalendarListEntry calendarListEntry = null;

    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    FloatingActionButton eventCreationActionButton;
    Long tendDateLong, tstartdateLong;
    EventAdapter eventCreationArrayAdapter;
    private List<EventVo> eventlist = new ArrayList<>();
    private RecyclerView recyclerView;
    CompactCalendarView compactCalendarView;
    List<EventLocal> eventLocals = new ArrayList<>();
    TextView emptyView, tittleText;
    private EventVo eventInfoObj;
    TimePickerDialog timePickDialogTime = null;
    private String dateString;
    public static String dateTimeString, dateTimeString1, dateTimeString2, dateTimeString3;
    TextView selectTimeButton, textViewTittle;
    ImageView toolbarTittleImageView;

    @Override
    public void onBackPressed() {
        try {
            if (getIntent().getExtras().getBoolean("fromSelectDate", false)) {
                showAreYouSurePopup();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.onBackPressed();
        }
    }

    private void showAreYouSurePopup() {
        final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SchedulerActivity.this, R.style.dialogAnimation));
        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        requestDialog.setCancelable(false);
        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

        if (tittleText.getText().toString().contains("first")) {
            tittleTextView.setText("Select your first availability");
            descriptionTextView.setText("Please select a date and time of your first availability.");
        } else if (tittleText.getText().toString().contains("second")) {
            tittleTextView.setText("Select your second availability");
            descriptionTextView.setText("Please select a date and time of your second availability.");
        } else if (tittleText.getText().toString().contains("third")) {
            tittleTextView.setText("Select your third availability");
            descriptionTextView.setText("Please select a date and time of your third availability.");
        } else {
            tittleTextView.setText("Select your availability");
            descriptionTextView.setText("Please select a date and time of your availability.");
        }
        negativeButton.setText("No Thanks");
        positiveButton.setText("OK");

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                requestDialog.dismiss();
                finish();
            }
        });

        requestDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduler);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Calendar");

        textViewTittle = (TextView) findViewById(R.id.myImageViewText);
        textViewTittle.setText("Calendar");
        textViewTittle.setTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendarView.computeScroll();
        final ImageView showPreviousMonthBut = (ImageView) findViewById(R.id.prev_button);
        final ImageView showNextMonthBut = (ImageView) findViewById(R.id.next_button);
        final TextView monthName = (TextView) findViewById(R.id.monthname);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        emptyView = (TextView) findViewById(R.id.empty_view);
        tittleText = (TextView) findViewById(R.id.tittleText);

//        toolbarTittleImageView = (ImageView)findViewById(R.id.toolbarTittleImageView);
//        toolbarTittleImageView.setVisibility(View.GONE);
        selectTimeButton = (TextView) findViewById(R.id.toolBarButton);
        selectTimeButton.setVisibility(View.GONE);

        monthName.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        showPreviousMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showPreviousMonth();
            }
        });

        showNextMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showNextMonth();
            }
        });

        eventCreationActionButton = (FloatingActionButton) findViewById(R.id.fab);
        eventCreationActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (authenticateVoLocal != null) {
                        if (authenticateVoLocal.getUserMappedStatus() != null) {
                            if (!authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                                Intent intent = new Intent(SchedulerActivity.this, EventCreationActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.stay, R.anim.slide_up);
                            } else {

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean("fromSelectDate", false)) {
                eventCreationActionButton.setVisibility(View.GONE);
                getSupportActionBar().setTitle(getIntent().getExtras().getString("tittle"));
                tittleText.setVisibility(View.VISIBLE);
                if (getIntent().getExtras().getString("tittle").contains("First")) {
                    tittleText.setText("Your first availability");
                } else if (getIntent().getExtras().getString("tittle").contains("Second")) {
                    tittleText.setText("Your second availability");
                } else if (getIntent().getExtras().getString("tittle").contains("Third")) {
                    tittleText.setText("Your third availability");
                }
                selectTimeButton.setVisibility(View.GONE);
                selectTimeButton.setText("SELECT");
            } else if (getIntent().getExtras().getBoolean("fromConfirmDate", false)) {
                eventCreationActionButton.setVisibility(View.GONE);
                getSupportActionBar().setTitle(getIntent().getExtras().getString("tittle"));
                tittleText.setVisibility(View.VISIBLE);
                tittleText.setText(getIntent().getExtras().getString("dateTittle"));
                selectTimeButton.setVisibility(View.VISIBLE);
                selectTimeButton.setText("CONFIRM");
            } else {
                eventCreationActionButton.setVisibility(View.VISIBLE);
                tittleText.setVisibility(View.GONE);
                selectTimeButton.setVisibility(View.GONE);
            }
        } else {
            eventCreationActionButton.setVisibility(View.VISIBLE);
            tittleText.setVisibility(View.GONE);
            selectTimeButton.setVisibility(View.GONE);
        }

        selectTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getExtras().getBoolean("fromSelectDate", false)) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = 7;
                    int minute = 0;
                    timePickDialogTime = new TimePickerDialog(SchedulerActivity.this,
                            new TimePickHandlerTime(), hour, minute, false);
                    timePickDialogTime.show();
                } else if (getIntent().getExtras().getBoolean("fromConfirmDate", false)) {
                    final Dialog requestDialog = new Dialog(new ContextThemeWrapper(SchedulerActivity.this, R.style.dialogAnimation));
                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    requestDialog.setCancelable(false);
                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                    TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                    TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);
                    tittleTextView.setText("Confirm Schedule");
                    descriptionTextView.setText("Would you like to confirm the " + tittleText.getText().toString());

                    negativeButton.setText("CANCEL");
                    positiveButton.setText("CONFIRM");

                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            requestDialog.dismiss();
                            if (eventVo != null) {
                                final EventVo eventVo1 = eventVo;
                                final Dialog loadingDialog = Utility.loadingDialog(SchedulerActivity.this);
                                ApiInterface apiService = ApiClient.getClient(SchedulerActivity.this).create(ApiInterface.class);
                                Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.confirmEvent(eventVo1);
                                call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                    @Override
                                    public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                        loadingDialog.dismiss();
                                        if (response.isSuccessful()) {
                                            eventVo = null;
                                            finish();
                                            if (confirmScheduleActivity != null)
                                                confirmScheduleActivity.finish();
                                        } else {
                                            Toast.makeText(SchedulerActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                        // Log error here since request failed
                                        loadingDialog.dismiss();
                                        Log.e("Error", t.toString());
                                        Toast.makeText(SchedulerActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                Map<String, Long> dateMap = new HashMap<>();
                                dateMap.put("date", getIntent().getExtras().getLong("date"));
                                final Dialog loadingDialog = Utility.loadingDialog(SchedulerActivity.this);
                                ApiInterface apiService = ApiClient.getClient(SchedulerActivity.this).create(ApiInterface.class);
                                Call<ResponseWrapper<Map<String, Boolean>>> call = null;
                                try {
                                    if (homeOwnerRequestVoStatic.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)) {
                                        call = apiService.confirmVisitForQuote(String.valueOf(homeOwnerRequestVoStatic.getId()), dateMap);
                                    } else {
                                        call = apiService.confirmScheduleForJob(String.valueOf(homeOwnerRequestVoStatic.getId()), dateMap);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                    @Override
                                    public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                        loadingDialog.dismiss();
                                        if (response.isSuccessful()) {
                                            finish();
                                            if (confirmScheduleActivity != null)
                                                confirmScheduleActivity.finish();
                                        } else {
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                        // Log error here since request failed
                                        loadingDialog.dismiss();
                                        Log.e("Error", t.toString());
                                    }
                                });
                            }
                        }
                    });

                    negativeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            requestDialog.dismiss();
                        }
                    });

                    requestDialog.show();
                }
            }
        });

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                eventLocals = compactCalendarView.getEvents(dateClicked);
                eventlist.clear();

                for (EventLocal eventLocalObject : eventLocals) {
                    EventVo eventDetails = new EventVo(eventLocalObject.getEventListModel().getId(), eventLocalObject.getEventListModel().getEventId(), eventLocalObject.getEventListModel().getDescription(), eventLocalObject.getEventListModel().getStartTime(), eventLocalObject.getEventListModel().getEndTime(), eventLocalObject.getEventListModel().getTitle(), eventLocalObject.getEventListModel().getParticipants(), eventLocalObject.getEventListModel().getAddress());
                    eventlist.add(eventDetails);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (eventlist.isEmpty()) {
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                        }
                        eventCreationArrayAdapter = new EventAdapter(eventlist);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SchedulerActivity.this);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(eventCreationArrayAdapter);
                    }
                });
                if (getIntent().getExtras() != null) {
                    if (getIntent().getExtras().getBoolean("fromSelectDate", false)) {
                        if (Calendar.getInstance().getTimeInMillis() < dateClicked.getTime()) {
                            selectTimeButton.setVisibility(View.VISIBLE);
                            String myFormat = "MMM dd, yyyy";
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                            dateString = sdf.format(dateClicked.getTime());
                            getSupportActionBar().setTitle(dateString);
                        } else {
                            selectTimeButton.setVisibility(View.GONE);
                            Toast.makeText(SchedulerActivity.this, "Please select future dates!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                monthName.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                eventLocals = compactCalendarView.getEvents(firstDayOfNewMonth);
                eventlist.clear();

                for (EventLocal eventLocalObject : eventLocals) {
                    EventVo eventDetails = new EventVo(eventLocalObject.getEventListModel().getId(), eventLocalObject.getEventListModel().getEventId(), eventLocalObject.getEventListModel().getDescription(), eventLocalObject.getEventListModel().getStartTime(), eventLocalObject.getEventListModel().getEndTime(), eventLocalObject.getEventListModel().getTitle(), eventLocalObject.getEventListModel().getParticipants(), eventLocalObject.getEventListModel().getAddress());
                    eventlist.add(eventDetails);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (eventlist.isEmpty()) {
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                        }
                        eventCreationArrayAdapter = new EventAdapter(eventlist);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SchedulerActivity.this);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(eventCreationArrayAdapter);
                    }
                });
            }
        });

        if (!isCalendarEnabled(SchedulerActivity.this)) {
            mProgress = new ProgressDialog(SchedulerActivity.this);
            mProgress.setMessage("Calling Google Calendar API ...");

            mCredential = GoogleAccountCredential.usingOAuth2(
                    getApplicationContext(), Arrays.asList(SCOPES))
                    .setBackOff(new ExponentialBackOff());

            getResultsFromApi();
        }

    }

    private class TimePickHandlerTime implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            dateTimeString = dateString + " " + String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US);
            timePickDialogTime.hide();
            if (getIntent().getExtras() != null) {
                if (getIntent().getExtras().getBoolean("fromSelectDate", false)) {
                    if (tittleText.getText().toString().contains("first")) {
                        dateTimeString1 = dateTimeString;
                        compactCalendarView.setCurrentDate(Calendar.getInstance().getTime());
                        getSupportActionBar().setTitle("Select Second Preferred Date");
                        tittleText.setText("Your second availability");
                        Animation slideAnimation = AnimationUtils.loadAnimation(SchedulerActivity.this, R.anim.slide_in_anim);
                        compactCalendarView.startAnimation(slideAnimation);
                        selectTimeButton.setVisibility(View.GONE);
                    } else if (tittleText.getText().toString().contains("second")) {
                        dateTimeString2 = dateTimeString;
                        compactCalendarView.setCurrentDate(Calendar.getInstance().getTime());
                        getSupportActionBar().setTitle("Select Third Preferred Date");
                        tittleText.setText("Your third availability");
                        Animation slideAnimation = AnimationUtils.loadAnimation(SchedulerActivity.this, R.anim.slide_in_anim);
                        compactCalendarView.startAnimation(slideAnimation);
                        selectTimeButton.setVisibility(View.GONE);
                    } else if (tittleText.getText().toString().contains("third")) {
                        dateTimeString3 = dateTimeString;
                        tittleText.setText("Your third availability");
                        finish();
                    }
                }
            }
        }
    }

    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Toast.makeText(SchedulerActivity.this, "No network connection available.", Toast.LENGTH_LONG).show();
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(SchedulerActivity.this,
                            "This app requires Google Play Services. Please install " +
                                    "Google Play Services on your device and relaunch this app.", Toast.LENGTH_LONG).show();
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                SchedulerActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("iTC Pro")
                    .build();
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        private List<String> getDataFromApi() throws IOException {
            // List the next 10 events from the primary calendar.
//            try {
//                calendarListEntry = mService.calendarList().get("calendarId").execute();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -1);
            DateTime min = new DateTime(cal.getTimeInMillis());

            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.MONTH, 6);
            DateTime max = new DateTime(cal1.getTimeInMillis());

            List<String> eventStrings = new ArrayList<String>();
            Events events = mService.events().list("primary")
                    .setMaxResults(1000)
                    .setTimeMin(min)
                    .setTimeMax(max)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();

            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    // All-day events don't have start times, so just use
                    // the start date.
                    start = event.getStart().getDate();
                }
                eventStrings.add(
                        String.format("%s (%s)", event.getSummary(), start));
                EventVo eventDetails = new EventVo(0, event.getId(), event.getDescription(), event.getStart().getDateTime().getValue(), event.getEnd().getDateTime().getValue(), event.getSummary(), null, null);
                EventLocal eventLocal = new EventLocal(getResources().getColor(R.color.colorAccent), event.getStart().getDateTime().getValue(), eventDetails, eventDetails);
                compactCalendarView.addEvent(eventLocal, false);
            }
            Calendar calendar = Calendar.getInstance();
            Date today = calendar.getTime();
            eventLocals = compactCalendarView.getEvents(today);

            for (EventLocal eventLocalObject : eventLocals) {
                EventVo eventDetails1 = new EventVo(0, eventLocalObject.getEventListModel().getEventId(), eventLocalObject.getEventListModel().getDescription(), eventLocalObject.getEventListModel().getStartTime(), eventLocalObject.getEventListModel().getEndTime(), eventLocalObject.getEventListModel().getTitle(), eventLocalObject.getEventListModel().getParticipants(), eventLocalObject.getEventListModel().getAddress());
                eventlist.add(eventDetails1);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (eventlist.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    }
                    eventCreationArrayAdapter = new EventAdapter(eventlist);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SchedulerActivity.this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(eventCreationArrayAdapter);
                    setCalendarEnabled(SchedulerActivity.this, true);
//                    try {
//                        Date date = new Date(getIntent().getExtras().getLong("date"));
//                        compactCalendarView.setCurrentDate(date);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
            });
            compactCalendarView.setCurrentDate(Calendar.getInstance().getTime());
            return eventStrings;
        }

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            mProgress.hide();
            if (output == null || output.size() == 0) {

            } else {
                output.add(0, "Data retrieved using the Google Calendar API:");
//                mOutputText.setText(TextUtils.join("\n", output));
            }
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            SchedulerActivity.REQUEST_AUTHORIZATION);
                } else {
//                    Toast.makeText(SchedulerActivity.this, "The following error occurred:\n"
//                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(SchedulerActivity.this, "Request cancelled.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (authenticateVoLocal != null) {
                if (authenticateVoLocal.getUserMappedStatus() != null) {
                    if (!authenticateVoLocal.getUserMappedStatus().equals(UserMappedStatus.NOT_MAPPED)) {
                        if (checkInternetConenction(SchedulerActivity.this)) {
                            getEvents();
                            if (isCalendarEnabled(SchedulerActivity.this)) {
                                mProgress = new ProgressDialog(SchedulerActivity.this);
                                mProgress.setMessage("Calling Google Calendar API ...");

                                mCredential = GoogleAccountCredential.usingOAuth2(
                                        getApplicationContext(), Arrays.asList(SCOPES))
                                        .setBackOff(new ExponentialBackOff());

                                getResultsFromApi();
                            }
                            eventlist.clear();
                            compactCalendarView.removeAllEvents();
                        } else
                            Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    } else {

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEvents() {
        final Dialog loadingDialog = Utility.loadingDialog(SchedulerActivity.this);
        ApiInterface apiService = ApiClient.getClient(SchedulerActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<EventVo>> call = apiService.getEvents();
        call.enqueue(new Callback<ResponseWrapper<EventVo>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<EventVo>> call, Response<ResponseWrapper<EventVo>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getList() != null) {
                            try {
                                if (!response.body().getList().isEmpty()) {
                                    List<EventVo> eventinfo = response.body().getList();

                                    for (EventVo eventObject : eventinfo) {

                                        eventInfoObj = new EventVo();

                                        eventInfoObj = eventObject;
                                        eventObject.getStartTime();
                                        eventObject.getEndTime();
                                        eventObject.getDescription();
                                        eventObject.getId();
//                                        int noofdays = (int) ((eventObject.getEndTime() - eventObject.getStartTime()) / (60 * 60 * 24) / 1000);
//                                        if (noofdays != 0) {
//                                            for (int i = 0; i <= noofdays; i++) {
//                                                long startTime = eventObject.getStartTime() + i * (24 * 60 * 60);
//                                                EventInfo eventDetails = new EventInfo(eventInfoObj.getId(), eventInfoObj.getDescription(), startTime, eventInfoObj.getEndTime(), eventInfoObj.getTitle(), eventInfoObj.getParticipants(), eventInfoObj.getAddress());
//                                                EventLocal eventLocal = new EventLocal(getResources().getColor(R.color.colorAccent), startTime, eventDetails, eventDetails);
//                                                compactCalendarView.addEvent(eventLocal, false);
//                                            }
//                                        } else {
                                        EventVo eventDetails = new EventVo(eventInfoObj.getId(), eventInfoObj.getId().toString(), eventInfoObj.getDescription(), eventInfoObj.getStartTime(), eventInfoObj.getEndTime(), eventInfoObj.getTitle(), eventInfoObj.getParticipants(), eventInfoObj.getAddress());
                                        EventLocal eventLocal = new EventLocal(getResources().getColor(R.color.colorAccent), eventObject.getStartTime(), eventDetails, eventDetails);
                                        compactCalendarView.addEvent(eventLocal, false);
//                                        }
                                    }

                                    Calendar calendar = Calendar.getInstance();
                                    Date today = calendar.getTime();
                                    eventLocals = compactCalendarView.getEvents(today);

                                    for (EventLocal eventLocalObject : eventLocals) {
                                        EventVo eventDetails = new EventVo(eventLocalObject.getEventListModel().getId(), eventLocalObject.getEventListModel().getId().toString(), eventLocalObject.getEventListModel().getDescription(), eventLocalObject.getEventListModel().getStartTime(), eventLocalObject.getEventListModel().getEndTime(), eventLocalObject.getEventListModel().getTitle(), eventLocalObject.getEventListModel().getParticipants(), eventLocalObject.getEventListModel().getAddress());
                                        eventlist.add(eventDetails);
                                    }

                                    if (eventlist.isEmpty()) {
                                        recyclerView.setVisibility(View.GONE);
                                        emptyView.setVisibility(View.VISIBLE);
                                    } else {
                                        recyclerView.setVisibility(View.VISIBLE);
                                        emptyView.setVisibility(View.GONE);
                                    }

                                    eventCreationArrayAdapter = new EventAdapter(eventlist);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SchedulerActivity.this);
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(eventCreationArrayAdapter);
//                                    try {
//                                        Date date = new Date(getIntent().getExtras().getLong("date"));
//                                        compactCalendarView.setCurrentDate(date);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                                } else {

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(recyclerView, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<EventVo>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}


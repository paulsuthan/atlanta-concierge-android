package com.itrustconcierge.atlantahome.Scheduler;

import java.util.List;

/**
 * Created by Paul on 9/9/2017.
 */

public class EventsCalender {

    private final List<EventLocal> eventLocals;
    private final long timeInMillis;

    EventsCalender(long timeInMillis, List<EventLocal> eventLocals) {
        this.timeInMillis = timeInMillis;
        this.eventLocals = eventLocals;
    }

    long getTimeInMillis() {
        return timeInMillis;
    }

    List<EventLocal> getEventLocals() {
        return eventLocals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventsCalender event = (EventsCalender) o;

        if (timeInMillis != event.timeInMillis) return false;
        return eventLocals != null ? eventLocals.equals(event.eventLocals) : event.eventLocals == null;

    }

    @Override
    public int hashCode() {
        int result = eventLocals != null ? eventLocals.hashCode() : 0;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "EventsCalender{" +
                "eventLocals=" + eventLocals +
                ", timeInMillis=" + timeInMillis +
                '}';
    }
}

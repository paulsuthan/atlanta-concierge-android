package com.itrustconcierge.atlantahome.Scheduler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.enums.EventType;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.R;

import java.util.List;

import static com.itrustconcierge.atlantahome.Utilities.Utility.getTime;

/**
 * Created by Paul on 9/8/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    private List<EventVo> eventListModels;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, date;
        ImageView avatar;
        LinearLayout linearLayout;
        ImageButton chat_ImageButton, call_ImageButton;
        TextView startTimeEvent;
        TextView endTimeEvent;
        TextView startTimeDescribtionEvent;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.eventname_creation);
            startTimeDescribtionEvent = (TextView) view.findViewById(R.id.starttime_eventdescribtion_creation);
            startTimeEvent = (TextView) view.findViewById(R.id.event_starttime_creation);
            endTimeEvent = (TextView) view.findViewById(R.id.event_endtime_creation);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        }
    }


    public EventAdapter(List<EventVo> eventListModels) {
        this.eventListModels = eventListModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final EventVo eventListModel = eventListModels.get(position);
        try {
            holder.name.setText(" " + eventListModel.getTitle());
            holder.startTimeEvent.setText(getTime(eventListModel.getStartTime()) + "  ");
            holder.endTimeEvent.setText(getTime(eventListModel.getEndTime()) + "  ");
            if (eventListModel.getDescription() != null)
                holder.startTimeDescribtionEvent.setText(" " + eventListModel.getDescription());

            if (eventListModel.getEventType() != null) {
                if (eventListModel.getEventType().equals(EventType.ALL_DAY)) {
                    holder.startTimeEvent.setText("All");
                    holder.endTimeEvent.setText("Day");
                }
            }
//            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(v.getContext(), EventCreationActivity.class);
//                    v.getContext().startActivity(i);
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return eventListModels.size();
    }
}
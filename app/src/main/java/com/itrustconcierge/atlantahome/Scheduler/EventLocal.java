package com.itrustconcierge.atlantahome.Scheduler;

import android.support.annotation.Nullable;

import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;

/**
 * Created by Paul on 9/8/2017.
 */

public class EventLocal {

    private int color;
    private long timeInMillis;
    private Object data;
    private EventVo eventListModel;

    public EventLocal(int color, long timeInMillis) {
        this.color = color;
        this.timeInMillis = timeInMillis;
    }

    public EventLocal(int color, long timeInMillis, Object data, EventVo eventListModel) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.data = data;
        this.eventListModel = eventListModel;
    }

    public EventVo getEventListModel() {
        return eventListModel;
    }

    public void setEventListModel(EventVo eventListModel) {
        this.eventListModel = eventListModel;
    }

    public int getColor() {
        return color;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    @Nullable
    public Object getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventLocal eventLocal = (EventLocal) o;

        if (color != eventLocal.color) return false;
        if (timeInMillis != eventLocal.timeInMillis) return false;
        return data != null ? data.equals(eventLocal.data) : eventLocal.data == null;

    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventLocal{" +
                "color=" + color +
                ", timeInMillis=" + timeInMillis +
                ", data=" + data +
                '}';
    }
}

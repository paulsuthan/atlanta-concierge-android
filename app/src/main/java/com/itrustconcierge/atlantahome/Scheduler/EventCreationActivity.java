package com.itrustconcierge.atlantahome.Scheduler;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.EventType;
import com.itrustconcierge.atlantahome.API_Manager.enums.ParticipantStatus;
import com.itrustconcierge.atlantahome.API_Manager.enums.ParticipantType;
import com.itrustconcierge.atlantahome.API_Manager.vo.AddressVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.EventVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.LocationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ParticipantVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.calendarListEntry;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.mService;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class EventCreationActivity extends AppCompatActivity {

    EditText tittleEditText, locationEditText, notesEdittext;
    Button startDateButton, endDateButton, startTimeButton, endTimeButton, doneButton;
    Switch allDayASwitch;
    LinearLayout showAsLayout;
    TimePickerDialog timePickDialogStarttime = null;
    TimePickerDialog timePickDialogEndTime = null;
    String startTime, endTime;
    TextView showAsTextView;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private String TAG = "Google Places API";
    LatLng latLng = null;
    TextView myImageViewText, toolBarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_creation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Add Event");
//        toolbar.setTitleTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        showAsLayout = (LinearLayout) findViewById(R.id.showAsLayout);
        allDayASwitch = (Switch) findViewById(R.id.allday);
        startDateButton = (Button) findViewById(R.id.start_date);
        endDateButton = (Button) findViewById(R.id.end_date);
        startTimeButton = (Button) findViewById(R.id.start_time);
        endTimeButton = (Button) findViewById(R.id.end_time);
        tittleEditText = (EditText) findViewById(R.id.titleEdittext);
        locationEditText = (EditText) findViewById(R.id.locationEdittext);
        notesEdittext = (EditText) findViewById(R.id.notesEdittext);
        showAsTextView = (TextView) findViewById(R.id.showAs);
        doneButton = (Button) findViewById(R.id.sendbutton);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        myImageViewText.setText("Add Event");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        toolBarButton.setVisibility(View.GONE);

        allDayASwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startTimeButton.setVisibility(View.GONE);
                    endTimeButton.setVisibility(View.GONE);
                } else {
                    startTimeButton.setVisibility(View.VISIBLE);
                    endTimeButton.setVisibility(View.VISIBLE);
                }
            }
        });

        locationEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(EventCreationActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                startDateButton.setText(sdf.format(myCalendar.getTime()));
            }

        };

        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                endDateButton.setText(sdf.format(myCalendar.getTime()));
            }

        };

        startDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(EventCreationActivity.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        endDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(EventCreationActivity.this, date2, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        startTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = 9;
                int minute = 0;
                timePickDialogStarttime = new TimePickerDialog(v.getContext(),
                        new TimePickHandlerStartTime(), hour, minute, false);
                timePickDialogStarttime.show();
            }
        });

        endTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = 10;
                int minute = 0;
                timePickDialogEndTime = new TimePickerDialog(v.getContext(),
                        new TimePickHandlerEndTime(), hour, minute, false);
                timePickDialogEndTime.show();
            }
        });

        showAsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu myPopup = new PopupMenu(EventCreationActivity.this, showAsTextView);
                try {
                    myPopup.getMenu().add("Busy");
                    myPopup.getMenu().add("Free");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                myPopup.show();

                myPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Log.d("Menu clicked", item.getTitle().toString());
                        showAsTextView.setText(item.getTitle().toString());
                        return false;
                    }
                });
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tittleEditText.getText().toString().isEmpty()) {
                    if (allDayASwitch.isChecked()) {
                        if (!startDateButton.getText().toString().isEmpty() && !endDateButton.getText().toString().isEmpty()) {
                            prepareData();
                        } else {
                            Snackbar.make(doneButton, "Please select start and end date!", Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        if (!startDateButton.getText().toString().isEmpty() && !endDateButton.getText().toString().isEmpty() && !startTimeButton.getText().toString().isEmpty() && !endTimeButton.getText().toString().isEmpty()) {
                            prepareData();
                        } else {
                            Snackbar.make(doneButton, "Please select start and end date and time!", Snackbar.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Snackbar.make(doneButton, "Please enter tittle!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void prepareData() {
        EventVo eventInfo = new EventVo();
        eventInfo.setTitle(tittleEditText.getText().toString());
        eventInfo.setDescription(notesEdittext.getText().toString());

        String startString, endString;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat conversionDate;
        if (allDayASwitch.isChecked()) {
            startString = startDateButton.getText().toString();
            endString = endDateButton.getText().toString();
            conversionDate = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            eventInfo.setEventType(EventType.ALL_DAY);
        } else {
            startString = startDateButton.getText().toString() + " " + startTimeButton.getText().toString();
            endString = endDateButton.getText().toString() + " " + endTimeButton.getText().toString();
            conversionDate = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
            eventInfo.setEventType(EventType.FIXED);
        }

        Date startDateConversion;
        Long tstartdateLong = null;
        try {
            startDateConversion = conversionDate.parse(String.valueOf(startString));
            long millisecondsStartDate = startDateConversion.getTime();
            tstartdateLong = millisecondsStartDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        eventInfo.setStartTime(tstartdateLong);

        Date endDateConversion;
        Long tendDateLong = null;
        try {
            endDateConversion = conversionDate.parse(String.valueOf(endString));
            long millisecondsendDate = endDateConversion.getTime();
            tendDateLong = millisecondsendDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        eventInfo.setEndTime(tendDateLong);
        AddressVo addressVo = new AddressVo();
        addressVo.setDisplayAddress(locationEditText.getText().toString());
        try {
            if (latLng != null) {
                LocationVo locationVo = new LocationVo();
                locationVo.setLat(String.valueOf(latLng.latitude));
                locationVo.setLng(String.valueOf(latLng.longitude));
                addressVo.setLocation(locationVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        eventInfo.setAddress(addressVo);

        try {
            ParticipantVo participantInfo = new ParticipantVo();
            participantInfo.setUserId(authenticateVoLocal.getId());
            participantInfo.setParticipantType(ParticipantType.SP);
            participantInfo.setStatus(ParticipantStatus.APPROVED);
            List<ParticipantVo> participantInfos = new ArrayList<>();
            participantInfos.add(participantInfo);
            eventInfo.setParticipants(participantInfos);
            if (showAsTextView.getText().toString().contains("Busy")) {
                eventInfo.setShowAsBusy(true);
            } else {
                eventInfo.setShowAsBusy(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (eventInfo.getStartTime() >= eventInfo.getEndTime()) {
            Snackbar.make(doneButton, "The End Date/Time should be greater than the Start Date/Time!", Snackbar.LENGTH_LONG).show();
        } else {
            if (checkInternetConenction(EventCreationActivity.this)) {
                addEvent(eventInfo);
            } else
                Snackbar.make(doneButton, NO_INTERNET, Snackbar.LENGTH_LONG).show();
        }
    }

    private void addEvent(final EventVo eventInfo) {
        final Dialog loadingDialog = Utility.loadingDialog(EventCreationActivity.this);
        ApiInterface apiService = ApiClient.getClient(EventCreationActivity.this).create(ApiInterface.class);
        Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.addEvent(eventInfo);
        call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    new CreateEvent(eventInfo).execute();

                    final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(EventCreationActivity.this, R.style.dialogAnimation));
                    requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                    requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                    requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    requestDialog.setCancelable(false);
                    requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                    TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                    TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                    Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                    Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                    tittleTextView.setText("Event added Successfully.");
                    descriptionTextView.setText("Your event was added successfully.");
                    negativeButton.setVisibility(View.GONE);
                    positiveButton.setText("Okay");

                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            requestDialog.dismiss();
                            finish();
                        }
                    });

                    requestDialog.show();
                } else {
                    Toast.makeText(EventCreationActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                // Log error here since request failed
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Toast.makeText(EventCreationActivity.this, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
            }
        });
    }

    private class CreateEvent extends AsyncTask<Void, Void, Void> {
        EventVo eventInfo;
        private String displayAddress = "";

        CreateEvent(EventVo event) {
            this.eventInfo = event;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (eventInfo.getAddress() != null) {
                    if (eventInfo.getAddress().getDisplayAddress() != null) {
                        displayAddress = eventInfo.getAddress().getDisplayAddress();
                    }
                }
                Event event = new Event()
                        .setSummary(eventInfo.getTitle())
                        .setLocation(displayAddress)
                        .setDescription(eventInfo.getDescription());

                DateTime startDateTime = new DateTime(eventInfo.getStartTime());
                EventDateTime start = new EventDateTime()
                        .setDateTime(startDateTime)
                        .setTimeZone(TimeZone.getDefault().getID());
                event.setStart(start);

                DateTime endDateTime = new DateTime(eventInfo.getEndTime());
                EventDateTime end = new EventDateTime()
                        .setDateTime(endDateTime)
                        .setTimeZone(TimeZone.getDefault().getID());
                event.setEnd(end);

//                if (eventInfo.getEventType() != null) {
//                    if (eventInfo.getEventType().equals(EventType.ALL_DAY)) {
//                        event.set
//                    }
//                }

                String calendarId = null;
                if (calendarListEntry != null) {
                    calendarId = calendarListEntry.getId();
                } else {
                    calendarId = "primary";
                }
                event = mService.events().insert(calendarId, event).execute();
                System.out.printf("Event created: %s\n", event.getHtmlLink());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {

        }
    }

    private class TimePickHandlerEndTime implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            endTimeButton.setText(String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US));
            endTime = String.format("%02d:%02d", hourOfDay, minute, Locale.US);
            timePickDialogEndTime.hide();
        }
    }

    private class TimePickHandlerStartTime implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            startTimeButton.setText(String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US));
            startTime = String.format("%02d:%02d", hourOfDay, minute, Locale.US);
            timePickDialogStarttime.hide();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                locationEditText.setText(place.getAddress());
                latLng = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}

package com.itrustconcierge.atlantahome;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.PaymentType;
import com.itrustconcierge.atlantahome.API_Manager.vo.IdListVo;
import com.itrustconcierge.atlantahome.Fragments.CardFragment;
import com.itrustconcierge.atlantahome.Fragments.DonePaymentFragment;
import com.itrustconcierge.atlantahome.Fragments.PaymentListFragment;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Fragments.CardFragment.cardDetail;
import static com.itrustconcierge.atlantahome.RequestFlow.QuoteSummaryActivity.paymentVo;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.formatter;

public class PaymentActivity extends AppCompatActivity {

    Button nextButton, summary, payment, done;
    private int n = 1;
    TextView price, myImageViewText, toolBarButton;
    ImageView toolbarTittleImageView;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        paymentVo = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);

        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Fragment selectedFragment = PaymentListFragment.newInstance("PENDING");
        FragmentTransaction transaction = getSupportFragmentManager().
                beginTransaction();
        transaction.replace(R.id.content, selectedFragment);
        transaction.commit();

        nextButton = (Button) findViewById(R.id.next_button);
        summary = (Button) findViewById(R.id.summary);
        payment = (Button) findViewById(R.id.payment);
        done = (Button) findViewById(R.id.done);
        price = (TextView) findViewById(R.id.price);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        myImageViewText.setText("PAYMENT SUMMARY");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        toolBarButton.setVisibility(View.GONE);

        if (paymentVo != null) {
            price.setText("$" + String.valueOf(formatter.format(paymentVo.getAmount())));
        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (n == 1) {
                    n = 2;
                    int colorFrom = getResources().getColor(R.color.grayDark);
                    int colorTo = getResources().getColor(R.color.blueButton);
                    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                    colorAnimation.setDuration(700); // milliseconds
                    colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                        @Override
                        public void onAnimationUpdate(ValueAnimator animator) {
                            summary.setBackground(getDrawable(R.drawable.background_button_oval));
                            summary.setBackgroundColor(getResources().getColor(R.color.gray));
                            summary.setTextColor(getResources().getColor(R.color.grayDark));

                            payment.setBackground(getDrawable(R.drawable.background_button_oval));
                            payment.setBackgroundColor((int) animator.getAnimatedValue());
                            payment.setTextColor(getResources().getColor(R.color.white));
                        }

                    });
                    colorAnimation.start();
                    Fragment selectedFragment = CardFragment.newInstance("", "");
                    FragmentTransaction transaction = getSupportFragmentManager().
                            beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.content, selectedFragment);
                    transaction.commit();
                } else if (n == 2) {
                    if (cardDetail != null) {
                        try {
                            ApiInterface apiService = ApiClient.getClient(PaymentActivity.this).create(ApiInterface.class);
                            IdListVo idListVo = new IdListVo();
                            List<Long> list = new ArrayList<>();
                            list.add(paymentVo.getId());
                            idListVo.setList(list);
                            final Dialog loadingDialog = Utility.loadingDialog(PaymentActivity.this);
                            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.makePayments(idListVo, cardDetail.getId());
                            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                @Override
                                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                    loadingDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        try {
                                            n = 3;
                                            authenticateVoLocal.setSubscriptionRequired(false);
                                            nextButton.setText("DONE");
                                            cardDetail = null;
                                            int colorFrom = getResources().getColor(R.color.grayDark);
                                            int colorTo = getResources().getColor(R.color.blueButton);
                                            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                                            colorAnimation.setDuration(700); // milliseconds
                                            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                                                @Override
                                                public void onAnimationUpdate(ValueAnimator animator) {
                                                    payment.setBackground(getDrawable(R.drawable.background_button_oval));
                                                    payment.setBackgroundColor(getResources().getColor(R.color.gray));
                                                    payment.setTextColor(getResources().getColor(R.color.grayDark));

                                                    done.setBackground(getDrawable(R.drawable.background_button_oval));
                                                    done.setBackgroundColor((int) animator.getAnimatedValue());
                                                    done.setTextColor(getResources().getColor(R.color.white));
                                                }

                                            });
                                            colorAnimation.start();
                                            Fragment selectedFragment = DonePaymentFragment.newInstance(price.getText().toString());
                                            FragmentTransaction transaction = getSupportFragmentManager().
                                                    beginTransaction();
                                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                            transaction.replace(R.id.content, selectedFragment);
                                            transaction.commit();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Snackbar.make(done, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                    loadingDialog.dismiss();
                                    // Log error here since request failed
                                    Log.e("Error", t.toString());
                                    Snackbar.make(done, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar.make(done, "Please select card for payment.", Snackbar.LENGTH_LONG).show();
                    }
                } else if (n == 3) {
                    if (paymentVo.getPaymentType().equals(PaymentType.SUBSCRIBE)) {
                        Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(PaymentActivity.this);
                    } else {
                        Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(PaymentActivity.this);
                    }
                    paymentVo = null;
                }
            }
        });
    }
}

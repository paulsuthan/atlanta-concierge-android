package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.ConatctType;
import com.itrustconcierge.atlantahome.API_Manager.vo.ContactVo;
import com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.ContactFranchiseActivity.contactFranchiseActivity;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.calendarListEntry;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.dateTimeString1;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.dateTimeString2;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.dateTimeString3;
import static com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity.mService;
import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;

public class ScheduleVisitActivity extends AppCompatActivity {

    EditText date1EditText, date2EditText, date3EditText, estimatedTimeEditText;
    TimePickerDialog timePickDialogTime1 = null, timePickDialogTime2 = null, timePickDialogTime3 = null;
    private String date1String, date2String, date3String;
    Button scheduleButton;
    LinearLayout estimationLayout;
    Spinner spinner;
    TextView tittleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_visit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("Select Availability");
        toolbar.setTitleTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        date1EditText = (EditText) findViewById(R.id.date1);
        date2EditText = (EditText) findViewById(R.id.date2);
        date3EditText = (EditText) findViewById(R.id.date3);
        estimatedTimeEditText = (EditText) findViewById(R.id.estimatedTime);
        scheduleButton = (Button) findViewById(R.id.done_button);
        estimationLayout = (LinearLayout) findViewById(R.id.estimationLayout);
        spinner = (Spinner) findViewById(R.id.timeUnitSpinner);
        tittleText = (TextView) findViewById(R.id.tittleText);
        tittleText.setText("When can your " + authenticateVoLocal.getTenantSetting().getDisplayName() + " contact you? \nPlease provide us with three(3) of your preferred times and dates.");

        String[] arraySpinner = new String[]{
                "Hour(s)", "Day(s)"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        spinner.setAdapter(adapter);

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date1EditText.setText(sdf.format(myCalendar.getTime()));
                date1String = sdf.format(myCalendar.getTime());
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                timePickDialogTime1 = new TimePickerDialog(ScheduleVisitActivity.this,
                        new TimePickHandlerTime1(), hour, minute, false);
                timePickDialogTime1.show();
            }

        };

        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date2EditText.setText(sdf.format(myCalendar.getTime()));
                date2String = sdf.format(myCalendar.getTime());
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                timePickDialogTime2 = new TimePickerDialog(ScheduleVisitActivity.this,
                        new TimePickHandlerTime2(), hour, minute, false);
                timePickDialogTime2.show();
            }

        };

        final DatePickerDialog.OnDateSetListener date3 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM dd, yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date3EditText.setText(sdf.format(myCalendar.getTime()));
                date3String = sdf.format(myCalendar.getTime());
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                timePickDialogTime3 = new TimePickerDialog(ScheduleVisitActivity.this,
                        new TimePickHandlerTime3(), hour, minute, false);
                timePickDialogTime3.show();
            }

        };

        date1EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ScheduleVisitActivity.this, date1, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.show();
                Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                intent.putExtra("fromSelectDate", true);
                intent.putExtra("tittle", "Select First Availability");
                v.getContext().startActivity(intent);
            }
        });

        date2EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ScheduleVisitActivity.this, date2, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.show();
                Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                intent.putExtra("fromSelectDate", true);
                intent.putExtra("tittle", "Select Second Availability");
                v.getContext().startActivity(intent);
            }
        });

        date3EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ScheduleVisitActivity.this, date3, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.show();
                Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                intent.putExtra("fromSelectDate", true);
                intent.putExtra("tittle", "Select Third Availability");
                v.getContext().startActivity(intent);
            }
        });

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!date1EditText.getText().toString().isEmpty() || !date2EditText.getText().toString().isEmpty() || !date3EditText.getText().toString().isEmpty()) {
                    if (checkInternetConenction(ScheduleVisitActivity.this)) {
                        try {
                            scheduleVisit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar.make(date1EditText, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(date1EditText, "Select a date!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            date1EditText.setText(dateTimeString1);
            date2EditText.setText(dateTimeString2);
            date3EditText.setText(dateTimeString3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dateTimeString1 = null;
        dateTimeString2 = null;
        dateTimeString3 = null;
    }

    private void scheduleVisit() {
        SimpleDateFormat conversionDate = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        Date date1, date2, date3;
        Long aLong1 = null, aLong2 = null, aLong3 = null;
        try {
            final List<Long> dates = new ArrayList<>();
            if (!date1EditText.getText().toString().isEmpty()) {
                date1 = conversionDate.parse(String.valueOf(date1EditText.getText().toString()));
                long millisecondsStartDate = date1.getTime();
                aLong1 = millisecondsStartDate;
                dates.add(aLong1);
            }
            if (!date2EditText.getText().toString().isEmpty()) {
                date2 = conversionDate.parse(String.valueOf(date2EditText.getText().toString()));
                long millisecondsStartDate2 = date2.getTime();
                aLong2 = millisecondsStartDate2;
                dates.add(aLong2);
            }
            if (!date3EditText.getText().toString().isEmpty()) {
                date3 = conversionDate.parse(String.valueOf(date3EditText.getText().toString()));
                long millisecondsStartDate3 = date3.getTime();
                aLong3 = millisecondsStartDate3;
                dates.add(aLong3);
            }
            ContactVo contactVo = new ContactVo();
            contactVo.setPreferredTimeToContact(dates);
            contactVo.setContactType(ConatctType.NOTIFY_ME);
            contactVo.setFirstName(authenticateVoLocal.getFirstName());
            contactVo.setLastName(authenticateVoLocal.getLastName());
            contactVo.setEmail(authenticateVoLocal.getEmail());
            contactVo.setMobileNumber(authenticateVoLocal.getMobileNumber());
            final Dialog loadingDialog = Utility.loadingDialog(ScheduleVisitActivity.this);
            ApiInterface apiService = ApiClient.getClient(ScheduleVisitActivity.this).create(ApiInterface.class);
            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.sendContact(contactVo);
            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        for (Long aLong : dates) {
                            new CreateEvent(aLong).execute();
                        }
                        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(ScheduleVisitActivity.this, R.style.dialogAnimation));
                        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        requestDialog.setCancelable(false);
                        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                        tittleTextView.setText("Thank you!");
                        descriptionTextView.setText("Our Atlanta Concierge will send an invite shortly to confirm a date and time based on the preferences you have provided.");

                        negativeButton.setVisibility(View.GONE);
                        positiveButton.setText("OK");

                        positiveButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                requestDialog.dismiss();
                                finish();
                                if (contactFranchiseActivity != null) {
                                    contactFranchiseActivity.finish();
                                }
                            }
                        });

                        requestDialog.show();
                    } else {
                        Snackbar.make(date1EditText, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                    // Log error here since request failed
                    loadingDialog.dismiss();
                    Log.e("Error", t.toString());
                    Snackbar.make(date1EditText, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(date1EditText, "Select date and time!", Snackbar.LENGTH_LONG).show();
        }
    }

    private class CreateEvent extends AsyncTask<Void, Void, Void> {
        Long aLong;

        CreateEvent(Long aLong) {
            this.aLong = aLong;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String description = "";

                Event event = new Event();

                try {
                    event = new Event()
                            .setSummary("Atlanta Concierge Discussion")
                            .setDescription(description);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                DateTime startDateTime = new DateTime(aLong);
                EventDateTime start = new EventDateTime()
                        .setDateTime(startDateTime)
                        .setTimeZone(TimeZone.getDefault().getID());
                event.setStart(start);

                DateTime endDateTime = new DateTime(aLong + 3600000);
                EventDateTime end = new EventDateTime()
                        .setDateTime(endDateTime)
                        .setTimeZone(TimeZone.getDefault().getID());
                event.setEnd(end);

                EventReminder[] reminderOverrides = new EventReminder[]{
                        new EventReminder().setMethod("email").setMinutes(24 * 60),
                        new EventReminder().setMethod("popup").setMinutes(60),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);

                String calendarId = null;
                if (calendarListEntry != null) {
                    calendarId = calendarListEntry.getId();
                } else {
                    calendarId = "primary";
                }
                event = mService.events().insert(calendarId, event).execute();
                System.out.printf("Event created: %s\n", event.getHtmlLink());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {

        }
    }

    private class TimePickHandlerTime1 implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            date1EditText.setText(date1String + " " + String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US));
            timePickDialogTime1.hide();
        }
    }

    private class TimePickHandlerTime2 implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            date2EditText.setText(date2String + " " + String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US));
            timePickDialogTime2.hide();
        }
    }

    private class TimePickHandlerTime3 implements TimePickerDialog.OnTimeSetListener {
        @SuppressLint("DefaultLocale")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            int hour = hourOfDay % 12;
            if (hour == 0)
                hour = 12;
            date3EditText.setText(date3String + " " + String.format("%02d:%02d %s", hour, minute,
                    hourOfDay < 12 ? "am" : "pm", Locale.US));
            timePickDialogTime3.hide();
        }
    }

}

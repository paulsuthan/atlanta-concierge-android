package com.itrustconcierge.atlantahome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.Utilities.StaticInfo;

public class HowCanWeHelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_can_we_help);

        Button completesubsciption = (Button) findViewById(R.id.completesub);
        Button talktoConcierge = (Button) findViewById(R.id.talkto);
        TextView hoNameTextView = (TextView) findViewById(R.id.HO_name);
        if (StaticInfo.authenticateVoLocal != null)
            hoNameTextView.setText("Hello " + StaticInfo.authenticateVoLocal.getFirstName());

        completesubsciption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HowCanWeHelpActivity.this, SubcriptionDetailsActivity.class);
                startActivity(intent);
            }
        });

        talktoConcierge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HowCanWeHelpActivity.this, ContactFranchiseActivity.class);
                startActivity(intent);
            }
        });
    }
}

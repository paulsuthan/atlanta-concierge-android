package com.itrustconcierge.atlantahome;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyVo;
import com.itrustconcierge.atlantahome.Fragments.CurrentPackagesFragment;
import com.itrustconcierge.atlantahome.Fragments.NewPackagesFragment;

import static com.itrustconcierge.atlantahome.Fragments.NewPackagesFragment.propertySubscriptionVos;

import java.util.ArrayList;
import java.util.List;

public class ReviewPackagesActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabs;
    public static PropertyVo propertyVoPackage;
    TextView toolBarButton, myImageViewText;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        propertySubscriptionVos = new ArrayList<>();
        propertyVoPackage = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_packages);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

//        getSupportActionBar().setTitle("Packages");
//        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= 21) {
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } else {

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        myImageViewText = (TextView) findViewById(R.id.myImageViewText);
        myImageViewText.setText("MAINTENANCE PACKAGES");
        myImageViewText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        toolBarButton.setVisibility(View.GONE);

        tabs = (TabLayout) findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.setTabMode(TabLayout.MODE_FIXED);
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewPackagesFragment(), "New Packages");
        adapter.addFragment(new CurrentPackagesFragment(), "Current Packages");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

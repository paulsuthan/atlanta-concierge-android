package com.itrustconcierge.atlantahome;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.ConatctType;
import com.itrustconcierge.atlantahome.API_Manager.vo.ContactVo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;

public class FranchiseNotFoundActivity extends AppCompatActivity {

    Button notifyManager, notifyMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchise_not_found);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Atlanta Concierge");
        toolbar.setTitleTextColor(Color.WHITE);

        notifyManager = (Button) findViewById(R.id.notifyManager);
        notifyMe = (Button) findViewById(R.id.notifyMeButton);
        TextView toolBarButton = (TextView) findViewById(R.id.toolBarButton);
        toolBarButton.setVisibility(View.GONE);

        notifyMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactVo contactVo = new ContactVo();
                contactVo.setContactType(ConatctType.NOTIFY_ME);
                contactVo.setFirstName(authenticateVoLocal.getFirstName());
                contactVo.setLastName(authenticateVoLocal.getLastName());
                contactVo.setEmail(authenticateVoLocal.getEmail());
                contactVo.setMobileNumber(authenticateVoLocal.getMobileNumber());
                final Dialog loadingDialog = Utility.loadingDialog(FranchiseNotFoundActivity.this);
                ApiInterface apiService = ApiClient.getClient(FranchiseNotFoundActivity.this).create(ApiInterface.class);
                Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.sendContact(contactVo);
                call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(FranchiseNotFoundActivity.this, R.style.dialogAnimation));
                            requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                            requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                            requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            requestDialog.setCancelable(false);
                            requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                            TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                            TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                            Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                            Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                            tittleTextView.setText("Thank you!");
                            descriptionTextView.setText("We will contact you as soon as we extend our services to your property area.");

                            negativeButton.setVisibility(View.GONE);
                            positiveButton.setText("OK");

                            positiveButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    requestDialog.dismiss();
                                    finish();
                                }
                            });

                            requestDialog.show();
                        } else {
                            Snackbar.make(notifyManager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                        // Log error here since request failed
                        loadingDialog.dismiss();
                        Log.e("Error", t.toString());
                        Snackbar.make(notifyManager, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        });

        notifyManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog requestDialogTop = new Dialog(new android.view.ContextThemeWrapper(FranchiseNotFoundActivity.this, R.style.dialogAnimation));
                requestDialogTop.requestWindowFeature(Window.FEATURE_NO_TITLE);
                requestDialogTop.setContentView(R.layout.notify_manager_fields);
                requestDialogTop.getWindow().setWindowAnimations(R.style.dialogAnimation);
                requestDialogTop.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                requestDialogTop.setCancelable(true);
                requestDialogTop.getWindow().setGravity(Gravity.BOTTOM);
                TextView toolBarButton = (TextView) requestDialogTop.findViewById(R.id.toolBarButton);
                toolBarButton.setVisibility(View.GONE);
                final EditText nameEditText = (EditText) requestDialogTop.findViewById(R.id.name);
                final EditText emailEditText = (EditText) requestDialogTop.findViewById(R.id.email);
                final EditText phoneEditText = (EditText) requestDialogTop.findViewById(R.id.mobile);
                Button positiveButton = (Button) requestDialogTop.findViewById(R.id.positivebutton);

                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (!nameEditText.getText().toString().isEmpty() && !emailEditText.getText().toString().isEmpty() && !phoneEditText.getText().toString().isEmpty()) {
                            ContactVo contactVo = new ContactVo();
                            contactVo.setContactType(ConatctType.REFER_FRACHISE);
                            contactVo.setFirstName(authenticateVoLocal.getFirstName());
                            contactVo.setLastName(authenticateVoLocal.getLastName());
                            contactVo.setEmail(authenticateVoLocal.getEmail());
                            contactVo.setMobileNumber(authenticateVoLocal.getMobileNumber());
                            ContactVo referContactVo1 = new ContactVo();
                            referContactVo1.setFirstName(nameEditText.getText().toString());
                            referContactVo1.setEmail(emailEditText.getText().toString());
                            referContactVo1.setMobileNumber(phoneEditText.getText().toString());
                            contactVo.setRefferedContact(referContactVo1);
                            final Dialog loadingDialog = Utility.loadingDialog(FranchiseNotFoundActivity.this);
                            ApiInterface apiService = ApiClient.getClient(FranchiseNotFoundActivity.this).create(ApiInterface.class);
                            Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.sendContact(contactVo);
                            call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                                @Override
                                public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                                    loadingDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        final Dialog requestDialog = new Dialog(new android.view.ContextThemeWrapper(FranchiseNotFoundActivity.this, R.style.dialogAnimation));
                                        requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        requestDialog.setContentView(R.layout.custom_alrert_dialogue);
                                        requestDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                                        requestDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        requestDialog.setCancelable(false);
                                        requestDialog.getWindow().setGravity(Gravity.BOTTOM);
                                        TextView tittleTextView = (TextView) requestDialog.findViewById(R.id.tilltle);
                                        TextView descriptionTextView = (TextView) requestDialog.findViewById(R.id.description);
                                        Button positiveButton = (Button) requestDialog.findViewById(R.id.positivebutton);
                                        Button negativeButton = (Button) requestDialog.findViewById(R.id.negativebutton);

                                        tittleTextView.setText("Thank you!");
                                        descriptionTextView.setText("We will contact you as soon as we extend our services to your property area.");

                                        negativeButton.setVisibility(View.GONE);
                                        positiveButton.setText("OK");

                                        positiveButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                // TODO Auto-generated method stub
                                                requestDialog.dismiss();
                                                requestDialogTop.dismiss();
                                                finish();
                                            }
                                        });

                                        requestDialog.show();
                                    } else {
                                        Snackbar.make(notifyManager, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                                    // Log error here since request failed
                                    loadingDialog.dismiss();
                                    Log.e("Error", t.toString());
                                    Snackbar.make(notifyManager, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                requestDialogTop.show();
            }
        });
    }
}

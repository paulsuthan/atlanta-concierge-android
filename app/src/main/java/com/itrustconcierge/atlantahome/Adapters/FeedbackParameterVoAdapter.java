package com.itrustconcierge.atlantahome.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.itrustconcierge.atlantahome.API_Manager.vo.FeedbackParameterVo;
import com.itrustconcierge.atlantahome.R;

import java.util.List;

/**
 * Created by Paul on 1/15/2018.
 */

public class FeedbackParameterVoAdapter extends RecyclerView.Adapter<FeedbackParameterVoAdapter.MyViewHolder> {

    public static List<FeedbackParameterVo> parameterInfos;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView18);
            textView = (TextView) view.findViewById(R.id.textView143);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        }
    }

    public FeedbackParameterVoAdapter(List<FeedbackParameterVo> parameterInfos, Context context) {
        this.parameterInfos = parameterInfos;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.write_review_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final FeedbackParameterVo parameterInfo = parameterInfos.get(position);
        try {
            holder.textView.setText(parameterInfo.getName());
            Glide.with(context).load(parameterInfo.getIconUrl())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageView);

            holder.ratingBar.setOnRatingBarChangeListener(onRatingChangedListener(holder, position));
            holder.ratingBar.setRating(Float.parseFloat(getItem(position).getRating().toString()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private RatingBar.OnRatingBarChangeListener onRatingChangedListener(final RecyclerView.ViewHolder holder, final int position) {
        return new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                getItem(position).setRating(Double.valueOf(v));
            }
        };
    }

    public FeedbackParameterVo getItem(int position) {
        return parameterInfos.get(position);
    }

    @Override
    public int getItemCount() {
        return parameterInfos.size();
    }

}
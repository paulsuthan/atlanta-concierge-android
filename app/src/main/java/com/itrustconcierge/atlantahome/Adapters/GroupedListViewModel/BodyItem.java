package com.itrustconcierge.atlantahome.Adapters.GroupedListViewModel;

/**
 * Created by Paul on 8/7/2017.
 */

public class BodyItem<T> extends ItemType {
    private T body;

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public int getType() {
        return TYPE_BODY;
    }
}

package com.itrustconcierge.atlantahome.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.IhcMessageTargetVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.RequestMessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class RequestMessageAdapter extends RecyclerView.Adapter<RequestMessageAdapter.MyViewHolder> {

    public static List<RequestMessageVo> chatMessageList;
    CharSequence flagOption[] = new CharSequence[]{"Flag", "Cancel"};
    boolean myMsg = false;
    Activity activity;

    public RequestMessageAdapter(List<RequestMessageVo> list, Activity activity) {
        chatMessageList = list;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_message_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RequestMessageVo message = chatMessageList.get(position);

        myMsg = message.getMessage().getSender().getId().equals(authenticateVoLocal.getId());
//        setAlignment(holder, myMsg);

        if (myMsg) {
            holder.contentWithBG.setBackgroundResource(R.drawable.message_bubble_right);
            holder.contentWithBG.setPadding(20, 15, 45, 10);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            layoutParams.rightMargin = 65;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);

            layoutParams = (LinearLayout.LayoutParams) holder.txtSendername.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtSendername.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            layoutParams.topMargin = 10;
            holder.txtInfo.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.imgMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.imgMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.userAvatar.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.userAvatar.setLayoutParams(layoutParams);
            holder.userAvatar.setBackground(activity.getDrawable(R.mipmap.ic_avatar));
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.bubble);
            holder.contentWithBG.setPadding(45, 15, 20, 10);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            layoutParams.leftMargin = 65;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);

            layoutParams = (LinearLayout.LayoutParams) holder.txtSendername.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtSendername.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            layoutParams.topMargin = 10;
            holder.txtInfo.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.imgMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.imgMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.userAvatar.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.userAvatar.setLayoutParams(layoutParams);
            holder.userAvatar.setBackground(activity.getDrawable(R.mipmap.ic_avatar));
        }

//        if (message.getType().equals(MessageType.IMAGE)) {
//            holder.contentWithBG.setVisibility(View.VISIBLE);
//            holder.txtMessage.setVisibility(View.GONE);
//            holder.imgMessage.setVisibility(View.VISIBLE);
//            String physicalFilename = message.getImage();
//            ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
//            Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.CHAT, physicalFilename, message.getMessage(), message.getSender().getId());
//            call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
//                @Override
//                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
//                    if (response.isSuccessful()) {
//                        if (response.body() != null) {
//                            if (response.body().getData() != null) {
//                                try {
//                                    UploadVo uploadVo1 = response.body().getData();
//                                    String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
//                                    Log.d("DownloadUrl", decodeUrl);
//                                    Glide.with(activity).load(decodeUrl).into(holder.imgMessage);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                    } else {
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
//                    // Log error here since request failed
//                    Log.e("Error", t.toString());
//                }
//            });
//        } else {
        holder.contentWithBG.setVisibility(View.VISIBLE);
        holder.txtMessage.setVisibility(View.VISIBLE);
        holder.imgMessage.setVisibility(View.GONE);
        if (message.getMessage().getSender() != null) {
            if (!myMsg)
                holder.txtSendername.setText(message.getMessage().getSender().getFirstName() + " " + message.getMessage().getSender().getLastName());
            else
                holder.txtSendername.setText("Me");
            holder.txtMessage.setText(message.getMessage().getMessage());
        } else {
            holder.txtSendername.setVisibility(View.GONE);
            holder.txtMessage.setText(message.getMessage().getMessage());
        }
//        }

        holder.bothLine.setVisibility(View.GONE);
        holder.hoLine.setVisibility(View.GONE);
        holder.conciergeLine.setVisibility(View.GONE);

        if (message.getMessage().getTargets().size() == 2) {
            holder.bothLine.setVisibility(View.VISIBLE);
            boolean isConcierge = false;
            for (IhcMessageTargetVo targetVo : message.getMessage().getTargets()) {
                if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                    isConcierge = true;
                }
            }
            if (isConcierge) {
                if (!myMsg) {
                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.concierge_icon));
                    } else {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.sp_avatar));
                    }
                }
            } else {
                if (!myMsg) {
                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.concierge_icon));
                    } else {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.sp_avatar));
                    }
                }
            }
        } else {
            holder.bothLine.setVisibility(View.GONE);
            boolean isConcierge = false;
            for (IhcMessageTargetVo targetVo : message.getMessage().getTargets()) {
                if (targetVo.getTarget().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                    isConcierge = true;
                }
            }
            if (isConcierge) {
                holder.conciergeLine.setVisibility(View.VISIBLE);
                if (!myMsg) {
                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.concierge_icon));
                        holder.conciergeLine.setVisibility(View.VISIBLE);
                        holder.hoLine.setVisibility(View.GONE);
                    } else {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.sp_avatar));
                        holder.conciergeLine.setVisibility(View.GONE);
                        holder.hoLine.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (!myMsg) {
                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getFranchise().getId())) {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.concierge_icon));
                        holder.conciergeLine.setVisibility(View.VISIBLE);
                        holder.hoLine.setVisibility(View.GONE);
                    } else {
                        holder.userAvatar.setBackground(activity.getDrawable(R.drawable.sp_avatar));
                        holder.conciergeLine.setVisibility(View.GONE);
                        holder.hoLine.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        try {
            if (position != 0) {
                if (message.getMessage().getTargets().size() == chatMessageList.get(position - 1).getMessage().getTargets().size()) {
                    holder.bothLine.setVisibility(View.GONE);
                    holder.hoLine.setVisibility(View.GONE);
                    holder.conciergeLine.setVisibility(View.GONE);
                }
//                else {
//                    if (message.getMessage().getSender().getId().equals(authenticateVoLocal.getId())) {
//                        if (message.getMessage().getSender().getId().equals(chatMessageList.get(position - 1).getMessage().getSender().getId())) {
//                            holder.bothLine.setVisibility(View.GONE);
//                            holder.hoLine.setVisibility(View.GONE);
//                            holder.conciergeLine.setVisibility(View.GONE);
//                        } else {
//                            holder.bothLine.setVisibility(View.GONE);
//                            holder.hoLine.setVisibility(View.GONE);
//                            holder.conciergeLine.setVisibility(View.VISIBLE);
//                        }
//                    } else {
//                        if (message.getMessage().getSender().getId().equals(chatMessageList.get(position - 1).getMessage().getSender().getId())) {
//                            holder.bothLine.setVisibility(View.GONE);
//                            holder.hoLine.setVisibility(View.GONE);
//                            holder.conciergeLine.setVisibility(View.GONE);
//                        } else {
//                            holder.bothLine.setVisibility(View.GONE);
//                            holder.hoLine.setVisibility(View.VISIBLE);
//                            holder.conciergeLine.setVisibility(View.GONE);
//                        }
//                    }
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (myMsg) {
            if (authenticateVoLocal.getProfileImage() != null) {
                ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.PROFILE_PIC, authenticateVoLocal.getProfileImage(), "image/jpeg", authenticateVoLocal.getId());
                call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
                                        UploadVo uploadVo1 = response.body().getData();
                                        String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                        Log.d("DownloadUrl", decodeUrl);
                                        Glide.with(activity).load(decodeUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.userAvatar) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                RoundedBitmapDrawable circularBitmapDrawable =
                                                        RoundedBitmapDrawableFactory.create(holder.userAvatar.getContext().getResources(), resource);
                                                circularBitmapDrawable.setCircular(true);
                                                holder.userAvatar.setImageDrawable(circularBitmapDrawable);
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                    }
                });
            }
        }

        holder.content.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                if (!myMsg) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setItems(flagOption, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    addFlag();
                                    break;
                                case 1:
                                    dialog.dismiss();
                                    break;
                            }
                        }

                        private void addFlag() {

                        }

                    });
                    builder.show();
                    return true;
                } else {
                    return false;
                }
            }
        });

        holder.txtInfo.setText(getDate(message.getMessage().getCreatedOn()));

//        holder.imgMessage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    List<String> images = new ArrayList<>();
//                    images.add(message.getImage());
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putSerializable("images", (Serializable) images);
//                    bundle1.putInt("position", 0);
//                    bundle1.putString("uploadType", "CHAT");
//                    bundle1.putString("id", message.getSender().getId()+"");
//                    FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager().beginTransaction();
//                    SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
//                    newFragment.setArguments(bundle1);
//                    newFragment.show(ft, "slideshow");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtSendername;
        public TextView txtMessage;
        public ImageView imgMessage;
        public ImageView imgLocation;
        public ImageView userAvatar;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;

        public LinearLayout bothLine;
        public LinearLayout conciergeLine;
        public LinearLayout hoLine;

        public MyViewHolder(View view) {
            super(view);
            txtSendername = (TextView) view.findViewById(R.id.sendername);
            txtMessage = (TextView) view.findViewById(R.id.txtMessage);
            imgMessage = (ImageView) view.findViewById(R.id.imgMessage);
            imgLocation = (ImageView) view.findViewById(R.id.imageView);
            userAvatar = (ImageView) view.findViewById(R.id.userAvatar);
            content = (LinearLayout) view.findViewById(R.id.content);
            contentWithBG = (LinearLayout) view.findViewById(R.id.contentWithBackground);
            txtInfo = (TextView) view.findViewById(R.id.txtInfo);
            bothLine = (LinearLayout) view.findViewById(R.id.bothLine);
            conciergeLine = (LinearLayout) view.findViewById(R.id.conciergeLine);
            hoLine = (LinearLayout) view.findViewById(R.id.hoLine);
        }
    }

    public void add(RequestMessageVo object) {
        chatMessageList.add(object);
    }

}

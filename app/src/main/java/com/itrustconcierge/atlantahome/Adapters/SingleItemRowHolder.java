package com.itrustconcierge.atlantahome.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.itrustconcierge.atlantahome.R;

/**
 * Created by Paul on 11/16/2017.
 */

public class SingleItemRowHolder extends RecyclerView.ViewHolder {
    protected ImageView itemImage;
    String path;
    ProgressBar progressBar;
    OnItemSelectedListener itemSelectedListener;

    public SingleItemRowHolder(View view, OnItemSelectedListener listener) {
        super(view);
        this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        this.itemSelectedListener = listener;
        itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelectedListener.onItemSelected(path);
            }
        });
    }

    public interface OnItemSelectedListener {
        void onItemSelected(String path);
    }

}
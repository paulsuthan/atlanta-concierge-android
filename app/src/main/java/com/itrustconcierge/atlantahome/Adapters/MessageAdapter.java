package com.itrustconcierge.atlantahome.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.MessageType;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.MessageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.Fragments.SlideshowDialogFragment;
import com.itrustconcierge.atlantahome.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

/**
 * Created by Paul on 8/25/2017.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    public static List<MessageVo> chatMessageList;
    CharSequence flagOption[] = new CharSequence[]{"Flag", "Cancel"};
    boolean myMsg = false;
    Activity activity;

    public MessageAdapter(List<MessageVo> list, Activity activity) {
        chatMessageList = list;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MessageVo message = chatMessageList.get(position);

        myMsg = message.getSender().getId().equals(authenticateVoLocal.getId());
//        setAlignment(holder, myMsg);

        if (myMsg) {
            holder.contentWithBG.setBackgroundResource(R.drawable.bubble_in_9);
            holder.contentWithBG.setPadding(10, 10, 15, 15);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);

            layoutParams = (LinearLayout.LayoutParams) holder.txtSendername.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtSendername.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.imgMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.imgMessage.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.bubble_out_9);
            holder.contentWithBG.setPadding(15, 10, 10, 15);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);

            layoutParams = (LinearLayout.LayoutParams) holder.txtSendername.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtSendername.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.imgMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.imgMessage.setLayoutParams(layoutParams);
        }

        if(message.getType() == null){
            message.setType(MessageType.MESSAGE);
        }

        if (message.getType().equals(MessageType.IMAGE)) {
            holder.contentWithBG.setVisibility(View.VISIBLE);
            holder.txtMessage.setVisibility(View.GONE);
            holder.imgMessage.setVisibility(View.VISIBLE);
            String physicalFilename = message.getImage();
            ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
            Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(UploadType.CHAT, physicalFilename, message.getMessage(), message.getSender().getId());
            call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                try {
                                    UploadVo uploadVo1 = response.body().getData();
                                    String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                    Log.d("DownloadUrl", decodeUrl);
                                    Glide.with(activity).load(decodeUrl).into(holder.imgMessage);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                }
            });
        } else {
            holder.contentWithBG.setVisibility(View.VISIBLE);
            holder.txtMessage.setVisibility(View.VISIBLE);
            holder.imgMessage.setVisibility(View.GONE);
            if (message.getSender() != null) {
                holder.txtSendername.setVisibility(View.GONE);
                holder.txtSendername.setText(message.getSender().getFirstName() + " " + message.getSender().getLastName());
                holder.txtMessage.setText(message.getMessage());
            } else {
                holder.txtSendername.setVisibility(View.GONE);
                holder.txtMessage.setText(message.getMessage());
            }
        }

        holder.content.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                if (!myMsg) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setItems(flagOption, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    addFlag();
                                    break;
                                case 1:
                                    dialog.dismiss();
                                    break;
                            }
                        }

                        private void addFlag() {

                        }

                    });
                    builder.show();
                    return true;
                } else {
                    return false;
                }
            }
        });

        holder.imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<String> images = new ArrayList<>();
                    images.add(message.getImage());
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("images", (Serializable) images);
                    bundle1.putInt("position", 0);
                    bundle1.putString("uploadType", "CHAT");
                    bundle1.putString("id", message.getSender().getId() + "");
                    FragmentTransaction ft = ((FragmentActivity) activity).getSupportFragmentManager().beginTransaction();
                    SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                    newFragment.setArguments(bundle1);
                    newFragment.show(ft, "slideshow");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.txtInfo.setText(getDate(message.getCreatedOn()));

    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtSendername;
        public TextView txtMessage;
        public ImageView imgMessage;
        public ImageView imgLocation;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;

        public MyViewHolder(View view) {
            super(view);
            txtSendername = (TextView) view.findViewById(R.id.sendername);
            txtMessage = (TextView) view.findViewById(R.id.txtMessage);
            imgMessage = (ImageView) view.findViewById(R.id.imgMessage);
            imgLocation = (ImageView) view.findViewById(R.id.imageView);
            content = (LinearLayout) view.findViewById(R.id.content);
            contentWithBG = (LinearLayout) view.findViewById(R.id.contentWithBackground);
            txtInfo = (TextView) view.findViewById(R.id.txtInfo);
        }
    }

    public void add(MessageVo object) {
        chatMessageList.add(object);
    }

}
package com.itrustconcierge.atlantahome.Adapters.GroupedListViewModel;

/**
 * Created by Paul on 8/7/2017.
 */

public abstract class ItemType {
    public static final int TYPE_TITTLE = 0;
    public static final int TYPE_BODY = 1;

    abstract public int getType();
}

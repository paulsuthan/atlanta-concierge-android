package com.itrustconcierge.atlantahome.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableItem;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableViewHolder;

/**
 * Created by Paul on 11/22/2017.
 */

public class SelectableImageViewHolder extends RecyclerView.ViewHolder {

    public static final int MULTI_SELECTION = 2;
    public static final int SINGLE_SELECTION = 1;
    public TextView textView;
    public ImageView imageView;
    public SelectableItem mItem;
    public CheckedTextView checkBox;
    OnItemSelectedListener itemSelectedListener;


    public SelectableImageViewHolder(View view, OnItemSelectedListener listener) {
        super(view);
        itemSelectedListener = listener;
        textView = (TextView) view.findViewById(R.id.myImageViewText);
        imageView = (ImageView) view.findViewById(R.id.myImageView);
        checkBox = (CheckedTextView) view.findViewById(R.id.checked_text_item);
        checkBox.setVisibility(View.GONE);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItem.isSelected() && getItemViewType() == MULTI_SELECTION) {
                    setChecked(false);
                } else {
                    setChecked(true);
                }
                itemSelectedListener.onItemSelected(mItem);

            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItem.isSelected() && getItemViewType() == MULTI_SELECTION) {
                    setChecked(false);
                } else {
                    setChecked(true);
                }
                itemSelectedListener.onItemSelected(mItem);

            }
        });
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItem.isSelected() && getItemViewType() == MULTI_SELECTION) {
                    setChecked(false);
                } else {
                    setChecked(true);
                }
                itemSelectedListener.onItemSelected(mItem);

            }
        });
    }

    public void setChecked(boolean value) {
        if (value) {
            textView.setBackgroundColor(Color.LTGRAY);
            checkBox.setVisibility(View.VISIBLE);
        } else {
            textView.setBackground(null);
            checkBox.setVisibility(View.GONE);
        }
        mItem.setSelected(value);
        checkBox.setChecked(value);
    }

    public interface OnItemSelectedListener {

        void onItemSelected(SelectableItem item);
    }

}

package com.itrustconcierge.atlantahome.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.opengl.Visibility;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.enums.TimeUnit;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyPackageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertySubscriptionVo;
import com.itrustconcierge.atlantahome.R;

import java.util.HashMap;
import java.util.List;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.itrustconcierge.atlantahome.Fragments.NewPackagesFragment.propertySubscriptionVos;

/**
 * Created by paul on 8/7/2017.
 */

public class PackageExpandableAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<PropertyPackageVo> _listDataHeader;
    private HashMap<PropertyPackageVo, List<String>> _listDataChild;
    private String newOrCurrentPackage;

    public PackageExpandableAdapter(Context context, List<PropertyPackageVo> listDataHeader,
                                    HashMap<PropertyPackageVo, List<String>> listChildData, String newOrCurrentPackage) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.newOrCurrentPackage = newOrCurrentPackage;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return childPosition;
        }
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String s = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.service_info_item, null);

//            convertView.setBackgroundColor(Color.WHITE);
//            parent.setBackgroundColor(Color.WHITE);
//            if(childPosition == 0){
//                convertView.setPadding(50,10,50,0);
//            }else{
//                convertView.setPadding(50,0,50,0);
//            }

//            convertView.setLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) convertView.getLayoutParams();
//            params.setMarginStart(30);
//            params.setMarginEnd(30);
//            paramsText.setMargins(0,0,0,0);
        }

        TextView txtListChildTittle = (TextView) convertView
                .findViewById(R.id.textView2);
        TextView txtListChildDescription = (TextView) convertView
                .findViewById(R.id.textView);

        View space = (View) convertView
                .findViewById(R.id.space);
        TextView anotherTittle = (TextView) convertView
                .findViewById(R.id.anotherTittle);

//        txtListChildTittle.setText(s);
        txtListChildDescription.setText(s);

        if (childPosition == 0) {
            space.setVisibility(View.VISIBLE);
            space.setBackgroundColor(Color.WHITE);
            anotherTittle.setVisibility(View.VISIBLE);
            anotherTittle.setText("Package Services:");
            anotherTittle.setTextColor(Color.BLACK);
            anotherTittle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        } else {
            space.setVisibility(View.GONE);
            anotherTittle.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, final ViewGroup parent) {
        final PropertyPackageVo propertyPackageVo = (PropertyPackageVo) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.package_list_item, null);
        }


        CheckBox lblListHeader = (CheckBox) convertView
                .findViewById(R.id.checkBox);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(propertyPackageVo.getName());
        final TextView description = (TextView) convertView
                .findViewById(R.id.description);
        TextView showHide = (TextView) convertView
                .findViewById(R.id.showHide);
        description.setText(propertyPackageVo.getName());
        if (newOrCurrentPackage == "currentPackageAdapter") {
            LinearLayout temp = (LinearLayout) convertView.findViewById(R.id.temp);
            temp.setVisibility(View.GONE);
        }

        try {
            if(propertyPackageVo.getTimeUnit().equals(TimeUnit.ANNUAL)){
                lblListHeader.setText("$" + propertyPackageVo.getYearlyAmount() + " (" + propertyPackageVo.getTimeUnit().name() + ") ");
            } else {
                lblListHeader.setText("$" + propertyPackageVo.getMonthlyAmount() + " (" + propertyPackageVo.getTimeUnit().name() + ") ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            final PropertySubscriptionVo propertySubscriptionVo = new PropertySubscriptionVo();

            lblListHeader.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        propertySubscriptionVo.setPropertyPackage(propertyPackageVo);
                        propertySubscriptionVo.setDuaration(1);
                        if(propertyPackageVo.getTimeUnit().equals(TimeUnit.ANNUAL)){
                            propertySubscriptionVo.setAmount(propertyPackageVo.getYearlyAmount());
                        } else {
                            propertySubscriptionVo.setAmount(propertyPackageVo.getMonthlyAmount());
                        }
                        propertySubscriptionVo.setDurationType(propertyPackageVo.getTimeUnit());
                        if (isChecked) {
                            propertySubscriptionVos.add(propertySubscriptionVo);
                        } else {
                            propertySubscriptionVos.remove(propertySubscriptionVo);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isExpanded) {
            showHide.setText("Hide details");
        } else {
            showHide.setText("Show details");
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

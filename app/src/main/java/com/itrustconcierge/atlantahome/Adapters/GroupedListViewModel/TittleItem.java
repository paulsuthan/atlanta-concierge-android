package com.itrustconcierge.atlantahome.Adapters.GroupedListViewModel;

/**
 * Created by Paul on 8/7/2017.
 */

public class TittleItem extends ItemType {
    private String tittle;

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    @Override
    public int getType() {
        return TYPE_TITTLE;
    }
}
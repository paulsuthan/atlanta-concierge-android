package com.itrustconcierge.atlantahome.Adapters;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.UploadType;
import com.itrustconcierge.atlantahome.API_Manager.vo.UploadVo;
import com.itrustconcierge.atlantahome.Fragments.SlideshowDialogFragment;
import com.itrustconcierge.atlantahome.PropertyActivity;
import com.itrustconcierge.atlantahome.R;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;

/**
 * Created by paul on 8/8/2017.
 */

public class ImageListViewAdapter extends RecyclerView.Adapter<SingleItemRowHolder> implements SingleItemRowHolder.OnItemSelectedListener {

    private final List<String> pathList;
    Activity activity;
    SingleItemRowHolder.OnItemSelectedListener listener;
    public static Map<String, String> imageUrlMap = new HashMap<>();
    UploadType uploadType;
    Boolean doUpload;
    Long id;

    public ImageListViewAdapter(Activity context, List<String> pathList, SingleItemRowHolder.OnItemSelectedListener listener, UploadType uploadType, Boolean doUpload, Long id) {
        this.pathList = pathList;
        this.activity = context;
        this.listener = listener;
        this.uploadType = uploadType;
        this.doUpload = doUpload;
        this.id = id;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_list_item, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v, this);
        return mh;
    }

    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, final int i) {
        final String imagePath = pathList.get(i);
        holder.path = imagePath;
        if (doUpload) {
            holder.progressBar.setVisibility(View.GONE);
            if (imagePath.equals("o")) {
                holder.itemImage.setImageResource(R.drawable.ic_photo_camera_black_24dp);
            } else {
                Glide.with(holder.itemImage.getContext()).load(imagePath).into(holder.itemImage);
            }
        } else {
            if (!holder.path.contains("http")) {
                ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
                Call<ResponseWrapper<UploadVo>> call1 = apiService.getDownloadUrl(uploadType, holder.path, "image/jpeg", id);
                call1.enqueue(new Callback<ResponseWrapper<UploadVo>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<UploadVo>> call, Response<ResponseWrapper<UploadVo>> response) {
                        if (response.isSuccessful()) {
                            holder.progressBar.setVisibility(View.GONE);
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    try {
                                        holder.progressBar.setVisibility(View.VISIBLE);
                                        UploadVo uploadVo1 = response.body().getData();
                                        String decodeUrl = java.net.URLDecoder.decode(uploadVo1.getUrl(), "UTF-8");
                                        Glide.with(holder.itemImage.getContext()).load(decodeUrl).listener(new RequestListener<String, GlideDrawable>() {
                                            @Override
                                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                holder.progressBar.setVisibility(View.GONE);
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                                           boolean isFromMemoryCache, boolean isFirstResource) {
                                                holder.progressBar.setVisibility(View.GONE);
                                                return false;
                                            }
                                        }).into(holder.itemImage);
                                        Log.d("DownloadUrl", decodeUrl);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<UploadVo>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                    }
                });
            } else {
                Glide.with(holder.itemImage.getContext()).load(holder.path).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                   boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.itemImage);
            }

            holder.itemImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pathList.isEmpty()) {
                        try {
                            Bundle bundle1 = new Bundle();
                            bundle1.putSerializable("images", (Serializable) pathList);
                            bundle1.putInt("position", i);
                            bundle1.putString("uploadType", String.valueOf(uploadType));
                            bundle1.putString("id", id + "");
                            FragmentTransaction ft = ((FragmentActivity) activity).getSupportFragmentManager().beginTransaction();
                            SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                            newFragment.setArguments(bundle1);
                            newFragment.show(ft, "slideshow");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != pathList ? pathList.size() : 0);
    }

    @Override
    public void onItemSelected(String path) {
        listener.onItemSelected(path);
    }

}
package com.itrustconcierge.atlantahome.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Space;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.vo.PropertyPackageVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.PropertySubscriptionVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by paul on 8/7/2017.
 */

public class CurrentPackageExpandableAdapter extends BaseExpandableListAdapter {

    private static List<PropertySubscriptionVo> propertySubscriptionVos = new ArrayList<>();
    private Context _context;
    private List<PropertyPackageVo> _listDataHeader;
    private HashMap<PropertyPackageVo, List<String>> _listDataChild;
    private String newOrCurrentPackage;
    private ExpandableListView expListView;

    public CurrentPackageExpandableAdapter(Context context, ExpandableListView expListView, List<PropertySubscriptionVo> propertySubscriptionVos, List<PropertyPackageVo> listDataHeader,
                                           HashMap<PropertyPackageVo, List<String>> listChildData, String newOrCurrentPackage) {
        this._context = context;
        this.expListView = expListView;
        this.propertySubscriptionVos = propertySubscriptionVos;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.newOrCurrentPackage = newOrCurrentPackage;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return childPosition;
        }
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String s = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.current_package_service_info_item, null);
//            convertView.setBackgroundColor(Color.LTGRAY);
        }
//        parent.setBackgroundColor(Color.GRAY);
        if (isLastChild) {
            expListView.setDividerHeight(15);
//            convertView.setMar;
        } else {
            expListView.setDividerHeight(0);
        }

        TextView txtListChildTittle = (TextView) convertView
                .findViewById(R.id.textView2);
        TextView txtListChildDescription = (TextView) convertView
                .findViewById(R.id.textView);


        Space space = (Space) convertView
                .findViewById(R.id.space);
        TextView anotherTittle = (TextView) convertView
                .findViewById(R.id.anotherTittle);

//        txtListChildTittle.setText(s);
        txtListChildDescription.setText(s);

        if (childPosition == 0) {
            space.setVisibility(View.VISIBLE);
            anotherTittle.setVisibility(View.VISIBLE);
            anotherTittle.setText("Package Services:");
            anotherTittle.setTextColor(Color.BLACK);
            anotherTittle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        } else {
            space.setVisibility(View.GONE);
            anotherTittle.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (_listDataChild.isEmpty()) {
            return 0;
        } else {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, final ViewGroup parent) {
        final PropertyPackageVo propertyPackageVo = (PropertyPackageVo) getGroup(groupPosition);
//        final PropertyPackageVo propertySubscriptionVos = (PropertyPackageVo) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.currnet_package_package_list_item, null);
        }

        expListView.setDividerHeight(15);

//        CheckBox lblListHeader = (CheckBox) convertView
//                .findViewById(R.id.checkBox);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
//        lblListHeader.setText(propertyPackageVo.getName());
        final TextView description = (TextView) convertView
                .findViewById(R.id.description);
        final TextView next_maintenance_date = (TextView) convertView
                .findViewById(R.id.next_maintenance_date);
        final TextView expires_on_date = (TextView) convertView
                .findViewById(R.id.Expires_on_date);
        final TextView showHide = (TextView) convertView
                .findViewById(R.id.showHide);
        description.setText(propertyPackageVo.getName());
        try {

            Utility date = new Utility();
            String temp1 = date.getDateOnly(propertySubscriptionVos.get(groupPosition).getNextMaintenceDate().longValue()).toString();
            next_maintenance_date.setText(temp1);
            String temp2 = date.getDateOnly(propertySubscriptionVos.get(groupPosition).getExpiryTime().longValue()).toString();
            expires_on_date.setText(temp2);


        } catch (Exception e) {
            e.printStackTrace();
        }

//        if(newOrCurrentPackage == "CurrentPackageExpandableAdapter"){
//            LinearLayout temp = (LinearLayout)convertView.findViewById(R.id.temp);
//            temp.setVisibility(View.GONE);
//        }


//        try {
//            lblListHeader.setText("$" + propertyPackageVo.getAmount() + " (" + propertyPackageVo.getTimeUnit().name() + ") ");
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//        try {
//            final PropertySubscriptionVo propertySubscriptionVo = new PropertySubscriptionVo();
//
//            lblListHeader.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    try {
//                        propertySubscriptionVo.setPropertyPackage(propertyPackageVo);
//                        propertySubscriptionVo.setDuaration(1);
//                        propertySubscriptionVo.setAmount(propertyPackageVo.getMonthlyAmount());
//                        propertySubscriptionVo.setDurationType(propertyPackageVo.getTimeUnit());
//                        if (isChecked) {
//                            propertySubscriptionVos.add(propertySubscriptionVo);
//                        } else {
//                            propertySubscriptionVos.remove(propertySubscriptionVo);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        if (isExpanded) {
            showHide.setText("Hide details");
        } else {
            showHide.setText("Show details");
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}


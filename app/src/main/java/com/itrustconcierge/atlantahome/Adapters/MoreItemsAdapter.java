package com.itrustconcierge.atlantahome.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.LoginActivity;
import com.itrustconcierge.atlantahome.PaymentListActivity;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Scheduler.SchedulerActivity;
import com.itrustconcierge.atlantahome.Utilities.BadgeViewUtil;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.MainActivity.unConsumedPaymentsSize;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.clearLogincredentials;

/**
 * Created by paul on 8/7/2017.
 */

public class MoreItemsAdapter extends RecyclerView.Adapter<MoreItemsAdapter.ViewHolder> {

    private List<String> mValues;
    Dialog noInternetDialog;
    private Activity activity;

    public MoreItemsAdapter(List<String> items, Activity activity) {
        this.mValues = items;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_more_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position));

        if (mValues.get(position).contains("LOGOUT")) {
            holder.mContentView.setTextColor(Color.RED);
            holder.mContentView.setGravity(Gravity.CENTER);
            holder.arrow.setVisibility(View.GONE);
        } else if (mValues.get(position).contains("space")) {
            holder.mContentView.setVisibility(View.INVISIBLE);
            holder.linearLayoutMore.setBackgroundColor(Color.TRANSPARENT);
            holder.arrow.setVisibility(View.INVISIBLE);
        } else if (mValues.get(position).contains("Payment Details")) {
            holder.mContentView.setTextColor(holder.mContentView.getContext().getResources().getColor(R.color.textColor));
            holder.mContentView.setGravity(Gravity.LEFT);
            holder.arrow.setVisibility(View.VISIBLE);
            if (unConsumedPaymentsSize != 0) {
                BadgeViewUtil badge = new BadgeViewUtil(activity, holder.mContentView);
                badge.setText(unConsumedPaymentsSize + "");
                badge.setBadgePosition(BadgeViewUtil.POSITION_CENTER);
                badge.show();
            }
        } else {
            holder.mContentView.setTextColor(holder.mContentView.getContext().getResources().getColor(R.color.textColor));
            holder.mContentView.setGravity(Gravity.LEFT);
            holder.arrow.setVisibility(View.VISIBLE);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternetConenction(activity)) {
                    if (mValues.get(position).contains("LOGOUT")) {
                        logOut();
                    } else if (mValues.get(position).contains("Calendar")) {
                        Intent intent = new Intent(v.getContext(), SchedulerActivity.class);
                        intent.putExtra("fromMore", true);
                        v.getContext().startActivity(intent);
                    } else if (mValues.get(position).contains("Payment Details")) {
                        Intent intent = new Intent(v.getContext(), PaymentListActivity.class);
                        intent.putExtra("fromMore", true);
                        v.getContext().startActivity(intent);
                    } else if (mValues.get(position).contains("Tell Friends & Family")) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out iTrust Home for your business");
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                "I’m using Atlanta Concierge to take care of my home and personal concierge needs. Check out the app at https://play.google.com/store/apps/details?id=com.itrustconcierge.atlantahome");
                        sendIntent.setType("text/plain");
                        v.getContext().startActivity(sendIntent);
                    }
                } else {
                    initializeNoInternetDialogue();
                }
            }
        });
    }

    private void logOut() {
        final Dialog loadingDialog = Utility.loadingDialog(activity);
        ApiInterface apiService = ApiClient.getClient(activity).create(ApiInterface.class);
        Call<ResponseBody> call = apiService.signOut();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    clearLogincredentials(activity);
                    StaticInfo.authenticateVoLocal = null;
                    Intent intent = new Intent(activity, LoginActivity.class);
                    intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activity.startActivity(intent);
                    ActivityCompat.finishAffinity(activity);
                } else {
                    Toast.makeText(activity, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingDialog.dismiss();
                Log.e("Error", t.toString());
                Toast.makeText(activity, SOMETHING_WENT_WRONG, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(activity, R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(activity)) {
                } else {
//                    connectionLostDialogue();
                }
            }
        });

        noInternetDialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView mContentView;
        public String mItem;
        public ImageView arrow;
        public LinearLayout linearLayoutMore;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.content);
            arrow = (ImageView) view.findViewById(R.id.arrow);
            linearLayoutMore = (LinearLayout) view.findViewById(R.id.linearLayoutMore);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }
}
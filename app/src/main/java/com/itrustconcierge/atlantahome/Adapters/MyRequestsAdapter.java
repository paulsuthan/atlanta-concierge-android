package com.itrustconcierge.atlantahome.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.itrustconcierge.atlantahome.API_Manager.enums.NotificationType;
import com.itrustconcierge.atlantahome.API_Manager.enums.RequestHistoryType;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.NotificationVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity;
import com.itrustconcierge.atlantahome.Utilities.BadgeViewUtil;

import java.util.List;

import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

public class MyRequestsAdapter extends RecyclerView.Adapter<MyRequestsAdapter.ViewHolder> {

    private final List<HomeOwnerRequestVo> mValues;
    Activity activity;
    Dialog noInternetDialog;
    PopupWindow noInternetPopupBar;

    public MyRequestsAdapter(Activity activity, List<HomeOwnerRequestVo> items) {
        this.activity = activity;
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_my_requests_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        try {
            holder.tittleTextView.setText(holder.mItem.getService().getName());
            if (holder.mItem.getDescription() != null) {
                holder.descriptionTextView.setVisibility(View.VISIBLE);
                holder.descriptionTextView.setText(holder.mItem.getDescription());
            } else {
                holder.descriptionTextView.setVisibility(View.GONE);
            }
            holder.statusTextView.setText(holder.mItem.getStatus().getName());
            holder.dateTextView.setText(getDate(holder.mItem.getCreatedOn()));

            if (holder.mItem.getStatus().equals(RequestHistoryType.VISIT_REQUESTED)
                    || holder.mItem.getStatus().equals(RequestHistoryType.JOB_SCHEDULE_REQUESTED)
                    || holder.mItem.getStatus().equals(RequestHistoryType.QUOTE)
                    || holder.mItem.getStatus().equals(RequestHistoryType.APPROVED_INVOICE)) {
                holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.greyUnRead));
                holder.descriptionTextView.setTypeface(holder.descriptionTextView.getTypeface(), Typeface.BOLD);
                holder.statusTextView.setTypeface(holder.statusTextView.getTypeface(), Typeface.BOLD);
                holder.dateTextView.setTypeface(holder.dateTextView.getTypeface(), Typeface.BOLD);
                holder.tittleTextView.setTypeface(holder.tittleTextView.getTypeface(), Typeface.BOLD);
                BadgeViewUtil badge = new BadgeViewUtil(activity, holder.descriptionTextView);
                badge.setText("1");
                badge.setBadgePosition(BadgeViewUtil.POSITION_CENTER);
                badge.show();
            } else {
                holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.white));
                holder.descriptionTextView.setTypeface(holder.descriptionTextView.getTypeface(), Typeface.NORMAL);
                holder.statusTextView.setTypeface(holder.statusTextView.getTypeface(), Typeface.NORMAL);
                holder.dateTextView.setTypeface(holder.dateTextView.getTypeface(), Typeface.NORMAL);
                holder.tittleTextView.setTypeface(holder.tittleTextView.getTypeface(), Typeface.NORMAL);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkInternetConenction(activity)) {
                        v.getContext().startActivity(new Intent(v.getContext(), WorkHistoryActivity.class).putExtra("requestId", holder.mItem.getId()));
                    } else {
//                    Snackbar.make(recyclerView, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                        initializeNoInternetDialogue();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(activity, R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(activity)) {
                } else {
                }
            }
        });

        noInternetDialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public HomeOwnerRequestVo mItem;
        TextView tittleTextView, descriptionTextView, statusTextView, dateTextView;
        CardView card_view;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            card_view = (CardView) view.findViewById(R.id.card_view);
            tittleTextView = (TextView) view.findViewById(R.id.tittleTextView);
            descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);
            statusTextView = (TextView) view.findViewById(R.id.statusTextView);
            dateTextView = (TextView) view.findViewById(R.id.dateTextView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mItem.getClass() + "'";
        }
    }
}

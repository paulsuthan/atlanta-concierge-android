package com.itrustconcierge.atlantahome.Adapters;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.Item;
import com.itrustconcierge.atlantahome.Utilities.SelectionRecyclerviewAdapter.SelectableItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 11/22/2017.
 */

public class SelectableImageAdapter extends RecyclerView.Adapter implements SelectableImageViewHolder.OnItemSelectedListener {

    private List<SelectableItem> mValues;
    private List<Item> items;
    private boolean isMultiSelectionEnabled = false;
    SelectableImageViewHolder.OnItemSelectedListener listener;

    public SelectableImageAdapter(SelectableImageViewHolder.OnItemSelectedListener listener,
                                  List<Item> items, boolean isMultiSelectionEnabled) {
        this.listener = listener;
        this.isMultiSelectionEnabled = isMultiSelectionEnabled;
        this.items = items;

        mValues = new ArrayList<>();
        for (Item item : items) {
            SelectableItem selectableItem = new SelectableItem(item, false);
            selectableItem.setId(item.getId());
            mValues.add(selectableItem);
        }
    }

    public void updateList(List<Item> list) {
        mValues = new ArrayList<>();
        if(list != null) {
            for (Item item : list) {
                SelectableItem selectableItem = new SelectableItem(item, false);
                selectableItem.setId(item.getId());
                mValues.add(selectableItem);
            }
            notifyDataSetChanged();
        }
    }

    @Override
    public SelectableImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checked_image_item, parent, false);

        return new SelectableImageViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        final SelectableImageViewHolder holder = (SelectableImageViewHolder) viewHolder;
        SelectableItem selectableItem = mValues.get(position);
        String name = selectableItem.getName();
        holder.textView.setText(name);
        Glide.with(holder.imageView.getContext()).load("https://s3.amazonaws.com/itrustconcierge/assets/services/" + selectableItem.getSurname()).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(holder.imageView.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
        if (isMultiSelectionEnabled) {
            TypedValue value = new TypedValue();
            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
            int checkMarkDrawableResId = value.resourceId;
            holder.checkBox.setCheckMarkDrawable(checkMarkDrawableResId);
        } else {
            TypedValue value = new TypedValue();
            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
            int checkMarkDrawableResId = value.resourceId;
            holder.checkBox.setCheckMarkDrawable(checkMarkDrawableResId);
        }

        holder.mItem = selectableItem;
        holder.setChecked(holder.mItem.isSelected());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<Item> getSelectedItems() {
        List<Item> selectedItems = new ArrayList<>();
        for (SelectableItem item : mValues) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (isMultiSelectionEnabled) {
            return SelectableImageViewHolder.MULTI_SELECTION;
        } else {
            return SelectableImageViewHolder.SINGLE_SELECTION;
        }
    }

    @Override
    public void onItemSelected(SelectableItem item) {
        if (!isMultiSelectionEnabled) {
            for (SelectableItem selectableItem : mValues) {
                if (!selectableItem.equals(item)
                        && selectableItem.isSelected()) {
                    selectableItem.setSelected(false);
                } else if (selectableItem.equals(item)
                        && item.isSelected()) {
                    selectableItem.setSelected(true);
                }
            }
            notifyDataSetChanged();
        }
        listener.onItemSelected(item);
    }
}
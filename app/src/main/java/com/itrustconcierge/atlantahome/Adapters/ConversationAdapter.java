package com.itrustconcierge.atlantahome.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.itrustconcierge.atlantahome.API_Manager.ApiClient;
import com.itrustconcierge.atlantahome.API_Manager.ApiInterface;
import com.itrustconcierge.atlantahome.API_Manager.ResponseWrapper;
import com.itrustconcierge.atlantahome.API_Manager.enums.ConversationType;
import com.itrustconcierge.atlantahome.API_Manager.vo.ConversationParticipantVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.ConversationVo;
import com.itrustconcierge.atlantahome.API_Manager.vo.HomeOwnerRequestVo;
import com.itrustconcierge.atlantahome.R;
import com.itrustconcierge.atlantahome.RequestFlow.RequestMessagesActivity;
import com.itrustconcierge.atlantahome.RequestFlow.WorkHistoryActivity;
import com.itrustconcierge.atlantahome.RequestHistory;
import com.itrustconcierge.atlantahome.Utilities.BadgeViewUtil;
import com.itrustconcierge.atlantahome.Utilities.StaticInfo;
import com.itrustconcierge.atlantahome.Utilities.Utility;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.itrustconcierge.atlantahome.Utilities.StaticInfo.authenticateVoLocal;
import static com.itrustconcierge.atlantahome.Utilities.Utility.NO_INTERNET;
import static com.itrustconcierge.atlantahome.Utilities.Utility.SOMETHING_WENT_WRONG;
import static com.itrustconcierge.atlantahome.Utilities.Utility.checkInternetConenction;
import static com.itrustconcierge.atlantahome.Utilities.Utility.getDate;

/**
 * Created by Paul on 10/3/2017.
 */

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.MyViewHolder> {

    public static ConversationVo conversationVoStatic;
    Context context;
    Dialog noInternetDialog;
    private List<ConversationVo> conversationVos;

    public ConversationAdapter(Context context, List<ConversationVo> conversationVos) {
        this.context = context;
        this.conversationVos = conversationVos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ConversationVo conversationVo = conversationVos.get(position);
        try {
            if (conversationVo.getType().equals(ConversationType.CHAT)) {
                if (conversationVo.getSource().getId().equals(authenticateVoLocal.getId())) {
                    holder.name.setText(conversationVo.getTarget().getFirstName() + " " + conversationVo.getTarget().getLastName());
                    try {
                        if (!authenticateVoLocal.getUserTenantId().equals(conversationVo.getTarget().getUserTenantId())) {
                            Drawable img = holder.name.getContext().getResources().getDrawable(R.drawable.sp_avatar);
                            img.setBounds(0, 0, 60, 60);
                            holder.name.setCompoundDrawables(img, null, null, null);
                            holder.name.setCompoundDrawablePadding(10);
                        } else {
                            Drawable img = holder.name.getContext().getResources().getDrawable(R.drawable.concierge_chat);
                            img.setBounds(0, 0, 60, 60);
                            holder.name.setCompoundDrawables(img, null, null, null);
                            holder.name.setCompoundDrawablePadding(10);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (conversationVo.getTarget().getId().equals(authenticateVoLocal.getId())) {
                    holder.name.setText(conversationVo.getSource().getFirstName() + " " + conversationVo.getSource().getLastName());
                    try {
                        if (!authenticateVoLocal.getUserTenantId().equals(conversationVo.getSource().getUserTenantId())) {
                            Drawable img = holder.name.getContext().getResources().getDrawable(R.drawable.sp_avatar);
                            img.setBounds(0, 0, 60, 60);
                            holder.name.setCompoundDrawables(img, null, null, null);
                            holder.name.setCompoundDrawablePadding(10);
                        } else {
                            Drawable img = holder.name.getContext().getResources().getDrawable(R.drawable.concierge_chat);
                            img.setBounds(0, 0, 60, 60);
                            holder.name.setCompoundDrawables(img, null, null, null);
                            holder.name.setCompoundDrawablePadding(10);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }

                if (conversationVo.getLastMessage() != null) {
                    if (conversationVo.getLastMessage().getMessage() != null) {
                        holder.description.setText(conversationVo.getLastMessage().getMessage());
                    }
                }
            } else if (conversationVo.getType().equals(ConversationType.REQUEST_CHAT)) {
                holder.name.setText(conversationVo.getRequest().getService().getName() + " - " + conversationVo.getRequest().getDescription());
                if (conversationVo.getDescription() != null) {
//                    holder.description.setText(conversationVo.getLastMessage().getMessage());
                    if(!conversationVo.getSource().getId().equals(authenticateVoLocal.getId()))
                        holder.description.setText(conversationVo.getSource().getFirstName()+" "+conversationVo.getSource().getLastName()+": "+conversationVo.getDescription());
                    else
                        holder.description.setText("You: "+conversationVo.getDescription());
                } else {
                    for(ConversationParticipantVo conversationParticipantVo : conversationVo.getParticipants()){
                        if(conversationParticipantVo.getParticipant().getId().equals(authenticateVoLocal.getId())) {
                            holder.description.setText("You: " + conversationParticipantVo.getLastMessage());
                        } else {
                            holder.description.setText(conversationParticipantVo.getParticipant().getFirstName()+" "+conversationParticipantVo.getParticipant().getLastName()+": "+conversationParticipantVo.getLastMessage());
                        }
                    }
                }
            }

            holder.date.setText(getDate(conversationVo.getLastUpdatedOn()));
            holder.status.setVisibility(View.GONE);

            if (conversationVo.getConsumed() == null)
                conversationVo.setConsumed(true);

            for(ConversationParticipantVo conversationParticipantVo : conversationVo.getParticipants()){
                if(conversationParticipantVo.getParticipant().getId().equals(authenticateVoLocal.getId())) {
                    if (!conversationParticipantVo.getConsumed()) {
                        holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.greyUnRead));
                        holder.name.setTypeface(holder.name.getTypeface(), Typeface.BOLD);
                        holder.date.setTypeface(holder.date.getTypeface(), Typeface.BOLD);
                        BadgeViewUtil badge = new BadgeViewUtil(context, holder.description);
                        badge.setText("1");
                        badge.setBadgePosition(BadgeViewUtil.POSITION_CENTER);
                        badge.show();
                    } else {
                        holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.white));
                        holder.name.setTypeface(holder.name.getTypeface(), Typeface.NORMAL);
                        holder.date.setTypeface(holder.date.getTypeface(), Typeface.NORMAL);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (checkInternetConenction(context)) {
                    if (conversationVo.getType().equals(ConversationType.CHAT)) {
                        Intent i = new Intent(v.getContext(), RequestHistory.class);
                        try {
                            if (conversationVo.getSource().getId().equals(authenticateVoLocal.getId())) {
                                conversationVoStatic = conversationVo;
                                i.putExtra("targetId", conversationVo.getTarget().getId());
                                i.putExtra("targetTenantId", conversationVo.getTarget().getUserTenantId());
                                i.putExtra("HO_Name", holder.name.getText().toString());
                            } else {
                                conversationVoStatic = conversationVo;
                                i.putExtra("targetId", conversationVo.getSource().getId());
                                i.putExtra("targetTenantId", conversationVo.getSource().getUserTenantId());
                                i.putExtra("HO_Name", holder.name.getText().toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        v.getContext().startActivity(i);
                    }  else if (conversationVo.getType().equals(ConversationType.REQUEST_CHAT)) {
                        markAsConsumed(authenticateVoLocal.getId());
                        final Dialog loadingDialog = Utility.loadingDialog(context);
                        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);
                        Call<ResponseWrapper<HomeOwnerRequestVo>> call = apiService.getRequest(conversationVo.getRequest().getId());
                        call.enqueue(new Callback<ResponseWrapper<HomeOwnerRequestVo>>() {
                            @Override
                            public void onResponse(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Response<ResponseWrapper<HomeOwnerRequestVo>> response) {
                                loadingDialog.dismiss();
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().getData() != null) {
                                            try {
                                                WorkHistoryActivity.homeOwnerRequestVoStatic = new HomeOwnerRequestVo();
                                                WorkHistoryActivity.homeOwnerRequestVoStatic = response.body().getData();
                                                Intent i = new Intent(v.getContext(), RequestMessagesActivity.class);
                                                try {
                                                    i.putExtra("requestId", WorkHistoryActivity.homeOwnerRequestVoStatic.getId().toString());
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                v.getContext().startActivity(i);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Snackbar.make(holder.date, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Snackbar.make(holder.date, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                    }
                                } else {
                                    Snackbar.make(holder.date, SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseWrapper<HomeOwnerRequestVo>> call, Throwable t) {
                                // Log error here since request failed
                                loadingDialog.dismiss();
                                Log.e("Error", t.toString());
                                Snackbar.make(holder.date, NO_INTERNET, Snackbar.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    initializeNoInternetDialogue();
                }
            }

            private void markAsConsumed(Long id) {
                ApiInterface apiService = ApiClient.getClient(holder.linearLayout.getContext()).create(ApiInterface.class);
                Call<ResponseWrapper<Map<String, Boolean>>> call = apiService.markConversationConsumed(id);
                call.enqueue(new Callback<ResponseWrapper<Map<String, Boolean>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<Map<String, Boolean>>> call, Response<ResponseWrapper<Map<String, Boolean>>> response) {
                        if (response.isSuccessful()) {
                            try {

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Map<String, Boolean>>> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("Error", t.toString());
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return conversationVos.size();
    }

    private void initializeNoInternetDialogue() {
        noInternetDialog = new Dialog(new android.view.ContextThemeWrapper(context, R.style.dialogAnimation));
        noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetDialog.setContentView(R.layout.no_internet_dialog);
        noInternetDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        noInternetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        noInternetDialog.setCancelable(true);
        noInternetDialog.getWindow().setGravity(Gravity.BOTTOM);

        Button cancelButton = (Button) noInternetDialog.findViewById(R.id.buttonCancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                noInternetDialog.dismiss();
                if (checkInternetConenction(context)) {
                } else {
                }
            }
        });

        noInternetDialog.show();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, date, description, status;
        //        ImageView avatar;
        LinearLayout linearLayout;
        View mView;
        CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            mView = view;
//            avatar = (ImageView) view.findViewById(R.id.view);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            status = (TextView) view.findViewById(R.id.status);
            description = (TextView) view.findViewById(R.id.description);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
            card_view = view.findViewById(R.id.card_view);
        }
    }
}